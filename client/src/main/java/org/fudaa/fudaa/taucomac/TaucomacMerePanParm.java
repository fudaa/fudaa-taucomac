/*
 * @file         TaucomacMerePanParm.java
 * @creation     2000-10-17
 * @modification $Date: 2006-09-19 15:08:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.taucomac;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
/**
 * methodes communes aux onglets
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:08:56 $ by $Author: deniger $
 * @author       Jean-Yves Riou
 */
public class TaucomacMerePanParm extends BuPanel {
  // objet table des unites
  private final TaucomacUnite unite_= new TaucomacUnite();
  //
  JComponent[] compTable= new JComponent[100];
  int[] phaseCompTable= new int[100];
  int ncompTable= 0;
  //
  JLabel[] unitTable= new JLabel[100];
  int[] kUnitTable= new int[100];
  int nunitTable= 0;
  //
  TitledBorder[] titreTable= new TitledBorder[100];
  int[] phaseTAbTitre= new int[100];
  int nTabTitre= 0;
  // methodes  de chargement des composants
  public void placeTitreMarge(
    final JPanel panel,
    final TitledBorder titre,
    final int kphase,
    final int itop,
    final int idroit,
    final int ibas,
    final int igauch) {
    panel.setBorder(
      BorderFactory.createCompoundBorder(
        titre,
        BorderFactory.createEmptyBorder(itop, idroit, ibas, igauch)));
    titreTable[nTabTitre]= titre;
    phaseTAbTitre[nTabTitre]= kphase;
    nTabTitre++;
  }
  public void placeTitre(final JPanel panel, final TitledBorder titre, final int kphase) {
    panel.setBorder(
      BorderFactory.createCompoundBorder(
        titre,
        BorderFactory.createEmptyBorder(5, 5, 5, 5)));
    titreTable[nTabTitre]= titre;
    phaseTAbTitre[nTabTitre]= kphase;
    nTabTitre++;
  }
  public void placeComp(final JPanel panel, final JComponent comp, final int kphase) {
    panel.add(comp);
    compTable[ncompTable]= comp;
    phaseCompTable[ncompTable]= kphase;
    ncompTable++;
  }
  public void placeCompn(final JPanel panel, final JComponent comp, final int kphase, final int n) {
    panel.add(comp, n);
    compTable[ncompTable]= comp;
    phaseCompTable[ncompTable]= kphase;
    ncompTable++;
  }
  public void placeUnit(final JLabel unit, final int kunit) {
    unitTable[nunitTable]= unit;
    kUnitTable[nunitTable]= kunit;
    nunitTable++;
  }
  public void placePanelG(
    final JPanel panel,
    final Component composant,
    final GridBagLayout lmh,
    final GridBagConstraints ch) {
    ch.gridwidth= 1; //nombre de colonnes pour le composant
    ch.gridheight= 1; //nombre de lignes
    ch.weightx= 100; //proportion des colonnes
    lmh.setConstraints(composant, ch);
    panel.add(composant);
  }
  public void placePanel(final JPanel panel, final Component comp) {
    panel.add(comp);
  }
  //===================================================================
  // change le style, les unites et rend invisible certains composants
  //===================================================================
  public void relook(
    final int style,
    final int phase,
    final int unit,
    final Font txfont,
    final Color txback,
    final Color txfore,
    final Font lafont1,
    final Color lafore1,
    final Font lafont2,
    final Color lafore2) {
    final int style_= style;
    final int phase_= phase;
    final int unit_= unit;
    //   System.out.println("************ reLook***********");
    //   System.out.println(" style_ =   :" +style_);
    //   System.out.println(" phase_ =   :"+phase_);
    //   System.out.println(" unit_ =   :"+unit_);
    for (int i= 0; i < ncompTable; i++) {
      if (compTable[i] instanceof JLabel) {
        if (style_ == 1) {
          compTable[i].setFont(lafont1);
          compTable[i].setForeground(lafore1);
        } else {
          compTable[i].setFont(lafont1);
          compTable[i].setForeground(lafore1);
        }
      }
      if (compTable[i] instanceof BuTextField) {
        if (style_ == 1) {
          compTable[i].setFont(txfont);
          compTable[i].setForeground(txfore);
          compTable[i].setBackground(txback);
        } else {
          compTable[i].setFont(lafont1);
          compTable[i].setForeground(lafore1);
          compTable[i].setBackground(Color.lightGray);
        }
      }
      if (compTable[i] instanceof JCheckBox) {
        if (style_ == 1) {
          compTable[i].setFont(txfont);
          compTable[i].setForeground(txfore);
          //compTable[i].setBackground(txback);
        } else {
          compTable[i].setFont(lafont1);
          compTable[i].setForeground(lafore1);
          //compTable[i].setBackground(Color.lightGray);
        }
      }
      if (phaseCompTable[i] <= phase_) {
        compTable[i].setVisible(true);
      } else {
        compTable[i].setVisible(false);
      }
    }
    for (int i= 0; i < nTabTitre; i++) {
      if (style_ == 1) {
        titreTable[i].setTitleColor(lafore2);
        titreTable[i].setTitleFont(lafont2);
      } else {
        titreTable[i].setTitleColor(lafore1);
        titreTable[i].setTitleFont(lafont1);
      }
    }
    for (int i= 0; i < nunitTable; i++) {
      if (kUnitTable[i] > 0) {
        unitTable[i].setText(unite_.getUnit(unit_, kUnitTable[i]));
      }
    }
    repaint();
  }
  //====================================
  //    Constructeur
  //====================================
  public TaucomacMerePanParm() {
    super();
  }
}
