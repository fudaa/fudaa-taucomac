/*
 * @file         TaucomacApplication.java
 * @creation     1998-10-02
 * @modification $Date: 2006-09-19 15:08:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.taucomac;
//import org.fudaa.ebli.bu.BuApplication;
import com.memoire.bu.BuApplication;
/**
 * L'application cliente Taucomac.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:08:56 $ by $Author: deniger $
 * @author       Jean-Yves Riou
 */
public class TaucomacApplication extends BuApplication {
  public static java.awt.Frame FRAME= null;
  public TaucomacApplication() {
    super();
    FRAME= this;
    final TaucomacImplementation app= new TaucomacImplementation();
    setImplementation(app);
  }
}
