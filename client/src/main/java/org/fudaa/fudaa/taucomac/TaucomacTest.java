/*
 * @file         TaucomacDonGen.java
 * @creation     2000-10-17
 * @modification $Date: 2006-09-19 15:08:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.taucomac;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.taucomac.SDonGeneral;
/**
 * Onglet des titre,Commentaire.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:08:56 $ by $Author: deniger $
 * @author       Jean-Yves Riou
 */
public class TaucomacTest extends BuPanel implements ActionListener {
  SDonGeneral parametresLocal= null;
  //donGen.titreEtude
  //donGen.comentEtude
  //donGen.typeSysUnit
  //donGen.typeChoixCal
  //donGen.typeStyle
  JLabel labelg0;
  JLabel labelg1;
  JLabel labelg2;
  JLabel labelg3;
  JLabel labelg4;
  JLabel labelg5;
  JLabel labeld0;
  JLabel labeld1;
  JLabel labeld2;
  JLabel labeld3;
  JLabel labeld4;
  JLabel labeld5;
  JLabel label1;
  JLabel label2;
  BuTextField parm1;
  BuTextField parm2;
  JPanel pnHaut;
  JPanel pnMil;
  JPanel pnBas;
  JPanel pnMilG;
  JPanel pnMilD;
  JPanel pnBasG;
  JPanel pnBasD;
  JPanel pnBasGG;
  JPanel pnBasGD;
  TitledBorder titreMilG;
  TitledBorder titreMilD;
  TitledBorder titreBasD;
  JRadioButton[] rbs_;
  JRadioButton[] rbd_;
  JRadioButton[] rbi_;
  public void placeLabel(final JPanel panel, final Component label) {
    panel.add(label);
  }
  public void placeComposantp(
    final JPanel panel,
    final Component composant,
    final GridBagLayout lmh,
    final GridBagConstraints ch) {
    ch.gridwidth= 1; //nombre de colonnes pour le composant
    ch.gridheight= 1; //nombre de lignes
    ch.weightx= 100; //proportion des colonnes
    lmh.setConstraints(composant, ch);
    panel.add(composant);
  }
  public TaucomacTest(final BuCommonInterface _appli) {
    super();
    //============
    pnHaut= new JPanel();
    pnHaut.setBorder(BorderFactory.createEmptyBorder(20, 120, 20, 120));
    pnHaut.setLayout(new BorderLayout());
    pnHaut.setPreferredSize(new Dimension(650, 80)); // avecgridbag  et Border
    //
    pnMil= new JPanel();
    pnMil.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    pnMil.setLayout(new BorderLayout(30, 20));
    pnMil.setPreferredSize(new Dimension(650, 90));
    //
    pnBas= new JPanel();
    pnBas.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    pnBas.setLayout(new BorderLayout(30, 20));
    pnBas.setPreferredSize(new Dimension(650, 200));
    //
    pnMilG= new JPanel();
    pnMilG.setLayout(new BorderLayout());
    titreMilG= new TitledBorder("Syst�me d'unit�s");
    pnMilG.setBorder(
      BorderFactory.createCompoundBorder(
        titreMilG,
        BorderFactory.createEmptyBorder(5, 5, 5, 5)));
    pnMilG.setPreferredSize(new Dimension(300, 80));
    //
    pnMilD= new JPanel();
    pnMilD.setLayout(new BorderLayout());
    titreMilD= new TitledBorder("Style");
    pnMilD.setBorder(
      BorderFactory.createCompoundBorder(
        titreMilD,
        BorderFactory.createEmptyBorder(5, 5, 5, 5)));
    pnMilD.setPreferredSize(new Dimension(300, 80));
    //
    pnMil.add(pnMilG, BorderLayout.WEST);
    pnMil.add(pnMilD, BorderLayout.EAST);
    //
    pnBasG= new JPanel();
    pnBasG.setLayout(new BorderLayout());
    pnBasG.setBorder(new TitledBorder(" "));
    pnBasG.setLayout(new BorderLayout());
    pnBasG.setPreferredSize(new Dimension(300, 200));
    //
    pnBasD= new JPanel();
    pnBasD.setLayout(new BorderLayout());
    titreBasD= new TitledBorder("Phase de calcul");
    pnBasD.setBorder(
      BorderFactory.createCompoundBorder(
        titreBasD,
        BorderFactory.createEmptyBorder(5, 5, 5, 5)));
    pnBasD.setPreferredSize(new Dimension(300, 120));
    pnBasGG= new JPanel();
    pnBasGD= new JPanel();
    pnBasG.add(pnBasGG, BorderLayout.WEST);
    pnBasG.add(pnBasGD, BorderLayout.EAST);
    pnBas.add(pnBasG, BorderLayout.WEST);
    pnBas.add(pnBasD, BorderLayout.EAST);
    //===========================================
    final GridBagLayout lm= new GridBagLayout();
    final GridBagConstraints c= new GridBagConstraints();
    c.ipadx= 30;
    c.ipady= 20;
    c.gridx= 0;
    c.gridy= 0;
    placeComposantp(this, pnHaut, lm, c);
    c.gridy= 1;
    placeComposantp(this, pnMil, lm, c);
    c.gridy= 2;
    placeComposantp(this, pnBas, lm, c);
    //===========================================
    label1= new JLabel("      Titre  ", SwingConstants.RIGHT);
    label2= new JLabel("Commentaire  ", SwingConstants.RIGHT);
    //parm1       = new BuTextField();
    parm1= BuTextField.createDoubleField();
    parm1.setHorizontalAlignment(SwingConstants.RIGHT);
    parm2= new BuTextField();
    final JPanel labelPane= new JPanel();
    labelPane.setLayout(new GridLayout(0, 1));
    //labelPane.add(label1);
    placeLabel(labelPane, label1);
    placeLabel(labelPane, label2);
    //labelPane.add(label2);
    final JPanel fieldPane= new JPanel();
    fieldPane.setLayout(new GridLayout(0, 1));
    //fieldPane.add(parm1);
    //fieldPane.add(parm2);
    placeLabel(fieldPane, parm1);
    placeLabel(fieldPane, parm2);
    pnHaut.add(labelPane, BorderLayout.WEST);
    pnHaut.add(fieldPane, BorderLayout.CENTER);
    //=============================================
    labelg0= new JLabel("                     ", SwingConstants.RIGHT);
    labelg1= new JLabel("               Force ", SwingConstants.RIGHT);
    labelg2= new JLabel("Pression et Coh�sion ", SwingConstants.RIGHT);
    labelg3= new JLabel("               Poids ", SwingConstants.RIGHT);
    labelg4= new JLabel("              Moment ", SwingConstants.RIGHT);
    labelg5= new JLabel("             Energie ", SwingConstants.RIGHT);
    labelg0.setFont(new Font("Helvetica", Font.BOLD, 10));
    labelg1.setFont(new Font("Helvetica", Font.BOLD, 10));
    labelg2.setFont(new Font("Helvetica", Font.BOLD, 10));
    labelg3.setFont(new Font("Helvetica", Font.BOLD, 10));
    labelg4.setFont(new Font("Helvetica", Font.BOLD, 10));
    labelg5.setFont(new Font("Helvetica", Font.BOLD, 10));
    labeld0= new JLabel("(tonnes)  (Newton)  ", SwingConstants.LEFT);
    labeld1= new JLabel("  tonne     MN      ", SwingConstants.LEFT);
    labeld2= new JLabel("  t/m2      MPa     ", SwingConstants.LEFT);
    labeld3= new JLabel("  t/m3      MN/m3   ", SwingConstants.LEFT);
    labeld4= new JLabel("  t.m       MN.m    ", SwingConstants.LEFT);
    labeld5= new JLabel("  t.m       kJ      ", SwingConstants.LEFT);
    final JPanel labelPaneg= new JPanel();
    labelPaneg.setLayout(new GridLayout(0, 1));
    labelPaneg.add(labelg0);
    labelPaneg.add(labelg1);
    labelPaneg.add(labelg2);
    labelPaneg.add(labelg3);
    labelPaneg.add(labelg4);
    labelPaneg.add(labelg5);
    final JPanel labelPaned= new JPanel();
    labelPaned.setLayout(new GridLayout(0, 1));
    labelPaned.add(labeld0);
    labelPaned.add(labeld1);
    labelPaned.add(labeld2);
    labelPaned.add(labeld3);
    labelPaned.add(labeld4);
    labelPaned.add(labeld5);
    pnBasGG.add(labelPaneg, BorderLayout.WEST);
    pnBasGG.add(labelPaned, BorderLayout.EAST);
    //================================================
    pnBasD.setLayout(new BuGridLayout(2, 5, 5, true, false));
    final ButtonGroup bi= new ButtonGroup();
    rbi_= new JRadioButton[3];
    for (int i= 0; i < 3; i++) {
      rbi_[i]= new JRadioButton();
      rbi_[i].addActionListener(this);
      bi.add(rbi_[i]);
    }
    int n;
    n= 0;
    pnBasD.add(rbi_[0], n++);
    pnBasD.add(new BuLabelMultiLine("DUCA,ALBE,COMME"), n++);
    pnBasD.add(rbi_[1], n++);
    pnBasD.add(new BuLabelMultiLine("Am�lioration nouveau logiciel"), n++);
    pnBasD.add(rbi_[2], n++);
    pnBasD.add(new BuLabelMultiLine("TAUCOMAC ducs d'Albe"), n++);
    //==============================================
    //(int _columns,  int _hgap, int _vgap, boolean _hfilled, boolean _vfilled)
    pnMilG.setLayout(new BuGridLayout(2, 5, 5, true, false));
    final ButtonGroup bg= new ButtonGroup();
    rbs_= new JRadioButton[2];
    for (int i= 0; i < 2; i++) {
      rbs_[i]= new JRadioButton();
      rbs_[i].addActionListener(this);
      bg.add(rbs_[i]);
    }
    n= 0;
    pnMilG.add(rbs_[0], n++);
    pnMilG.add(new BuLabelMultiLine("Syst�me d'unit� tonnes"), n++);
    pnMilG.add(rbs_[1], n++);
    pnMilG.add(new BuLabelMultiLine("Syst�me d'unit� Newton/Joules"), n++);
    //================================================
    pnMilD.setLayout(new BuGridLayout(2, 5, 5, true, false));
    final ButtonGroup bd= new ButtonGroup();
    rbd_= new JRadioButton[2];
    for (int i= 0; i < 2; i++) {
      rbd_[i]= new JRadioButton();
      rbd_[i].addActionListener(this);
      bd.add(rbd_[i]);
    }
    n= 0;
    pnMilD.add(rbd_[0], n++);
    pnMilD.add(new BuLabelMultiLine("Style standard"), n++);
    pnMilD.add(rbd_[1], n++);
    pnMilD.add(new BuLabelMultiLine("Autre style"), n++);
    //===================================================
    setParametres(parametresLocal);
  }
  public void actionPerformed(final ActionEvent e) {
    //      String cmd=e.getActionCommand();
    //    Object src=e.getSource();
    parametresLocal= getParametres();
    setVisible(true);
  }
  public void setParametres(final SDonGeneral parametresProjet_) {
    parametresLocal= parametresProjet_;
    if (parametresLocal != null) {
      parm1.setText(parametresLocal.titreEtude);
      parm2.setText(parametresLocal.comentEtude);
      setSelectedPhase(parametresLocal.typeChoixCal);
      setSelectedUnit(parametresLocal.typeSysUnit);
      setSelectedStyle(parametresLocal.typeStyle);
    }
  }
  public SDonGeneral getParametres() {
    if (parametresLocal == null) {
      parametresLocal= new SDonGeneral();
    }
    parametresLocal.titreEtude= parm1.getText();
    parametresLocal.comentEtude= parm2.getText();
    parametresLocal.typeSysUnit= getSelectedUnit();
    parametresLocal.typeChoixCal= getSelectedPhase();
    parametresLocal.typeStyle= getSelectedStyle();
    return parametresLocal;
  }
  //==========================================================
  public int getSelectedStyle() {
    for (int k= 0; k < 2; k++) {
      //reLook();
      if (rbd_[k].isSelected()) {
        System.out.println("*style =    " + k);
      }
      if (rbd_[k].isSelected()) {
        return k;
      }
    }
    return 0;
  }
  public int getSelectedPhase() {
    for (int i= 0; i < 3; i++) {
      if (rbi_[i].isSelected()) {
        System.out.println("*phase =    " + i);
      }
      if (rbi_[i].isSelected()) {
        return i;
      }
    }
    return 0;
  }
  public int getSelectedUnit() {
    for (int j= 0; j < 2; j++) {
      if (rbs_[j].isSelected()) {
        System.out.println("*unit =    " + j);
      }
      if (rbs_[j].isSelected()) {
        return j;
      }
    }
    return 0;
  }
  protected void setSelectedStyle(final int nStyle) {
    rbd_[nStyle - 1].setSelected(true);
  }
  protected void setSelectedUnit(final int nChoix) {
    rbs_[nChoix - 1].setSelected(true);
  }
  protected void setSelectedPhase(final int nPhase) {
    rbi_[nPhase - 1].setSelected(true);
  }
  public void relook(
    final int style,
    final int phase,
    final int unit,
    final Font txfont,
    final Color txback,
    final Color txfore,
    final Font lafont1,
    final Color lafore1,
    final Font lafont2,
    final Color lafore2) {
    final int style_= style;
    //    int phase_ = phase;
    //    int unit_ = unit;
    System.out.println("**** style_ =   :" + style_);
    //System.out.println(" phase_ =   :"+phase_);
    //System.out.println(" unit_ =   :"+unit_);
    //String toto = new String(" ");
    //toto = unite_.getUnit(1, 1);
    //System.out.println("toto = "+ toto);
    //unit1.setText(toto);
    //unit2.setVisible(false);
    //BuTextField parm1 ;
    //JLabel    label1 ;
    //label1.setFont(lafont1);
    //label1.setForeground(lafore1);
    //parm1.setFont(txfont);
    //parm1.setBackground(txback);
    if (style_ == 1) {
      titreMilG.setTitleColor(lafore2);
      titreMilD.setTitleColor(lafore2);
      titreBasD.setTitleColor(lafore2);
      titreMilG.setTitleFont(lafont2);
      titreMilD.setTitleFont(lafont2);
      titreBasD.setTitleFont(lafont2);
      label1.setFont(lafont1);
      label2.setFont(lafont1);
      label1.setForeground(lafore1);
      label2.setForeground(lafore1);
      parm1.setFont(txfont);
      parm1.setBackground(txback);
      parm1.setForeground(txfore);
      parm2.setFont(txfont);
      parm2.setBackground(txback);
      parm2.setForeground(txfore);
    } else {
      titreMilG.setTitleColor(lafore1);
      titreMilD.setTitleColor(lafore1);
      titreBasD.setTitleColor(lafore1);
      titreMilG.setTitleFont(lafont1);
      titreMilD.setTitleFont(lafont1);
      titreBasD.setTitleFont(lafont1);
      label1.setFont(lafont1);
      label2.setFont(lafont1);
      label1.setForeground(lafore1);
      label2.setForeground(lafore1);
      parm1.setFont(lafont1);
      //parm1.setBackground(lafore1);
      parm2.setFont(lafont1);
      //parm2.setBackground(lafore1);
    }
    repaint();
  }
  //==========================================================
}
