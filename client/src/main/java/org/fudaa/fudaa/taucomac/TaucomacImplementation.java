/*
 * @file         TaucomacImplementation.java
 * @creation     2000-10-23
 * @modification $Date: 2007-05-04 14:01:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.taucomac;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.beans.PropertyVetoException;
import java.io.File;
import java.util.List;
import java.util.Map;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.memoire.bu.*;
import com.memoire.fu.FuLib;

import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.objet.IPersonne;
import org.fudaa.dodico.corba.taucomac.ICalculTaucomac;
import org.fudaa.dodico.corba.taucomac.ICalculTaucomacHelper;
import org.fudaa.dodico.corba.taucomac.IParametresTaucomac;
import org.fudaa.dodico.corba.taucomac.IResultatsTaucomac;
import org.fudaa.dodico.corba.taucomac.SParametresTAU;

import org.fudaa.dodico.taucomac.DCalculTaucomac;

import org.fudaa.fudaa.commun.dodico.FudaaDodicoTacheConnexion;
import org.fudaa.fudaa.commun.dodico.FudaaImplementation;
import org.fudaa.fudaa.commun.projet.*;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.utilitaire.ServeurCopieEcran;
/**
 * L'implementation du client Taucomac.
 *
 * @version      $Id: TaucomacImplementation.java,v 1.17 2007-05-04 14:01:06 deniger Exp $
 * @author       Jean-Yves Riou
 */
public class TaucomacImplementation
  extends FudaaImplementation
  implements FudaaParamListener, FudaaProjetListener {
  //public final static String LOCAL_UPDATE= ".";
  public final static boolean IS_APPLICATION= true;
  public static ICalculTaucomac SERVEUR_TAUCOMAC;
  public static IConnexion CONNEXION_TAUCOMAC;
  public static IPersonne PERSONNE;
  protected IParametresTaucomac taucomacParams;
  protected IResultatsTaucomac taucomacResults;
  protected static BuInformationsSoftware isTaucomac_=
    new BuInformationsSoftware();
  protected static BuInformationsDocument idTaucomac_=
    new BuInformationsDocument();
  protected TaucomacFilleParametres fp_;
  protected TaucomacFilleResultats fr_;
  protected FudaaProjet projet_;
  protected JFrame ts_;
  protected BuAssistant assistant_;
  protected BuHelpFrame aide_;
  protected BuTaskView taches_;
  protected FudaaParamEventView msgView_;
  protected FudaaProjetInformationsFrame fProprietes_;
  static {
    isTaucomac_.name= "Taucomac";
    isTaucomac_.version= "0.01";
    isTaucomac_.date= "01-oct-2001";
    isTaucomac_.rights= "Tous droits r�serv�s. CETMEF (c)1999,2000";
    isTaucomac_.contact= "xxxxxxxxxxxxx@equipement.gouv.fr";
    isTaucomac_.license= "GPL2";
    isTaucomac_.languages= "fr,en";
    isTaucomac_.logo= TaucomacResource.TAUCOMAC.getIcon("taucomac_logo");
    isTaucomac_.banner= TaucomacResource.TAUCOMAC.getIcon("taucomac_banner");
    isTaucomac_.http= "http://www.utc.fr/fudaa/taucomac/";
    isTaucomac_.update= "http://www.utc.fr/fudaa/distrib/deltas/taucomac/";
    //  is_.man    ="http://marina.cetmef.equipement.gouv.fr/fudaa/manuels/taucomac/";
    //  is_.man    =REMOTE_MAN; non ne marche pas
    isTaucomac_.authors= new String[] { "Jean-Yves Riou" };
    isTaucomac_.contributors=
      new String[] { "Equipes Dodico, Ebli et Fudaa", "xxxxxxxxxx" };
    isTaucomac_.documentors= new String[] { "     " };
    isTaucomac_.testers= new String[] { "     " };
    idTaucomac_.name= "Etude";
    idTaucomac_.version= "0.0";
    idTaucomac_.organization= "CETMEF";
    idTaucomac_.author= System.getProperty("user.name");
    idTaucomac_.contact= idTaucomac_.author + "@cete13.equipement.gouv.fr";
    idTaucomac_.date= FuLib.date();
    // id _.logo        =EbliResource.EBLI.getIcon("minlogo.gif");
    BuPrinter.INFO_LOG= isTaucomac_;
    BuPrinter.INFO_DOC= idTaucomac_;
  }
  public BuInformationsSoftware getInformationsSoftware() {
    return isTaucomac_;
  }
  public static BuInformationsSoftware informationsSoftware() {
    return isTaucomac_;
  }
  public BuInformationsDocument getInformationsDocument() {
    return idTaucomac_;
  }
  public void init() {
    super.init();
    ts_= null;
    fp_= null;
    projet_= null;
    aide_= null;
    try {
      setTitle(isTaucomac_.name + " " + isTaucomac_.version);
      final BuMenuBar mb= BuMenuBar.buildBasicMenuBar();
      setMainMenuBar(mb);
      mb.addActionListener(this);
      mb.addMenu(buildCalculMenu(IS_APPLICATION));
      final BuToolBar tb= BuToolBar.buildBasicToolBar();
      setMainToolBar(tb);
      tb.addActionListener(this);
      tb.removeAll();
      tb.addToolButton("Creer", "CREER", BuResource.BU.getIcon("CREER"), true);
      tb.addToolButton(
        "Ouvrir",
        "OUVRIR",
        BuResource.BU.getIcon("OUVRIR"),
        true);
      tb.addSeparator();
      tb.addToolButton(
        "Enregitrer",
        "ENREGISTRER",
        BuResource.BU.getIcon("ENREGISTRER"),
        true);
      tb.addToolButton(
        "EnregisterSous",
        "ENREGISTRERSOUS",
        BuResource.BU.getIcon("ENREGISTRERSOUS"),
        true);
      tb.addSeparator();
      //tb.addToolButton("Fermer","FERMER",BuResource.BU.getIcon("FERMER"),true);
      tb.addToolButton(
        "Imprimer",
        "IMPRIMER",
        BuResource.BU.getIcon("IMPRIMER"),
        true);
      //tb.addToolButton("Quitter","QUITTER",BuResource.BU.getIcon("QUITTER"),true);
      tb.addSeparator();
      tb.addToolButton(
        "Connecter",
        "CONNECTER",
        FudaaResource.FUDAA.getIcon("connecter"),
        true);
      //tb.addToolButton("Calculer","CALCULER",false);
      tb.addToolButton(
        "Parametres",
        "PARAMETRE",
        BuResource.BU.getIcon("PARAMETRE"),
        true);
      tb.addToolButton(
        "Resultats",
        "RESULTAT",
        BuResource.BU.getIcon("RESULTAT"),
        true);
      //((BuMenu)mb.getMenu("MENU_EDITION")).addMenuItem("Console","CONSOLE",false);
      setEnabledForAction("CREER", true);
      setEnabledForAction("OUVRIR", true);
      setEnabledForAction("REOUVRIR", false);
      setEnabledForAction("ENREGISTRER", false);
      setEnabledForAction("ENREGISTRERSOUS", false);
      setEnabledForAction("FERMER", false);
      setEnabledForAction("PROPRIETE", false);
      setEnabledForAction("QUITTER", true);
      setEnabledForAction("IMPORTER", false);
      setEnabledForAction("EXPORTER", false);
      setEnabledForAction("IMPRIMER", true);
      setEnabledForAction("PREFERENCE", true);
      setEnabledForAction("PARAMETRE", false);
      setEnabledForAction("RESULTAT", false);
      removeAction("REOUVRIR");
      removeAction("PREVISUALISER");
      //=== essai suppression importer exporter
      // removeAction("EXPORTER");
      // removeAction("IMPORTER");
      // ((JMenu)mb.getMenu("Fichier")).remove("IMPORTER");
      //((JMenu)mb.getMenu("Fichier")).removeAction("IMPORTER");
      // JMenu m=(JMenu)mb.getMenu("FICHIER");
      // System.out.println("m = "+m);
      //((JMenu)m.remove("IMPORTER");
      //========================================
      setEnabledForAction("AIDE_INDEX", true);
      setEnabledForAction("AIDE_ASSISTANT", true);
      //
      // remarque removeMenuItem(DString -cmd) n'existe plus
      //
      final BuMainPanel mp= getApp().getMainPanel();
      // mp.setPreferredSize(new Dimension(1000,600));
      mp.setPreferredSize(new Dimension(1024, 768)); //pas pris en compte ??
      /*deniger tout est pris en compte dans les preferences.*/
      mp.setSize(new Dimension(1024, 768));
      final BuColumn rc= mp.getRightColumn();
      assistant_= new TaucomacAssistant();
      taches_= new BuTaskView();
      final BuScrollPane sp= new BuScrollPane(taches_);
      sp.setPreferredSize(new Dimension(150, 80));
      //new BuTaskOperation(this, "SpyTask", "oprServeurCopie" ).run();
      rc.addToggledComponent("Assistant", "ASSISTANT", assistant_, this);
      rc.addToggledComponent(
        BuResource.BU.getString("T�ches"),
        "TACHE",
        sp,
        this);
      mp.setLogo(isTaucomac_.logo);
      mp.setTaskView(taches_);
      msgView_= new FudaaParamEventView();
      final BuScrollPane sp2= new BuScrollPane(msgView_);
      sp2.setPreferredSize(new Dimension(150, 80));
      // lc.addToggledComponent("Messages", "MESSAGE", sp2, this);    jyr
      rc.addToggledComponent("Messages", "MESSAGE", sp2, this);
      setParamEventView(msgView_);
      //==========================================
      FudaaParamChangeLog.CHANGE_LOG.setApplication(
        (BuApplication)TaucomacApplication.FRAME,
        msgView_);
      //===========================================
    } catch (final Throwable t) {
      System.err.println("$$$ " + t);
      t.printStackTrace();
    }
  }
  public void start() {
    super.start();
    // about();
    assistant_.changeAttitude(
      BuAssistant.PAROLE,
      "Bienvenue !\n" + isTaucomac_.name + " " + isTaucomac_.version);
    final BuMainPanel mp= getMainPanel();
    projet_=
      new FudaaProjet(
        getApp(),
        new FudaaFiltreFichier("taucomac"));
    projet_.addFudaaProjetListener(this);
    System.out.println("Taucomacimplementation start projet_" + projet_);
    mp.doLayout();
    mp.validate();
    assistant_.addEmitters((Container)getApp());
    assistant_.changeAttitude(
      BuAssistant.ATTENTE,
      "Vous pouvez cr�er un\nnouveau projet Taucomac\nou en ouvrir un");
    /*deniger
    si l'utilisateur n'a pas modifie ses preferences, on maximise la fenetre
    par defaut.*/
    //Recuperation de la preferences avec par defaut, l'option maximisation (1)
    final int windowSize= BuPreferences.BU.getIntegerProperty("window.size", 1);
    //on applique la preferences.
    BuPreferences.BU.putIntegerProperty("window.size", windowSize);
    /*si vous voulez vraiement que la fenetre soit toujours maximal,decommetez
      la lign suivante:*/
    BuPreferences.BU.putIntegerProperty("window.size", 1);
    /*Dans ce cas,il faut enlever la panneau de preferences
    BuDirWinPreferencesPanel qui ne sert a rien*/
    /*deniger fin*/
    // Application des pr�f�rences
    BuPreferences.BU.applyOn(this);
    // Preferences Taucomac
    TaucomacPreferences.TAUCOMAC.applyOn(this);
    fProprietes_= new FudaaProjetInformationsFrame(this);
    fProprietes_.setVisible(false);
    fProprietes_.setProjet(projet_);
    addInternalFrame(fProprietes_);
    fProprietes_.setVisible(false);
    if (fProprietes_.isVisible()) {
      try {
        fProprietes_.setClosed(true);
      } catch (final Exception ex) {}
    }
    FudaaParamEventProxy.FUDAA_PARAM.addFudaaParamListener(this);
  }

  public void setParamEventView(final FudaaParamEventView v) {
    msgView_= v;
  }
  public FudaaParamEventView getParamEventView() {
    return msgView_;
  }
  // Accesseurs
  // Menu Calcul
  protected BuMenu buildCalculMenu(final boolean _app) {
    final BuMenu r= new BuMenu("Taucomac", "CALCUL");
    r.addMenuItem("Param�tres", "PARAMETRE", false);
    r.addMenuItem("R�sutats", "RESULTAT", false);
    r.addSeparator();
    return r;
  }
  // Actions
  public void actionPerformed(final ActionEvent _evt) {
    String arg= null;
    String action= _evt.getActionCommand();
    System.err.println("ACTION=" + action);
    final int i= action.indexOf('(');
    if (i >= 0) {
      arg= action.substring(i + 1, action.length() - 1);
      action= action.substring(0, i);
    }
    if (action.equals("CREER")) {
      creer();
    } else if (action.equals("OUVRIR")) {
      ouvrir();
    } else if (action.equals("REOUVRIR")) {
      ouvrir(arg);
    } else if (action.equals("ENREGISTRER")) {
      enregistrer();
    } else if (action.equals("ENREGISTRERSOUS")) {
      enregistrerSous();
    } else if (action.equals("FERMER")) {
      fermer();
    } else      //if( action.endsWith("IN")&&
      //   (action.startsWith("ASC")||
      //    action.startsWith("XML")) ) importerProjet();
      //else
      if (action.equals("PREFERENCE")) {
        preferences();
      } else if (action.equals("PROPRIETE")) {
        fProprietes_.setVisible(true);
        if (fProprietes_.isClosed()) {
          try {
            fProprietes_.setClosed(false);
          } catch (final Exception ex) {}
        }
        activateInternalFrame(fProprietes_);
      } else if (action.equals("PARAMETRE")) {
        parametre();
      } else        //if( action.equals("RAPPORT") ) rapport();
        //else
        if (action.equals("RESULTAT")) {
          resultats();
        } else if (action.equals("ASSISTANT") || action.equals("CALQUE")) {
      final BuColumn rc= getMainPanel().getRightColumn();
      rc.toggleComponent(action);
      setCheckedForAction(action, rc.isToggleComponentVisible(action));
    } else if (action.equals("MESSAGE")) {
      final BuColumn lc= getMainPanel().getLeftColumn();
      lc.toggleComponent(action);
      setCheckedForAction(action, lc.isToggleComponentVisible(action));
    } else {
      super.actionPerformed(_evt);
    }
  }
  // LidoParamListener
  public void paramStructCreated(final FudaaParamEvent e) {
    System.out.println("Implementation  paramStructCreated  " + e);
    if (projet_.containsResults()) {
      new BuDialogMessage(
        getApp(),
        getInformationsSoftware(),
        "Vous avez modifi� un param�tre.\n"
          + "Les r�sultats ne sont plus valides:\n"
          + "ils vont etre effac�s.\n")
        .activate();
      setEnabledForAction("RESULTAT", false);
      setEnabledForAction("EXPORTER", false);
      setEnabledForAction("INFOCALCUL", false);
      projet_.clearResults();
    }
    FudaaParamChangeLog.CHANGE_LOG.addChangeLog(e.getMessage());
    setEnabledForAction("ENREGISTRER", true);
  }
  public void paramStructDeleted(final FudaaParamEvent e) {
    System.out.println("Implementation  paramStructDeleted   " + e);
    if (projet_.containsResults()) {
      new BuDialogMessage(
        getApp(),
        getInformationsSoftware(),
        "Vous avez modifi� un param�tre.\n"
          + "Les r�sultats ne sont plus valides:\n"
          + "ils vont etre effac�s.\n")
        .activate();
      setEnabledForAction("RESULTAT", false);
      setEnabledForAction("EXPORTER", false);
      setEnabledForAction("INFOCALCUL", false);
      projet_.clearResults();
    }
    FudaaParamChangeLog.CHANGE_LOG.addChangeLog(e.getMessage());
    setEnabledForAction("ENREGISTRER", true);
  }
  public void paramStructModified(final FudaaParamEvent e) {
    System.out.println("Implementation  paramStructModified   " + e);
    System.out.println(
      "Implementation projet_.containsResults  : " + projet_.containsResults());
    if (projet_.containsResults()) {
      new BuDialogMessage(
        getApp(),
        getInformationsSoftware(),
        "Vous avez modifi� un param�tre.\n"
          + "Les r�sultats ne sont plus valides:\n"
          + "ils vont etre effac�s.\n")
        .activate();
      setEnabledForAction("RESULTAT", false);
      setEnabledForAction("EXPORTER", false);
      setEnabledForAction("INFOCALCUL", false);
      projet_.clearResults();
    }
    FudaaParamChangeLog.CHANGE_LOG.addChangeLog(e.getMessage());
    setEnabledForAction("ENREGISTRER", true);
  }
  // fudaaprojet listener
  public void dataChanged(final FudaaProjetEvent e) {
    System.out.println("Implementation  dataChanged" + e);
    switch (e.getID()) {
      case FudaaProjetEvent.RESULT_ADDED :
      case FudaaProjetEvent.RESULTS_CLEARED :
          FudaaParamChangeLog.CHANGE_LOG.setDirty(true);
          setEnabledForAction("ENREGISTRER", true);
          break;
    }
  }
  public void statusChanged(final FudaaProjetEvent _e) {
    System.out.println("Implementation  statusChanged" + _e);
    if (_e.getID() == FudaaProjetEvent.HEADER_CHANGED) {
      FudaaParamChangeLog.CHANGE_LOG.setDirty(true);
      setEnabledForAction("ENREGISTRER", true);
    }
  }
  public void displayURL(String _url) {
    final String netHelp= System.getProperty("net.access.man");
    if (!"remote".equals(netHelp)) {
      _url=
        "file:"
          + System.getProperty("user.dir")
          + "/manuels/taucomac/utilisation.html";
    }
    if (BuPreferences
      .BU
      .getStringProperty("browser.internal")
      .equals("true")) {
      if (aide_ == null) {
        aide_= new BuHelpFrame();
        aide_.addInternalFrameListener(this);
      }
      addInternalFrame(aide_);
      aide_.setDocumentUrl(_url);
    } else {
      if (_url == null) {
        final BuInformationsSoftware il= getInformationsSoftware();
        _url= il.http;
      }
      BuBrowserControl.displayURL(_url);
    }
  }
  public void oprServeurCopie() {
    System.err.println("Lancement du serveur de copie d'ecran");
    new ServeurCopieEcran(getMainPanel(), "ScreenSpy");
  }
  public void oprCalculer() {
    System.out.println("Implementation oprCalculer()");
    if (!isConnected()) {
      new BuDialogError(
        getApp(),
        isTaucomac_,
        "vous n'etes pas connect� � un serveur TAUCOMAC ! ")
        .activate();
      return;
    }
    setEnabledForAction("CALCULER", false);
    final BuMainPanel mp= getMainPanel();
    System.err.println("Transmission des parametres...");
    mp.setMessage("Transmission des parametres...");
    mp.setProgression(0);
    //=================================================================================
    taucomacParams.parametresTAU(
      (SParametresTAU)projet_.getParam(TaucomacResource.TAUCOMAC01));
    // � voir !!!
    //=================================================================================
    System.err.println("Execution du calcul...");
    mp.setMessage("Execution du calcul...");
    mp.setProgression(20);
    try {
      //==================================================
      SERVEUR_TAUCOMAC.calcul(CONNEXION_TAUCOMAC); // lancement du calcul
      receptionResultats(true);
      //==================================================
    } catch (final Throwable u) {
      new BuDialogError(getApp(), isTaucomac_, u.toString()).activate();
      u.printStackTrace();
      //   SResultatsOUT out=taucomacResults.resultatsOUT();
      //  if( out!=null ) afficheResultatOUT(out);
      return;
    }
  }
  public void oprReceptionResultats() {
    System.out.println("Implementation oprRecptionResultats()");
    try {
      receptionResultats(false);
    } catch (final Exception e) {
      new BuDialogError(getApp(), isTaucomac_, e.toString()).activate();
      e.printStackTrace();
      //  SResultatsOUT out=taucomacResults.resultatsOUT();
      //  if( out!=null ) afficheResultatOUT(out);
    }
  }
  // Methodes privees
  protected void creer() {
    //System.out.println("ouvrir  projetconfigure "+projet_.estConfigure());
    //System.out.println("ouvrir  projetestvierge "+projet_.estVierge());
    if (!projet_.estVierge()) {
      fermer();
    }
    projet_.creer();
    if (!projet_.estConfigure()) {
      projet_.fermer(); // si creation de projet annulee
    } else { // nouveau projet cree
      if (fp_ != null) {
        closeFilleParams();
      }
      //creeParametres();
      setEnabledForAction("PARAMETRE", true);
      setEnabledForAction("FERMER", true);
      setEnabledForAction("ENREGISTRER", true);
      setEnabledForAction("ENREGISTRERSOUS", true);
      setEnabledForAction("PROPRIETE", true);
      setEnabledForAction("CALCULER", true);
      setEnabledForAction("REOUVRIR", false);
      setEnabledForAction("EXPORTER", false);
      setEnabledForAction("INFOCALCUL", false);
      //================================================
      // rajout� ici temporairement par RIOU pour tests
      //================================================
      //    setEnabledForAction("RESULTAT"      , true);
      //================================================
      setTitle(projet_.getFichier());
    }
  }
  protected void ouvrir() {
    if (!projet_.estVierge()) {
      fermer();
    }
    projet_.setEnrResultats(true); // ===========on force
    projet_.ouvrir(); //ici deserialisation
    if (!projet_.estConfigure()) {
      projet_.fermer(); // si ouverture de projet echouee
      setEnabledForAction("PARAMETRE", false);
      setEnabledForAction("FERMER", false);
      setEnabledForAction("ENREGISTRER", false);
      setEnabledForAction("ENREGISTRERSOUS", false);
      setEnabledForAction("PROPRIETE", false);
      setEnabledForAction("CALCULER", false);
      setEnabledForAction("INFOCALCUL", false);
      setEnabledForAction("REOUVRIR", false);
    } else {
      if (fp_ != null) {
        closeFilleParams();
      }
      if (fp_ == null) {
        fp_= new TaucomacFilleParametres(getApp(), projet_, this);
        fp_.addInternalFrameListener(this);
        addInternalFrame(fp_);
        assistant_.addEmitters(fp_);
        setTitle(projet_.getFichier());
        fp_.setSize(825, 550);
        fp_.ongletCourant= fp_.tpMain.getSelectedIndex();
      }
      final SParametresTAU d= (SParametresTAU)projet_.getParam("params");
      fp_.setParametresTaucomac(d);
      fp_.setVisible(false);
      new BuDialogMessage(getApp(), isTaucomac_, "Param�tres charg�s")
        .activate();
      setEnabledForAction("PARAMETRE", true);
      setEnabledForAction("FERMER", true);
      setEnabledForAction("ENREGISTRER", true);
      setEnabledForAction("ENREGISTRERSOUS", true);
      setEnabledForAction("PROPRIETE", true);
      setEnabledForAction("REOUVRIR", true);
      //======================================
      // System.out.println("mode                **** "+projet_.getMode( ));
      //System.out.println("isEnrResultats      **** "+projet_.containsResults() );
      if (projet_.containsResults()) {
        //   System.out.println("containsResults   **** "+projet_.containsResults() );
        setEnabledForAction("RESULTAT", true);
        //====================================
        //   		SResultatsTAU wtaucomacResults=(SResultatsTAU)projet_.getResult(TaucomacResource.TAUCOMAC01);
        //System.out.println(" xxx = "+taucomacResults);
      }
      setEnabledForAction("CALCULER", true);
      setEnabledForAction("INFOCALCUL", false);
      setTitle(projet_.getFichier());
    }
  }
  protected void ouvrir(final String arg) {
    System.out.println(" projet ouvrir arg = " + arg);
    if (!projet_.estVierge()) {
      fermer();
    }
    projet_.ouvrir(arg);
    if (!projet_.estConfigure()) {
      projet_.fermer(); // si ouverture de projet echouee
      setEnabledForAction("PARAMETRE", false);
      setEnabledForAction("MAILLEUR", false);
      setEnabledForAction("EXPORTER", false);
      setEnabledForAction("FERMER", false);
      setEnabledForAction("ENREGISTRER", false);
      setEnabledForAction("ENREGISTRERSOUS", false);
      setEnabledForAction("PROPRIETE", false);
      setEnabledForAction("CALCULER", false);
      setEnabledForAction("REOUVRIR", false);
    } else {
      if (fp_ != null) {
        closeFilleParams();
      }
      // HACK pour recuperer les projets ancien modele
      if (!projet_.containsParam(TaucomacResource.TAUCOMAC01)) {
        projet_.addParam(TaucomacResource.TAUCOMAC01, new SParametresTAU());
      }
      new BuDialogMessage(getApp(), isTaucomac_, "Param�tres charg�s")
        .activate();
      setEnabledForAction("PARAMETRE", true);
      // setEnabledForAction("EXPORTER"       , true);    avril 2001
      setEnabledForAction("FERMER", true);
      setEnabledForAction("ENREGISTRER", true);
      setEnabledForAction("ENREGISTRERSOUS", true);
      setEnabledForAction("PROPRIETE", true);
      setEnabledForAction("REOUVRIR", true);
      setEnabledForAction("CALCULER", true);
      setEnabledForAction("INFOCALCUL", false);
      setTitle(projet_.getFichier());
      if (arg == null) {
        final String r= projet_.getFichier();
        getMainMenuBar().addRecentFile(r, "taucomac");
        TaucomacPreferences.TAUCOMAC.writeIniFile();
      }
    }
  }
  protected void enregistrer() {
    if (fp_ != null) {
      if (!fp_.isClosed()) {
        fp_.valider();
      }
    }
    System.out.println(
      "Enregistre containsResults  " + projet_.containsResults());
    if (projet_.containsResults()) {
      projet_.setEnrResultats(true);
    }
    projet_.addParam("params", fp_.getParametresTauco());
    projet_.enregistre();
    FudaaParamChangeLog.CHANGE_LOG.setDirty(false);
    setEnabledForAction("ENREGISTRER", false);
  }
  protected void enregistrerSous() {
    if (fp_ != null) {
      if (!fp_.isClosed()) {
        fp_.valider();
      }
    }
    projet_.addParam("params", fp_.getParametresTauco());
    projet_.enregistreSous();
    FudaaParamChangeLog.CHANGE_LOG.setDirty(false);
    setEnabledForAction("ENREGISTRER", false);
    setTitle(projet_.getFichier());
  }
  protected void fermer() {
    //System.out.println("**** fermer");
    //if(  projet_.estConfigure() ) System.out.println(" projetconfigure "+projet_.estConfigure());
    //if(  FudaaParamChangeLog.CHANGE_LOG.isDirty() ) System.out.println(" changelog "+FudaaParamChangeLog.CHANGE_LOG.isDirty() );
    if (projet_.estConfigure() && FudaaParamChangeLog.CHANGE_LOG.isDirty()) {
      final int res=
        new BuDialogConfirmation(
          getApp(),
          isTaucomac_,
          "Votre projet va etre ferm�.\n"
            + "Voulez-vous l'enregistrer avant?\n")
          .activate();
      if (res == JOptionPane.YES_OPTION) {
        enregistrer();
      }
    }
    if (fProprietes_.isVisible()) {
      fProprietes_.setVisible(false);
    }
    final BuDesktop dk= getMainPanel().getDesktop();
    if (fp_ != null) {
      closeFilleParams();
      dk.removeInternalFrame(fp_);
      fp_.removeInternalFrameListener(this);
      fp_.delete();
      fp_= null;
    }
    // ajoute par jyr
    if (projet_.containsResults()) {
      // new BuDialogMessage(getApp(), getInformationsSoftware(),
      //   "Vous avez modifi� un param�tre.\n"+
      //   "Les r�sultats ne sont plus valides:\n"+
      //   "ils vont etre effac�s.\n").activate();
      setEnabledForAction("RESULTAT", false);
      setEnabledForAction("EXPORTER", false);
      setEnabledForAction("INFOCALCUL", false);
      projet_.clearResults();
    }
    projet_.fermer();
    FudaaParamChangeLog.CHANGE_LOG.setDirty(false);
    dk.repaint();
    setEnabledForAction("PARAMETRE", false);
    setEnabledForAction("FERMER", false);
    setEnabledForAction("ENREGISTRER", false);
    setEnabledForAction("ENREGISTRERSOUS", false);
    setEnabledForAction("PROPRIETE", false);
    setEnabledForAction("CALCULER", false);
    setEnabledForAction("REOUVRIR", false);
    setEnabledForAction("INFOCALCUL", false);
    setTitle("Taucomac");
  }
  protected void closeFilleParams() {
    try {
      fp_.setClosed(true);
      fp_.setSelected(false);
    } catch (final PropertyVetoException e) {}
  }
  protected void buildPreferences(final List _prefs){
      _prefs.add(new BuLookPreferencesPanel(this));
      _prefs.add(new BuDesktopPreferencesPanel(this));
      _prefs.add(new BuLanguagePreferencesPanel(this));
      _prefs.add(new BuBrowserPreferencesPanel(this));
      /*deniger: preferences pour agrandir la fenetre.*/
      _prefs.add(new BuDirWinPreferencesPanel(this));
      /*deniger fin*/
      _prefs.add(new BuUserPreferencesPanel(this));
      _prefs.add(new TaucomacPreferencesPanel(this));
      //preferences_.addTab(new VolumePreferencesPanel    (this));
  }
  protected void parametre() {
    if (fp_ == null) {
      System.out.println(" projet_   " + projet_);
      System.out.println(" this(imp) " + this);
      System.out.println(" app " + getApp());
      fp_= new TaucomacFilleParametres(getApp(), projet_, this);
      fp_.addInternalFrameListener(this);
      addInternalFrame(fp_);
      assistant_.addEmitters(fp_);
    } else {
      if (fp_.isClosed()) {
        addInternalFrame(fp_);
      } else {
        activateInternalFrame(fp_);
      }
    }
    fp_.setSize(825, 550);
    fp_.ongletCourant= fp_.tpMain.getSelectedIndex();
  }
  protected void resultats() {
    if (fr_ == null) {
      fr_= new TaucomacFilleResultats(getApp(), projet_, this, taucomacResults);
      fr_.addInternalFrameListener(this);
      addInternalFrame(fr_);
      assistant_.addEmitters(fr_);
    } else {
      if (fr_.isClosed()) {
        addInternalFrame(fr_);
        if (projet_.containsResults()) {
          fr_.setValeurs(); // chargement des champs de FilleResultats
        }
      } else {
        activateInternalFrame(fr_);
      }
    }
  }
  // appelee dans taucomacFilleParametres
  //protected void calculer()
  public void calculer() {
    if (!isConnected()) {
      new BuDialogError(
        getApp(),
        isTaucomac_,
        "vous n'etes pas connect� � un serveur!")
        .activate();
      return;
    }
    System.out.println("Implementation calculer()");
    if (fp_ != null) {
      if (!fp_.isClosed()) {
        fp_.valider();
      }
    }
    if (valideContraintesCalcul()) {
      new BuTaskOperation(this, "Calcul", "oprCalculer").start();
    }
  }
  //protected void exportResultats(String key)
  public void exportResultats() {
    System.out.println("Implementation exportResultats()");
    final JFileChooser chooser= new JFileChooser(); //
    chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {
      public boolean accept(final java.io.File f) {
        return true;
      }
      public String getDescription() {
        return "*.*";
      }
    });
    chooser.setCurrentDirectory(
      new java.io.File(System.getProperty("user.dir")));
    final int returnVal= chooser.showSaveDialog(TaucomacApplication.FRAME);
    String filename= null;
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      filename= chooser.getSelectedFile().getPath();
      if (filename == null) {
        return;
      }
      if (new File(filename).exists()) {
        if (new BuDialogConfirmation(getApp(),
          isTaucomac_,
          "Le fichier :\n"
            + filename
            + "\nexiste d�j�. Voulez-vous l'�craser?\n")
          .activate()
          == JOptionPane.NO_OPTION) {
          return;
        }
      }
      /*
      	try {
           if( projet_.containsResults() )
           {
             //TaucomacExport.exportResultats ( taucomacResults,isTaucomac_ , filename);
           }
         	else new BuDialogError(getApp(), getInformationsSoftware(),
          	"Il n'y a pas de r�sultats :\nvous n'avez pas lance de calcul")
          	.activate();
      	 	}

       		 catch( java.io.IOException ex )
      		{
         	new BuDialogError(getApp(), getInformationsSoftware(),
          	 ex.toString())
            .activate();
      		 }
        */
    }
  }
  protected void receptionResultats(final boolean calcul) {
    System.out.println("Implementation reception Resultats()");
    final BuMainPanel mp= getMainPanel();
    System.err.println("Reception des resultats...");
    mp.setMessage("Reception des resultats...");
    mp.setProgression(90);
    // important permet d'inialiser l'existence des resultats
    // pour utiliser    if( projet_.containsResults() )
    projet_.addResult(
      TaucomacResource.TAUCOMAC01,
      taucomacResults.resultatsTAU());
    //======================================================================================
    //  les resultats sont dans   seuilResults.resultatsSEU()
    //=======================================================================================
    System.out.println("===============Apr�s calcul()");
    System.out.println(" taucomacResults" + taucomacResults);
    System.err.println("Operation terminee.");
    mp.setMessage("Operation terminee.");
    //==============================================
    setEnabledForAction("RESULTAT", true);
    //setEnabledForAction("EXPORTER"  , true);    avril 2001
    //==============================================
    mp.setProgression(100);
  }
  public void fullscreen() {
    super.fullscreen();
  }
  private boolean valideContraintesCalcul() {
    //System.out.println("Implementation valideContraintesCalcul()");
    // la verif se fait dans SeuilFilleParametres
    //SParametresTAU p01=(SParametresTAU)projet_.getParam(TaucomacResource.TAUCOMAC);  // a voir
    return true;
  }
  public void exit() {
    fermer();
    super.exit();
  }
  public boolean confirmExit() {
    return true;
  }
  public boolean isCloseFrameMode() {
    return false;
  }
  public TaucomacImplementation getTaucomacImplementation() {
    return (this);
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#clearVariables()
   */
  protected void clearVariables() {
  CONNEXION_TAUCOMAC=null;
  SERVEUR_TAUCOMAC=null;
  }
  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#getTacheConnexionMap()
   */
  protected FudaaDodicoTacheConnexion[] getTacheConnexionMap() {
    final FudaaDodicoTacheConnexion c=new FudaaDodicoTacheConnexion(SERVEUR_TAUCOMAC,CONNEXION_TAUCOMAC);
    return new FudaaDodicoTacheConnexion[]{c};
  }
  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#getTacheDelegateClass()
   */
  protected Class[] getTacheDelegateClass() {
    return new Class[]{DCalculTaucomac.class};
  }
  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#initConnexions(java.util.Map)
   */
  protected void initConnexions(final Map _r) {
  final FudaaDodicoTacheConnexion c=(FudaaDodicoTacheConnexion)_r.get(DCalculTaucomac.class);
  CONNEXION_TAUCOMAC=c.getConnexion();
  SERVEUR_TAUCOMAC=ICalculTaucomacHelper.narrow(c.getTache());
  }


  /**
   * @see org.fudaa.fudaa.commun.impl.FudaaCommonImplementation#getApplicationPreferences()
   */
  public BuPreferences getApplicationPreferences() {
    return TaucomacPreferences.TAUCOMAC;
  }
} // fin classe
