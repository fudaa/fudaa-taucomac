/*
 * @file         TaucomacGraphe.java
 * @creation     2000-10-24
 * @modification $Date: 2006-09-19 15:08:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.taucomac;
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Vector;

import org.fudaa.dodico.corba.taucomac.SParametresTAU;

import org.fudaa.ebli.graphe.Aspect;
import org.fudaa.ebli.graphe.Axe;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.ebli.graphe.CourbeDefault;
import org.fudaa.ebli.graphe.Graphe;
import org.fudaa.ebli.graphe.Marges;
import org.fudaa.ebli.graphe.Valeur;

import org.fudaa.fudaa.commun.projet.FudaaProjet;
/**
 * @version      $Id: TaucomacGraphe.java,v 1.11 2006-09-19 15:08:56 deniger Exp $
 * @author       Jean-Yves Riou
 */
class TaucomacGraphe extends BGraphe implements PropertyChangeListener {
  FudaaProjet project_; // projet (pour acceder aux parametres)
  private SParametresTAU parm_;
  public final class SPoint01 {
    public double x= 0D;
    public double y= 0D;
    public SPoint01() {}
    public SPoint01(final double _x, final double _y) {
      x= _x;
      y= _y;
    }
  }; // class SPoint01
  //===================================
  // abcisse de l'origine (axe du tube)
  //===================================
  double xl0= 5.; // on prend xl0 metres � gauche et et � droite du tube
  // pour le  trace des couches
  // rajoute systematiquement � tous les x dans TraceGraphe
  //========================
  // variables de travail
  //========================
  int ic; // indice courbe
  int ip; // indice point
  int il; // indice libelle
  //=======================================
  // description des courbes  (npt points)
  //=======================================
  //int ncmax            = 100;                // nombre max de courbes
  int ncmax= 200;
  //int nptmax           = 50;                 // nombre max de points par courbes
  int nptmax= 100;
  int nc; // nombre de courbes
  int[] npt= new int[ncmax];
  SPoint01[][] coord= new SPoint01[ncmax][nptmax]; // coordonn�es
  Color[] aspcont= new Color[ncmax]; // contour
  Color[] aspsurf= new Color[ncmax]; // surface
  String[] titc= new String[ncmax]; // titre
  String[] trac= new String[ncmax];
  String[] type= new String[ncmax];
  // type de trace
  boolean[] marq= new boolean[ncmax]; // marqueur
  //================================================
  // description des libelles   (1 point + 1 texte)
  //================================================
  int nlibmax= 100; // nombre max de libelles
  int nlib; // nombre de libelles
  String[] lib= new String[nlibmax]; // type de "Courbe" ou "Texte"
  SPoint01[] clib= new SPoint01[nlibmax]; // coordonn�es
  Color[] asplib= new Color[nlibmax]; // contour
  double[] vinflib= new double[nlibmax];
  double[] vsupflib= new double[nlibmax];
  //==============================================
  private Graphe graphe_;
  private final Color[] col=
    {
      Color.orange,
      Color.pink,
      Color.green,
      Color.yellow,
      Color.cyan,
      Color.magenta,
      Color.gray,
      Color.blue,
      Color.red,
      Color.orange,
      Color.pink,
      Color.green,
      Color.yellow,
      Color.cyan,
      Color.magenta,
      Color.gray,
      Color.blue,
      Color.red,
      Color.orange,
      Color.pink,
      Color.green,
      Color.yellow,
      Color.cyan,
      Color.magenta,
      Color.gray,
      Color.blue,
      Color.red };
  //  marges supplementaires en coor. utilisateur
  //  valeur peut etre changee dans les procedures
  double deltaxref= 1.;
  double deltayref= 1.;
  //==============
  // constructeur
  //==============
  public TaucomacGraphe(final SParametresTAU paramTaucoFille) {
    System.out.println("TaucomacGraphe");
    parm_= paramTaucoFille;
    System.out.println("--titreEtude   :" + parm_.donGen.titreEtude);
    liste();
    graphe_= null;
    initraz();
    dessinprinc(); //tube et couches
    initGraphe();
    initAxes();
    traceGraphe();
    finGraphe();
  } // fin constructeur
  //================
  public void traceGraphe() {
    CourbeDefault courbe_;
    for (int i= 0; i < nc; i++) {
      courbe_= new CourbeDefault();
      final Aspect asp= new Aspect();
      asp.contour_= aspcont[i];
      asp.surface_= aspsurf[i];
      // asp.largeur = 20 ;            // ne marche pas ??
      courbe_.aspect_= asp;
      courbe_.titre_= titc[i];
      courbe_.trace_= trac[i];
      courbe_.type_= type[i];
      // System.out.println("courbe_.trace =" + courbe_.trace);
      courbe_.marqueurs_= marq[i];
      final Vector valcourbe= new Vector();
      for (int j= 0; j < npt[i]; j++) {
        coord[i][j].x= coord[i][j].x + xl0;
        final Valeur v= new Valeur();
        v.v_= coord[i][j].y;
        v.s_= coord[i][j].x;
        System.out.println(
          "ic ip x y =" + i + " " + j + " " + v.v_ + " " + v.s_);
        valcourbe.add(v);
      }
      courbe_.valeurs_= valcourbe;
      courbe_.visible_= true;
      graphe_.ajoute(courbe_);
    }
    for (int i= 0; i < nlib; i++) {
      courbe_= new CourbeDefault();
      final Aspect asp= new Aspect();
      asp.contour_= asplib[i];
      asp.surface_= asplib[i];
      asp.texte_= asplib[i];
      courbe_.aspect_= asp;
      courbe_.marqueurs_= false;
      final Vector valcourbe= new Vector();
      final Valeur v= new Valeur();
      v.v_= clib[i].y;
      v.s_= clib[i].x + xl0;
      v.titre_= lib[i];
      v.vsup_= vsupflib[i];
      v.vinf_= vinflib[i];
      valcourbe.add(v);
      courbe_.valeurs_= valcourbe;
      courbe_.visible_= true;
      graphe_.ajoute(courbe_);
    }
  }
  public void initGraphe() {
    final Marges marges= new Marges();
    graphe_= new Graphe();
    graphe_.animation_= false;
    graphe_.legende_= true;
    graphe_.marges_= marges;
    marges.gauche_= 40;
    marges.droite_= 20;
    marges.haut_= 10;
    marges.bas_= 30;
  }
  public void finGraphe() {
    /*setInteractif(true);
    graphe_.visible=true;
    setGraphe(graphe_);
    System.out.println("construction de graphe_ ="+graphe_);
    fullRepaint();*/
    graphe_.visible_= true;
    setInteractif(true);
    setGraphe(graphe_);
    fullRepaint();
  }
  public void propertyChange(final PropertyChangeEvent _evt) {
    fullRepaint();
  }
  private void initAxes() {
    Axe axeX;
    Axe axeY;
    double minx= coord[0][0].x;
    double maxx= coord[0][0].x;
    double miny= coord[0][0].y;
    double maxy= coord[0][0].y;
    for (int j= 0; j < nc; j++) {
      for (int k= 0; k < npt[j]; k++) {
        minx= (coord[j][k].x < minx) ? coord[j][k].x : minx;
        maxx= (coord[j][k].x > maxx) ? coord[j][k].x : maxx;
        miny= (coord[j][k].y < miny) ? coord[j][k].y : miny;
        maxy= (coord[j][k].y > maxy) ? coord[j][k].y : maxy;
      }
    }
    miny= miny - deltayref;
    maxy= maxy + deltayref;
    minx= minx - deltaxref;
    maxx= maxx + deltaxref;
    System.out.println("minx =" + minx);
    System.out.println("maxx =" + maxx);
    System.out.println("miny =" + miny);
    System.out.println("maxy =" + maxy);
    //Axe des x
    axeX= new Axe();
    axeX.titre_= "";
    axeX.unite_= "";
    axeX.vertical_= true;
    axeX.graduations_= true;
    axeX.grille_= false;
    //axeX.minimum = minx;
    //axeX.maximum = maxx;
    axeX.minimum_= miny;
    axeX.maximum_= maxy;
    axeX.pas_= Math.floor((axeX.maximum_ - axeX.minimum_) / 10.);
    axeX.visible_= true;
    graphe_.ajoute(axeX);
    //Axe des y
    axeY= new Axe();
    axeY.titre_= "";
    axeY.unite_= "";
    axeY.vertical_= true;
    axeY.graduations_= true;
    axeY.grille_= false;
    //axeY.minimum = Math.floor(miny -  0.5);
    //axeY.maximum = Math.floor(maxy +  0.5);
    axeY.minimum_= Math.floor(minx - 0.5);
    axeY.maximum_= Math.floor(maxx + 0.5);
    axeY.pas_= Math.floor((axeY.maximum_ - axeY.minimum_) / 10.);
    axeY.visible_= true;
    graphe_.ajoute(axeY);
  }
  //====================================================
  // initialisation  raz dessin
  //====================================================
  private void initraz() {
    System.out.println("initraz");
    for (ic= 0; ic < ncmax; ic++) {
      aspcont[ic]= Color.black;
      aspsurf[ic]= Color.black;
      titc[ic]= " ";
      trac[ic]= "lineaire";
      type[ic]= "courbe";
      marq[ic]= false;
      for (ip= 0; ip < nptmax; ip++) {
        coord[ic][ip]= new SPoint01();
      }
    }
    for (ic= 0; ic < nptmax; ic++) {
      for (ip= 0; ip < nptmax; ip++) {
        coord[ic][ip].x= -999.;
        coord[ic][ip].y= -999.;
      }
    }
    for (int i= 0; i < nlibmax; i++) {
      clib[i]= new SPoint01();
      clib[i].x= -999.;
      clib[i].y= -999.;
      asplib[i]= Color.black;
      vinflib[i]= 0.;
      vsupflib[i]= 0.;
      lib[i]= "";
    }
    nlib= 0;
    nc= 0;
  }
  //====================================================
  // dessin d'un tube et des couches du sol
  //====================================================
  private void dessinprinc() {
    System.out.println("dessinprinc");
    /*
    parm_.donGen.titreEtude;
    parm_.donGen.comentEtude;
    parm_.cotes.zTete;
    parm_.cotes.zPied;
    parm_.tubes.diametre;
    parm_.cotes.zToitSup;
    parm_.cotes.zToitInf;
    parm_.cotes.nbTubes;
    parm_.cotes.nbTronconsTub;
    for(int j=0 ; j< parm_.cotes.nbTronconsTub; j++)
    {
    parm_.cotes.carTronc[j].longueur);
    parm_.cotes.carTronc[j].epaisseur);
    }
    */
    double x1, y1, x2, y2, x3, y3, x4, y4;
    double diam;
    diam= parm_.tubes.diametre / 1000.; // mm => m
    //================
    // trace du tube
    //================
    ic= nc;
    ip= 0;
    type[ic]= "polygone";
    //aspcont[ic]    = Color.blue;
    aspsurf[ic]= Color.darkGray;
    aspcont[ic]= Color.black;
    // aspsurf[ic]    = Color.black;
    // les 4 points du seuil 1,2,3,4 dans sens trigo
    // on centre le dessin sur l'axe du tube (x= 0.)
    x1= -1. * diam / 2.;
    y1= parm_.cotes.zPied;
    x2= x1;
    y2= parm_.cotes.zTete;
    x3= diam / 2.;
    y3= y2;
    x4= x3;
    y4= y1;
    //
    coord[ic][ip].x= x1;
    coord[ic][ip].y= y1;
    ip++;
    coord[ic][ip].x= x2;
    coord[ic][ip].y= y2;
    ip++;
    coord[ic][ip].x= x3;
    coord[ic][ip].y= y3;
    ip++;
    coord[ic][ip].x= x4;
    coord[ic][ip].y= y4;
    ip++;
    coord[ic][ip].x= x1;
    coord[ic][ip].y= y1;
    ip++;
    npt[ic]= ip;
    ic++;
    nc= ic;
    //=================
    // troncons de tube
    //==================
    x1= (-1. * diam / 2.) + 0.05;
    y1= parm_.cotes.zTete;
    x2= (diam / 2.) - 0.05;
    y2= y1;
    x3= x2;
    x4= x1;
    //System.out.println("Cotes nbTronconsTub  :"+parm_.cotes.nbTronconsTub);
    //for(int j=0 ; j< parm_.cotes.nbTronconsTub; j++)
    //    {
    //    System.out.println("Cotes  carTronc[j].longueur  " +j+" "+parm_.cotes.carTronc[j].longueur);
    //	System.out.println("Cotes  carTronc[j].epaisseur " +j+" "+parm_.cotes.carTronc[j].epaisseur);
    //    }
    if (parm_.cotes.nbTronconsTub > 0) {
      for (int j= 0; j < parm_.cotes.nbTronconsTub; j++) {
        double h;
        h= parm_.cotes.carTronc[j].longueur;
        if (h > 0.) {
          y3= y2 - h - 0.05;
          y4= y3;
          System.out.println("j h " + j + " " + h);
          //
          ip= 0;
          ic= nc;
          type[ic]= "polygone";
          //aspcont[ic]    = Color.blue;
          aspsurf[ic]= Color.lightGray;
          aspcont[ic]= Color.black;
          //
          coord[ic][ip].x= x1;
          coord[ic][ip].y= y1;
          ip++;
          coord[ic][ip].x= x2;
          coord[ic][ip].y= y2;
          ip++;
          coord[ic][ip].x= x3;
          coord[ic][ip].y= y3;
          ip++;
          coord[ic][ip].x= x4;
          coord[ic][ip].y= y4;
          ip++;
          coord[ic][ip].x= x1;
          coord[ic][ip].y= y1;
          ip++;
          npt[ic]= ip;
          ic++;
          nc= ic;
          //
          y1= y3 + 0.05;
          y2= y1;
        }
      }
    }
    //=============
    //  axe du tube
    //=============
    x1= 0.;
    y1= parm_.cotes.zTete + 0.5;
    y2= parm_.cotes.zPied - 0.5;
    ip= 0;
    ic= nc;
    aspcont[ic]= Color.red;
    coord[ic][ip].x= x1;
    coord[ic][ip].y= y1;
    ip++;
    coord[ic][ip].x= x1;
    coord[ic][ip].y= y2;
    ip++;
    npt[ic]= ip;
    ic++;
    nc= ic;
    //=======================
    //   toit sup et inf
    //=======================
    System.out.println("Cotes zToitSup  :" + parm_.cotes.zToitSup);
    System.out.println("Cotes zToitInf  :" + parm_.cotes.zToitInf);
    if (parm_.cotes.zToitSup != -999.) {
      x1= 0.1 - xl0;
      y1= parm_.cotes.zToitSup;
      x2= xl0 - 0.1;
      y2= y1;
      ip= 0;
      ic= nc;
      aspcont[ic]= Color.red;
      coord[ic][ip].x= x1;
      coord[ic][ip].y= y1;
      ip++;
      coord[ic][ip].x= x2;
      coord[ic][ip].y= y2;
      ip++;
      npt[ic]= ip;
      ic++;
      nc= ic;
      lib[nlib]= "z toit sup.";
      clib[nlib].x= 1. - xl0;
      clib[nlib].y= parm_.cotes.zToitSup;
      asplib[nlib]= Color.red;
      vinflib[nlib]= 0.;
      vsupflib[nlib]= 0.;
      nlib++;
    }
    if (parm_.cotes.zToitInf != -999.) {
      x1= 0.1 - xl0;
      y1= parm_.cotes.zToitInf;
      x2= +xl0 - 0.1;
      y2= parm_.cotes.zToitInf;
      ip= 0;
      ic= nc;
      aspcont[ic]= Color.red;
      coord[ic][ip].x= x1;
      coord[ic][ip].y= y1;
      ip++;
      coord[ic][ip].x= x2;
      coord[ic][ip].y= y2;
      ip++;
      npt[ic]= ip;
      ic++;
      nc= ic;
      lib[nlib]= "z toit inf.";
      clib[nlib].x= 1. - xl0;
      clib[nlib].y= parm_.cotes.zToitInf;
      asplib[nlib]= Color.red;
      vinflib[nlib]= 0.;
      vsupflib[nlib]= 0.;
      nlib++;
    }
    //===================
    //  sol rupt.
    //==================
    System.out.println("rupture nbCouchesRupt:" + parm_.rupture.nbCouchesRupt);
    for (int j= 0; j < parm_.rupture.nbCouchesRupt; j++) {
      System.out.println(
        "couchesRupt[j].zToit          "
          + j
          + " "
          + parm_.rupture.couchesRupt[j].zToit);
      System.out.println(
        "couchesRuptDr[j].zToit        "
          + j
          + " "
          + parm_.rupture.couchesRuptDr[j].zToit);
    }
    if (parm_.rupture.nbCouchesRupt > 0) {
      x1= (diam / 2.) + 0.05;
      x2= xl0 / 2;
      // dessin couche 1
      //================
      y1= parm_.cotes.zToitSup;
      lib[nlib]= "Rupture";
      clib[nlib].x= (x1 + x2) / 2.;
      clib[nlib].y= parm_.cotes.zToitSup;
      asplib[nlib]= Color.black;
      vinflib[nlib]= 0.;
      vsupflib[nlib]= 0.;
      nlib++;
      y2= parm_.cotes.zToitInf;
      if (parm_.rupture.nbCouchesRupt > 1) {
        y2= parm_.rupture.couchesRupt[1].zToit;
      }
      ip= 0;
      ic= nc;
      type[ic]= "polygone";
      aspsurf[ic]= Color.cyan;
      aspcont[ic]= Color.black;
      coord[ic][ip].x= x1;
      coord[ic][ip].y= y1;
      ip++;
      coord[ic][ip].x= x2;
      coord[ic][ip].y= y1;
      ip++;
      coord[ic][ip].x= x2;
      coord[ic][ip].y= y2;
      ip++;
      coord[ic][ip].x= x1;
      coord[ic][ip].y= y2;
      ip++;
      npt[ic]= ip;
      ic++;
      nc= ic;
      if (parm_.rupture.nbCouchesRupt == 2) {
        // couche 2
        //=========
        y3= parm_.cotes.zToitInf;
        ip= 0;
        ic= nc;
        type[ic]= "polygone";
        aspsurf[ic]= Color.magenta;
        aspcont[ic]= Color.black;
        coord[ic][ip].x= x1;
        coord[ic][ip].y= y2;
        ip++;
        coord[ic][ip].x= x2;
        coord[ic][ip].y= y2;
        ip++;
        coord[ic][ip].x= x2;
        coord[ic][ip].y= y3;
        ip++;
        coord[ic][ip].x= x1;
        coord[ic][ip].y= y3;
        ip++;
        npt[ic]= ip;
        ic++;
        nc= ic;
      }
    } // nbCouchesRupt > 0
    //============
    //    Pressio
    //=============
    System.out.println(
      "pressio  nbCouchesPlast:" + parm_.pressio.nbCouchesPlast);
    for (int j= 0; j < parm_.pressio.nbCouchesPlast; j++) {
      if (j > 1) {
        System.out.println(
          "pressio  ztoit :" + j + " " + parm_.pressio.couchesPlast[j].zToit);
      }
    } //for
    if (parm_.pressio.nbCouchesPlast > 0) {
      x1= xl0 / 2;
      x2= xl0 - 0.1;
      // dessin couche 1
      //================
      y1= parm_.cotes.zToitSup;
      lib[nlib]= "Pressio.";
      clib[nlib].x= (x1 + x2) / 2.;
      clib[nlib].y= parm_.cotes.zToitSup;
      asplib[nlib]= Color.black;
      vinflib[nlib]= 0.;
      vsupflib[nlib]= 0.;
      nlib++;
      for (int i= 0; i < parm_.pressio.nbCouchesPlast; i++) {
        if (i == 0) {
          y1= parm_.cotes.zToitSup;
          y2= parm_.pressio.couchesPlast[1].zToit;
        } else {
          y1= y2;
          if (i < parm_.pressio.nbCouchesPlast - 1) {
            y2= parm_.pressio.couchesPlast[i + 1].zToit;
          } else {
            y2= parm_.cotes.zToitInf;
          }
        }
        ip= 0;
        ic= nc;
        type[ic]= "polygone";
        aspsurf[ic]= col[i];
        aspcont[ic]= Color.black;
        coord[ic][ip].x= x1;
        coord[ic][ip].y= y1;
        ip++;
        coord[ic][ip].x= x2;
        coord[ic][ip].y= y1;
        ip++;
        coord[ic][ip].x= x2;
        coord[ic][ip].y= y2;
        ip++;
        coord[ic][ip].x= x1;
        coord[ic][ip].y= y2;
        ip++;
        npt[ic]= ip;
        ic++;
        nc= ic;
      } //for
    } //nbCouchesPlast > 0
    //=====
  } //dessinprinc
  //=========================================================
  public void liste() {
    System.out.println("liste");
    System.out.println("***titreEtude   :" + parm_.donGen.titreEtude);
    System.out.println("comentEtude  :" + parm_.donGen.comentEtude);
    System.out.println("Cotes zTete  :" + parm_.cotes.zTete);
    System.out.println("Cotes zPied  :" + parm_.cotes.zPied);
    System.out.println("Cotes zToitSup  :" + parm_.cotes.zToitSup);
    System.out.println("Cotes zToitInf  :" + parm_.cotes.zToitInf);
    System.out.println("Cotes nbTubes  :" + parm_.cotes.nbTubes);
    System.out.println("Cotes nbTronconsTub  :" + parm_.cotes.nbTronconsTub);
    for (int j= 0; j < parm_.cotes.nbTronconsTub; j++) {
      System.out.println(
        "Cotes  carTronc[j].longueur  "
          + j
          + " "
          + parm_.cotes.carTronc[j].longueur);
      System.out.println(
        "Cotes  carTronc[j].epaisseur "
          + j
          + " "
          + parm_.cotes.carTronc[j].epaisseur);
    }
    System.out.println("Tubes  	diametre :" + parm_.tubes.diametre);
    System.out.println("Tubes     distAction:" + parm_.tubes.distAction);
    System.out.println("Tubes     distPerpend:" + parm_.tubes.distPerpend);
    System.out.println("rupture nbCouchesRupt:" + parm_.rupture.nbCouchesRupt);
    for (int j= 0; j < parm_.rupture.nbCouchesRupt; j++) {
      System.out.println(
        "couchesRupt[j].zToit          "
          + j
          + " "
          + parm_.rupture.couchesRupt[j].zToit);
      System.out.println(
        "couchesRuptDr[j].zToit        "
          + j
          + " "
          + parm_.rupture.couchesRuptDr[j].zToit);
    }
    System.out.println(
      "pressio  nbCouchesPlast:" + parm_.pressio.nbCouchesPlast);
    System.out.println("pressio  comportAmar:" + parm_.pressio.comportAmar);
    for (int j= 0; j < parm_.pressio.nbCouchesPlast; j++) {
      if (j > 1) {
        System.out.println(
          "pressio  ztoit :" + j + " " + parm_.pressio.couchesPlast[j].zToit);
      }
    } //for
  }
  /*
  private void prepGraphResTrav()
  {

     //========================================================
      double  inclinaison    = parm_.inclinaison;
      double  oblicite       = parm_.oblicite;
      double  raycourb       = parm_.rayonCourb;

    //==========================================================
    // cas des resultats
    //==========================================================
      String  typeCretCal    = res_.resultatsSEU().typeCretCal  ;
      String  rebord         = res_.resultatsSEU().rebord  ;
      double  pelle          = res_.resultatsSEU().pelle  ;
      double  epais          = res_.resultatsSEU().epaisCret  ;
      double  larg           = res_.resultatsSEU().largEcoul  ;
      double  hc             = res_.resultatsSEU().hc  ;
      double  chargAmont     = res_.resultatsSEU().chargAmont ;
      double  hautAmont      = res_.resultatsSEU().hautAmont  ;
      double  cotAmont       = res_.resultatsSEU().cotAmont  ;
      double  chargAval      = res_.resultatsSEU().chargAval  ;
      double  hautAval       = res_.resultatsSEU().hautAval  ;
      double  cotAval        = res_.resultatsSEU().cotAval  ;
     //==============
      double refam;
      double refav;

     // refam = cotAmont - hautAmont;
     // refav = cotAval  - hautAval;
    //
    // determination des zref
    //
    refam =  parm_.profAmont.xy[0].y ;

    for(int i = 0; i <  parm_.profAmont.nbPoints;i++)
    {
      if(  parm_.profAmont.xy[i].y < refam)
      {
      refam = parm_.profAmont.xy[i].y ;
      }
    }
    refav =  parm_.profAval.xy[0].y ;

    for(int i = 0; i <  parm_.profAval.nbPoints;i++)
    {
      if(  parm_.profAval.xy[i].y < refav)
      {
      refav = parm_.profAval.xy[i].y ;
      }

     }
     //  System.out.println("rebord = "+ rebord);
      if(rebord.equals("droit")) { raycourb = 0.;}
     //   System.out.println("inclinaison   = "+ inclinaison);
    // variables de travail

         int ic;   // indice courbe
         int ip;   // indice point
         double x1,y1,x2,y2,x3,y3,x4,y4;
         double x5,y5,xw,yw;
         double alphainc; // inclinaison en radians
         double alphaw ;  // angle en radians
         double deltaalpha;

    //==========================================================
    //  dessin de la vue en   travers du seuil
    //  ======================================
    //  on se fixe largref = max(hauteur seuil, 2.00m = largrefmin )
    //
    //  on dessine les charges sur largref,les zref sur largref
    //  le dessin fait en gros 4*largref
    //
    // l'origine en x est au point les plus bas du seuil
    //  vers l'aval
    //
    //
    //===========================================================
     for(  ic=0; ic< ncmax; ic++)
     {

          aspcont[ic]    = Color.black;
          aspsurf[ic]    = Color.black;
          titc[ic]       = " ";
   		  trac [ic]      = "lineaire";
   		  type[ic]       = "courbe";
   		  marq [ic]      =  false;


           for(  ip = 0; ip < nptmax ; ip++)
      	   {
             coord[ic][ip] = new SPoint01();
           }
     }
      for(  ic=0; ic< nptmax; ic++)
      {
     	 for(  ip = 0; ip < nptmax ; ip++)
      	{
          coord[ic][ip].x  =-999. ;
          coord[ic][ip].y  =-999. ;

     	}
      }
      for(int il = 0; il< nlibmax; il++)
      {
         clib[il]        = new SPoint01();
         clib[il].x     =-999. ;
         clib[il].y     =-999. ;
         asplib [il]    = Color.black;
  		 vinflib [il]   = 0.;
  		 vsupflib[il]   = 0.;
  		 lib[il]        = "";
      }
    //
    deltaxref = 0.;
    deltayref = 1.;
    y5 = 0.;
    // initialisation
    nlib = 0;
    nc = 0;
    int nptarc = 11; // nombre de points sur arc de cercle si seuil epais
    double largrefmin = 2.;
    double largref    = largrefmin;
    if(pelle > largrefmin)   { largref = pelle;}

    double yminw = refam ;    // pour trace des lignes de rappel
    if(refav< yminw){ yminw = refav;}

    double ymaxw =  refam+ chargAmont ;
    if(refav+ chargAval > ymaxw) {ymaxw = refav+ chargAval ;}
    if(refam+ pelle + hc> ymaxw) {ymaxw = refam+ pelle+ hc ;}
     // pour meme dimension horizontale et verticale
    if(2.*largref < (ymaxw-yminw)){ largref = (ymaxw-yminw)/2.;}
    if(2.*largref > (ymaxw-yminw))
    {
     deltayref = ( 2.*largref-(ymaxw-yminw)/2.) /2.;
    }

    //================
    // trace du seuil
    //================
    ic = nc;
    ip = 0;
    aspcont[ic]    = Color.black;
    aspsurf[ic]    = Color.black;

    // les 4 points du seuil 1,2,3,4 dans sens trigo

    x4 = 0.;           // l'origine des x
    y4 = refam ;
    x1 = x4-epais;
    y1 = y4;
    alphainc = (inclinaison*Math.PI)/180.;
    x2 = x1 + pelle*Math.sin(alphainc);
    y2 = y1 + pelle*Math.cos(alphainc);
    x3 = x2 +epais;
    y3 = y2;

    //============

    coord[ic][ip].x = x1;
    coord[ic][ip].y = y1;
    ip++;
    coord[ic][ip].x = x2;
    coord[ic][ip].y = y2;
    ip++;

    //    System.out.println("raycourb = "+ raycourb);

    if(raycourb != 0.)
    {
     double  salphaw = (epais/2.)/raycourb;
     alphaw =  Math.asin(salphaw);
     // System.out.println("alphaw = "+ alphaw);
       	x5 = (x2+x3)/2.;
       	y5 = y2 - raycourb*Math.cos(alphaw);
       	deltaalpha =2.*alphaw/(nptarc-1.);
     	alphaw= -1.*alphaw;
      		for(int i = 0; i < nptarc -1; i++)
      		{
      		 alphaw = alphaw + deltaalpha;
       		 coord[ic][ip].x = x5 + raycourb*Math.sin(alphaw);
    		 coord[ic][ip].y = y5 + raycourb*Math.cos(alphaw);
    			ip++;

      		}
     	 		coord[ic][ip].x = x4;
      			coord[ic][ip].y = y4;
      			ip++;
    }
    else
    {
   		 	coord[ic][ip].x = x3;
    		coord[ic][ip].y = y3;
    		ip++;
    		coord[ic][ip].x = x4;
    		coord[ic][ip].y = y4;
    		ip++;
    }
    npt[ic] = ip;
    ic++; nc = ic;
   //==========================
   //     trace des zref
   //==========================
   ip = 0;
   ic = nc ;
            coord[ic][ip].x = x1 - 2.*largref;
     		coord[ic][ip].y = refam;
    		ip++;
    		coord[ic][ip].x = x4;
    		coord[ic][ip].y = refam;
    		ip++;
    		coord[ic][ip].x = x4 ;
    		coord[ic][ip].y = refav ;
    		ip++;
    		coord[ic][ip].x = x4 + 2.*largref;
    		coord[ic][ip].y = refav;
    		ip++;
    npt[ic] = ip;
    ic++; nc = ic;
   //==============================
   //     trace de hc
   //===============================
   ip = 0;
   ic = nc ;
     aspcont[ic]    = Color.red;
     aspsurf[ic]    = Color.red;
            coord[ic][ip].x = x2;
    		coord[ic][ip].y = y2+  hc;
    		if(raycourb != 0.)
    		{coord[ic][ip].y = coord[ic][ip].y + (y5+raycourb - y2); }
    		ip++;
    		coord[ic][ip].x = x3;
     		coord[ic][ip].y = y3+  hc;
    		if(raycourb != 0.)
    		{coord[ic][ip].y = coord[ic][ip].y+( y5+raycourb - y2); }
   			ip++;
    npt[ic] = ip;
    ic++; nc = ic;
   //
         lib[nlib] = "Hauteur critique";
         clib[nlib].x     = x3 ;
         clib[nlib].y     = y3+  hc;
         if(raycourb != 0.)
         { clib[nlib].y   =  clib[nlib].y     +( y5+raycourb - y2);}
         asplib [nlib]    = Color.red;
  		 vinflib [nlib]   = 0.;
  		 vsupflib[nlib]   = 0.;
      nlib++;

    //     trace ligne de rappel  amont
    //     on sait que axe a -1. du minimum   (voir trace des axes)

      ip = 0;  ic = nc ;
       aspcont[ic]    = Color.green;
       aspsurf[ic]    = Color.green;
               coord[ic][ip].x = x1 - largref ;
       		coord[ic][ip].y = cotAmont  ;
    		ip++;
    		coord[ic][ip].x = x1 - largref;
    		coord[ic][ip].y = yminw -deltayref;
    		ip++;
      npt[ic] = ip;  ic++; nc = ic;

   //     trace de ligne de rappel  aval
      ip = 0;  ic = nc ;
       aspcont[ic]    = Color.green;
       aspsurf[ic]    = Color.green;
               coord[ic][ip].x = x4 + largref ;
    		coord[ic][ip].y = cotAval ;
    		ip++;
    		coord[ic][ip].x = x4 +  largref;
    		coord[ic][ip].y = yminw -deltayref ;
    		ip++;
      npt[ic] = ip;  ic++; nc = ic;

   // trace ligne de rappel horizontale
   //  ip = 0;  ic = nc ;
   //    aspcont[ic]    = Color.blue;
   //    aspsurf[ic]    = Color.blue;
   //            coord[ic][ip].x = x4 + 2.*largref ;
   // 		coord[ic][ip].y = yminw -deltayref ;
   // 		ip++;
   // 		coord[ic][ip].x = x4 -epais -2.*largref;
   // 		coord[ic][ip].y = yminw -deltayref ;
   // 		ip++;
   //   npt[ic] = ip;  ic++; nc = ic;


   //     trace de hauteur amont
      ip = 0;  ic = nc ;
       aspcont[ic]    = Color.blue;
       aspsurf[ic]    = Color.blue;
            coord[ic][ip].x = x1 - largref ;
    		coord[ic][ip].y = cotAmont  ;
    		ip++;
    		coord[ic][ip].x = x1 - 2.*largref;
    		coord[ic][ip].y = cotAmont  ;
    		ip++;
      npt[ic] = ip;  ic++; nc = ic;
     //
         lib[nlib] = "  Cote amont";
         clib[nlib].x     = x1- 1.5*largref  ;
         clib[nlib].y     = cotAmont  - 0.2 ;
         asplib [nlib]    = Color.blue;
  		 vinflib [nlib]   = 0.;
  		 vsupflib[nlib]   = 0.;
         nlib++;
   //     trace de hauteur  aval
      ip = 0;  ic = nc ;
       aspcont[ic]    = Color.blue;
       aspsurf[ic]    = Color.blue;
            coord[ic][ip].x = x4 + largref   ;
    		coord[ic][ip].y = cotAval    ;
    		ip++;
    		coord[ic][ip].x = x4 + 2.*largref;
    		coord[ic][ip].y = cotAval    ;
    		ip++;
       npt[ic] = ip;  ic++; nc = ic;

       lib[nlib] = "Cote aval";
         clib[nlib].x     = x4 + 1.5*largref  ;
         clib[nlib].y     = cotAval+ - 0.2 ;
         asplib [nlib]    = Color.blue;
  		 vinflib [nlib]   = 0.;
  		 vsupflib[nlib]   = 0.;
  		 nlib++;

    //     trace de charge amont
      ip = 0;  ic = nc ;
       aspcont[ic]    = Color.green;
       aspsurf[ic]    = Color.green;
            coord[ic][ip].x = x1 - largref ;
    		coord[ic][ip].y = refam +chargAmont  ;
    		ip++;
    		coord[ic][ip].x = x1 - 2.*largref;
    		coord[ic][ip].y = refam +chargAmont;
    		ip++;
      npt[ic] = ip;  ic++; nc = ic;
      //
         lib[nlib] = "  Charge amont";
         clib[nlib].x     = x1 - 1.5*largref  ;
         clib[nlib].y     = refam +  chargAmont;
         asplib [nlib]    = Color.green;
  		 vinflib [nlib]   = 0.;
  		 vsupflib[nlib]   = 0.;
      nlib++;


   //     trace de charge  aval
      ip = 0;  ic = nc ;
       aspcont[ic]    = Color.green;
       aspsurf[ic]    = Color.green;
            coord[ic][ip].x = x4 + largref   ;
    		coord[ic][ip].y = refav + chargAval ;
    		ip++;
    		coord[ic][ip].x = x4 + 2.*largref;
    		coord[ic][ip].y = refav + chargAval  ;
    		ip++;
      npt[ic] = ip;  ic++; nc = ic;
      //
         lib[nlib] = "Charge aval";
         clib[nlib].x     = x4 + 1.5*largref  ;
         clib[nlib].y     = refav+ chargAval   ;
         asplib [nlib]    = Color.green;
  		 vinflib [nlib]   = 0.;
  		 vsupflib[nlib]   = 0.;
         nlib++;
  //
         lib[nlib] = "  Profil  amont";
         clib[nlib].x     = x1 - 0.5*largref;
         clib[nlib].y     = yminw -deltayref;
         asplib [nlib]    = Color.green;
  		 vinflib [nlib]   = 0.;
  		 vsupflib[nlib]   = 0.;
         nlib++;
  //
         lib[nlib] = "Profil  aval";
         clib[nlib].x     = x4 + 1.5*largref ;
         clib[nlib].y     = yminw -deltayref  ;
         asplib [nlib]    = Color.green;
  		 vinflib [nlib]   = 0.;
  		 vsupflib[nlib]   = 0.;
         nlib++;
   }// fin prepGraphResTrav
  */
  /*
  private void prepGraphResPlanArchi_V1()   // conserve pour on ne sait jamais
  {

    //========================================================
     double  inclinaison    = parm_.inclinaison;
     double  oblicite       = parm_.oblicite;
     double  raycourb       = parm_.rayonCourb;
     int nptAm 			 = parm_.profAmont.nbPoints;
     int nptAv				 = parm_.profAval.nbPoints;
    //  on connait aussi 	parm_.profAmont.xy[ ].x
   // 					parm_.profAval .xy[ ].y

   //==========================================================
   // cas des resultats
   //==========================================================
     String  typeCretCal    = res_.resultatsSEU().typeCretCal  ;
     String  rebord         = res_.resultatsSEU().rebord  ;
     double  pelle          = res_.resultatsSEU().pelle  ;
     double  epais          = res_.resultatsSEU().epaisCret  ;
     double  larg           = res_.resultatsSEU().largEcoul  ;
     double  hc             = res_.resultatsSEU().hc  ;
     double  chargAmont     = res_.resultatsSEU().chargAmont ;
     double  hautAmont      = res_.resultatsSEU().hautAmont  ;
     double  cotAmont       = res_.resultatsSEU().cotAmont  ;
     double  chargAval      = res_.resultatsSEU().chargAval  ;
     double  hautAval       = res_.resultatsSEU().hautAval  ;
     double  cotAval        = res_.resultatsSEU().cotAval  ;



     System.out.println("cotAmont = "+ cotAmont);
     System.out.println("cotAval  = "+ cotAval);
     for(int i = 0; i <  parm_.profAmont.nbPoints;i++)
   {

    // System.out.println("amont " +i+" "+ parm_.profAmont.xy[i].x +" "+parm_.profAmont.xy[i].y);

   }


   for(int i = 0; i <  parm_.profAval.nbPoints;i++)
   {

   //  System.out.println("aval " +i+" "+   parm_.profAval.xy[i].x +" "+parm_.profAval.xy[i].y);

   }

     //  System.out.println("oblicit�= "+ oblicite);
    // if(rebord.equals("droit")) { inclinaison = 0.;}
    //   System.out.println("inclinaison   = "+ inclinaison);
    // variables de travail

        int ic;   // indice courbe
        int ip;   // indice point
        double largAm;
        double largAv;
        double x1,y1,x2,y2,x3,y3,x4,y4;
        double x5,y5,x6,y6,x7,y7,x8,y8;
        double alphaw ;  // angle en radians
        double deltax;

   //==========================================================
   //  dessin de la vue en   plan du seuil
   //  ======================================
   //  on se fixe largref = max(hauteur seuil, 2.00m = largrefmin )
   //
   //
   //  le dessin fait en gros 4*largref
   //
   //  l'origine en x est au point les plus bas du seuil
   //  vers l'aval
   //
   //
   //===========================================================
    for(  ic=0; ic< ncmax; ic++)
    {

         aspcont[ic]    = Color.black;
         aspsurf[ic]    = Color.black;
         titc[ic]       = " ";
  		  trac [ic]      = "lineaire";
  		  type[ic]       = "courbe";
  		  marq [ic]      =  false;


          for(  ip = 0; ip < nptmax ; ip++)
     	   {
            coord[ic][ip] = new SPoint01();
          }
    }
     for(  ic=0; ic< nptmax; ic++)
     {
    	 for(  ip = 0; ip < nptmax ; ip++)
     	{
         coord[ic][ip].x  =-999. ;
         coord[ic][ip].y  =-999. ;

    	}
     }
     for(int il = 0; il< nlibmax; il++)
     {
        clib[il]        = new SPoint01();
        clib[il].x     =-999. ;
        clib[il].y     =-999. ;
        asplib [il]    = Color.black;
  		 vinflib [il]   = 0.;
  		 vsupflib[il]   = 0.;
  		 lib[il]        = "";
     }

   // initialisation

   nlib = 0;
   nc = 0;

   double largref;

   // calcul largeurs des profils

    largAm = Math.abs( parm_.profAmont.xy[0].x - parm_.profAmont.xy[nptAm-1 ].x);
    largAv = Math.abs( parm_.profAval .xy[0].x - parm_.profAval .xy[nptAv-1 ].x);
    deltayref = largAm/8.;
    if(largAv > largAm) {deltayref = largAv/8.; }


  // pour que l'oblicite ne soit pas deformee

  largref = largAm/2.;
  if(largAv > largAm) {largref = largAv/2.; }

  deltaxref = largref/5.;

   //================
   // trace du seuil
   //================
   ic = nc;
   ip = 0;
   aspcont[ic]    = Color.black;
   aspsurf[ic]    = Color.black;

   // les 4 points du seuil 1,2,3,4 dans sens trigo

   x4 = 0.;           // l'origine des x
   y4 = -1.*larg/2. ;
   x3 = 0.;
   y3 = larg/2.;
   x1 = x4 - epais;
   y1 = y4;
   x2 = x1;
   y2 = y3;
   			  // si oblicite # 0. on bouge pas le point 4
   alphaw = (oblicite*Math.PI)/180.;
   deltax = (larg/2.)* Math.sin(alphaw);
   //x1 = x1 - deltax;
   x2 = x2 - deltax;
   x3 = x3 - deltax;

   coord[ic][ip].x = x1;
   coord[ic][ip].y = y1;
   ip++;
   coord[ic][ip].x = x2;
   coord[ic][ip].y = y2;
   ip++;
   coord[ic][ip].x = x3;
   coord[ic][ip].y = y3;
   ip++;
   coord[ic][ip].x = x4;
   coord[ic][ip].y = y4;
   ip++;
   coord[ic][ip].x = x1;
   coord[ic][ip].y = y1;
   ip++;
   npt[ic] = ip;
   ic++; nc = ic;
  //=======================================
  //     trace  des profils
  // 4 points 5... 8 dans le sens trigo
  //=========================================

  x6 = x2 - largref;
  x5 = x6;
  x8 = x4 + largref;
  x7 = x8;
  y5 = -1.* largAm/2.;
  y6 = largAm/2.;
  y7 = largAv/2.;
  y8 = -1.*largAv/2.;

  ip = 0; ic = nc ;
  			aspcont[ic]    = Color.blue;
      		aspsurf[ic]    = Color.blue;
           coord[ic][ip].x = x5;
   		coord[ic][ip].y = y5 ;
   		ip++;
   		coord[ic][ip].x = x1;
   		coord[ic][ip].y = y5;
  			ip++;
  npt[ic] = ip; ic++; nc = ic;
  //
  ip = 0; ic = nc ;
  			aspcont[ic]    = Color.blue;
      		aspsurf[ic]    = Color.blue;
           coord[ic][ip].x = x6;
   		coord[ic][ip].y = y6 ;
   		ip++;
   		coord[ic][ip].x = x2;
   		coord[ic][ip].y = y6;
  			ip++;
  npt[ic] = ip; ic++; nc = ic;
  //
  ip = 0; ic = nc ;
  			aspcont[ic]    = Color.blue;
      		aspsurf[ic]    = Color.blue;
           coord[ic][ip].x = x3;
   		coord[ic][ip].y = y7 ;
   		ip++;
   		coord[ic][ip].x = x7;
   		coord[ic][ip].y = y7;
  			ip++;
  npt[ic] = ip; ic++; nc = ic;
  //
  ip = 0; ic = nc ;
  			aspcont[ic]    = Color.blue;
      		aspsurf[ic]    = Color.blue;
           coord[ic][ip].x = x4;
   		coord[ic][ip].y = y8 ;
   		ip++;
   		coord[ic][ip].x = x8;
   		coord[ic][ip].y = y8;
  			ip++;
  npt[ic] = ip; ic++; nc = ic;

  //====================================
  // trace des lignes de rappel
  //===================================


  ip = 0; ic = nc ;
           aspcont[ic]    = Color.blue;
      		aspsurf[ic]    = Color.blue;
           coord[ic][ip].x = x1;
   		coord[ic][ip].y = y1 ;
   		ip++;
   		coord[ic][ip].x = x1;
   		coord[ic][ip].y = y5;
  			ip++;
  npt[ic] = ip; ic++; nc = ic;
  //
  ip = 0; ic = nc ;
  			aspcont[ic]    = Color.blue;
      		aspsurf[ic]    = Color.blue;
           coord[ic][ip].x = x2;
   		coord[ic][ip].y = y2 ;
   		ip++;
   		coord[ic][ip].x = x2;
   		coord[ic][ip].y = y6;
  			ip++;
  npt[ic] = ip; ic++; nc = ic;
  //
  ip = 0; ic = nc ;
  			aspcont[ic]    = Color.blue;
      		aspsurf[ic]    = Color.blue;
           coord[ic][ip].x = x3;
   		coord[ic][ip].y = y3 ;
   		ip++;
   		coord[ic][ip].x = x3;
   		coord[ic][ip].y = y7;
  			ip++;
  npt[ic] = ip; ic++; nc = ic;
  //
  ip = 0; ic = nc ;
           aspcont[ic]    = Color.blue;
      		aspsurf[ic]    = Color.blue;
           coord[ic][ip].x = x4;
   		coord[ic][ip].y = y4 ;
   		ip++;
   		coord[ic][ip].x = x4;
   		coord[ic][ip].y = y8;
  			ip++;
  npt[ic] = ip; ic++; nc = ic;


  //  Libelles

        lib[nlib] = "Profil  amont";
        clib[nlib].x     = x5 + deltaxref;
        clib[nlib].y     = y5 -  deltayref/2.;
        asplib [nlib]    = Color.blue;
  		 vinflib [nlib]   = 0.;
  	 vsupflib[nlib]   = 0.;
        nlib++;
  //
        lib[nlib] = "Profil  aval";
        clib[nlib].x     = x8 -deltaxref;
        clib[nlib].y     = y8 - deltayref/2. ;
        asplib [nlib]    = Color.blue;
  		 vinflib [nlib]   = 0.;
  	 vsupflib[nlib]   = 0.;
        nlib++;



  }// fin prepGraphPlan
  */
  /*
  private void prepGraphParTrav()
  {
     //   System.out.println("prepGraphParTrav ");
    //==========================================
    //  dessin de la vue en   travers du seuil
    //  ======================================
    // liste();

      double  inclinaison    = parm_.inclinaison;
      double  oblicite       = parm_.oblicite;
      double  raycourb       = parm_.rayonCourb;
      String  typeCretCal    = parm_.typeCret;
      double  pelle          = parm_.pelle  ;
      double  epais          = parm_.epaisCret  ;
      double  larg           = parm_.largEcoul  ;
      double  cotAmont       = parm_.coteAmont  ;
      double  cotAval        = parm_.coteAval  ;

      double hc = hc_;        //hauteur critique


      double refam;       // zref amont <= profil
      double refav;       //   "  aval   "   "

    // variables de travail

         int ic;   // indice courbe
         int ip;   // indice point
         double  largref; // 4*largerf = largeur dessin coord utilisateur
         double x1,y1,x2,y2,x3,y3,x4,y4;
         double x5,y5,xw,yw;
         double alphainc; // inclinaison en radians
         double alphaw ;  // angle en radians
         double deltaalpha;
    //
    // initialisation
    //
    nlib = 0;          // nombre de libelles
    nc = 0;            // nombre de courbes
    int nptarc = 11;   // nombre de points sur arc de cercle si seuil epais
    deltaxref = 0.;    // marge utilisateur
    deltayref = 1.;    // marge utilisateur
    y5 = 0.;     	   //  centre cercle pour compil doit etre initialise
    //
    // determination des zref
    //
    refam =  parm_.profAmont.xy[0].y ;

    for(int i = 0; i <  parm_.profAmont.nbPoints;i++)
    {
      if(  parm_.profAmont.xy[i].y < refam)
      {
      refam = parm_.profAmont.xy[i].y ;
      }
    }
    refav =  parm_.profAval.xy[0].y ;

    for(int i = 0; i <  parm_.profAval.nbPoints;i++)
    {
      if(  parm_.profAval.xy[i].y < refav)
      {
      refav = parm_.profAval.xy[i].y ;
      }
    }
    // =========================================================
    //    initialisation des variables
    //===========================================================
     for(  ic=0; ic< ncmax; ic++)
     {

          aspcont[ic]    = Color.black;
          aspsurf[ic]    = Color.black;
          titc[ic]       = " ";
   		  trac [ic]      = "lineaire";
   		  type[ic]       = "courbe";
   		  marq [ic]      =  false;


           for(  ip = 0; ip < nptmax ; ip++)
      	   {
             coord[ic][ip] = new SPoint01();
           }
     }
      for(  ic=0; ic< nptmax; ic++)
      {
     	 for(  ip = 0; ip < nptmax ; ip++)
      	{
          coord[ic][ip].x  =-999. ;
          coord[ic][ip].y  =-999. ;

     	}
      }
      for(int il = 0; il< nlibmax; il++)
      {
         clib[il]        = new SPoint01();
         clib[il].x     =-999. ;
         clib[il].y     =-999. ;
         asplib [il]    = Color.black;
  		 vinflib [il]   = 0.;
  		 vsupflib[il]   = 0.;
  		 lib[il]        = "";
      }
    //=========================================================
    //    attribution de valeurs quand inexistance
    //========================================================

      if( inclinaison  == -999.) { inclinaison = 0. ; }
      if(  pelle       == -999.) { pelle       = 0.8; }
      if(  epais       == -999.) { epais       = 0.2; }
      if(  cotAmont    == -999.) { cotAmont    = refam + 2.*pelle;}
      if(  cotAval     == -999.) { cotAval     = refav + 0.5*pelle;}
      if( raycourb     == -999.) { raycourb    = 0.;}

    //===============================================================
    //   on se fixe largref = max(hauteur seuil, 2.00m = largrefmin )
    //   puis de facon a avoir orthonorme on a
    //   en horizontal 4.*largref + 2.* deltaxref
    //   en verical   ( ymaxw -yminw ) + 2.*deltayref
    //   avec yminw = point le plus bas -deltayref(pour ligne de rappel)
    //   on dessine les charges sur largref,les zref sur +/-largref
    //
    //   l'origine en x est au point les plus bas du seuil
    //   vers l'aval
    //   les operations +/- deltaxref et floor sont faites dans axe()
    //=================================================================


    double yminw = refam ;
    if(refav< yminw){ yminw = refav;}
    yminw = yminw - deltayref;     // pour trace des lignes de rappel
    double ymaxw =  cotAmont ;
    if( cotAval > ymaxw) {ymaxw = cotAval ;}

    // on doit avoir
    // ( ymaxw-yminw ) + 2.*deltayref = 4.*largref+ epais + 2.* deltaxref
    // d'ou :
     double ymaxwf = Math.floor(ymaxw +0.5);
     double yminwf = Math.floor(yminw-0.5);

     largref =  (( ymaxwf-yminwf ) + 2.*deltayref  -epais - 2.*deltaxref ) /4.;


    //================
    // trace du seuil
    //================
    ic = nc;
    ip = 0;
    aspcont[ic]    = Color.black;
    aspsurf[ic]    = Color.black;

    // les 4 points du seuil 1,2,3,4 dans sens trigo

    x4 = 0.;           // l'origine des x
    y4 = refam ;
    x1 = x4-epais;
    y1 = y4;
    alphainc = (inclinaison*Math.PI)/180.;
    x2 = x1 + pelle*Math.sin(alphainc);
    y2 = y1 + pelle*Math.cos(alphainc);
    x3 = x2 +epais;
    y3 = y2;

    //============

    coord[ic][ip].x = x1;
    coord[ic][ip].y = y1;
    ip++;
    coord[ic][ip].x = x2;
    coord[ic][ip].y = y2;
    ip++;


    //    System.out.println("raycourb = "+ raycourb);

    if(raycourb != 0.)
    {
     double  salphaw = (epais/2.)/raycourb;
     alphaw =  Math.asin(salphaw);
     // System.out.println("alphaw = "+ alphaw);
       	x5 = (x2+x3)/2.;
       	y5 = y2 - raycourb*Math.cos(alphaw);
       	deltaalpha =2.*alphaw/(nptarc-1.);
     	alphaw= -1.*alphaw;
      		for(int i = 0; i < nptarc -1; i++)
      		{
      		 alphaw = alphaw + deltaalpha;
       		 coord[ic][ip].x = x5 + raycourb*Math.sin(alphaw);
    		 coord[ic][ip].y = y5 + raycourb*Math.cos(alphaw);
    			ip++;

      		}
     	 		coord[ic][ip].x = x4;
      			coord[ic][ip].y = y4;
      			ip++;
    }
    else
    {
   		 	coord[ic][ip].x = x3;
    		coord[ic][ip].y = y3;
    		ip++;
    		coord[ic][ip].x = x4;
    		coord[ic][ip].y = y4;
    		ip++;
    }
    npt[ic] = ip;
    ic++; nc = ic;
   //==========================
   //     trace des zref
   //==========================
   ip = 0;
   ic = nc ;
            coord[ic][ip].x = x1 - 2.*largref;
     		coord[ic][ip].y = refam;
    		ip++;
    		coord[ic][ip].x = x4;
    		coord[ic][ip].y = refam;
    		ip++;
    		coord[ic][ip].x = x4 ;
    		coord[ic][ip].y = refav ;
    		ip++;
    		coord[ic][ip].x = x4 + 2.*largref;
    		coord[ic][ip].y = refav;
    		ip++;
    npt[ic] = ip;
    ic++; nc = ic;
   //==============================
   //     trace de hc
   //===============================

   ip = 0;
   ic = nc ;
     aspcont[ic]    = Color.red;
     aspsurf[ic]    = Color.red;
            coord[ic][ip].x = x2;
    		coord[ic][ip].y = y2+  hc;
    		if(raycourb != 0.)
    		{coord[ic][ip].y = coord[ic][ip].y + (y5+raycourb - y2); }
    		ip++;
    		coord[ic][ip].x = x3;
     		coord[ic][ip].y = y3+  hc;
    		if(raycourb != 0.)
    		{coord[ic][ip].y = coord[ic][ip].y+( y5+raycourb - y2); }
   			ip++;
    npt[ic] = ip;
    ic++; nc = ic;
   //
         lib[nlib] = "Hauteur critique";
         clib[nlib].x     = x3 ;
         clib[nlib].y     = y3+  hc;
         if(raycourb != 0.)
         { clib[nlib].y   =  clib[nlib].y     +( y5+raycourb - y2);}
         asplib [nlib]    = Color.red;
  		 vinflib [nlib]   = 0.;
  		 vsupflib[nlib]   = 0.;
      nlib++;


    //     trace ligne de rappel  amont
    //     on sait que axe a -1. du minimum   (voir trace des axes)

      ip = 0;  ic = nc ;
       aspcont[ic]    = Color.green;
       aspsurf[ic]    = Color.green;
               coord[ic][ip].x = x1 - largref ;
       		coord[ic][ip].y = cotAmont  ;
    		ip++;
    		coord[ic][ip].x = x1 - largref;
    		coord[ic][ip].y = yminw -deltayref;
    		ip++;
      npt[ic] = ip;  ic++; nc = ic;

   //     trace de ligne de rappel  aval
      ip = 0;  ic = nc ;
       aspcont[ic]    = Color.green;
       aspsurf[ic]    = Color.green;
               coord[ic][ip].x = x4 + largref ;
    		coord[ic][ip].y = cotAval ;
    		ip++;
    		coord[ic][ip].x = x4 +  largref;
    		coord[ic][ip].y = yminw -deltayref ;
    		ip++;
      npt[ic] = ip;  ic++; nc = ic;

   // trace ligne de rappel horizontale
   //  ip = 0;  ic = nc ;
   //    aspcont[ic]    = Color.blue;
   //    aspsurf[ic]    = Color.blue;
   //            coord[ic][ip].x = x4 + 2.*largref ;
   //			coord[ic][ip].y = yminw -deltayref ;
    //		ip++;
    //		coord[ic][ip].x = x4 -epais -2.*largref;
    //		coord[ic][ip].y = yminw -deltayref ;
    //		ip++;
    // npt[ic] = ip;  ic++; nc = ic;


   //     trace de hauteur amont
      ip = 0;  ic = nc ;
       aspcont[ic]    = Color.blue;
       aspsurf[ic]    = Color.blue;
            coord[ic][ip].x = x1 - largref ;
    		coord[ic][ip].y = cotAmont  ;
    		ip++;
    		coord[ic][ip].x = x1 - 2.*largref;
    		coord[ic][ip].y = cotAmont  ;
    		ip++;
      npt[ic] = ip;  ic++; nc = ic;
     //
         lib[nlib] = "  Cote amont";
         clib[nlib].x     = x1- 1.5*largref  ;
         clib[nlib].y     = cotAmont  + 0.2 ;
         asplib [nlib]    = Color.blue;
  		 vinflib [nlib]   = 0.;
  		 vsupflib[nlib]   = 0.;
         nlib++;
   //     trace de hauteur  aval
      ip = 0;  ic = nc ;
       aspcont[ic]    = Color.blue;
       aspsurf[ic]    = Color.blue;
            coord[ic][ip].x = x4 + largref   ;
    		coord[ic][ip].y = cotAval    ;
    		ip++;
    		coord[ic][ip].x = x4 + 2.*largref;
    		coord[ic][ip].y = cotAval    ;
    		ip++;
       npt[ic] = ip;  ic++; nc = ic;

         lib[nlib] = "Cote aval";
         clib[nlib].x     = x4 + 1.5*largref  ;
         clib[nlib].y     = cotAval+ + 0.2 ;
         asplib [nlib]    = Color.blue;
  		 vinflib [nlib]   = 0.;
  		 vsupflib[nlib]   = 0.;
  		 nlib++;


         lib[nlib] = "  Profil  amont";
         clib[nlib].x     = x1 - 1. *largref;
         clib[nlib].y     = yminw -deltayref;
         asplib [nlib]    = Color.green;
  		 vinflib [nlib]   = 0.;
  		 vsupflib[nlib]   = 0.;
         nlib++;
  //
         lib[nlib] = "Profil  aval";
         clib[nlib].x     = x4 + 1.*largref ;
         clib[nlib].y     = yminw -deltayref  ;
         asplib [nlib]    = Color.green;
  		 vinflib [nlib]   = 0.;
  		 vsupflib[nlib]   = 0.;
         nlib++;
   }// fin prepGraphParTrav
  */
  //**********************************************************************
  /*
  private void prepGraphResPlan()     // nouvelle version
   {

      //========================================================
       double  inclinaison    = parm_.inclinaison;
       double  oblicite       = parm_.oblicite;
       double  raycourb       = parm_.rayonCourb;
       int nptAm 			 = parm_.profAmont.nbPoints;
       int nptAv				 = parm_.profAval.nbPoints;
      //  on connait aussi 	parm_.profAmont.xy[ ].x
  	 // 					parm_.profAval .xy[ ].y

     //==========================================================
     // cas des resultats
     //==========================================================
       String  typeCretCal    = res_.resultatsSEU().typeCretCal  ;
       String  rebord         = res_.resultatsSEU().rebord  ;
       double  pelle          = res_.resultatsSEU().pelle  ;
       double  epais          = res_.resultatsSEU().epaisCret  ;
       double  larg           = res_.resultatsSEU().largEcoul  ;
       double  hc             = res_.resultatsSEU().hc  ;
       double  chargAmont     = res_.resultatsSEU().chargAmont ;
       double  hautAmont      = res_.resultatsSEU().hautAmont  ;
       double  cotAmont       = res_.resultatsSEU().cotAmont  ;
       double  chargAval      = res_.resultatsSEU().chargAval  ;
       double  hautAval       = res_.resultatsSEU().hautAval  ;
       double  cotAval        = res_.resultatsSEU().cotAval  ;
      //=========================================================
       double xminAm,xmaxAm,yminAm;
       double xgcoupAm,xdcoupAm;   // x largeur miroir gauche  et droite
       int iptminAm;  // indice pt ymin
       double xminAv,xmaxAv,yminAv;
       double xgcoupAv,xdcoupAv ;   // x largeur miroir gauche  et droite
       int iptminAv;  // indice pt ymin
      //==========================
       int i1,i2,kcoupw ;
       double w,cw,y1w,y2w,xw;
       int kcoupg,kcoupd;  //  kcoup = 0  si pas coupe  = 1 si coupe
      //============================================
       System.out.println("cotAmont = "+ cotAmont);
       System.out.println("cotAval  = "+ cotAval);
      //========================================================
      //    pretraitement profil amont
      //========================================================
       xminAm =  parm_.profAmont.xy[0].x;
       xmaxAm =  parm_.profAmont.xy[parm_.profAmont.nbPoints-1].x;
       yminAm =  999999.;
       iptminAm = -999;
       xw = 0.;
       cw = 0.;
       xgcoupAm = 0.;
       xdcoupAm = 0.;
     //====================
     // recherche du ymin
     //=====================
     for(int i = 0; i <  parm_.profAmont.nbPoints;i++)
     {
       System.out.println("amont " +i+" "+ parm_.profAmont.xy[i].x +" "+parm_.profAmont.xy[i].y);
       if( parm_.profAmont.xy[i].y  <  yminAm)
       {
        		iptminAm = i;
        		yminAm = parm_.profAmont.xy[i].y;
       }
     }
     //=============================
     // recherche de largeur miroir
     //=============================
     kcoupg = 0;
     kcoupd = 0;

     for(int i = 0; i <  (parm_.profAmont.nbPoints-1);i++)
     {
       i1 = i;
       i2 = i+1;
       y1w =  parm_.profAmont.xy[i1].y;
       y2w =  parm_.profAmont.xy[i2].y;
       w = (y1w -cotAmont)* (y2w -cotAmont);
       kcoupw = 0;
       if(w  < 0.)
       {
       	if(y1w != y2w)
       	{
        	kcoupw = 1;
        	cw = ( cotAmont - y1w)/ (y2w - y1w);
         xw  =  parm_.profAmont.xy[i1].x + ( parm_.profAmont.xy[i2].x - parm_.profAmont.xy[i1].x )*cw;
         }
       }
       if(w  == 0.)
       {
        	if( y1w ==  cotAmont)
        	{
        	   kcoupw = 1;
        	   xw = parm_.profAmont.xy[i1].x;
        	}
         if( y2w ==  cotAmont)
        	{
        	   kcoupw = 1;
        	   xw = parm_.profAmont.xy[i2].x;
        	}
       }
       if(kcoupw == 1)
       {
          if(i1 < iptminAm)
          {
             kcoupg = 1;
             xgcoupAm = xw;
          }
          if(i2 > iptminAm)
          {
              kcoupd = 1;
              xdcoupAm = xw;
          }
       }
     }
     if(kcoupg == 0)
     {
       if (  cotAmont >  parm_.profAmont.xy[0].y )
       {
           kcoupg = 1;
           xgcoupAm = parm_.profAmont.xy[0].x;
       }
     }
     if(kcoupd == 0)
     {
       if (  cotAmont >  parm_.profAmont.xy[parm_.profAmont.nbPoints-1].y )
       {
           kcoupd = 1;
           xdcoupAm = parm_.profAmont.xy[parm_.profAmont.nbPoints-1].x;
       }
         }
     if (kcoupg == 1)
     {
       System.out.println("xgcoupAm  = "+ xgcoupAm);
     }
     else
     {
        System.out.println("**** Impossible determiner xgcoupAm ****   ");
     }
     if (kcoupd == 1)
     {
       System.out.println("xdcoupAm  = "+ xdcoupAm);
     }
     else
     {
     	 System.out.println("***** Impossible determiner xdcoupAm **** ");
     }

     //===============================================================



     for(int i = 0; i <  parm_.profAval.nbPoints;i++)
     {

       System.out.println("aval " +i+" "+   parm_.profAval.xy[i].x +" "+parm_.profAval.xy[i].y);

     }
     //========================================================
      //    pretraitement profil aval
      //========================================================
       xminAv =  parm_.profAval.xy[0].x;
       xmaxAv =  parm_.profAval.xy[parm_.profAval.nbPoints-1].x;
       yminAv =  999999.;
       iptminAv = -999;
       xw = 0.;
       cw = 0.;
       xgcoupAv = 0.;
       xdcoupAv = 0.;
     //====================
     // recherche du ymin
     //=====================
     for(int i = 0; i <  parm_.profAval.nbPoints;i++)
     {
       System.out.println("aval " +i+" "+ parm_.profAval.xy[i].x +" "+parm_.profAval.xy[i].y);
       if( parm_.profAval.xy[i].y  <  yminAv)
       {
        		iptminAv = i;
        		yminAv = parm_.profAval.xy[i].y;
       }
     }
     //=============================
     // recherche de largeur miroir
     //=============================
     kcoupg = 0;
     kcoupd = 0;

     for(int i = 0; i <  (parm_.profAval.nbPoints-1);i++)
     {
       i1 = i;
       i2 = i+1;
       y1w =  parm_.profAval.xy[i1].y;
       y2w =  parm_.profAval.xy[i2].y;
       w = (y1w -cotAval)* (y2w -cotAval);
       kcoupw = 0;
       if(w  < 0.)
       {
       	if(y1w != y2w)
       	{
        	kcoupw = 1;
        	cw = ( cotAval - y1w)/ (y2w - y1w);
         xw  =  parm_.profAval.xy[i1].x + ( parm_.profAval.xy[i2].x - parm_.profAval.xy[i1].x )*cw;
         }
       }
       if(w  == 0.)
       {
        	if( y1w ==  cotAval)
        	{
        	   kcoupw = 1;
        	   xw = parm_.profAval.xy[i1].x;
        	}
         if( y2w ==  cotAval)
        	{
        	   kcoupw = 1;
        	   xw = parm_.profAval.xy[i2].x;
        	}
       }
       if(kcoupw == 1)
       {
          if(i1 < iptminAv)
          {
             kcoupg = 1;
             xgcoupAv = xw;
          }
          if(i2 > iptminAv)
          {
              kcoupd = 1;
              xdcoupAv = xw;
          }
       }
     }
     if(kcoupg == 0)
     {
       if (  cotAval >  parm_.profAval.xy[0].y )
       {
           kcoupg = 1;
           xgcoupAv = parm_.profAval.xy[0].x;
       }
     }
     if(kcoupd == 0)
     {
       if (  cotAval >  parm_.profAval.xy[parm_.profAval.nbPoints-1].y )
       {
           kcoupd = 1;
           xdcoupAv = parm_.profAval.xy[parm_.profAval.nbPoints-1].x;
       }
         }
     if (kcoupg == 1)
     {
       System.out.println("xgcoupAv  = "+ xgcoupAv);
     }
     else
     {
        System.out.println("**** Impossible determiner xgcoupAv ****   ");
     }
     if (kcoupd == 1)
     {
       System.out.println("xdcoupAv  = "+ xdcoupAv);
     }
     else
     {
     	 System.out.println("***** Impossible determiner xdcoupAv **** ");
     }

     //===============================================================

     //  System.out.println("oblicit�= "+ oblicite);
      // if(rebord.equals("droit")) { inclinaison = 0.;}
      //   System.out.println("inclinaison   = "+ inclinaison);
      // variables de travail

          int ic;   // indice courbe
          int ip;   // indice point
          double largAm;
          double largAv;
          double x1,y1,x2,y2,x3,y3,x4,y4;
          double x5,y5,x6,y6,x7,y7,x8,y8;
          double alphaw ;  // angle en radians
          double deltax;

     //==========================================================
     //  dessin de la vue en   plan du seuil
     //  ======================================
     //  on se fixe largref = max(hauteur seuil, 2.00m = largrefmin )
     //
     //
     //  le dessin fait en gros 4*largref
     //
     //  l'origine en x est au point les plus bas du seuil
     //  vers l'aval
     //
     //
     //===========================================================

     // instantiation

      for(  ic=0; ic< ncmax; ic++)
      {

           aspcont[ic]    = Color.black;
           aspsurf[ic]    = Color.black;
           titc[ic]       = " ";
    		  trac [ic]      = "lineaire";
    		  type [ic]      = "courbe";
    		  marq [ic]      =  false;


            for(  ip = 0; ip < nptmax ; ip++)
       	   {
              coord[ic][ip] = new SPoint01();
            }
      }
       for(  ic=0; ic< nptmax; ic++)
       {
      	 for(  ip = 0; ip < nptmax ; ip++)
       	{
           coord[ic][ip].x  =-999. ;
           coord[ic][ip].y  =-999. ;

      	}
       }
       for(int il = 0; il< nlibmax; il++)
       {
          clib[il]        = new SPoint01();
          clib[il].x     =-999. ;
          clib[il].y     =-999. ;
          asplib [il]    = Color.black;
   		 vinflib [il]   = 0.;
   		 vsupflib[il]   = 0.;
   		 lib[il]        = "";
       }

     // initialisation

     nlib = 0;
     nc = 0;

     double largref;

     // calcul largeurs des profils

      largAm = Math.abs( parm_.profAmont.xy[0].x - parm_.profAmont.xy[nptAm-1 ].x);
      largAv = Math.abs( parm_.profAval .xy[0].x - parm_.profAval .xy[nptAv-1 ].x);
      deltayref = largAm/8.;
      if(largAv > largAm) {deltayref = largAv/8.; }


    // pour que l'oblicite ne soit pas deformee

    largref = largAm/2.;
    if(largAv > largAm) {largref = largAv/2.; }

    deltaxref = largref/5.;

     //================
     // trace du seuil
     //================
     double largmax ;
     largmax = largAv;
     if(largAm >= largAv)
     {
      largmax = largAm;
     }



     ic = nc;
     ip = 0;
     aspcont[ic]    = Color.black;
     aspsurf[ic]    = Color.black;

     // les 4 points du seuil 1,2,3,4 dans sens trigo

     x4 = 0.;           // l'origine des x
  //   y4 = -1.*larg/2. ;
  	y4 = -1.*largmax/2. ;
     x3 = 0.;
  //   y3 = larg/2.;
     y3 = largmax/2.;
     x1 = x4 - epais;
     y1 = y4;
     x2 = x1;
     y2 = y3;
     			  // si oblicite # 0. on bouge pas le point 4
     alphaw = (oblicite*Math.PI)/180.;
   //  deltax = (larg/2.)* Math.sin(alphaw);
  	deltax = (largmax/2.)* Math.sin(alphaw);
   //  x1 = x1 - deltax;
     x2 = x2 - deltax;
     x3 = x3 - deltax;

     coord[ic][ip].x = x1;
     coord[ic][ip].y = y1;
     ip++;
     coord[ic][ip].x = x2;
     coord[ic][ip].y = y2;
     ip++;
     coord[ic][ip].x = x3;
     coord[ic][ip].y = y3;
     ip++;
     coord[ic][ip].x = x4;
     coord[ic][ip].y = y4;
     ip++;
     coord[ic][ip].x = x1;
     coord[ic][ip].y = y1;
     ip++;
     npt[ic] = ip;
     ic++; nc = ic;
     //
     //====================================

     double ydesam[] =  new double [nptmax];   // ydessin profil amont sur seuil
     double xdesam[] = new double [nptmax];   // xdessin profil amont sur seuil
     double ydesbasAm,ydeshautAm;      // ydessin largeur miroir pt. bas et pt. haut
     double xdesbasAm,xdeshautAm;      // x  "      "             "    "    "     "
     double ydesbasAv,ydeshautAv;      // ydessin largeur miroir pt. bas et pt. haut
     double xdesbasAv,xdeshautAv;      // x  "      "             "    "    "     "
     double ydesav[] =  new double [nptmax];   // ydessin profil aval sur seuil
     double xdesav[] =  new double [nptmax];   // xdessin profil aval sur seuil
     double deltaym ,deltayv;
     double deltaxm ,deltaxv;


     deltaym = (largmax - largAm)/2.;      // deltay si le plus petit profil
     deltayv = (largmax - largAv)/2.;
     deltaxm  = deltaym* Math.sin(alphaw); // deltax si le plus petit profil
     deltaxv  = deltayv* Math.sin(alphaw);

     //calcul des coordonees dessin

     for(int i = 0; i <  parm_.profAmont.nbPoints;i++)
     {
      ydesam[i] = y1+deltaym+ (parm_.profAmont.xy[i].x - parm_.profAmont.xy[0].x)*(y2-y1)/largmax;
      xdesam[i] = x1+deltaxm+(parm_.profAmont.xy[i].x - parm_.profAmont.xy[0].x)*(x2-x1)/largmax;
     }

     ydesbasAm  =  y1+deltaym+ (xgcoupAm  - parm_.profAmont.xy[0].x)*(y2-y1)/largmax;
     xdesbasAm =   x1+deltaxm+ (xgcoupAm  - parm_.profAmont.xy[0].x)*(x2-x1)/largmax;
     ydeshautAm  = y1+deltaym+ (xdcoupAm  - parm_.profAmont.xy[0].x)*(y2-y1)/largmax;
     xdeshautAm =  x1+deltaxm+ (xdcoupAm  - parm_.profAmont.xy[0].x)*(x2-x1)/largmax;

     for(int i = 0; i <  parm_.profAval.nbPoints;i++)
     {
      ydesav[i] = y4+deltayv+ (parm_.profAval.xy[i].x-parm_.profAval.xy[0].x)*(y3-y4)/largmax;
      xdesav[i] = x4+deltaxv+(parm_.profAval.xy[i].x-parm_.profAval.xy[0].x)*(x3-x4)/largmax;
     }


     ydesbasAv  =  y4+deltayv + (xgcoupAv  - parm_.profAval.xy[0].x)*(y3-y4)/largmax;
     xdesbasAv=    x4+deltaxv + (xgcoupAv  - parm_.profAval.xy[0].x)*(x3-x4)/largmax;
     ydeshautAv  = y4+deltayv + (xdcoupAv  - parm_.profAval.xy[0].x)*(y3-y4)/largmax;
     xdeshautAv =  x4+deltaxv + (xdcoupAv  - parm_.profAval.xy[0].x)*(x3-x4)/largmax;




     //===========================
     // trace largeur miroir amont
     //===========================
       ip = 0; ic = nc ;
             type[ic]      = "polygone";
    			aspcont[ic]    = Color.blue;
        		aspsurf[ic]    = Color.blue;
             coord[ic][ip].x = xdesbasAm ;
     		coord[ic][ip].y = ydesbasAm ;
     		ip++;
     		coord[ic][ip].x = xdeshautAm ;
     		coord[ic][ip].y = ydeshautAm ;
     		ip++;
     		coord[ic][ip].x = x4 - largref ;
     		coord[ic][ip].y = ydeshautAm ;
     		ip++;
             coord[ic][ip].x = x4 - largref;
     		coord[ic][ip].y = ydesbasAm  ;
    			ip++;
    			 // coord[ic][ip].x = xdesbasAm ;
     		 // coord[ic][ip].y = ydesbasAm ;
     		 // ip++;
    			npt[ic] = ip; ic++; nc = ic;

     //===========================
     // trace largeur miroir aval
     //===========================
       ip = 0; ic = nc ;
                type[ic]      = "polygone";
    			aspcont[ic]    = Color.blue;
        		aspsurf[ic]    = Color.blue;
             coord[ic][ip].x = xdesbasAv ;
     		coord[ic][ip].y = ydesbasAv ;
     		ip++;
     		coord[ic][ip].x = xdeshautAv ;
     		coord[ic][ip].y = ydeshautAv ;
     		ip++;
     		coord[ic][ip].x = x4 + largref ;
     		coord[ic][ip].y = ydeshautAv ;
     		ip++;
             coord[ic][ip].x = x4 + largref;
     		coord[ic][ip].y = ydesbasAv  ;
    			ip++;
    			//coord[ic][ip].x = xdesbasAv ;
     		//coord[ic][ip].y = ydesbasAv ;
     		//ip++;
    			npt[ic] = ip; ic++; nc = ic;

     //========================
     // trace du profil amont
     //========================
     for(int i = 0; i <  parm_.profAmont.nbPoints;i++)
     {
      ydesam[i] = y1+deltaym+ (parm_.profAmont.xy[i].x - parm_.profAmont.xy[0].x)*(y2-y1)/largmax;
      xdesam[i] = x1+deltaxm+(parm_.profAmont.xy[i].x - parm_.profAmont.xy[0].x)*(x2-x1)/largmax;
      ip = 0; ic = nc ;
      		marq [ic]      =  true;
    			aspcont[ic]    = Color.black;
        		aspsurf[ic]    = Color.black;
             coord[ic][ip].x = xdesam[i] ;
     		coord[ic][ip].y = ydesam[i] ;
     		ip++;
     		coord[ic][ip].x = x4 - largref;
     		coord[ic][ip].y = ydesam[i] ;
    			ip++;
    			npt[ic] = ip; ic++; nc = ic;
     }


     ip = 0; ic = nc ;
      		marq [ic]      =  true;
    			aspcont[ic]    = Color.black;
        		aspsurf[ic]    = Color.black;
             coord[ic][ip].x = x4 - largref;
     		coord[ic][ip].y = ydesam[0];
     		ip++;
     		coord[ic][ip].x = x4 - largref;
     		coord[ic][ip].y = ydesam[parm_.profAmont.nbPoints-1];
    			ip++;
    			npt[ic] = ip; ic++; nc = ic;




     //========================
     // trace du profil aval
     //========================
     for(int i = 0; i <  parm_.profAval.nbPoints;i++)
     {
      ydesav[i] = y4+deltayv+ (parm_.profAval.xy[i].x - parm_.profAval.xy[0].x)*(y3-y4)/largmax;
      xdesav[i] = x4+deltaxv+(parm_.profAval.xy[i].x - parm_.profAval.xy[0].x)*(x3-x4)/largmax;
      ip = 0; ic = nc ;
             marq [ic]      =  true;
    			aspcont[ic]    = Color.black;
        		aspsurf[ic]    = Color.black;
             coord[ic][ip].x = xdesav[i] ;
     		coord[ic][ip].y = ydesav[i] ;
     		ip++;
     		coord[ic][ip].x = x4 + largref;
     		coord[ic][ip].y = ydesav[i] ;
    			ip++;
    			npt[ic] = ip; ic++; nc = ic;
     }


        ip = 0; ic = nc ;
             marq [ic]      =  true;
    			aspcont[ic]    = Color.black;
        		aspsurf[ic]    = Color.black;
             coord[ic][ip].x = x4 + largref ;
     		coord[ic][ip].y = ydesav[0];
     		ip++;
     		coord[ic][ip].x = x4 + largref;
     		coord[ic][ip].y = ydesav[parm_.profAval.nbPoints-1];
    			ip++;
    			npt[ic] = ip; ic++; nc = ic;


   //===================
   // trace du seuil
   //===================
    ic = nc;
     ip = 0;
     aspcont[ic]    = Color.black;
     aspsurf[ic]    = Color.black;

     coord[ic][ip].x = x1;
     coord[ic][ip].y = y1;
     ip++;
     coord[ic][ip].x = x2;
     coord[ic][ip].y = y2;
     ip++;
     coord[ic][ip].x = x3;
     coord[ic][ip].y = y3;
     ip++;
     coord[ic][ip].x = x4;
     coord[ic][ip].y = y4;
     ip++;
     coord[ic][ip].x = x1;
     coord[ic][ip].y = y1;
     ip++;
     npt[ic] = ip;
     ic++; nc = ic;


   //=========================================

    x6 = x2 - largref;
    x5 = x6;
    x8 = x4 + largref;
    x7 = x8;
    y5 = -1.* largAm/2.;
    y6 = largAm/2.;
    y7 = largAv/2.;
    y8 = -1.*largAv/2.;




    //  Libelles

          lib[nlib] = "   Profil  amont";
          clib[nlib].x     = x5 + deltaxref;
          clib[nlib].y     = y5 -  deltayref/2.;
          asplib [nlib]    = Color.black;
   		 vinflib [nlib]   = 0.;
  		 vsupflib[nlib]   = 0.;
          nlib++;
  //
          lib[nlib] = "Profil  aval";
          clib[nlib].x     = x8 -deltaxref;
          clib[nlib].y     = y8 - deltayref/2. ;
          asplib [nlib]    = Color.black;
   		 vinflib [nlib]   = 0.;
  		 vsupflib[nlib]   = 0.;
          nlib++;



    }// fin prepGraphPlan
  */
} // finclasse
