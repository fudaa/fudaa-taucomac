/*
 * @file         TaucomacResource.java
 * @creation     2000-10-24
 * @modification $Date: 2005-08-16 13:29:19 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.taucomac;
//import org.fudaa.fudaa.commun.*;
import com.memoire.bu.BuResource;
//import org.fudaa.ebli.bu.*;
/**
 * Le gestionnaire de ressources de Taucomac.
 *
 * @version      $Revision: 1.5 $ $Date: 2005-08-16 13:29:19 $ by $Author: deniger $
 * @author       Jean-Yves Riou 
 */
public class TaucomacResource extends BuResource {
  public final static String TAUCOMAC01= "01";
  public final static String SEUILOUT= "OUT";
  public final static TaucomacResource TAUCOMAC= new TaucomacResource();
}
