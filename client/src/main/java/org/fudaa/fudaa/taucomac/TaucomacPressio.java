/*
 * @file         TaucomacPressio.java
 * @creation     2000-10-17
 * @modification $Date: 2006-09-19 15:08:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.taucomac;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.taucomac.SPressio;
import org.fudaa.dodico.corba.taucomac.SPtInter;
import org.fudaa.dodico.corba.taucomac.SSolPlast;
/**
 * Onglet proprietes pressiometriques du sol
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:08:56 $ by $Author: deniger $
 * @author       Jean-Yves Riou
 */
public class TaucomacPressio extends TaucomacMerePanParm implements ActionListener, FocusListener {

  SPressio parametresLocal = null;
  /*
   struct SPtInter  							//Point d'interaction
   {                                            //deformation
   reel def;                                 //effort
   reel eff;
   };
   typedef sequence<SPtInter> VSPtInters;

   struct SSolPlast     						//  couches de sol elastoplastique
   {
   reel 		zToit;
   reel 		modulePressio;
   reel 		pressionFluage;
   reel 		pressionLimite;
   reel 		coefRheo;
   reel 		coefReduc;
   entier 		modeSaisCourb;                  // 0 => ecran 1 => fichier
   entier     	nbPtCourb;  					// nbre points de la courbe
   VSPtInters 	ptCourb;                        // def et eff de la courbe
   };

   typedef sequence<SSolPlast> VSSolPlasts;


   struct SPressio
   {
   entier			nbCouchesPlast;     // sol �lastoplastique  (ncouch <= 10)
   entier          comportAmar;        // comportement amarrage 1 => Tres Court terme
   //                       2 => Court terme
   //                       3 => Long terme
   reel            coefPartInter;      // coef. partiel sur la courbe d'interation
   VSSolPlasts     couchesPlast;
   };
   SPressio            pressio;
   */
  private BuCommonInterface appli_;
  static protected TaucomacPressioDialog dial;
  protected boolean blocus1 = true;
  SPtInter points[] = null;
  public int colDialog_ = -1;
  // indice colonne qui ouvre la TaucomacPressioDialog
  public int selectCol = -9;
  public int selectLin = -9;
  public int linMem = -9;
  public int colMem = -9;
  public int anclinMem = -9;
  public int ancolMem = -9;
  private final boolean DEBUG = true;
  private final boolean ALLOW_COLUMN_SELECTION = true;
  private final boolean ALLOW_ROW_SELECTION = true;
  data[][] donnees_;
  private int donneesNbColonne_;
  private int donneesNbLigne_;
  JPanel pnHaut;
  JPanel pnMil;
  JPanel pnMilG;
  JPanel pnMilC;
  JPanel pnMilD;
  JPanel pnBasG;
  JPanel pnBasD;
  JPanel pnBas;
  JLabel la_commentaire;
  JLabel la_nbCouches;
  BuTextField tf_nbCouches;
  TitledBorder titreBas;
  JRadioButton[] rbi_;
  JLabel la_coefpart;
  BuTextField tf_coefpart;
  public int localPhase_;
  MyTableModel myModel = new MyTableModel();
  JTable table;
  /*
   JTable       table    = new JTable(myModel)
   {
   public TableCellRenderer getCellRenderer(int row, int column)
   {
   return super.getCellRenderer(row, column);
   }
   };
   */
  public int nbMaxCouchSol_ = 10;
  public TaucomacCellEditor[] anEditor;
  //=========
  final Object[][] data1 = { { "Entree  de la courbe point par point"}, { "Edition de la courbe"},
      { "Cote du toit de la couche"}, { "Module pressiometrique"}, { "Pression de fluage"},
      { "Pression limite"}, { "coefficient rheologique"}, { "Coefficient de r�duction (*)"}};
  MyTableModel1 myModel1 = new MyTableModel1();
  JTable table1 = new JTable(myModel1);
  public TaucomacCellEditor anEditor21 = new TaucomacCellEditor();
  //=========
  final Object[][] data2 = { { ""}, { ""}, { " m "}, { " t/m2 "}, { " t/m2 "}, { " t/m2 "}, { ""},
      { ""}};
  MyTableModel2 myModel2 = new MyTableModel2();
  JTable table2 = new JTable(myModel2);
  public TaucomacCellEditor anEditor22 = new TaucomacCellEditor();

  //==================
  class data {

    Color couleur_ = null;
    String valeur_;

    public data(final String valeur) {
      valeur_ = valeur;
    }

    public String toString(){
      return valeur_;
    }

    public void setCouleur(final Color _c){
      couleur_ = _c;
    }

    public Color getCouleur(){
      return couleur_;
    }

    public void setValeur(final String _valeur){
      valeur_ = _valeur;
    }
  };

  //===============
  class ColorRenderer extends JLabel implements TableCellRenderer {

    Border unselectedBorder = null;
    Border selectedBorder = null;
    boolean isBordered = true;
    Color defautCouleur_ = Color.blue;

    public ColorRenderer(final boolean _isBordered, final boolean isOpaque, final Color _couleurParDefaut) {
      super();
      if (_couleurParDefaut != null) {
        defautCouleur_ = _couleurParDefaut;
      }
      this.isBordered = _isBordered;
      this.setOpaque(isOpaque);
      this.setHorizontalAlignment(RIGHT);
    }

    public Component getTableCellRendererComponent(final JTable _t,final Object value,final boolean isSelected,
      final boolean hasFocus,final int row,final int column){
      setText("" + value);
      final Color CouleurSpecifique = ((data) value).getCouleur();
      if (CouleurSpecifique != null) {
        this.setBackground(CouleurSpecifique);
      } else {
        this.setBackground(defautCouleur_);
      }
      if (isBordered) {
        if (isSelected) {
          if (selectedBorder == null) {
            selectedBorder = BorderFactory.createMatteBorder(2, 5, 2, 5, _t
                .getSelectionBackground());
          }
          this.setBorder(selectedBorder);
        }
        else {
          if (unselectedBorder == null) {
            unselectedBorder = BorderFactory.createMatteBorder(2, 5, 2, 5, _t.getBackground());
          }
          this.setBorder(unselectedBorder);
        }
      }
      return this;
    }
  };

  //====================================
  //    Constructeur
  //====================================
  public TaucomacPressio(final BuCommonInterface _appli) {
    super();
    appli_ = _appli;
    //====================================
    donneesNbColonne_ = 10;
    donneesNbLigne_ = 8;
    initialiseData(donneesNbLigne_, donneesNbColonne_);
    //====
    table = new JTable(myModel) {

      ColorRenderer cr = new ColorRenderer(false, true, Color.cyan);

      public TableCellRenderer getCellRenderer(final int row,final int column){
        return cr;
      }
    };
    //=====
    pnHaut = new JPanel();
    final BuGridLayout loCal2 = new BuGridLayout();
    loCal2.setColumns(2);
    loCal2.setHgap(5);
    loCal2.setVgap(10);
    loCal2.setHfilled(false);
    loCal2.setCfilled(true);
    pnHaut.setLayout(loCal2);
    pnHaut.setBorder(new EmptyBorder(5, 60, 5, 440)); //top,left,bottom,right
    //pnHaut.setPreferredSize(new Dimension(650,120));
    la_nbCouches = new JLabel(" Nombre de couches du sol  ", SwingConstants.RIGHT);
    tf_nbCouches = BuTextField.createIntegerField();
    tf_nbCouches.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_nbCouches.addFocusListener(this);
    tf_nbCouches.setColumns(4);
    int n = 0;
    n = 0;
    placeCompn(pnHaut, la_nbCouches, 0, n);
    n++;
    placeCompn(pnHaut, tf_nbCouches, 0, n);
    //====================================
    pnBasG = new JPanel();
    titreBas = new TitledBorder("Comportement du sol pour l'amarrage");
    placeTitre(pnBasG, titreBas, 0);
    pnBasG.setLayout(new BorderLayout(30, 20));
    pnBasG.setPreferredSize(new Dimension(300, 120));
    pnBasG.setLayout(new BuGridLayout(2, 5, 5, true, false));
    final ButtonGroup bi = new ButtonGroup();
    rbi_ = new JRadioButton[3];
    for (int i = 0; i < 3; i++) {
      rbi_[i] = new JRadioButton();
      rbi_[i].addActionListener(this);
      bi.add(rbi_[i]);
    }
    n = 0;
    pnBasG.add(rbi_[0], n++);
    pnBasG.add(new BuLabelMultiLine("Tr�s court terme"), n++);
    pnBasG.add(rbi_[1], n++);
    pnBasG.add(new BuLabelMultiLine("Court terme"), n++);
    pnBasG.add(rbi_[2], n++);
    pnBasG.add(new BuLabelMultiLine("Long terme"), n++);
    //=====================================
    pnBasD = new JPanel();
    final BuGridLayout loCal7 = new BuGridLayout();
    loCal7.setColumns(2);
    loCal7.setHgap(5);
    loCal7.setVgap(10);
    loCal7.setHfilled(false);
    loCal7.setCfilled(true);
    pnBasD.setLayout(loCal7);
    pnBasD.setBorder(new EmptyBorder(40, 20, 10, 20)); //top,left,bottom,right
    la_coefpart = new JLabel(" Coefficient partiel sur la courbe d'interaction ",
        SwingConstants.RIGHT);
    tf_coefpart = BuTextField.createDoubleField();
    tf_coefpart.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_coefpart.setColumns(4);
    n = 0;
    placeCompn(pnBasD, la_coefpart, 2, n);
    n++;
    placeCompn(pnBasD, tf_coefpart, 2, n);
    //=================
    pnBas = new JPanel();
    pnBas.setBorder(new TitledBorder(""));
    pnBas.setLayout(new BorderLayout());
    pnBas.setBorder(new EmptyBorder(10, 20, 10, 20)); //top,left,bottom,right
    pnBas.add(pnBasG, BorderLayout.WEST);
    pnBas.add(pnBasD, BorderLayout.EAST);
    //======================
    pnMil = new JPanel();
    pnMil.setBorder(new TitledBorder(""));
    pnMil.setLayout(new BorderLayout());
    pnMil.setBorder(new EmptyBorder(10, 0, 10, 0)); //top,left,bottom,right
    // pnMil.setPreferredSize(new Dimension(700,250));
    pnMilC = new JPanel();
    // pnMilC.setPreferredSize(new Dimension(500,80 ));
    pnMilG = new JPanel();
    pnMilD = new JPanel();
    //===================
    //    Tables
    //===================
    table1.setBackground(Color.orange);
    table1.setPreferredScrollableViewportSize(new Dimension(230, 130));
    table1.getColumn(table1.getColumnName(0)).setCellEditor(anEditor21);
    final JScrollPane scrollPane1 = new JScrollPane(table1);
    (table1.getTableHeader()).setReorderingAllowed(false);
    table1.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    (table1.getColumn(table1.getColumnName(0))).setPreferredWidth(230);
    table2.setBackground(Color.orange);
    table2.setPreferredScrollableViewportSize(new Dimension(65, 130));
    table2.getColumn(table2.getColumnName(0)).setCellEditor(anEditor22);
    final JScrollPane scrollPane2 = new JScrollPane(table2);
    (table2.getTableHeader()).setReorderingAllowed(false);
    table2.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    table.setColumnSelectionAllowed(false);
    table.setRowSelectionAllowed(false);
    table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    table.setCellSelectionEnabled(true);
    table.setBackground(Color.cyan);
    table.setForeground(Color.black);
    table.setSelectionBackground(Color.white);
    table.setSelectionForeground(Color.blue);
    table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    anEditor = new TaucomacCellEditor[nbMaxCouchSol_];
    TableColumn kolw = null;
    for (int i = 0; i < nbMaxCouchSol_; i++) {
      kolw = table.getColumnModel().getColumn(i);
      kolw.setPreferredWidth(80);
    }
    for (int i = 0; i < nbMaxCouchSol_; i++) {
      kolw = table.getColumnModel().getColumn(i);
      anEditor[i] = new TaucomacCellEditor();
      // table.getColumn(table.getColumnName(i)).setCellEditor(anEditor[i]);
      kolw.setCellEditor(anEditor[i]);
    }
    lookInit();
    //
    if (ALLOW_ROW_SELECTION) { // true by default
      final ListSelectionModel rowSM = table.getSelectionModel();
      rowSM.addListSelectionListener(new ListSelectionListener() {

        public void valueChanged(final ListSelectionEvent e){
          //Ignore extra messages.
          if (e.getValueIsAdjusting()) {
            return;
          }
          final ListSelectionModel lsm = (ListSelectionModel) e.getSource();
          if (lsm.isSelectionEmpty()) {
            //  System.out.println("No rows are selected.");
            selectLin = -9;
          }
          else {
            final int selectedRow = lsm.getMinSelectionIndex();
            //System.out.println("Row " + selectedRow
            //                  + " is now selected.");
            selectLin = selectedRow;
          }
        }
      });
    }
    else {
      table.setRowSelectionAllowed(false);
    }
    //
    if (ALLOW_COLUMN_SELECTION) { // false by default
      if (ALLOW_ROW_SELECTION) {
        //We allow both row and column selection, which
        //implies that we *really* want to allow individual
        //cell selection.
        table.setCellSelectionEnabled(true);
      }
      table.setColumnSelectionAllowed(true);
      final ListSelectionModel colSM = table.getColumnModel().getSelectionModel();
      colSM.addListSelectionListener(new ListSelectionListener() {

        public void valueChanged(final ListSelectionEvent e){
          //Ignore extra messages.
          if (e.getValueIsAdjusting()) {
            return;
          }
          final ListSelectionModel lsm = (ListSelectionModel) e.getSource();
          if (lsm.isSelectionEmpty()) {
            // System.out.println("No columns are selected.");
            selectCol = -9;
          }
          else {
            final int selectedCol = lsm.getMinSelectionIndex();
            // System.out.println("Column " + selectedCol
            //                    + " is now selected.");
            selectCol = selectedCol;
          }
        }
      });
    }
    if (DEBUG) {
      table.addMouseListener(new MouseAdapter() {

        public void mouseClicked(final MouseEvent e){
          selectAvise(selectLin, selectCol);
        }
      });
    }
    table.setPreferredScrollableViewportSize(new Dimension(445, 130));
    final JScrollPane scrollPane = new JScrollPane(table);
    (table.getTableHeader()).setReorderingAllowed(false);
    table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    la_commentaire = new JLabel(
        "(*)Coefficient de reduction des proprietes pressiometriques du haut de la couche par rapport � celles du bas de la couche ",
        SwingConstants.LEFT);
    pnMilC.add(scrollPane);
    pnMilG.add(scrollPane1);
    pnMilD.add(scrollPane2);
    pnMil.add(pnMilC, BorderLayout.CENTER);
    pnMil.add(pnMilG, BorderLayout.WEST);
    pnMil.add(pnMilD, BorderLayout.EAST);
    pnMil.add(la_commentaire, BorderLayout.SOUTH);
    //====================================
    this.setLayout(new BorderLayout());
    this.setBorder(BorderFactory.createEmptyBorder(10, 5, 10, 5));
    this.add(pnHaut, BorderLayout.NORTH);
    this.add(pnMil, BorderLayout.CENTER);
    this.add(pnBas, BorderLayout.SOUTH);
    //
    dial = new TaucomacPressioDialog(appli_, this);
    //initialisation des parametres locaux
    setParametres(parametresLocal);
  }

  private  void initialiseData(final int _nbLigne,final int _nbCol){
    donnees_ = new data[_nbLigne][_nbCol];
    for (int i = 0; i < _nbLigne; i++) {
      for (int j = 0; j < _nbCol; j++) {
        donnees_[i][j] = new data("");
      }
    }
  }

  //
  public void focusLost(final FocusEvent e){
    final Object src = e.getSource();
    // System.out.println("FocusLost " + e );
    //System.out.println("pressio focusLost  "+e + " "+src );
    if (src instanceof JTextField) {
      if (src == tf_nbCouches) {
        //System.out.println("focusLost source " + src);
        String tmpS = "";
        final String blanc = "";
        int n = 0;
        tmpS = tf_nbCouches.getText();
        if (tmpS.equals(blanc)) {
          n = 0;
        }
        else {
          n = Integer.parseInt(tmpS);
        }
        //System.out.println("n " + n);
        myModel.setNbMaxColEditable(n);
        table.repaint();
      }
    }
  }

  public void focusGained(final FocusEvent e){
  // System.out.println("FocusGained " + e );
  }

  public void actionPerformed(final ActionEvent e){
    //System.out.println("actionPerformed " + e );
    parametresLocal = getParametres();
    setVisible(true);
    //      Object src=e.getSource();
    //   System.out.println("evenement " + e +" source "+src);
  }

  public void setParametres(final SPressio parametresProjet_){
    // System.out.println(" ===== Pressio set Parametres =====");
    parametresLocal = parametresProjet_;
    if (parametresLocal != null) {
      if (parametresLocal.coefPartInter >= 0) {
        tf_coefpart.setValue(new Double(parametresLocal.coefPartInter));
      }
      else {
        tf_coefpart.setValue("");
      }
      //===
      setSelectedAmarrage(parametresLocal.comportAmar);
      myModel.setNbMaxColEditable(parametresLocal.nbCouchesPlast);
      //==
      if (parametresLocal.nbCouchesPlast > 0) {
        tf_nbCouches.setValue(new Integer(parametresLocal.nbCouchesPlast));
      }
      else {
        tf_nbCouches.setValue("");
      }
      // System.out.println("*** setParametres nbCouchesPlast  = " +parametresLocal.nbCouchesPlast );
      if (parametresLocal.nbCouchesPlast > 0) {
        for (int j = 0; j < parametresLocal.nbCouchesPlast; j++) {
          // System.out.println("*** setParametres   j = " +j);
          parametresLocal.couchesPlast[j].modeSaisCourb = parametresProjet_.couchesPlast[j].modeSaisCourb;
          //System.out.println("*** setParametres modeSaisCourb = "+ parametresLocal.couchesPlast[j].modeSaisCourb );
          if (parametresLocal.couchesPlast[j].modeSaisCourb == 1) {
            table.setValueAt("oui", 0, j);
            table.setValueAt("*", 1, j);
            ((data) myModel.getValueAt(0, j)).setCouleur(Color.green);
            ((data) myModel.getValueAt(1, j)).setCouleur(Color.blue);
            table.repaint();
            final int n = parametresLocal.couchesPlast[j].nbPtCourb;
            //System.out.println("*** setParametres nbre de points = "+n );
            for (int i = 0; i < n; i++) {
              parametresLocal.couchesPlast[j].ptCourb[i] = parametresProjet_.couchesPlast[j].ptCourb[i];
              parametresLocal.couchesPlast[j].ptCourb[i].eff = parametresProjet_.couchesPlast[j].ptCourb[i].eff;
              parametresLocal.couchesPlast[j].ptCourb[i].def = parametresProjet_.couchesPlast[j].ptCourb[i].def;
              //System.out.println("Point "+(i+1)+" : def : "+ parametresLocal.couchesPlast[j].ptCourb[i].def);
              //System.out.println("      "+(i+1)+" : eff : "+ parametresLocal.couchesPlast[j].ptCourb[i].eff);
            }
          }
          else {
            table.setValueAt("non", 0, j);
            table.setValueAt("", 1, j);
            ((data) myModel.getValueAt(0, j)).setCouleur(Color.red);
            ((data) myModel.getValueAt(1, j)).setCouleur(Color.gray);
            table.repaint();
          }
          if (j > 0) {
            if (parametresLocal.couchesPlast[j].zToit != -999.) {
              table.setValueAt(new Double(parametresLocal.couchesPlast[j].zToit), 2, j);
            }
            else {
              table.setValueAt("", 2, j);
            }
          }
          if (parametresLocal.couchesPlast[j].modulePressio != -999.) {
            table.setValueAt(new Double(parametresLocal.couchesPlast[j].modulePressio), 3, j);
          }
          else {
            table.setValueAt("", 3, j);
          }
          if (parametresLocal.couchesPlast[j].pressionFluage != -999.) {
            table.setValueAt(new Double(parametresLocal.couchesPlast[j].pressionFluage), 4, j);
          }
          else {
            table.setValueAt("", 4, j);
          }
          if (parametresLocal.couchesPlast[j].pressionLimite != -999.) {
            table.setValueAt(new Double(parametresLocal.couchesPlast[j].pressionLimite), 5, j);
          }
          else {
            table.setValueAt("", 5, j);
          }
          if (parametresLocal.couchesPlast[j].coefRheo != -999.) {
            table.setValueAt(new Double(parametresLocal.couchesPlast[j].coefRheo), 6, j);
          }
          else {
            table.setValueAt("", 6, j);
          }
          if (parametresLocal.couchesPlast[j].coefReduc != -999.) {
            table.setValueAt(new Double(parametresLocal.couchesPlast[j].coefReduc), 7, j);
          }
          else {
            table.setValueAt("", 7, j);
          }
        }
      }
    }
  }

  public SPressio getParametres(){
    // System.out.println("=== Pressio get Parametres ===");
    if (parametresLocal == null) {
      parametresLocal = new SPressio();
      /*deniger*/
      parametresLocal.couchesPlast = new SSolPlast[nbMaxCouchSol_];
      /*deniger fin*/
      // System.out.println(" parametresLocal = "+parametresLocal);
      for (int i = 0; i < nbMaxCouchSol_; i++) {
        parametresLocal.couchesPlast[i] = new SSolPlast();
      }
    }
    String changed = "";
    double tmpD = 0.;
    int tmpI = 0;
    String tmpS = "";
    final String blanc = "";
    tmpD = parametresLocal.coefPartInter;
    tmpS = tf_coefpart.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.coefPartInter = -999.;
    }
    else {
      parametresLocal.coefPartInter = Double.parseDouble(tmpS);
    }
    if (parametresLocal.coefPartInter != tmpD) {
      changed += "coefPartInter|";
    }
    tmpI = parametresLocal.comportAmar;
    parametresLocal.comportAmar = getSelectedAmarrage();
    if (parametresLocal.comportAmar != tmpI) {
      changed += "comportAmar|";
    }
    tmpI = parametresLocal.nbCouchesPlast;
    tmpS = tf_nbCouches.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.nbCouchesPlast = 0;
    }
    else {
      parametresLocal.nbCouchesPlast = Integer.parseInt(tmpS);
    }
    if (parametresLocal.nbCouchesPlast != tmpI) {
      changed += "nbCouchesPlast|";
    }
    //System.out.println(" parametresLocal .nbCouchesPlast= "+parametresLocal.nbCouchesPlast);
    // Remplissage des structures:
    for (int j = 0; j < parametresLocal.nbCouchesPlast; j++) {
      // System.out.println("========= getcouchesPlast [j]. j =  "  +j+" =======");
      if (parametresLocal.couchesPlast[j] == null) {
        parametresLocal.couchesPlast[j] = new SSolPlast();
      }
      //System.out.println(" parametresLocal.couchesPlast[j]= "+parametresLocal.couchesPlast[j]);
      if (j > 0) {
        //  parametresLocal.couchesPlast[j].zToit  = new Double(data[2][j].toString()).doubleValue();
        tmpD = parametresLocal.couchesPlast[j].zToit;
        tmpS = String.valueOf(donnees_[2][j]);
        if (tmpS.equals(blanc)) {
          parametresLocal.couchesPlast[j].zToit = -999.;
        }
        else {
          parametresLocal.couchesPlast[j].zToit = Double.parseDouble(tmpS);
        }
        if (parametresLocal.couchesPlast[j].zToit != tmpD) {
          changed += "couchesPlast[j].zToit|";
        }
      }
      else {
        parametresLocal.couchesPlast[j].zToit = -999.;
      }
      /*
       parametresLocal.couchesPlast[j].modulePressio   = new Double(data[3][j].toString()).doubleValue();
       parametresLocal.couchesPlast[j].pressionFluage  = new Double(data[4][j].toString()).doubleValue();
       parametresLocal.couchesPlast[j].pressionLimite  = new Double(data[5][j].toString()).doubleValue();
       parametresLocal.couchesPlast[j].coefRheo        = new Double(data[6][j].toString()).doubleValue();
       parametresLocal.couchesPlast[j].coefReduc       = new Double(data[7][j].toString()).doubleValue();
       */
      tmpD = parametresLocal.couchesPlast[j].modulePressio;
      tmpS = String.valueOf(donnees_[3][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.couchesPlast[j].modulePressio = -999.;
      }
      else {
        parametresLocal.couchesPlast[j].modulePressio = Double.parseDouble(tmpS);
      }
      if (parametresLocal.couchesPlast[j].modulePressio != tmpD) {
        changed += "couchesPlast[j].modulePressio|";
      }
      //
      tmpD = parametresLocal.couchesPlast[j].pressionFluage;
      tmpS = String.valueOf(donnees_[4][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.couchesPlast[j].pressionFluage = -999.;
      }
      else {
        parametresLocal.couchesPlast[j].pressionFluage = Double.parseDouble(tmpS);
      }
      if (parametresLocal.couchesPlast[j].pressionFluage != tmpD) {
        changed += "couchesPlast[j].pressionFluage|";
      }
      //
      tmpD = parametresLocal.couchesPlast[j].pressionLimite;
      tmpS = String.valueOf(donnees_[5][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.couchesPlast[j].pressionLimite = -999.;
      }
      else {
        parametresLocal.couchesPlast[j].pressionLimite = Double.parseDouble(tmpS);
      }
      if (parametresLocal.couchesPlast[j].pressionLimite != tmpD) {
        changed += "couchesPlast[j].pressionLimite|";
      }
      //
      tmpD = parametresLocal.couchesPlast[j].coefRheo;
      tmpS = String.valueOf(donnees_[6][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.couchesPlast[j].coefRheo = -999.;
      }
      else {
        parametresLocal.couchesPlast[j].coefRheo = Double.parseDouble(tmpS);
      }
      if (parametresLocal.couchesPlast[j].coefRheo != tmpD) {
        changed += "couchesPlast[j].coefRheo|";
      }
      //
      tmpD = parametresLocal.couchesPlast[j].coefReduc;
      tmpS = String.valueOf(donnees_[7][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.couchesPlast[j].coefReduc = -999.;
      }
      else {
        parametresLocal.couchesPlast[j].coefReduc = Double.parseDouble(tmpS);
      }
      if (parametresLocal.couchesPlast[j].coefReduc != tmpD) {
        changed += "couchesPlast[j].coefReduc|";
      }
      /*
       System.out.println("getcouchesPlast [j].zToit           " +parametresLocal.couchesPlast[j].zToit);
       System.out.println("getcouchesPlast [j].modulePressio   " +parametresLocal.couchesPlast[j].modulePressio );
       System.out.println("getcouchesPlast [j].pressionFluage  " +parametresLocal.couchesPlast[j].pressionFluage);
       System.out.println("getcouchesPlast [j].pressionLimite  " +parametresLocal.couchesPlast[j].pressionLimite);
       System.out.println("getcouchesPlast [j].coefRheo        " +parametresLocal.couchesPlast[j].coefRheo);
       System.out.println("getcouchesPlast [j].coefReduc       " +parametresLocal.couchesPlast[j].coefReduc);
       */
      String chaine = "";
      chaine = String.valueOf(myModel.getValueAt(0, j));
      //if(donnees_[0][j].equals("non"))
      if (chaine.equals("non")) {
        parametresLocal.couchesPlast[j].modeSaisCourb = 0;
      }
      //if(donnees_[0][j].equals("oui"))
      if (chaine.equals("oui")) {
        parametresLocal.couchesPlast[j].modeSaisCourb = 1;
      }
      System.out.println("modeSaisCourb =" + parametresLocal.couchesPlast[j].modeSaisCourb);
    }
    //     StringTokenizer tok = new StringTokenizer(changed, "|");
    //  while( tok.hasMoreTokens() ) {
    //  FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(new FudaaParamEvent(
    //  this, 0, TaucomacResource.TAUCO, params , TaucomacResource.TAUCO+":"+tok.nextToken()));
    return parametresLocal;
  }

  //===============================================
  class MyTableModel extends AbstractTableModel {

    final String[] columnNames = { " 1  ", " 2 ", " 3 ", " 4 ", " 5 ", " 6 ", " 7 ", " 8 ", " 9 ",
        " 10 "};
    /*deniger*/
    private int nbMaxColEditable_ = columnNames.length;

    public void setNbMaxColEditable(final int _n){
      if (_n < 0) {
        nbMaxColEditable_ = 0;
      } else if (_n > columnNames.length) {
        nbMaxColEditable_ = columnNames.length;
      }
      if (_n != nbMaxColEditable_) {
        nbMaxColEditable_ = _n;
        fireTableStructureChanged();
        //System.out.println("changement");
      }
    }

    public boolean isCellEditable(final int row,final int col){
      if (col == 0 && row == 2) {
        return false;
      }
      if (row == 7) {
        if (localPhase_ == 2) {
          return true;
        }
        return false;
      }
      if ((col < 0) || (col >= nbMaxColEditable_)) {
        return false;
      }
      return true;
    }

    public int getColumnCount(){
      // System.out.println("getColumnCount");
      return nbMaxColEditable_;
    }

    public int getRowCount(){
      return donnees_.length;
    }

    public String getColumnName(final int col){
      if (col < columnNames.length) {
        return columnNames[col];
      }
      return "";
    }

    public Object getValueAt(final int row,final int col){
      // System.out.println("row"+row+" col "+ col);
      if ((row < donnees_.length) && (col < columnNames.length)) {
        return donnees_[row][col];
      }
      return "";
    }

    /*public Class getColumnClass(int c) {
     return getValueAt(0, c).getClass();
     }*/
    public boolean isNombre(final String chaine){
      final int taille = chaine.length();
      if (taille != 0) {
        int i = 0;
        boolean bool = true;
        while (i < taille
          && (chaine.charAt(i) >= '0' && chaine.charAt(i) <= '9' || chaine.charAt(i) == '-' || chaine
              .charAt(i) == '.')) {
          if (i + 1 < taille && chaine.charAt(i + 1) == '-') {
            return false;
          }
          if (chaine.charAt(i) == '.') {
            if (!bool) {
              return false;
            }
            bool = false;
          }
          i++;
        }
        if (i == taille) {
          return true;
        }
        return false;
      }
      return false;
    }

    public void setValueAt(final Object value,final int row,final int col){
      //System.out.println("le contenu avant : "+ data[row][col]);
      if (isNombre(value.toString())) {
        if (value.toString().charAt(0) == '.') {
          donnees_[row][col].setValeur("0" + value.toString());
        } else {
          donnees_[row][col].setValeur(value.toString());
          //System.out.println("Vous avez rentr� "+value+" � la case ("+row+","+col+").");
        }
      }
      else {
        donnees_[row][col].setValeur(value.toString());
      }
    }
  }

  class MyTableModel1 extends AbstractTableModel {

    final String[] columnNames = { "Couche"};

    public int getColumnCount(){
      return columnNames.length;
    }

    public int getRowCount(){
      return data1.length;
    }

    public Object getValueAt(final int row,final int col){
      return data1[row][col];
    }

    public String getColumnName(final int col){
      return columnNames[col];
    }

    public boolean isCellEditable(final int row,final int col){
      return (col != 0) ;
    }
  };

  class MyTableModel2 extends AbstractTableModel {

    final String[] columnNames = { "Unit�"};

    public int getColumnCount(){
      return columnNames.length;
    }

    public int getRowCount(){
      return data2.length;
    }

    public Object getValueAt(final int row,final int col){
      return data2[row][col];
    }

    public String getColumnName(final int col){
      return columnNames[col];
    }

    public boolean isCellEditable(final int row,final int col){
      return col>0;
    }
  };

  public void majlook(final int style,final int phase,final int unit){
    //    int style_ =   style;
    final int phase_ = phase;
    final int unit_ = unit;
    localPhase_ = phase_;
    if (unit_ == 0) {
      data2[3][0] = " t/m2";
      data2[4][0] = " t/m2";
      data2[5][0] = " t/m2";
    }
    else {
      data2[3][0] = " MPa";
      data2[4][0] = " MPa";
      data2[5][0] = " MPa";
    }
    final int il = 7;
    for (int ic = 0; ic < donneesNbColonne_; ic++) {
      if (localPhase_ > 1) {
        ((data) myModel.getValueAt(il, ic)).setCouleur(Color.cyan);
      }
      else {
        ((data) myModel.getValueAt(il, ic)).setCouleur(Color.gray);
      }
    }
    table.repaint();
  }

  public Class getColumnClass(final int c){
    return getValueAt(0, c).getClass();
  }

  public Object getValueAt(final int row,final int col){
    return donnees_[row][col];
  }

  //=============================
  //===============================
  //  pour TaucomacPressioDialog
  //===============================
  public void openPressioDialog(final int col){
    // System.out.println("OPENparamDialogcol = "+col);
    colDialog_ = col;
    final int n = parametresLocal.couchesPlast[colDialog_].nbPtCourb;
    // System.out.println("nbre de points = "+n );
    if (points == null) {
      points = new SPtInter[10];
      for (int i = 0; i < 10; i++) {
        points[i] = new SPtInter();
      }
    }
    for (int i = 0; i < n; i++) {
      points[i] = parametresLocal.couchesPlast[colDialog_].ptCourb[i];
      points[i].def = parametresLocal.couchesPlast[colDialog_].ptCourb[i].def;
      points[i].eff = parametresLocal.couchesPlast[colDialog_].ptCourb[i].eff;
      //System.out.println("Point "+(i+1)+" : def : "+ parametresLocal.couchesPlast[colDialog_].ptCourb[i].def);
      //System.out.println("      "+(i+1)+" : eff : "+ parametresLocal.couchesPlast[colDialog_].ptCourb[i].eff);
      //System.out.println("Point "+(i+1)+" : def : "+ points[i].def);
      //System.out.println("      "+(i+1)+" : eff : "+ points[i].eff);
    }
    dial.setNbrepoints(n);
    dial.setParametresPoints(points);
    dial.setVisible(true);
  } //open

  //=========================================
  // recuperation des parametres de dial
  //=========================================
  public void getparamDialog(){
    if (points == null) {
      points = new SPtInter[10];
      for (int i = 0; i < 10; i++) {
        points[i] = new SPtInter();
      }
    }
    int n = 0;
    if (parametresLocal.couchesPlast[colDialog_].ptCourb == null) {
      parametresLocal.couchesPlast[colDialog_].ptCourb = new SPtInter[10];
    }
    n = dial.getNbrepoints();
    parametresLocal.couchesPlast[colDialog_].nbPtCourb = n;
    //   System.out.println("nbre de points = "+n );
    points = dial.getParametresPoints();
    for (int i = 0; i < n; i++) {
      points[i] = dial.getParametresPoints()[i];
      points[i].eff = dial.getParametresPoints()[i].eff;
      points[i].def = dial.getParametresPoints()[i].def;
      parametresLocal.couchesPlast[colDialog_].ptCourb[i] = points[i];
      parametresLocal.couchesPlast[colDialog_].ptCourb[i].def = points[i].def;
      parametresLocal.couchesPlast[colDialog_].ptCourb[i].eff = points[i].eff;
    }
    if (n > 0) {
      for (int j = 0; j < n; j++) {
        // System.out.println("Point "+(j+1)+" : def : "+ parametresLocal.couchesPlast[colDialog_].ptCourb[j].def);
        // System.out.println("      "+(j+1)+" : eff : "+ parametresLocal.couchesPlast[colDialog_].ptCourb[j].eff);
      }
    }
  }

  public int getSelectedAmarrage(){
    for (int i = 0; i < 3; i++) {
      if (rbi_[i].isSelected()) {
        System.out.println("comport ammarage =    " + i);
      }
      if (rbi_[i].isSelected()) {
        return i;
      }
    }
    return 0;
  }

  protected void setSelectedAmarrage(final int comport){
    rbi_[comport].setSelected(true);
  }

  //=============================
  void selectAvise(final int line,final int col){
    int row_;
    int col_;
    int krep = 0;
    if (line >= 0) {
      linMem = line;
    }
    if (col >= 0) {
      colMem = col;
    }
    final javax.swing.table.TableModel model = table.getModel();
    if (ancolMem >= 0 && anclinMem >= 0) {
      if (anclinMem > 1) {
        if (((data) myModel.getValueAt(anclinMem, ancolMem)).getCouleur() == Color.white) {
          ((data) myModel.getValueAt(anclinMem, ancolMem)).setCouleur(Color.cyan);
        }
      }
    }
    if (colMem >= 0 && linMem >= 0) {
      if (linMem > 1) {
        if (((data) myModel.getValueAt(linMem, colMem)).getCouleur() != Color.gray) {
          ((data) myModel.getValueAt(linMem, colMem)).setCouleur(Color.white);
        }
      }
      String chaine = "";
      chaine = String.valueOf(model.getValueAt(linMem, colMem));
      // System.out.println("line  "+linMem+" "+" col "+colMem+" chaine " + chaine);
      row_ = linMem;
      col_ = colMem;
//      value = chaine;
      //System.out.println("row col = "+ row_+" "+col_);
      if (row_ >= 0) {
        if (col_ >= 0) {
          if (row_ == 0) {
            if (chaine.equals("non")) {
              model.setValueAt("oui", row_, col_);
              model.setValueAt("*", row_ + 1, col_);
              ((data) myModel.getValueAt(row_, col_)).setCouleur(Color.green);
              ((data) myModel.getValueAt(row_ + 1, col_)).setCouleur(Color.blue);
              krep++;
              table.repaint();
            }
            else if (chaine.equals("oui")) {
              if (krep == 0) {
                model.setValueAt("non", row_, col_);
                model.setValueAt("", row_ + 1, col_);
                ((data) myModel.getValueAt(row_, col_)).setCouleur(Color.red);
                ((data) myModel.getValueAt(row_ + 1, col_)).setCouleur(Color.gray);
                table.repaint();
              }
            }
          } //row
          krep = 0;
          if (row_ == 1) {
            if (chaine.equals("*")) {
              //ouverture de TaucomacPressioDialog
              openPressioDialog(col_);
            }
          } //row
        } //col
      } //row
      table.repaint();
      anclinMem = linMem;
      ancolMem = colMem;
    } //colMem
  }

  //===========================
  public void lookInit(){
    for (int il = 0; il < donneesNbLigne_; il++) {
      for (int ic = 0; ic < donneesNbColonne_; ic++) {
        if (il == 0) {
          myModel.setValueAt("non", il, ic);
          ((data) myModel.getValueAt(il, ic)).setCouleur(Color.red);
          ((data) myModel.getValueAt(il + 1, ic)).setCouleur(Color.gray);
        }
        if (localPhase_ == 0) {
          if (il == 7) {
            ((data) myModel.getValueAt(il, ic)).setCouleur(Color.gray);
          }
        }
      }
    }
    ((data) myModel.getValueAt(2, 0)).setCouleur(Color.gray);
    myModel.fireTableStructureChanged();
    table.repaint();
  }
  //========================
} //class
