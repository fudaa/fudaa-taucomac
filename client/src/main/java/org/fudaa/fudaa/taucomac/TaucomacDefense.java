/*
 * @file         TaucomacDefense.java
 * @creation     2000-10-17
 * @modification $Date: 2006-09-19 15:08:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.taucomac;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.taucomac.SDefense;
import org.fudaa.dodico.corba.taucomac.SDefenses;
import org.fudaa.dodico.corba.taucomac.SPtInter;
/**
 * Onglet Defenses
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:08:56 $ by $Author: deniger $
 * @author       Jean-Yves Riou
 */
public class TaucomacDefense
  extends TaucomacMerePanParm
  implements ActionListener, FocusListener {
  SDefenses parametresLocal= null;
  /*
   struct SDefense 								 // defenses
   {
   reel 			hauteur;
   reel 			zsup;
   reel          zinf;
   entier        nbPtCourb;
   VSPtInters 	ptCourb;
   };

   typedef sequence<SDefense> VSDefenses;        //  liste des defenses

     struct SDefenses
   {
   	entier			nbDefenses;			// defenses
     reel            valCarBas;
     reel            valCarHaut;
     reel            valCalBas;
     reel            valCalHaut;
     reel            valAcBas;
     reel            valAcHaut;
     VSDefenses      defenses;
   };
   SDefenses           defens;
   */
  static protected TaucomacPressioDialog dial;
  final BuCommonInterface appli_;
  protected boolean blocus1= true;
  SPtInter points[]= null;
  public int colDialog_= -1;
  // indice colonne qui ouvre la TaucomacPressioDialog
  public int selectCol= -9;
  public int selectLin= -9;
  public int linMem= -9;
  public int colMem= -9;
  public int anclinMem= -9;
  public int ancolMem= -9;
  private final boolean DEBUG= true;
  private final boolean ALLOW_COLUMN_SELECTION= true;
  private final boolean ALLOW_ROW_SELECTION= true;
  data[][] donnees_;
  private int donneesNbColonne_;
  private int donneesNbLigne_;
  JPanel pnHaut;
  JPanel pnMil;
  JPanel pnMilG;
  JPanel pnMilC;
  JPanel pnMilD;
  JPanel pnBasD;
  JPanel pnBas;
  JLabel la_nbDefenses;
  BuTextField tf_nbDefenses;
  JLabel titreBas;
  JLabel la_bid1;
  JLabel la_bid2;
  JLabel la_bid3;
  JLabel la_bas;
  JLabel la_haut;
  JLabel la_valcar;
  JLabel la_valcal;
  JLabel la_valacc;
  BuTextField tf_valcarbas;
  BuTextField tf_valcalbas;
  BuTextField tf_valaccbas;
  BuTextField tf_valcarhaut;
  BuTextField tf_valcalhaut;
  BuTextField tf_valacchaut;
  public int localPhase_;
  public int krep= 1;
  public int krep1= 0;
  MyTableModel myModel= new MyTableModel();
  JTable table;
  public int nbMaxDefenses_= 5;
  public TaucomacCellEditor[] anEditor;
  //=========
  final Object[][] data1= { { "Entree  de la courbe effort-deformation" }, {
      "Nombre de points de la courbe " }, {
      "Hauteur de la defense" }, {
      "Cote inferieure e la defense" }, {
      "Cote superieure de la defense" }
  };
  MyTableModel1 myModel1= new MyTableModel1();
  JTable table1= new JTable(myModel1);
  public TaucomacCellEditor anEditor21= new TaucomacCellEditor();
  //=========
  final Object[][] data2= { { "" }, {
      "" }, {
      " mm " }, {
      " m " }, {
      " m " }
  };
  MyTableModel2 myModel2= new MyTableModel2();
  JTable table2= new JTable(myModel2);
  public TaucomacCellEditor anEditor22= new TaucomacCellEditor();
  //==================
  class data {
    Color couleur_= null;
    String valeur_;
    public data(final String valeur) {
      valeur_= valeur;
    }
    public String toString() {
      return valeur_;
    }
    public void setCouleur(final Color _c) {
      couleur_= _c;
    }
    public Color getCouleur() {
      return couleur_;
    }
    public void setValeur(final String _valeur) {
      valeur_= _valeur;
    }
  };
  //===============
  class ColorRenderer extends JLabel implements TableCellRenderer {
    Border unselectedBorder= null;
    Border selectedBorder= null;
    boolean isBordered= true;
    Color defautCouleur_= Color.blue;
    public ColorRenderer(
      final boolean _isBordered,
      final boolean isOpaque,
      final Color _couleurParDefaut) {
      super();
      if (_couleurParDefaut != null) {
        defautCouleur_= _couleurParDefaut;
      }
      this.isBordered= _isBordered;
      this.setOpaque(isOpaque);
      this.setHorizontalAlignment(RIGHT);
    }
    public Component getTableCellRendererComponent(
      final JTable _table,
      final Object value,
      final boolean isSelected,
      final boolean hasFocus,
      final int row,
      final int column) {
      setText("" + value);
      final Color CouleurSpecifique= ((data)value).getCouleur();
      if (CouleurSpecifique != null) {
        this.setBackground(CouleurSpecifique);
      } else {
        this.setBackground(defautCouleur_);
      }
      if (isBordered) {
        if (isSelected) {
          if (selectedBorder == null) {
            selectedBorder=
              BorderFactory.createMatteBorder(
                2,
                5,
                2,
                5,
                _table.getSelectionBackground());
          }
          this.setBorder(selectedBorder);
        } else {
          if (unselectedBorder == null) {
            unselectedBorder=
              BorderFactory.createMatteBorder(
                2,
                5,
                2,
                5,
                _table.getBackground());
          }
          this.setBorder(unselectedBorder);
        }
      }
      return this;
    }
  };
  //====================================
  //    Constructeur
  //====================================
  public TaucomacDefense(final BuCommonInterface _appli) {
    super();
    appli_=_appli;
    //====================================
    donneesNbColonne_= 5;
    donneesNbLigne_= 5;
    initialiseData(donneesNbLigne_, donneesNbColonne_);
    //====
    table= new JTable(myModel) {
      ColorRenderer cr= new ColorRenderer(false, true, Color.cyan);
      public TableCellRenderer getCellRenderer(final int row, final int column) {
        return cr;
      }
    };
    //=====
    pnHaut= new JPanel();
    final BuGridLayout loCal2= new BuGridLayout();
    loCal2.setColumns(2);
    loCal2.setHgap(5);
    loCal2.setVgap(10);
    loCal2.setHfilled(false);
    loCal2.setCfilled(true);
    pnHaut.setLayout(loCal2);
    pnHaut.setBorder(new EmptyBorder(30, 60, 20, 440)); //top,left,bottom,right
    //pnHaut.setPreferredSize(new Dimension(650,120));
    la_nbDefenses= new JLabel(" Nombre de d�fenses differentes ", SwingConstants.RIGHT);
    tf_nbDefenses= BuTextField.createIntegerField();
    tf_nbDefenses.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_nbDefenses.addFocusListener(this);
    tf_nbDefenses.setColumns(4);
    int n= 0;
    n= 0;
    placeCompn(pnHaut, la_nbDefenses, 0, n);
    n++;
    placeCompn(pnHaut, tf_nbDefenses, 0, n);
    //=====================================
    pnBasD= new JPanel();
    final BuGridLayout loCal7= new BuGridLayout();
    loCal7.setColumns(3);
    loCal7.setHgap(5);
    loCal7.setVgap(10);
    loCal7.setHfilled(false);
    loCal7.setCfilled(true);
    pnBasD.setLayout(loCal7);
    pnBasD.setBorder(new EmptyBorder(40, 20, 10, 210)); //top,left,bottom,right
    titreBas=
      new JLabel("Coefficients partiels sur la force appliqu�e par la defense");
    la_bid1= new JLabel("  ", SwingConstants.RIGHT);
    la_bid2= new JLabel("  ", SwingConstants.RIGHT);
    la_bid3= new JLabel("  ", SwingConstants.RIGHT);
    la_bas= new JLabel(" bas ", SwingConstants.CENTER);
    la_haut= new JLabel(" haut ", SwingConstants.CENTER);
    la_valcar= new JLabel("Valeur caracteristique", SwingConstants.RIGHT);
    la_valcal= new JLabel("      Valeur de calcul", SwingConstants.RIGHT);
    la_valacc= new JLabel("   Valeur accidentelle", SwingConstants.RIGHT);
    tf_valcarbas= BuTextField.createDoubleField();
    tf_valcarbas.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_valcarbas.setColumns(6);
    tf_valcalbas= BuTextField.createDoubleField();
    tf_valcalbas.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_valcalbas.setColumns(6);
    tf_valaccbas= BuTextField.createDoubleField();
    tf_valaccbas.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_valaccbas.setColumns(6);
    tf_valcarhaut= BuTextField.createDoubleField();
    tf_valcarhaut.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_valcarhaut.setColumns(6);
    tf_valcalhaut= BuTextField.createDoubleField();
    tf_valcalhaut.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_valcalhaut.setColumns(6);
    tf_valacchaut= BuTextField.createDoubleField();
    tf_valacchaut.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_valacchaut.setColumns(6);
    n= 0;
    placeCompn(pnBasD, titreBas, 2, n);
    n++;
    placeCompn(pnBasD, la_bid1, 2, n);
    n++;
    placeCompn(pnBasD, la_bid2, 2, n);
    n++;
    placeCompn(pnBasD, la_bid3, 2, n);
    n++;
    placeCompn(pnBasD, la_bas, 2, n);
    n++;
    placeCompn(pnBasD, la_haut, 2, n);
    n++;
    placeCompn(pnBasD, la_valcar, 2, n);
    n++;
    placeCompn(pnBasD, tf_valcarbas, 2, n);
    n++;
    placeCompn(pnBasD, tf_valcarhaut, 2, n);
    n++;
    placeCompn(pnBasD, la_valcal, 2, n);
    n++;
    placeCompn(pnBasD, tf_valcalbas, 2, n);
    n++;
    placeCompn(pnBasD, tf_valcalhaut, 2, n);
    n++;
    placeCompn(pnBasD, la_valacc, 2, n);
    n++;
    placeCompn(pnBasD, tf_valaccbas, 2, n);
    n++;
    placeCompn(pnBasD, tf_valacchaut, 2, n);
    //=================
    pnBas= new JPanel();
    pnBas.setBorder(new TitledBorder(""));
    pnBas.setLayout(new BorderLayout());
    pnBas.setBorder(new EmptyBorder(10, 20, 10, 20)); //top,left,bottom,right
    // pnBas.add(pnBasG,BorderLayout.WEST);
    pnBas.add(pnBasD, BorderLayout.CENTER);
    //======================
    pnMil= new JPanel();
    pnMil.setBorder(new TitledBorder(""));
    pnMil.setLayout(new BorderLayout());
    pnMil.setBorder(new EmptyBorder(20, 20, 10, 20)); //top,left,bottom,right
    // pnMil.setPreferredSize(new Dimension(700,250));
    pnMilC= new JPanel();
    // pnMilC.setPreferredSize(new Dimension(500,80 ));
    pnMilG= new JPanel();
    pnMilD= new JPanel();
    //===================
    //    Tables
    //===================
    table1.setBackground(Color.orange);
    table1.setPreferredScrollableViewportSize(new Dimension(230, 90));
    table1.getColumn(table1.getColumnName(0)).setCellEditor(anEditor21);
    final JScrollPane scrollPane1= new JScrollPane(table1);
    (table1.getTableHeader()).setReorderingAllowed(false);
    table1.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    (table1.getColumn(table1.getColumnName(0))).setPreferredWidth(230);
    table2.setBackground(Color.orange);
    table2.setPreferredScrollableViewportSize(new Dimension(75, 90));
    table2.getColumn(table2.getColumnName(0)).setCellEditor(anEditor22);
    final JScrollPane scrollPane2= new JScrollPane(table2);
    (table2.getTableHeader()).setReorderingAllowed(false);
    table2.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    //
    table.setColumnSelectionAllowed(false);
    table.setRowSelectionAllowed(false);
    table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    table.setCellSelectionEnabled(true);
    table.setBackground(Color.cyan);
    table.setForeground(Color.black);
    table.setSelectionBackground(Color.white);
    table.setSelectionForeground(Color.blue);
    table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    anEditor= new TaucomacCellEditor[nbMaxDefenses_];
    TableColumn kolw= null;
    for (int i= 0; i < nbMaxDefenses_; i++) {
      anEditor[i]= new TaucomacCellEditor();
      kolw= table.getColumnModel().getColumn(i);
      kolw.setPreferredWidth(80);
      kolw.setCellEditor(anEditor[i]);
    }
    lookInit();
    //
    if (ALLOW_ROW_SELECTION) { // true by default
      final ListSelectionModel rowSM= table.getSelectionModel();
      rowSM.addListSelectionListener(new ListSelectionListener() {
        public void valueChanged(final ListSelectionEvent e) {
          //Ignore extra messages.
          if (e.getValueIsAdjusting()) {
            return;
          }
          final ListSelectionModel lsm= (ListSelectionModel)e.getSource();
          if (lsm.isSelectionEmpty()) {
            //  System.out.println("No rows are selected.");
            selectLin= -9;
          } else {
            final int selectedRow= lsm.getMinSelectionIndex();
            //System.out.println("Row " + selectedRow
            //                  + " is now selected.");
            selectLin= selectedRow;
          }
        }
      });
    } else {
      table.setRowSelectionAllowed(false);
    }
    //
    if (ALLOW_COLUMN_SELECTION) { // false by default
      if (ALLOW_ROW_SELECTION) {
        //We allow both row and column selection, which
        //implies that we *really* want to allow individual
        //cell selection.
        table.setCellSelectionEnabled(true);
      }
      table.setColumnSelectionAllowed(true);
      final ListSelectionModel colSM= table.getColumnModel().getSelectionModel();
      colSM.addListSelectionListener(new ListSelectionListener() {
        public void valueChanged(final ListSelectionEvent e) {
          //Ignore extra messages.
          if (e.getValueIsAdjusting()) {
            return;
          }
          final ListSelectionModel lsm= (ListSelectionModel)e.getSource();
          if (lsm.isSelectionEmpty()) {
            // System.out.println("No columns are selected.");
            selectCol= -9;
          } else {
            final int selectedCol= lsm.getMinSelectionIndex();
            // System.out.println("Column " + selectedCol
            //                    + " is now selected.");
            selectCol= selectedCol;
          }
        }
      });
    }
    if (DEBUG) {
      table.addMouseListener(new MouseAdapter() {
        public void mouseClicked(final MouseEvent e) {
          selectAvise(selectLin, selectCol);
        }
      });
    }
    //
    table.setPreferredScrollableViewportSize(new Dimension(390, 90));
    //table.setCellSelectionEnabled(true);//   a mieux tester
    // rend col et ligne selctionnables
    /* initialisation du tableau*/
    anEditor= new TaucomacCellEditor[nbMaxDefenses_];
    for (int i= 0; i < nbMaxDefenses_; i++) {
      anEditor[i]= new TaucomacCellEditor();
      table.getColumn(table.getColumnName(i)).setCellEditor(anEditor[i]);
    }
    //table.setRowSelectionInterval(0,0);
    //table.setColumnSelectionInterval(0,5);
    final JScrollPane scrollPane= new JScrollPane(table);
    (table.getTableHeader()).setReorderingAllowed(false);
    table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    //On regle la taille des colonnes
    //(table.getColumn(table.getColumnName(0))).setPreferredWidth(120);
    pnMilC.add(scrollPane);
    pnMilG.add(scrollPane1);
    pnMilD.add(scrollPane2);
    pnMil.add(pnMilC, BorderLayout.CENTER);
    pnMil.add(pnMilG, BorderLayout.WEST);
    pnMil.add(pnMilD, BorderLayout.EAST);
    //====================================
    this.setLayout(new BorderLayout());
    this.setBorder(BorderFactory.createEmptyBorder(10, 5, 10, 5));
    this.add(pnHaut, BorderLayout.NORTH);
    this.add(pnMil, BorderLayout.CENTER);
    this.add(pnBas, BorderLayout.SOUTH);
    //
    dial= new TaucomacPressioDialog(appli_, this);
    //initialisation des parametres locaux
    setParametres(parametresLocal);
  }
  //==============
  private  void initialiseData(final int _nbLigne, final int _nbCol) {
    donnees_= new data[_nbLigne][_nbCol];
    for (int i= 0; i < _nbLigne; i++) {
      for (int j= 0; j < _nbCol; j++) {
        donnees_[i][j]= new data("");
      }
    }
  }
  //
  public void focusLost(final FocusEvent e) {
    final Object src= e.getSource();
    // System.out.println("FocusLost " + e );
    //System.out.println("pressio focusLost  "+e + " "+src );
    if (src instanceof JTextField) {
      if (src == tf_nbDefenses) {
        //System.out.println("focusLost source " + src);
        String tmpS= "";
        final String blanc= "";
        int n= 0;
        tmpS= tf_nbDefenses.getText();
        if (tmpS.equals(blanc)) {
          n= 0;
        } else {
          n= Integer.parseInt(tmpS);
        }
        //System.out.println("n " + n);
        myModel.setNbMaxColEditable(n);
        table.repaint();
      }
    }
  }
  public void focusGained(final FocusEvent e) {
    // System.out.println("FocusGained " + e );
  }
  public void actionPerformed(final ActionEvent e) {
    //System.out.println("actionPerformed " + e );
    parametresLocal= getParametres();
    setVisible(true);
    //      Object src=e.getSource();
    //   System.out.println("evenement " + e +" source "+src);
  }
  public void setParametres(final SDefenses parametresProjet_) {
    //    System.out.println(" ===== Defensesset Parametres =====");
    parametresLocal= parametresProjet_;
    if (parametresLocal != null) {
      myModel.setNbMaxColEditable(parametresLocal.nbDefenses);
      if (parametresLocal.nbDefenses > 0) {
        tf_nbDefenses.setValue(new Integer(parametresLocal.nbDefenses));
      } else {
        tf_nbDefenses.setValue("");
      }
      if (parametresLocal.valCarBas >= 0) {
        tf_valcarbas.setValue(new Double(parametresLocal.valCarBas));
      } else {
        tf_valcarbas.setValue("");
      }
      if (parametresLocal.valCarHaut >= 0) {
        tf_valcarhaut.setValue(new Double(parametresLocal.valCarHaut));
      } else {
        tf_valcarhaut.setValue("");
      }
      if (parametresLocal.valCalBas >= 0) {
        tf_valcalbas.setValue(new Double(parametresLocal.valCalBas));
      } else {
        tf_valcalbas.setValue("");
      }
      if (parametresLocal.valCalHaut >= 0) {
        tf_valcalhaut.setValue(new Double(parametresLocal.valCalHaut));
      } else {
        tf_valcalhaut.setValue("");
      }
      if (parametresLocal.valAcBas >= 0) {
        tf_valaccbas.setValue(new Double(parametresLocal.valAcBas));
      } else {
        tf_valaccbas.setValue("");
      }
      if (parametresLocal.valAcHaut >= 0) {
        tf_valacchaut.setValue(new Double(parametresLocal.valAcHaut));
      } else {
        tf_valacchaut.setValue("");
      }
      if (parametresLocal.nbDefenses > 0) {
        for (int j= 0; j < parametresLocal.nbDefenses; j++) {
          System.out.println(
            "set  nbPtCourb  = " + parametresLocal.defenses[j].nbPtCourb);
          if (parametresLocal.defenses[j].nbPtCourb > 0) {
            table.setValueAt(
              new Integer(parametresLocal.defenses[j].nbPtCourb),
              1,
              j);
          } else {
            table.setValueAt("", 1, j);
          }
          if (parametresLocal.defenses[j].hauteur != -999.) {
            table.setValueAt(
              new Double(parametresLocal.defenses[j].hauteur),
              2,
              j);
          } else {
            table.setValueAt("", 2, j);
          }
          if (parametresLocal.defenses[j].zsup != -999.) {
            table.setValueAt(
              new Double(parametresLocal.defenses[j].zinf),
              3,
              j);
          } else {
            table.setValueAt("", 3, j);
          }
          if (parametresLocal.defenses[j].zinf != -999.) {
            table.setValueAt(
              new Double(parametresLocal.defenses[j].zsup),
              4,
              j);
          } else {
            table.setValueAt("", 4, j);
          }
          //System.out.println("set donnees_[4][j]"+donnees_[4][j] +" zsup "+parametresLocal.defenses[j].zsup);
          final int n= parametresLocal.defenses[j].nbPtCourb;
          //System.out.println("*** setParametres nbre de points = "+n );
          for (int i= 0; i < n; i++) {
            parametresLocal.defenses[j].ptCourb[i]=
              parametresProjet_.defenses[j].ptCourb[i];
            parametresLocal.defenses[j].ptCourb[i].eff=
              parametresProjet_.defenses[j].ptCourb[i].eff;
            parametresLocal.defenses[j].ptCourb[i].def=
              parametresProjet_.defenses[j].ptCourb[i].def;
            //System.out.println("Point "+(i+1)+" : def : "+ parametresLocal.defenses[j].ptCourb[i].def);
            //System.out.println("      "+(i+1)+" : eff : "+ parametresLocal.defenses[j].ptCourb[i].eff);
          }
        }
      }
    }
  }
  public SDefenses getParametres() {
    //   System.out.println(" DefensesgetParametres");
    if (parametresLocal == null) {
      parametresLocal= new SDefenses();
      parametresLocal.defenses= new SDefense[nbMaxDefenses_];
      // System.out.println(" parametresLocal = "+parametresLocal);
      for (int i= 0; i < nbMaxDefenses_; i++) {
        parametresLocal.defenses[i]= new SDefense();
      }
    }
    String changed= "";
    double tmpD= 0.;
    int tmpI= 0;
    String tmpS= "";
    final String blanc= "";
    tmpI= parametresLocal.nbDefenses;
    tmpS= tf_nbDefenses.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.nbDefenses= 0;
    } else {
      parametresLocal.nbDefenses= Integer.parseInt(tmpS);
    }
    if (parametresLocal.nbDefenses != tmpI) {
      changed += "nbDefenses|";
    }
    tmpD= parametresLocal.valCarBas;
    tmpS= tf_valcarbas.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.valCarBas= -999.;
    } else {
      parametresLocal.valCarBas= Double.parseDouble(tmpS);
    }
    if (parametresLocal.valCarBas != tmpD) {
      changed += " valCarBas|";
    }
    tmpD= parametresLocal.valCarHaut;
    tmpS= tf_valcarhaut.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.valCarHaut= -999.;
    } else {
      parametresLocal.valCarHaut= Double.parseDouble(tmpS);
    }
    if (parametresLocal.valCarHaut != tmpD) {
      changed += " valCarHaut|";
    }
    tmpD= parametresLocal.valCalBas;
    tmpS= tf_valcalbas.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.valCalBas= -999.;
    } else {
      parametresLocal.valCalBas= Double.parseDouble(tmpS);
    }
    if (parametresLocal.valCalBas != tmpD) {
      changed += "valCalBas|";
    }
    tmpD= parametresLocal.valCalHaut;
    tmpS= tf_valcalhaut.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.valCalHaut= -999.;
    } else {
      parametresLocal.valCalHaut= Double.parseDouble(tmpS);
    }
    if (parametresLocal.valCalHaut != tmpD) {
      changed += "valCalHaut|";
    }
    tmpD= parametresLocal.valAcBas;
    tmpS= tf_valaccbas.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.valAcBas= -999.;
    } else {
      parametresLocal.valAcBas= Double.parseDouble(tmpS);
    }
    if (parametresLocal.valAcBas != tmpD) {
      changed += "valAcBas|";
    }
    tmpD= parametresLocal.valAcHaut;
    tmpS= tf_valacchaut.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.valAcHaut= -999.;
    } else {
      parametresLocal.valAcHaut= Double.parseDouble(tmpS);
    }
    if (parametresLocal.valAcHaut != tmpD) {
      changed += "valAcHaut|";
    }
    // Remplissage des structures:
    for (int j= 0; j < parametresLocal.nbDefenses; j++) {
      //    System.out.println("========= getdefenses [j]. j =  "  +j+" =======");
      if (parametresLocal.defenses[j] == null) {
        parametresLocal.defenses[j]= new SDefense();
      }
      //System.out.println(" parametresLocal.defenses[j]= "+parametresLocal.defenses[j]);
      tmpI= parametresLocal.defenses[j].nbPtCourb;
      tmpS= String.valueOf(donnees_[1][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.defenses[j].nbPtCourb= 0;
      } else {
        parametresLocal.defenses[j].nbPtCourb= Integer.parseInt(tmpS);
      }
      if (parametresLocal.defenses[j].nbPtCourb != tmpI) {
        changed += "defenses[j].nbPtCourb|";
      }
      //System.out.println("get nbPtCourb  = " + parametresLocal.defenses[j].nbPtCourb);
      tmpD= parametresLocal.defenses[j].hauteur;
      tmpS= String.valueOf(donnees_[2][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.defenses[j].hauteur= -999.;
      } else {
        parametresLocal.defenses[j].hauteur= Double.parseDouble(tmpS);
      }
      if (parametresLocal.defenses[j].hauteur != tmpD) {
        changed += "defenses[j].hauteur|";
      }
      tmpD= parametresLocal.defenses[j].zinf;
      tmpS= String.valueOf(donnees_[3][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.defenses[j].zinf= -999.;
      } else {
        parametresLocal.defenses[j].zinf= Double.parseDouble(tmpS);
      }
      if (parametresLocal.defenses[j].zinf != tmpD) {
        changed += "defenses[j].zinf|";
      }
      //System.out.println("get donnees_[4][j]"+donnees_[4][j] +" zsup "+parametresLocal.defenses[j].zsup);
      tmpD= parametresLocal.defenses[j].zsup;
      tmpS= String.valueOf(donnees_[4][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.defenses[j].zsup= -999.;
      } else {
        parametresLocal.defenses[j].zsup= Double.parseDouble(tmpS);
      }
      if (parametresLocal.defenses[j].zsup != tmpD) {
        changed += "defenses[j].zsup|";
      }
    }
    //      StringTokenizer tok = new StringTokenizer(changed, "|");
    //  while( tok.hasMoreTokens() ) {
    //  FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(new FudaaParamEvent(
    //  this, 0, TaucomacResource.TAUCO, params , TaucomacResource.TAUCO+":"+tok.nextToken()));
    return parametresLocal;
  }
  //===============================================
  class MyTableModel extends AbstractTableModel {
    final String[] columnNames= { " 1  ", " 2 ", " 3 ", " 4 ", " 5 " };
    /*deniger*/
    private int nbMaxColEditable_= columnNames.length;
    public void setNbMaxColEditable(final int _n) {
      if (_n < 0) {
        nbMaxColEditable_= 0;
      } else if (_n > columnNames.length) {
        nbMaxColEditable_= columnNames.length;
      }
      if (_n != nbMaxColEditable_) {
        nbMaxColEditable_= _n;
        fireTableStructureChanged();
        //System.out.println("changement");
      }
    }
    public boolean isCellEditable(final int row, final int col) {
      if (row == 0 || row == 1) {
        return false;
      }
      if (row == 3 || row == 4) {
        return (localPhase_ > 0) ;
      }
      return ((col >=0) && (col < nbMaxColEditable_)) ;
    }
    public int getColumnCount() {
      // System.out.println("getColumnCount");
      return nbMaxColEditable_;
    }
    public int getRowCount() {
      return donnees_.length;
    }
    public String getColumnName(final int col) {
      if (col < columnNames.length) {
        return columnNames[col];
      }
      return "";
    }
    public Object getValueAt(final int row, final int col) {
      if ((row < donnees_.length) && (col < columnNames.length)) {
        return donnees_[row][col];
      }
        return "";
    }
    /*deniger fin*/
    /*public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }*/
    public boolean isNombre(final String chaine) {
      final int taille= chaine.length();
      if (taille != 0) {
        int i= 0;
        boolean bool= true;
        while (i < taille
          && (chaine.charAt(i) >= '0'
            && chaine.charAt(i) <= '9'
            || chaine.charAt(i) == '-'
            || chaine.charAt(i) == '.')) {
          if (i + 1 < taille && chaine.charAt(i + 1) == '-') {
            return false;
          }
          if (chaine.charAt(i) == '.') {
            if (!bool) {
              return false;
            }
            bool= false;
          }
          i++;
        }
        return (i == taille);
      }
      return false;
    }
    public void setValueAt(final Object value, final int row, final int col) {
      // System.out.println("le contenu avant : "+ donnees_[row][col]);
      if (isNombre(value.toString())) {
        if (value.toString().charAt(0) == '.') {
          donnees_[row][col].setValeur("0" + value.toString());
        } else {
          donnees_[row][col].setValeur(value.toString());
          //System.out.println("Vous avez rentr� "+value+" � la case ("+row+","+col+").");
        }
      } else {
        //System.out.println("Vous avez cliqu� "+value+" � la case (" +row+ "," +col+ ").");
        //System.out.println("dont le contenu est : "+ data[row][col]);
        donnees_[row][col].setValeur(value.toString());
      }
    }
  }
  class MyTableModel1 extends AbstractTableModel {
    final String[] columnNames= { "Defense" };
    public int getColumnCount() {
      return columnNames.length;
    }
    public int getRowCount() {
      return data1.length;
    }
    public Object getValueAt(final int row, final int col) {
      return data1[row][col];
    }
    public String getColumnName(final int col) {
      return columnNames[col];
    }
    public boolean isCellEditable(final int row, final int col) {
      return col!=0;
    }
  };
  class MyTableModel2 extends AbstractTableModel {
    final String[] columnNames= { "Unit�" };
    public int getColumnCount() {
      return columnNames.length;
    }
    public int getRowCount() {
      return data2.length;
    }
    public Object getValueAt(final int row, final int col) {
      return data2[row][col];
    }
    public String getColumnName(final int col) {
      return columnNames[col];
    }
    public boolean isCellEditable(final int row, final int col) {
      return col!=0;
    }
  };
  public void majlook(final int style, final int phase, final int unit) {
    //    int style_ =   style;
    final int phase_= phase;
    //   int unit_ = unit;
    localPhase_= phase_;
    if (localPhase_ > 0) {
      for (int i= 0; i < donneesNbColonne_; i++) {
        ((data)myModel.getValueAt(3, i)).setCouleur(Color.cyan);
        ((data)myModel.getValueAt(4, i)).setCouleur(Color.cyan);
      }
    } else {
      for (int i= 0; i < donneesNbColonne_; i++) {
        ((data)myModel.getValueAt(3, i)).setCouleur(Color.gray);
        ((data)myModel.getValueAt(4, i)).setCouleur(Color.gray);
      }
    }
  }
  public Class getColumnClass(final int c) {
    return getValueAt(0, c).getClass();
  }
  public Object getValueAt(final int row, final int col) {
    return donnees_[row][col];
  }
  //===============================
  //  pour TaucomacDefenseDialog
  //===============================
  public void openDefenseDialog(final int col) {
    System.out.println("OPENparamDialogcol = " + col);
    colDialog_= col;
    final int n= parametresLocal.defenses[colDialog_].nbPtCourb;
    // System.out.println("nbre de points = "+n );
    if (points == null) {
      points= new SPtInter[10];
      for (int i= 0; i < 10; i++) {
        points[i]= new SPtInter();
      }
    }
    for (int i= 0; i < n; i++) {
      points[i]= parametresLocal.defenses[colDialog_].ptCourb[i];
      points[i].def= parametresLocal.defenses[colDialog_].ptCourb[i].def;
      points[i].eff= parametresLocal.defenses[colDialog_].ptCourb[i].eff;
      //System.out.println("Point "+(i+1)+" : def : "+ parametresLocal.defenses[colDialog_].ptCourb[i].def);
      //System.out.println("      "+(i+1)+" : eff : "+ parametresLocal.defenses[colDialog_].ptCourb[i].eff);
      //System.out.println("Point "+(i+1)+" : def : "+ points[i].def);
      //System.out.println("      "+(i+1)+" : eff : "+ points[i].eff);
    }
    dial.setNbrepoints(n);
    dial.setParametresPoints(points);
    dial.setVisible(true);
  } //open
  //=========================================
  // recuperation des parametres de dial
  //=========================================
  public void getparamDialog() {
    if (points == null) {
      points= new SPtInter[10];
      for (int i= 0; i < 10; i++) {
        points[i]= new SPtInter();
      }
    }
    int n= 0;
    if (parametresLocal.defenses[colDialog_].ptCourb == null) {
      parametresLocal.defenses[colDialog_].ptCourb= new SPtInter[10];
    }
    n= dial.getNbrepoints();
    parametresLocal.defenses[colDialog_].nbPtCourb= n;
    //   System.out.println("nbre de points = "+n );
    points= dial.getParametresPoints();
    for (int i= 0; i < n; i++) {
      points[i]= dial.getParametresPoints()[i];
      points[i].eff= dial.getParametresPoints()[i].eff;
      points[i].def= dial.getParametresPoints()[i].def;
      parametresLocal.defenses[colDialog_].ptCourb[i]= points[i];
      parametresLocal.defenses[colDialog_].ptCourb[i].def= points[i].def;
      parametresLocal.defenses[colDialog_].ptCourb[i].eff= points[i].eff;
    }
    if (n > 0) {
      for (int j= 0; j < n; j++) {
        // System.out.println("Point "+(j+1)+" : def : "+ parametresLocal.defenses[colDialog_].ptCourb[j].def);
        // System.out.println("      "+(j+1)+" : eff : "+ parametresLocal.defenses[colDialog_].ptCourb[j].eff);
      }
    }
    myModel.setValueAt(String.valueOf(n), 1, colDialog_);
  }
  void selectAvise(final int line, final int col) {
    int row_;
    int col_;
    if (line >= 0) {
      linMem= line;
    }
    if (col >= 0) {
      colMem= col;
    }
    if (anclinMem > 1) {
      if (((data)myModel.getValueAt(anclinMem, ancolMem)).getCouleur()
        == Color.white) {
        ((data)myModel.getValueAt(anclinMem, ancolMem)).setCouleur(Color.cyan);
      }
    }
    final javax.swing.table.TableModel model= table.getModel();
    if (colMem >= 0 && linMem >= 0) {
      if (linMem > 1) {
        if (((data)myModel.getValueAt(linMem, colMem)).getCouleur()
          != Color.gray) {
          ((data)myModel.getValueAt(linMem, colMem)).setCouleur(Color.white);
        }
      }
  //    String chaine= "";
      /*chaine= */String.valueOf(model.getValueAt(linMem, colMem));
      // System.out.println("line  "+linMem+" "+" col "+colMem+" chaine " + chaine);
      row_= linMem;
      col_= colMem;
//      value= chaine;
      //System.out.println("row col = "+ row_+" "+col_);
      if (row_ == 0) {
        //ouverture de TaucomacPressioDialog
        openDefenseDialog(col_);
        krep= -1;
      } //row
      table.repaint();
      anclinMem= linMem;
      ancolMem= colMem;
    } //colMem
  }
  //===========================
  public void lookInit() {
    for (int ic= 0; ic < donneesNbColonne_; ic++) {
      ((data)myModel.getValueAt(1, ic)).setCouleur(Color.cyan);
      ((data)myModel.getValueAt(2, ic)).setCouleur(Color.cyan);
      myModel.setValueAt("*", 0, ic);
      ((data)myModel.getValueAt(0, ic)).setCouleur(Color.blue);
      if (localPhase_ > 0) {
        ((data)myModel.getValueAt(3, ic)).setCouleur(Color.cyan);
        ((data)myModel.getValueAt(4, ic)).setCouleur(Color.cyan);
      } else {
        ((data)myModel.getValueAt(3, ic)).setCouleur(Color.gray);
        ((data)myModel.getValueAt(4, ic)).setCouleur(Color.gray);
      }
    }
    myModel.fireTableStructureChanged();
    table.repaint();
  }
  //========================
} //class
