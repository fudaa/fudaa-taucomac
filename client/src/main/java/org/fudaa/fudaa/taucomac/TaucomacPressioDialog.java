/*
* @file         TaucomacPressioDailog.java
* @creation     2000-10-17
* @modification $Date: 2006-09-19 15:08:56 $
* @license      GNU General Public License 2
* @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail         devel@fudaa.org
*/
package org.fudaa.fudaa.taucomac;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.WindowConstants;
import javax.swing.table.AbstractTableModel;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogConfirmation;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuLabel;

import org.fudaa.dodico.corba.taucomac.SPtInter;
/**
 * Fenetre d'affichage d'un tableau �ditable avec controle des abscisses rentr�es.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:08:56 $ by $Author: deniger $
 * @author       Jean-Yves RIOU
 */
public class TaucomacPressioDialog extends JDialog implements ActionListener {
  protected TaucomacImplementation info= new TaucomacImplementation();
  BuCommonInterface appli;
  // D�claration des variables
  final Object[][] data= { { "1", "", "" }, {
      "2", "", "" }, {
      "3", "", "" }, {
      "4", "", "" }, {
      "5", "", "" }, {
      "6", "", "" }, {
      "7", "", "" }, {
      "8", "", "" }, {
      "9", "", "" }, {
      "10", "", "" }
  };
  SPtInter points[]= null;
  private BuDialogMessage message= null;
  public BuCommonInterface appli_;
  //protected BuAssistant          assistant_;
  public TaucomacPressio mere_;
  public TaucomacDefense mere1_;
  public int codeMere;
  public TaucomacCellEditor anEditor1= new TaucomacCellEditor();
  public TaucomacCellEditor anEditor2= new TaucomacCellEditor();
  int nombrePt;
  private boolean erreur= false;
  boolean sup= true;
  // Zones de saisie
  MyTableModel myModel= new MyTableModel();
  JTable table= new JTable(myModel);
  BuButton graph= new BuButton(" Fermer ");
  BuButton bvalider= new BuButton(" Valider   ");
  BuButton bsupprimer= new BuButton(" RAZ ");
  public void placeComposant(
    final GridBagLayout lm,
    final Component composant,
    final GridBagConstraints c) {
    lm.setConstraints(composant, c);
    getContentPane().add(composant);
  }
  // Constructeurs
  public TaucomacPressioDialog(
    final BuCommonInterface _appli,
    final TaucomacDefense _mere) {
    super(
      _appli instanceof Frame ? (Frame)_appli : (Frame) null,
      "Courbe D�formation-Effort ",
      true);
    appli_= _appli;
    mere1_= _mere;
    codeMere= 1;
    setGui();
  }
  public TaucomacPressioDialog(
    final BuCommonInterface _appli,
    final TaucomacPressio _mere) {
    super(
      _appli instanceof Frame ? (Frame)_appli : (Frame) null,
      "Courbe D�formation-Effort ",
      true);
    appli_= _appli;
    mere_= _mere;
    codeMere= 0;
    setGui();
  }
  public void setGui() {
    setSize(600, 400);
    //setResizable(false);
    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    if (appli_ instanceof Frame) {
      final Point pos= ((Frame)appli_).getLocationOnScreen();
      pos.x += (((Frame)appli_).getWidth() - getWidth()) / 2;
      pos.y += (((Frame)appli_).getHeight() - getHeight()) / 2;
      setLocation(pos);
    }
    info= (TaucomacImplementation)appli_.getImplementation();
    appli= appli_;
    // Cr�ation du layout manager
    final GridBagLayout lm= new GridBagLayout();
    final GridBagConstraints c= new GridBagConstraints();
    getContentPane().setLayout(lm);
    // Contraintes communes � tous les composants
    //c.gridy       = GridBagConstraints.RELATIVE;
    //c.anchor      = GridBagConstraints.WEST;
    c.ipadx= 5; // espace autour des composants
    c.ipady= 5;
    c.gridwidth= 1; //nombre de colonnes pour le composant
    c.gridheight= 1; //nombre de lignes
    c.weighty= 100; //proportion des lignes
    c.weightx= 100; //proportion des colonnes
    c.insets.left= 40;
    // premiere colonne de la fenetre
    c.gridx= 0;
    c.gridy= 0;
    c.gridwidth= 2;
    final BuLabel comm=
      new BuLabel("Rentrer les lignes dans l'ordre croissant des d�formations ");
    placeComposant(lm, comm, c);
    c.gridy= 1;
    c.gridwidth= 1;
    table.setPreferredScrollableViewportSize(new Dimension(250, 160));
    table.getColumn(table.getColumnName(1)).setCellEditor(anEditor1);
    table.getColumn(table.getColumnName(2)).setCellEditor(anEditor2);
    table.setRowSelectionInterval(0, 0);
    table.setColumnSelectionInterval(1, 1);
    final JScrollPane scrollPane= new JScrollPane(table);
    (table.getTableHeader()).setReorderingAllowed(false);
    //table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    //On regle la taille des colonnes
     (table.getColumn(table.getColumnName(0))).setPreferredWidth(55);
    // Couleur du tableau
    //table.setBackground(Color.pink);// couleur de font du tableau
    //table.setForeground(Color.blue);// Couleur du text du tableau
    placeComposant(lm, scrollPane, c);
    c.gridy= 2;
    c.anchor= GridBagConstraints.WEST;
    placeComposant(lm, graph, c);
    graph.addActionListener(new TaucomacPressioDialogListener());
    c.gridy= 2;
    c.anchor= GridBagConstraints.EAST;
    placeComposant(lm, bvalider, c);
    bvalider.addActionListener(new TaucomacvalidListener());
    // deuxieme colonne
    c.gridx= 1;
    c.gridy= 1;
    c.anchor= GridBagConstraints.CENTER;
    c.gridy= 2;
    placeComposant(lm, bsupprimer, c);
    bsupprimer.addActionListener(new TaucomacSuppListener());
    //setNbrepoints (nombrePt);
    //setParametresPoints(points);
  }
  public void actionPerformed(final ActionEvent e) {
    nombrePt= getNbrepoints();
    points= getParametresPoints();
    if (points != null) {
      setParametresPoints(points);
    }
    setVisible(true);
  }
  // initialisation des points[]
  public void setParametresPoints(final SPtInter[] points_) {
    points= points_;
    if (points != null) {
      //System.out.println("-- nombrePt : "+nombrePt);
      for (int j= 0; j < 10; j++) {
        table.setValueAt("", j, 1);
        table.setValueAt("", j, 2);
      }
      if (nombrePt > 0) {
        for (int j= 0; j < nombrePt; j++) {
          points[j]= points_[j];
          points[j].def= points_[j].def;
          points[j].eff= points_[j].eff;
          table.setValueAt(new Double(points[j].def), j, 1);
          table.setValueAt(new Double(points[j].eff), j, 2);
        }
      }
    }
  }
  // Accesseur des parametres
  public SPtInter[] getParametresPoints() {
    anEditor1.stopCellEditing();
    anEditor2.stopCellEditing();
    if (points == null) {
      points= new SPtInter[10];
    }
    int n= 0;
    while (n < 10
      && !data[n][1].toString().equals("")
      && !data[n][2].toString().equals("")) {
      n= n + 1;
    }
    nombrePt= n;
    while (n < 10
      && data[n][1].toString().equals("")
      && data[n][2].toString().equals("")) {
      n++;
    }
    if (n < 10 && sup) {
      erreur= true;
      if (n == nombrePt) {
        messageAssistant(
          "Vous devez compl�ter tous les champs"
            + "\nde la ligne n� "
            + (n + 1));
      } else {
        if (n == nombrePt + 1) {
          messageAssistant(
            "Vous devez compl�ter la ligne n� "
              + (nombrePt + 1)
              + "\npour que la ligne n� "
              + (n + 1)
              + " soit"
              + "\nprise en compte");
        } else {
          messageAssistant(
            "Vous devez compl�ter les lignes n� "
              + (nombrePt + 1)
              + "\n� "
              + n
              + " pour que les lignes n� "
              + (n + 1)
              + " soit"
              + "\nprise en compte");
        }
      }
    } else {
      erreur= false;
    }
    for (int i= 0; i < nombrePt; i++) {
      points[i]= new SPtInter();
    }
    // Remplissage des structures:
    // System.out.println("===Dialog  getParametresPoints() ===");
    for (int j= 0; j < nombrePt; j++) {
      points[j].def= new Double(data[j][1].toString()).doubleValue();
      points[j].eff= new Double(data[j][2].toString()).doubleValue();
      //  System.out.println("Point "+(j+1)+" : def : "+ points[j].def);
      //  System.out.println("      "+(j+1)+" : eff : "+ points[j].eff);
    }
    return points;
  }
  public int getNbrepoints() {
    return nombrePt;
  }
  public void setNbrepoints(final int nbre) {
    nombrePt= nbre;
    //System.out.println("** nombrePt : "+nombrePt);
    //if(nbre>0) bsupprimer.setEnabled(true);
  }
  /*--- message de l'assistant ---*/
  private void messageAssistant(final String s) {
    message= new BuDialogMessage(appli_, TaucomacImplementation.isTaucomac_, s);
    message.setSize(500, 150);
    message.setTitle("ERREUR");
    message.setResizable(false);
    message.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    final Point pos= this.getLocationOnScreen();
    pos.x= pos.x + this.getWidth() / 2 - message.getWidth() / 2;
    pos.y= pos.y + this.getHeight() / 2 - message.getHeight() / 2;
    message.setLocation(pos);
    message.setVisible(true);
  }
  boolean testErreur() {
    sup= true;
    getParametresPoints();
    final int[] bool= new int[10];
    final int[] bool1= new int[10];
    for (int i= 0; i < 10; i++) {
      bool[i]= 1;
      bool1[i]= 1;
    }
    // Si tout est bon, on ferme la fenetre
    for (int l= 0; l < nombrePt; l++) {
      final int o= l + 1;
      if (l > 0) {
        if (Double.parseDouble(data[l][1].toString())
          <= Double.parseDouble(data[l - 1][1].toString())) {
          bool1[l]= 0; //d�croissance
          messageAssistant(
            "Les abscisses des lignes "
              + l
              + " et "
              + o
              + " \ndoivent �tre strictement croissantes.");
          //System.out.println(l+"3 : "+bool[l]);
        }
        if (Double.parseDouble(data[l][1].toString())
          > Double.parseDouble(data[l - 1][1].toString())) {
          bool1[l]= 1;
          //System.out.println(l+"4 : "+bool[l]);
        }
      }
    }
    if ((bool[0] == 1)
      && (bool[1] == 1)
      && bool[2] == 1
      && bool[3] == 1
      && bool[4] == 1
      && bool[5] == 1
      && bool[6] == 1
      && bool[7] == 1
      && bool[8] == 1
      && bool[9] == 1
      && bool1[0] == 1
      && (bool1[1] == 1)
      && bool1[2] == 1
      && bool1[3] == 1
      && bool1[4] == 1
      && bool1[5] == 1
      && bool1[6] == 1
      && bool1[7] == 1
      && bool1[8] == 1
      && bool1[9] == 1
      && !erreur) {
      getParametresPoints();
      if (!erreur) {
        if (getNbrepoints() == 0) {
          messageAssistant("Aucune valeur" + " n'a �t� d�finie");
          return false;
        }
        return true;
      }
      return true;
    }
    return false;
  }
  // BOUTTONS
  class TaucomacPressioDialogListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      //     if(testErreur())
      // dessin ?? jyr  voir new TaucomacConstruitGrapheRappel(appli,1);
      dispose();
    }
  }
  class TaucomacvalidListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      if (testErreur()) {
        if (codeMere == 0) {
          mere_.getparamDialog();
        }
        if (codeMere == 1) {
          mere1_.getparamDialog();
        }
        dispose();
      }
    }
  }
  class TaucomacSuppListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      sup= false;
      getNbrepoints();
      getParametresPoints();
      if (getNbrepoints() > 0) {
        getParametresPoints();
        final BuDialogConfirmation mess=
          new BuDialogConfirmation(
            appli_,
            TaucomacImplementation.isTaucomac_,
            "Voulez vous supprimer toutes les valeurs ?");
        mess.setTitle("ATTENTION");
        if (mess.activate() == JOptionPane.YES_OPTION) {
          for (int row= 0; row < nombrePt; row++) {
            for (int col= 1; col < 3; col++) {
              table.setValueAt("", row, col);
            }
          }
          setNbrepoints(0);
          getParametresPoints();
          if (codeMere == 0) {
            mere_.getparamDialog();
          }
          if (codeMere == 1) {
            mere1_.getparamDialog();
          }
          dispose();
        }
      } else {
        for (int row= 0; row < 10; row++) {
          for (int col= 1; col < 3; col++) {
            table.setValueAt("", row, col);
          }
        }
        setNbrepoints(0);
        getParametresPoints();
        dispose();
      }
    }
  }
  // TABLE MODEL
  class MyTableModel extends AbstractTableModel {
    final String[] columnNames= { "N�", "D�formation", "Effort", };
    public int getColumnCount() {
      return columnNames.length;
    }
    public int getRowCount() {
      return data.length;
    }
    public String getColumnName(final int col) {
      return columnNames[col];
    }
    public Object getValueAt(final int row, final int col) {
      return data[row][col];
    }
    /*public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }*/
    public boolean isCellEditable(final int row, final int col) {
      return col>0;
    }
    public boolean isNombre(final String chaine) {
      final int taille= chaine.length();
      if (taille != 0) {
        int i= 0;
        boolean bool= true;
        while (i < taille
          && (chaine.charAt(i) >= '0'
            && chaine.charAt(i) <= '9'
            || chaine.charAt(i) == '-'
            || chaine.charAt(i) == '.')) {
          if (i + 1 < taille && chaine.charAt(i + 1) == '-') {
            return false;
          }
          if (chaine.charAt(i) == '.') {
            if (!bool) {
              return false;
            }
            bool= false;
          }
          i++;
        }
        return (i == taille);
      }
        return false;
    }
    // RECUPERATION DES DONNEES
    public void setValueAt(final Object value, final int row, final int col) {
      if (isNombre(value.toString())) {
        if (value.toString().charAt(0) == '.') {
          data[row][col]= "0" + value.toString();
        } else {
          data[row][col]= value;
        }
      } else {
        data[row][col]= "";
      }
    }
  }
}
