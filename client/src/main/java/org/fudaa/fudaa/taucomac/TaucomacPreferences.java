/*
 * @file         TaucomacPreferences.java
 * @creation     2000-10-24
 * @modification $Date: 2006-09-19 15:08:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.taucomac;
import com.memoire.bu.BuPreferences;
/**
 * Preferences pour Taucomac.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:08:56 $ by $Author: deniger $
 * @author       Jean-Yves Riou 
 */
public class TaucomacPreferences extends BuPreferences {
  public final static TaucomacPreferences TAUCOMAC= new TaucomacPreferences();
  public void applyOn(final Object _o) {
    if (!(_o instanceof TaucomacImplementation)) {
      throw new RuntimeException("" + _o + " is not a TaucomacImplementation.");
    //    TaucomacImplementation _appli=(TaucomacImplementation)_o;
    }
  }
}
