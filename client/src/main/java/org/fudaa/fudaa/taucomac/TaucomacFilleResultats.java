/*
 * @file         TaucomacFilleResultats.java
 * @creation     2000-10-24
 * @modification $Date: 2006-09-19 15:08:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.taucomac;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.print.PageFormat;
import java.beans.PropertyVetoException;

import javax.swing.JComponent;

import com.memoire.bu.BuCommonInterface;

import org.fudaa.dodico.corba.taucomac.IResultatsTaucomac;

import org.fudaa.ebli.impression.EbliFilleImprimable;
import org.fudaa.ebli.impression.EbliPrinter;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaParamEventProxy;
import org.fudaa.fudaa.commun.projet.FudaaParamListener;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.commun.projet.FudaaProjetEvent;
import org.fudaa.fudaa.commun.projet.FudaaProjetListener;
/**
 * Une fenetre fille pour editer les resultats
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:08:56 $ by $Author: deniger $
 * @author       Jean-Yves Riou 
 */
public class TaucomacFilleResultats
  extends EbliFilleImprimable
  implements FudaaProjetListener, FudaaParamListener, ActionListener, FocusListener {
  //===================================================
  //    Variables
  //===================================================
  //  general
  TaucomacImplementation imp_; // implementation
  FudaaProjet project_; // projet
  BuCommonInterface appli_; // application
  IResultatsTaucomac res_; // resultats
  JComponent content_; // panneau general
  //==========================================================
  //   Constructeur
  //==========================================================
  public TaucomacFilleResultats(
    final BuCommonInterface _appli,
    final FudaaProjet projet,
    final TaucomacImplementation _imp,
    final IResultatsTaucomac _taucomacResults) {
    super("", false, true, false, true);
    appli_= _appli;
    project_= projet;
    imp_= _imp;
    res_= _taucomacResults;
    FudaaParamEventProxy.FUDAA_PARAM.addFudaaParamListener(this);
    project_.addFudaaProjetListener(this);
    // pack();
  } // fin constructeur
  //==============================================================
  // Methodes publiques
  //==============================================================
  public void setProjet(final FudaaProjet project) {
    project_= project;
    updatePanels();
  }
  public int getNumberOfPages() {
    return 1;
  }
  public int print(final Graphics _g, final PageFormat _format, final int _numPage) {
    return EbliPrinter.printComponent(_g, _format, content_, true, _numPage);
  }
  public void focusGained(final FocusEvent e) {
    //Object src=e.getSource();
    System.out.println("**** TaucomacFilleResultats  focusGained");
  }
  public void focusLost(final FocusEvent e) {
    //Object src=e.getSource();
    //System.out.println("source " + src);
  }
  public void actionListener(final ActionEvent e) {
    //Object src=e.getSource();
  }
  public void actionPerformed(final ActionEvent e) {
    //     Object src=e.getSource();
    //if( src==btFermer_ ) 	fermer();
    /*
    else
     if( src==btVuePlan_ )	vuePlan();
     else
     if( src==btVueTrav_ )	vueTrav();
     else
     if( src==btExporter_ )	exporter();
     else
    
    try {
      setClosed(true);
      setSelected(false);
    } catch( PropertyVetoException ex ) {}
    */
  }
  public void valider() // appelee par SeuilImplementation a voir
  {
    try {
      // System.out.println("fille parametres  valider()");
      //getValeurs ();
    } catch (final IllegalArgumentException e) {
      return;
    }
    try {
      setClosed(true);
      setSelected(false);
    } catch (final PropertyVetoException e) {}
  }
  public void fermer() {
    //    System.out.println("fille Resultats fermer()");
    //    System.out.println(" LidoMode "+imp_.LidoMode);
    //
    try {
      setClosed(true);
      setSelected(false);
    } catch (final PropertyVetoException e) {}
  }
  public void exporter() // on aurait pu deporter le source de imp_.exportResultats()
  { // mais entraine trop de modif
    try {
      imp_.exportResultats();
    } catch (final IllegalArgumentException e) {
      return;
    }
  }
  public void delete() // a conserver sinon on ne peut sortir
  {
    FudaaParamEventProxy.FUDAA_PARAM.removeFudaaParamListener(this);
    if (project_ != null) {
      project_.removeFudaaProjetListener(this);
    }
    project_= null;
  }
  //
  // LidoParamListener
  //
  public void paramStructCreated(final FudaaParamEvent e) {
    if (e.getSource() != this) {
      updatePanels();
    }
  }
  public void paramStructDeleted(final FudaaParamEvent e) {
    if (e.getSource() != this) {
      updatePanels();
    }
  }
  public void paramStructModified(final FudaaParamEvent e) {
    if (e.getSource() != this) {
      updatePanels();
    }
  }
  //
  // fudaaprojet listener
  //
  public void dataChanged(final FudaaProjetEvent e) {
    switch (e.getID()) {
      case FudaaProjetEvent.PARAM_ADDED :
      case FudaaProjetEvent.PARAM_IMPORTED :
        {
          if (e.getSource() != this) {
            updatePanels();
          }
          break;
        }
    }
  }
  public void statusChanged(final FudaaProjetEvent e) {
    switch (e.getID()) {
      case FudaaProjetEvent.PROJECT_OPENED :
      case FudaaProjetEvent.PROJECT_CLOSED :
        {
          if (e.getSource() != this) {
            updatePanels();
          }
          break;
        }
    }
  }
  //=====================================================
  //   Methodes privees
  //=====================================================
  private void updatePanels() {
    // System.out.println("SeuilFilleParametres: update");
    setValeurs();
  }
  //private void setValeurs()
  public void setValeurs() {}
} // fin de classe
