/*
 * @file         TaucomacCasCharge.java
 * @creation     2000-10-17
 * @modification $Date: 2006-09-19 15:08:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.taucomac;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuGridLayout;

import org.fudaa.dodico.corba.taucomac.SCasCharges;
/**
 * Onglet des Cas de Charges
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:08:56 $ by $Author: deniger $
 * @author       Jean-Yves Riou
 */
public class TaucomacCasCharge
  extends TaucomacMerePanParm
  implements ActionListener {
  SCasCharges parametresLocal= null;
  // SCasCharges         casCharg;
  // struct SCasCharges
  //duraAcost;
  //duraAcostTet;
  //duraAmarHoriz;
  //duraAmarHorizVert;
  //acciAcost;
  //acciAcostTet;
  //acciAmarHoriz;
  //acciAmarHorizVert;
  JPanel pn1234;
  JPanel pn5678;
  TitledBorder titre1234;
  TitledBorder titre5678;
  JPanel pncb12;
  JPanel pncb34;
  JPanel pncb56;
  JPanel pncb78;
  TitledBorder titre12;
  TitledBorder titre34;
  TitledBorder titre56;
  TitledBorder titre78;
  JCheckBox cb1_;
  JCheckBox cb2_;
  JCheckBox cb3_;
  JCheckBox cb4_;
  JCheckBox cb5_;
  JCheckBox cb6_;
  JCheckBox cb7_;
  JCheckBox cb8_;
  public TaucomacCasCharge(final BuCommonInterface _appli) {
    super();
    cb1_= new JCheckBox("accostage");
    cb2_= new JCheckBox("accostage + charge en tete ");
    cb3_= new JCheckBox("force horizontale ");
    cb4_= new JCheckBox("force horizontale + force verticale ");
    cb5_= new JCheckBox("accostage");
    cb6_= new JCheckBox("accostage + charge en tete ");
    cb7_= new JCheckBox("force horizontale ");
    cb8_= new JCheckBox("force horizontale + force verticale ");
    cb1_.setSelected(true);
    cb2_.setSelected(true);
    cb3_.setSelected(true);
    cb4_.setSelected(true);
    cb5_.setSelected(true);
    cb6_.setSelected(true);
    cb7_.setSelected(true);
    cb8_.setSelected(true);
    cb1_.addActionListener(this);
    cb2_.addActionListener(this);
    cb3_.addActionListener(this);
    cb4_.addActionListener(this);
    cb5_.addActionListener(this);
    cb6_.addActionListener(this);
    cb7_.addActionListener(this);
    cb8_.addActionListener(this);
    pncb12= new JPanel();
    titre12= new TitledBorder("Accostage");
    placeTitre(pncb12, titre12, 2);
    pncb12.setPreferredSize(new Dimension(250, 90));
    pncb12.setLayout(new BuGridLayout(1, 5, 5, true, false));
    placeComp(pncb12, cb1_, 2);
    placeComp(pncb12, cb2_, 2);
    pncb34= new JPanel();
    titre34= new TitledBorder("Amarrage");
    placeTitre(pncb34, titre34, 2);
    pncb34.setPreferredSize(new Dimension(280, 90));
    pncb34.setLayout(new BuGridLayout(1, 5, 5, true, false));
    placeComp(pncb34, cb3_, 2);
    placeComp(pncb34, cb4_, 2);
    pncb56= new JPanel();
    titre56= new TitledBorder("Accostage");
    placeTitre(pncb56, titre56, 2);
    pncb56.setPreferredSize(new Dimension(250, 90));
    pncb56.setLayout(new BuGridLayout(1, 5, 5, true, false));
    placeComp(pncb56, cb5_, 2);
    placeComp(pncb56, cb6_, 2);
    pncb78= new JPanel();
    titre78= new TitledBorder("Amarrage");
    placeTitre(pncb78, titre78, 2);
    pncb78.setPreferredSize(new Dimension(280, 90));
    pncb78.setLayout(new BuGridLayout(1, 5, 5, true, false));
    placeComp(pncb78, cb7_, 2);
    placeComp(pncb78, cb8_, 2);
    pn1234= new JPanel();
    titre1234= new TitledBorder("Situation durable et situation transitoire");
    placeTitreMarge(pn1234, titre1234, 2, 5, 180, 5, 40);
    pn1234.setLayout(new BorderLayout());
    pn1234.setPreferredSize(new Dimension(500, 150));
    pn1234.add(pncb12, BorderLayout.WEST);
    pn1234.add(pncb34, BorderLayout.EAST);
    pn5678= new JPanel();
    titre5678= new TitledBorder("Situation accidentelle");
    placeTitreMarge(pn5678, titre5678, 2, 5, 180, 5, 40);
    pn5678.setLayout(new BorderLayout());
    pn5678.setPreferredSize(new Dimension(500, 150));
    pn5678.add(pncb56, BorderLayout.WEST);
    pn5678.add(pncb78, BorderLayout.EAST);
    this.setLayout(new BuGridLayout(1, 5, 5, false, true));
    this.add(pn1234);
    this.add(pn5678);
    //===================================================
    setParametres(parametresLocal);
  }
  public void actionPerformed(final ActionEvent e) {
    //      String cmd=e.getActionCommand();
    //    Object src=e.getSource();
    parametresLocal= getParametres();
    setVisible(true);
  }
  public void setParametres(final SCasCharges parametresProjet_) {
    parametresLocal= parametresProjet_;
    if (parametresLocal != null) {
      cb1_.setSelected(true);
      cb2_.setSelected(true);
      cb3_.setSelected(true);
      cb4_.setSelected(true);
      cb5_.setSelected(true);
      cb6_.setSelected(true);
      cb7_.setSelected(true);
      cb8_.setSelected(true);
      if (parametresLocal.duraAcost == 0) {
        cb1_.setSelected(false);
      }
      if (parametresLocal.duraAcostTet == 0) {
        cb2_.setSelected(false);
      }
      if (parametresLocal.duraAmarHoriz == 0) {
        cb3_.setSelected(false);
      }
      if (parametresLocal.duraAmarHorizVert == 0) {
        cb4_.setSelected(false);
      }
      if (parametresLocal.acciAcost == 0) {
        cb5_.setSelected(false);
      }
      if (parametresLocal.acciAcostTet == 0) {
        cb6_.setSelected(false);
      }
      if (parametresLocal.acciAmarHoriz == 0) {
        cb7_.setSelected(false);
      }
      if (parametresLocal.acciAmarHorizVert == 0) {
        cb8_.setSelected(false);
      }
    }
  }
  public SCasCharges getParametres() {
    if (parametresLocal == null) {
      parametresLocal= new SCasCharges();
    }
    parametresLocal.duraAcost= 0;
    parametresLocal.duraAcostTet= 0;
    parametresLocal.duraAmarHoriz= 0;
    parametresLocal.duraAmarHorizVert= 0;
    parametresLocal.acciAcost= 0;
    parametresLocal.acciAcostTet= 0;
    parametresLocal.acciAmarHoriz= 0;
    parametresLocal.acciAmarHorizVert= 0;
    if (cb1_.isSelected()) {
      parametresLocal.duraAcost= 1;
    }
    if (cb2_.isSelected()) {
      parametresLocal.duraAcostTet= 1;
    }
    if (cb3_.isSelected()) {
      parametresLocal.duraAmarHoriz= 1;
    }
    if (cb4_.isSelected()) {
      parametresLocal.duraAmarHorizVert= 1;
    }
    if (cb5_.isSelected()) {
      parametresLocal.acciAcost= 1;
    }
    if (cb6_.isSelected()) {
      parametresLocal.acciAcostTet= 1;
    }
    if (cb7_.isSelected()) {
      parametresLocal.acciAmarHoriz= 1;
    }
    if (cb8_.isSelected()) {
      parametresLocal.acciAmarHorizVert= 1;
    }
    return parametresLocal;
  }
  //==========================================================
}
