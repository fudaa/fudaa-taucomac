/*
 * @file         TaucomacDonGen.java
 * @creation     2000-10-17
 * @modification $Date: 2006-09-19 15:08:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.taucomac;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.StringTokenizer;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.AbstractTableModel;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuTextField;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.taucomac.SCarTronc;
import org.fudaa.dodico.corba.taucomac.SCotesPrinc;

import org.fudaa.fudaa.commun.projet.FudaaParamEvent;
import org.fudaa.fudaa.commun.projet.FudaaParamEventProxy;
/**
 * Onglet des titre,Commentaire.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:08:56 $ by $Author: deniger $
 * @author       Jean-Yves Riou
 */
public class TaucomacCotesPrinc
  extends TaucomacMerePanParm
  implements ActionListener, FocusListener {
  SCotesPrinc parametresLocal;
  /*
   struct SCarTronc        					//troncons de tube
  {
    reel  longueur;
    reel  epaisseur;
    reel  limElast;
  };
  typedef sequence<SCarTronc> VSCarTroncons;
  struct SCotesPrinc
  {
  entier         	nbTubes;  			// tubes
   reel  			zTete;  			// cotes principales
   reel  			zPied;
   reel  			zToitSup;
   reel  			zToitInf;
   entier         	nbTronconsTub;   	// nombre de troncon pour les tubes
   VSCarTroncons  	carTronc;        // caracteristiques des troncons
  };
  SCotesPrinc         cotes;
  */
  JPanel pnHaut;
  JPanel pnMil;
  JPanel pnMilG;
  JPanel pnMilC;
  JPanel pnMilD;
  JPanel pnBas;
  JLabel la_zTete;
  JLabel la_nbTubes;
  JLabel la_nbTronconsTub;
  JLabel la_zPied;
  JLabel la_zToitSup;
  JLabel la_zToitInf;
  JLabel unit_zTete;
  JLabel unit_nbTubes;
  JLabel unit_nbTronconsTub;
  JLabel unit_zPied;
  JLabel unit_zToitSup;
  JLabel unit_zToitInf;
  BuTextField tf_zTete;
  BuTextField tf_nbTubes;
  BuTextField tf_nbTronconsTub;
  BuTextField tf_zPied;
  BuTextField tf_zToitSup;
  BuTextField tf_zToitInf;
  final Object[][] data=
    { { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" }, {
      "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" }, {
      "", "", "", "", "", "", "", "", "", "", "", "", "", "", "" }
  };
  MyTableModel myModel= new MyTableModel();
  JTable table= new JTable(myModel);
  public int nbMaxTronconTub_= 15;
  public TaucomacCellEditor[] anEditor;
  //=========
  final Object[][] data1= { { "Longueur" }, {
      "Epaisseur" }, {
      "Limite elatique" }
  };
  MyTableModel1 myModel1= new MyTableModel1();
  JTable table1= new JTable(myModel1);
  public TaucomacCellEditor anEditor21= new TaucomacCellEditor();
  //=========
  final Object[][] data2= { { " m " }, {
      " mm " }, {
      " t/m2 " }
  };
  MyTableModel2 myModel2= new MyTableModel2();
  JTable table2= new JTable(myModel2);
  public TaucomacCellEditor anEditor22= new TaucomacCellEditor();
  //====================================
  //    Constructeur
  //====================================
  public TaucomacCotesPrinc(final BuCommonInterface _appli) {
    super();
    //====================================
    pnHaut= new JPanel();
    final BuGridLayout loCal2= new BuGridLayout();
    loCal2.setColumns(3);
    loCal2.setHgap(5);
    loCal2.setVgap(10);
    loCal2.setHfilled(true);
    loCal2.setCfilled(true);
    pnHaut.setLayout(loCal2);
    pnHaut.setBorder(new EmptyBorder(5, 60, 5, 60)); //top,left,bottom,right
    //pnHaut.setPreferredSize(new Dimension(650,120));
    la_zTete= new JLabel("                    Cote de la tete  ", SwingConstants.RIGHT);
    la_nbTubes=
      new JLabel("             Nombre de tubes (1 a 4) ", SwingConstants.RIGHT);
    la_nbTronconsTub=
      new JLabel("Nombre de troncons par tube (1 a 15) ", SwingConstants.RIGHT);
    tf_zTete= BuTextField.createDoubleField();
    tf_nbTubes= BuTextField.createIntegerField();
    tf_nbTronconsTub= BuTextField.createIntegerField();
    tf_nbTronconsTub.addFocusListener(this);
    tf_zTete.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_nbTubes.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_nbTronconsTub.setHorizontalAlignment(SwingConstants.RIGHT);
    unit_zTete= new JLabel(" m ", SwingConstants.LEFT);
    unit_nbTubes= new JLabel(" ", SwingConstants.LEFT);
    unit_nbTronconsTub= new JLabel(" ", SwingConstants.LEFT);
    tf_zTete.setColumns(6);
    int n= 0;
    n= 0;
    placeCompn(pnHaut, la_zTete, 0, n);
    n++;
    placeCompn(pnHaut, tf_zTete, 0, n);
    n++;
    placeCompn(pnHaut, unit_zTete, 0, n);
    n++;
    placeCompn(pnHaut, la_nbTubes, 1, n);
    n++;
    placeCompn(pnHaut, tf_nbTubes, 1, n);
    n++;
    placeCompn(pnHaut, unit_nbTubes, 1, n);
    n++;
    placeCompn(pnHaut, la_nbTronconsTub, 0, n);
    n++;
    placeCompn(pnHaut, tf_nbTronconsTub, 0, n);
    n++;
    placeCompn(pnHaut, unit_nbTronconsTub, 0, n);
    n++;
    placeUnit(unit_zTete, 1);
    //====================================
    pnBas= new JPanel();
    final BuGridLayout loCal3= new BuGridLayout();
    loCal3.setColumns(3);
    loCal3.setHgap(5);
    loCal3.setVgap(10);
    loCal3.setHfilled(true);
    loCal3.setCfilled(true);
    pnBas.setLayout(loCal3);
    pnBas.setBorder(new EmptyBorder(5, 60, 5, 60)); //top,left,bottom,right
    //pnBas.setPreferredSize(new Dimension(650,120));
    la_zPied=
      new JLabel("                         cote du pied ", SwingConstants.RIGHT);
    la_zToitSup=
      new JLabel("cote du toit du sol valeur superieure ", SwingConstants.RIGHT);
    la_zToitInf=
      new JLabel(" ''       ''    ''    ''   inferieure ", SwingConstants.RIGHT);
    tf_zPied= BuTextField.createDoubleField();
    tf_zToitSup= BuTextField.createDoubleField();
    tf_zToitInf= BuTextField.createDoubleField();
    tf_zPied.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_zToitSup.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_zToitInf.setHorizontalAlignment(SwingConstants.RIGHT);
    unit_zPied= new JLabel(" m ", SwingConstants.LEFT);
    unit_zToitSup= new JLabel(" m ", SwingConstants.LEFT);
    unit_zToitInf= new JLabel(" m ", SwingConstants.LEFT);
    tf_zPied.setColumns(6);
    n= 0;
    placeCompn(pnBas, la_zPied, 0, n);
    n++;
    placeCompn(pnBas, tf_zPied, 0, n);
    n++;
    placeCompn(pnBas, unit_zPied, 0, n);
    n++;
    placeCompn(pnBas, la_zToitSup, 0, n);
    n++;
    placeCompn(pnBas, tf_zToitSup, 0, n);
    n++;
    placeCompn(pnBas, unit_zToitSup, 0, n);
    n++;
    placeCompn(pnBas, la_zToitInf, 2, n);
    n++;
    placeCompn(pnBas, tf_zToitInf, 2, n);
    n++;
    placeCompn(pnBas, unit_zToitInf, 2, n);
    n++;
    placeUnit(unit_zPied, 1);
    placeUnit(unit_zToitSup, 1);
    placeUnit(unit_zToitInf, 1);
    //  pnBas.setPreferredSize(new Dimension(600,120));
    //=======================================================
    pnMil= new JPanel();
    pnMil.setBorder(new TitledBorder(""));
    pnMil.setLayout(new BorderLayout());
    pnMil.setBorder(new EmptyBorder(40, 20, 40, 20)); //top,left,bottom,right
    // pnMil.setPreferredSize(new Dimension(700,250));
    pnMilC= new JPanel();
    // pnMilC.setPreferredSize(new Dimension(500,80 ));
    pnMilG= new JPanel();
    pnMilD= new JPanel();
    //===================
    //    Tables
    //===================
    table1.setBackground(Color.orange);
    table1.setPreferredScrollableViewportSize(new Dimension(120, 50));
    table1.getColumn(table1.getColumnName(0)).setCellEditor(anEditor21);
    final JScrollPane scrollPane1= new JScrollPane(table1);
    (table1.getTableHeader()).setReorderingAllowed(false);
    table1.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    (table1.getColumn(table1.getColumnName(0))).setPreferredWidth(120);
    table2.setBackground(Color.orange);
    table2.setPreferredScrollableViewportSize(new Dimension(75, 50));
    table2.getColumn(table2.getColumnName(0)).setCellEditor(anEditor22);
    final JScrollPane scrollPane2= new JScrollPane(table2);
    (table2.getTableHeader()).setReorderingAllowed(false);
    table2.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    table.setColumnSelectionAllowed(true);
    table.setRowSelectionAllowed(false);
    table.setBackground(Color.cyan);
    table.setForeground(Color.black);
    table.setSelectionBackground(Color.white);
    table.setSelectionForeground(Color.blue);
    table.setPreferredScrollableViewportSize(new Dimension(500, 50));
    /* initialisation du tableau*/
    anEditor= new TaucomacCellEditor[nbMaxTronconTub_];
    for (int i= 0; i < nbMaxTronconTub_; i++) {
      anEditor[i]= new TaucomacCellEditor();
      table.getColumn(table.getColumnName(i)).setCellEditor(anEditor[i]);
    }
    /*deniger fin*/
    table.setRowSelectionInterval(0, 0);
    table.setColumnSelectionInterval(1, 1);
    final JScrollPane scrollPane= new JScrollPane(table);
    (table.getTableHeader()).setReorderingAllowed(false);
    table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    //On regle la taille des colonnes
    //(table.getColumn(table.getColumnName(0))).setPreferredWidth(120);
    pnMilC.add(scrollPane);
    pnMilG.add(scrollPane1);
    pnMilD.add(scrollPane2);
    pnMil.add(pnMilC, BorderLayout.CENTER);
    pnMil.add(pnMilG, BorderLayout.WEST);
    pnMil.add(pnMilD, BorderLayout.EAST);
    //====================================
    this.setLayout(new BorderLayout());
    this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    this.add(pnHaut, BorderLayout.NORTH);
    this.add(pnMil, BorderLayout.CENTER);
    this.add(pnBas, BorderLayout.SOUTH);
    //initialisation des parametres locaux
    setParametres(parametresLocal);
  }
  public void focusLost(final FocusEvent e) {
    final Object src= e.getSource();
    //System.out.println("filleparam focusLost  "+e + " "+src );
    if (src instanceof JTextField) {
      if (src == tf_nbTronconsTub) {
        //System.out.println("focusLost source " + src);
        String tmpS= "";
        final String blanc= "";
        int n= 0;
        tmpS= tf_nbTronconsTub.getText();
        if (tmpS.equals(blanc)) {
          n= 0;
        } else {
          n= Integer.parseInt(tmpS);
        }
        // 2 lignes annulees temporairement
        myModel.setNbMaxColEditable(n);
        table.repaint();
      }
    }
  }
  public void focusGained(final FocusEvent e) {}
  public void actionPerformed(final ActionEvent e) {
    parametresLocal= getParametres();
    setVisible(true);
  }
  public void setParametres(final SCotesPrinc parametresProjet_) {
    parametresLocal= parametresProjet_;
    if (parametresLocal != null) {
      myModel.setNbMaxColEditable(parametresLocal.nbTronconsTub);
      if (parametresLocal.zTete != -999.) {
        tf_zTete.setValue(new Double(parametresLocal.zTete));
      } else {
        tf_zTete.setValue("");
      }
      if (parametresLocal.nbTubes != -999.) {
        tf_nbTubes.setValue(new Integer(parametresLocal.nbTubes));
      } else {
        tf_nbTubes.setValue("");
      }
      if (parametresLocal.nbTronconsTub > 0) {
        tf_nbTronconsTub.setValue(new Integer(parametresLocal.nbTronconsTub));
      } else {
        tf_nbTronconsTub.setValue("");
      }
      if (parametresLocal.zPied != -999.) {
        tf_zPied.setValue(new Double(parametresLocal.zPied));
      } else {
        tf_zPied.setValue("");
      }
      if (parametresLocal.zToitSup != -999.) {
        tf_zToitSup.setValue(new Double(parametresLocal.zToitSup));
      } else {
        tf_zToitSup.setValue("");
      }
      if (parametresLocal.zToitInf != -999.) {
        tf_zToitInf.setValue(new Double(parametresLocal.zToitInf));
      } else {
        tf_zToitInf.setValue("");
      }
      if (parametresLocal.nbTronconsTub > 0) {
        for (int j= 0; j < parametresLocal.nbTronconsTub; j++) {
          /*
          table.setValueAt(new Double(parametresLocal.carTronc[j].longueur) ,0,j);
          table.setValueAt(new Double(parametresLocal.carTronc[j].epaisseur),1,j);
          table.setValueAt(new Double(parametresLocal.carTronc[j].limElast) ,2,j);
          */
          if (parametresLocal.carTronc[j].longueur != -999.) {
            table.setValueAt(
              new Double(parametresLocal.carTronc[j].longueur),
              0,
              j);
          } else {
            table.setValueAt("", 0, j);
          }
          if (parametresLocal.carTronc[j].epaisseur != -999.) {
            table.setValueAt(
              new Double(parametresLocal.carTronc[j].epaisseur),
              1,
              j);
          } else {
            table.setValueAt("", 1, j);
          }
          if (parametresLocal.carTronc[j].limElast != -999.) {
            table.setValueAt(
              new Double(parametresLocal.carTronc[j].limElast),
              2,
              j);
          } else {
            table.setValueAt("", 2, j);
          }
          System.out.println(
            "set carTronc[j].longueur " + parametresLocal.carTronc[j].longueur);
          System.out.println(
            "set carTronc[j].epaisseur "
              + parametresLocal.carTronc[j].epaisseur);
          System.out.println(
            "set carTronc[j].limElast  "
              + parametresLocal.carTronc[j].limElast);
        }
      }
    }
  }
  public SCotesPrinc getParametres() {
    if (parametresLocal == null) {
      parametresLocal= new SCotesPrinc();
      /*deniger*/
      parametresLocal.carTronc= new SCarTronc[nbMaxTronconTub_];
      /*deniger fin*/
      System.out.println(" parametresLocal = " + parametresLocal);
      for (int i= 0; i < nbMaxTronconTub_; i++) {
        parametresLocal.carTronc[i]= new SCarTronc();
      }
    }
    String changed= "";
    double tmpD= 0.;
    int tmpI= 0;
    String tmpS= "";
    final String blanc= "";
    //    int n = 0;
    tmpD= parametresLocal.zTete;
    tmpS= tf_zTete.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.zTete= -999.;
    } else {
      parametresLocal.zTete= Double.parseDouble(tmpS);
    }
    if (parametresLocal.zTete != tmpD) {
      changed += "zTete|";
    }
    tmpI= parametresLocal.nbTubes;
    tmpS= tf_nbTubes.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.nbTubes= 0;
    } else {
      parametresLocal.nbTubes= Integer.parseInt(tmpS);
    }
    if (parametresLocal.nbTubes != tmpI) {
      changed += "nbTubes|";
    }
    tmpI= parametresLocal.nbTronconsTub;
    tmpS= tf_nbTronconsTub.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.nbTronconsTub= 0;
    } else {
      parametresLocal.nbTronconsTub= Integer.parseInt(tmpS);
    }
    if (parametresLocal.nbTronconsTub != tmpI) {
      changed += "nbTronconsTub|";
    }
    tmpD= parametresLocal.zPied;
    tmpS= tf_zPied.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.zPied= -999.;
    } else {
      parametresLocal.zPied= Double.parseDouble(tmpS);
    }
    if (parametresLocal.zPied != tmpD) {
      changed += "zPied|";
    }
    tmpD= parametresLocal.zToitSup;
    tmpS= tf_zToitSup.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.zToitSup= -999.;
    } else {
      parametresLocal.zToitSup= Double.parseDouble(tmpS);
    }
    if (parametresLocal.zToitSup != tmpD) {
      changed += "zToitSup|";
    }
    tmpD= parametresLocal.zToitInf;
    tmpS= tf_zToitInf.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.zToitInf= -999.;
    } else {
      parametresLocal.zToitInf= Double.parseDouble(tmpS);
    }
    if (parametresLocal.zToitInf != tmpD) {
      changed += "zTete|";
    }
    // Remplissage des structures:
    for (int j= 0; j < parametresLocal.nbTronconsTub; j++) {
      //     System.out.println(" parametresLocal .carTronc= "+parametresLocal.carTronc);
      if (parametresLocal.carTronc[j] == null)
        //==================== se plante ici
        {
        parametresLocal.carTronc[j]= new SCarTronc();
      }
      // System.out.println(" parametresLocal .carTronc[j]= "+parametresLocal.carTronc[j]);
      /*
      parametresLocal.carTronc[j].longueur  = new Double(data[0][j].toString()).doubleValue();
      parametresLocal.carTronc[j].epaisseur = new Double(data[1][j].toString()).doubleValue();
      parametresLocal.carTronc[j].limElast  = new Double(data[2][j].toString()).doubleValue();
      */
      //System.out.println(" data[0][j]= "+ data[0][j]);
      tmpD= parametresLocal.carTronc[j].longueur;
      //System.out.println("CotesPrinc getcarTronc[j].longueur  j = " +j + " data(0,j) "+ data[0][j]);
      tmpS= String.valueOf(data[0][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.carTronc[j].longueur= -999.;
      } else {
        parametresLocal.carTronc[j].longueur= Double.parseDouble(tmpS);
      }
      if (parametresLocal.carTronc[j].longueur != tmpD) {
        changed += "carTronc[j].longueur|";
      }
      //System.out.println(" data[1][j]= "+ data[1][j]);
      tmpD= parametresLocal.carTronc[j].epaisseur;
      tmpS= String.valueOf(data[1][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.carTronc[j].epaisseur= -999.;
      } else {
        parametresLocal.carTronc[j].epaisseur= Double.parseDouble(tmpS);
      }
      if (parametresLocal.carTronc[j].epaisseur != tmpD) {
        changed += "carTronc[j].epaisseur|";
      }
      //  System.out.println(" data[2][j]= "+ data[1][j]);
      tmpD= parametresLocal.carTronc[j].limElast;
      tmpS= String.valueOf(data[2][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.carTronc[j].limElast= -999.;
      } else {
        parametresLocal.carTronc[j].limElast= Double.parseDouble(tmpS);
      }
      if (parametresLocal.carTronc[j].limElast != tmpD) {
        changed += "carTronc[j].limElast|";
      // System.out.println("get carTronc[j].longueur "  +parametresLocal.carTronc[j].longueur);
      //System.out.println("get carTronc[j].epaisseur " +parametresLocal.carTronc[j].epaisseur);
      //System.out.println("get carTronc[j].limElast  " +parametresLocal.carTronc[j].limElast);
      }
    }
    final StringTokenizer tok= new StringTokenizer(changed, "|");
    while (tok.hasMoreTokens()) {
      FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(
        new FudaaParamEvent(
          this,
          0,
          TaucomacResource.TAUCOMAC01,
          parametresLocal,
          TaucomacResource.TAUCOMAC01 + ":" + tok.nextToken()));
    }
    return parametresLocal;
  }
  //===============================================
  class MyTableModel extends AbstractTableModel {
    final String[] columnNames=
      {
        " 1  ",
        " 2 ",
        " 3 ",
        " 4 ",
        " 5 ",
        " 6 ",
        " 7 ",
        " 8 ",
        " 9 ",
        " 10 ",
        " 11 ",
        " 12 ",
        " 13 ",
        " 14 ",
        " 15 " };
    /*deniger*/
    private int nbMaxColEditable_= columnNames.length;
    public void setNbMaxColEditable(final int _n) {
      //System.out.println("*** _n"+ _n);
      if (_n < 0) {
        nbMaxColEditable_= 0;
      } else if (_n > columnNames.length) {
        nbMaxColEditable_= columnNames.length;
      }
      if (_n != nbMaxColEditable_) {
        nbMaxColEditable_= _n;
        fireTableStructureChanged();
        //    System.out.println("*** changement");
      }
    }
    public boolean isCellEditable(final int row, final int col) {
      // System.out.println("*** nbMaxColEditable_ =  "+nbMaxColEditable_);
      if ((col < 0) || (col >= nbMaxColEditable_)) {
        return false;
      }
      return true;
    }
    public int getColumnCount() {
      System.out.println("getColumnCount");
      return nbMaxColEditable_;
    }
    public int getRowCount() {
      return data.length;
    }
    public String getColumnName(final int col) {
      if (col < columnNames.length) {
        return columnNames[col];
      }
      return "";
    }
    public Object getValueAt(final int row, final int col) {
      if ((row < data.length) && (col < columnNames.length)) {
        return data[row][col];
      }
      return "";
    }
    /*deniger fin*/
    /*public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }*/
    public boolean isNombre(final String chaine) {
      final int taille= chaine.length();
      if (taille != 0) {
        int i= 0;
        boolean bool= true;
        while (i < taille
          && (chaine.charAt(i) >= '0'
            && chaine.charAt(i) <= '9'
            || chaine.charAt(i) == '-'
            || chaine.charAt(i) == '.')) {
          if (i + 1 < taille && chaine.charAt(i + 1) == '-') {
            return false;
          }
          if (chaine.charAt(i) == '.') {
            if (!bool) {
              return false;
            }
            bool= false;
          }
          i++;
        }
        return (i == taille);
      }
      return false;
    }
    public void setValueAt(final Object value, final int row, final int col) {
      if (isNombre(value.toString())) {
        if (value.toString().charAt(0) == '.') {
          data[row][col]= "0" + value.toString();
        } else {
          data[row][col]= value;
        }
        System.out.println(
          "Vous avez rentr� "
            + value
            + " � la case ("
            + row
            + CtuluLibString.VIR
            + col
            + ").");
      } else {
        data[row][col]= "";
      }
    }
  }
  class MyTableModel1 extends AbstractTableModel {
    final String[] columnNames= { "Troncon" };
    public int getColumnCount() {
      return columnNames.length;
    }
    public int getRowCount() {
      return data1.length;
    }
    public Object getValueAt(final int row, final int col) {
      return data1[row][col];
    }
    public String getColumnName(final int col) {
      return columnNames[col];
    }
    public boolean isCellEditable(final int row, final int col) {
      return col!=0;
    }
  };
  class MyTableModel2 extends AbstractTableModel {
    final String[] columnNames= { "Unit�" };
    public int getColumnCount() {
      return columnNames.length;
    }
    public int getRowCount() {
      return data2.length;
    }
    public Object getValueAt(final int row, final int col) {
      return data2[row][col];
    }
    public String getColumnName(final int col) {
      return columnNames[col];
    }
    public boolean isCellEditable(final int row, final int col) {
      return col!=0;
    }
  };
  public void majlook(
    final int style,
    final int phase,
    final int unit,
    final Font txfont,
    final Color txback,
    final Color txfore,
    final Font lafont1,
    final Color lafore1,
    final Font lafont2,
    final Color lafore2) {
    //    int style_ =   style;
    //  int phase_ = phase;
    final int unit_= unit;
    if (unit_ == 0) {
      data2[2][0]= " t/m2";
    } else {
      data2[2][0]= " MPa";
    }
  }
}
