/*
 * @file         TaucomacDessinContent.java
 * @creation     2001-12-20
 * @modification $Date: 2006-09-19 15:08:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.taucomac;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuPanel;

import org.fudaa.dodico.corba.taucomac.SParametresTAU;
/**
 * Fenetre de dessin des donnees de taucomac
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:08:56 $ by $Author: deniger $
 * @author       Jean-Yves RIOU
 */
public class TaucomacDessinContent extends BuPanel {
  private BuButton btFermer_;
  private TaucomacGraphe graphe_;
  public TaucomacDessinContent(final SParametresTAU paramTaucoFille) {
    setLayout(new BuBorderLayout());
    setBorder(new EmptyBorder(5, 5, 5, 5));
    setSize(new Dimension(590, 650));
    graphe_= new TaucomacGraphe(paramTaucoFille);
    // graphe_.setPreferredSize(new Dimension(400,400));
    graphe_.setPreferredSize(new Dimension(580, 580));
    final BuPanel pnDes= new BuPanel();
    // pnDes.setPreferredSize(new Dimension(500,500));
    pnDes.setPreferredSize(new Dimension(590, 590));
    pnDes.setBackground(Color.white);
    pnDes.setOpaque(true);
    pnDes.add(BuBorderLayout.CENTER, graphe_);
    final BuPanel pnValider= new BuPanel();
    btFermer_= new BuButton(" Fermer ");
    btFermer_.setActionCommand("FERMER");
    pnValider.add(btFermer_);
    add(BuBorderLayout.CENTER, pnDes);
    add(BuBorderLayout.SOUTH, pnValider);
    revalidate();
    graphe_.fullRepaint();
  }
  public void addActionListener(final ActionListener _al) {
    btFermer_.addActionListener(_al);
  }
  public void fullRepaint() {
    graphe_.fullRepaint();
    System.out.println("ok******************************");
  }
}
