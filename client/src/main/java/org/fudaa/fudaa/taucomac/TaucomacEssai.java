/*
 * @file         TaucomacDonGen.java
 * @creation     2000-10-17
 * @modification $Date: 2006-09-19 15:08:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.taucomac;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.table.AbstractTableModel;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.taucomac.SCotesPrinc;
/**
 * Onglet des titre,Commentaire.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:08:56 $ by $Author: deniger $
 * @author       Jean-Yves Riou
 */
public class TaucomacEssai extends TaucomacMerePanParm implements ActionListener {

  //public TaucomacUnite     unite_;
  SCotesPrinc parametresLocal = null;
  /*
   struct SCarTronc        					//troncons de tube
   {
   reel  longueur;
   reel  epaisseur;
   reel  limElast;
   };
   typedef sequence<SCarTronc> VSCarTroncons;
   SDonGeneral parametresLocal = null;
   struct SCotesPrinc
   {
   entier         	nbTubes;  			// tubes
   reel  			zTete;  			// cotes principales
   reel  			zPied;
   reel  			zToitSup;
   reel  			zToitInf;
   entier         	nbTronconsTub;   	// nombre de troncon pour les tubes
   VSCarTroncons  	carTronc;        // caracteristiques des troncons
   };
   SCotesPrinc         cotes;
   */
  //private   TaucomacUnite          unit_;
  //private  TaucomacUnite unite_ =   new TaucomacUnite();
  JLabel label1;
  JLabel label2;
  JLabel label3;
  JLabel label4;
  JLabel unit1;
  JLabel unit2;
  JLabel unit3;
  JLabel unit4;
  BuTextField parm1;
  BuTextField parm2;
  BuTextField parm3;
  BuTextField parm4;
  JLabel label11;
  JLabel label12;
  BuTextField parm11;
  BuTextField parm12;
  JLabel unit11;
  JLabel unit12;
  TitledBorder titi;
  TitledBorder tutu;
  final Object[][] data = { { "wwtitreeeeeST n�1", "", "", "", "", "", ""},
      { "ST n�2", "", "", "", "", "", ""}, { "ST n�3", "", "", "", "", "", ""},
      { "ST n�4", "", "", "", "", "", ""}, { "ST n�5", "", "", "", "", "", ""},
      { "ST n�6", "", "", "", "", "", ""}, { "ST n�7", "", "", "", "", "", ""},
      { "ST n�8", "", "", "", "", "", ""}, { "ST n�9", "", "", "", "", "", ""},
      { "ST n�10", "", "", "", "", "", ""}};
  MyTableModel myModel = new MyTableModel();
  JTable table = new JTable(myModel);
  public TaucomacCellEditor anEditor1 = new TaucomacCellEditor();
  public TaucomacCellEditor anEditor2 = new TaucomacCellEditor();
  public TaucomacCellEditor anEditor3 = new TaucomacCellEditor();
  public TaucomacCellEditor anEditor4 = new TaucomacCellEditor();
  public TaucomacCellEditor anEditor5 = new TaucomacCellEditor();
  public TaucomacCellEditor anEditor6 = new TaucomacCellEditor();
  JPanel pnHaut;
  JPanel pnMil;
  JPanel pnMilG;
  JPanel pnMilD;
  JPanel pnBas;
  String toto;

  /*
   public void placeLabel(JPanel panel,Component label,int n)
   {
   panel.add (label,n);
   }
   */
  //====================================
  //    Constructeur
  //====================================
  public TaucomacEssai(final BuCommonInterface _appli) {
    super();
    pnHaut = new JPanel();
    final BuGridLayout loCal2 = new BuGridLayout();
    loCal2.setColumns(3);
    loCal2.setHgap(5);
    loCal2.setVgap(10);
    loCal2.setHfilled(true);
    loCal2.setCfilled(true);
    pnHaut.setLayout(loCal2);
    pnHaut.setBorder(BorderFactory.createCompoundBorder(titi, BorderFactory.createEmptyBorder(5, 5,
        5, 5)));
    // pnHaut.setPreferredSize(new Dimension(600,300));
    /*
     pnHaut.setLayout(new BorderLayout());
     pnHaut.setPreferredSize(new Dimension(600,120)); // avec gridbag  et Border
     */
    pnMil = new JPanel();
    //pnMil.setBorder(new TitledBorder("bb"));
    tutu = new TitledBorder("Tuuuuuuu");
    pnMil.setBorder(tutu);
    pnMil.setLayout(new BorderLayout());
    // pnMil.setPreferredSize(new Dimension(700,250));
    pnBas = new JPanel();
    pnBas.setBorder(BorderFactory.createCompoundBorder(BorderFactory
        .createTitledBorder(" Titretitre"), BorderFactory.createEmptyBorder(5, 120, 5, 120)));
    //pnBas.setBorder(new TitledBorder("cc"));
    pnBas.setLayout(new BorderLayout());
    pnBas.setPreferredSize(new Dimension(400, 80));
    pnMilG = new JPanel();
    pnMilG.setLayout(new BorderLayout());
    pnMilG.setBorder(new TitledBorder("ee"));
    pnMilD = new JPanel();
    pnMilD.setBorder(new TitledBorder("ff"));
    pnMilD.setLayout(new BorderLayout());
    pnMilG.setPreferredSize(new Dimension(100, 200));
    pnMilD.setPreferredSize(new Dimension(500, 200));
    pnMil.add(pnMilG, BorderLayout.WEST);
    pnMil.add(pnMilD, BorderLayout.EAST);
    //================================================
    final GridBagLayout lm = new GridBagLayout();
    final GridBagConstraints c = new GridBagConstraints();
    c.ipadx = 5;
    c.ipady = 5;
    c.gridx = 0;
    c.gridy = 0;
    placePanelG(this, pnHaut, lm, c);
    c.gridy = 1;
    placePanelG(this, pnMil, lm, c);
    c.gridy = 2;
    placePanelG(this, pnBas, lm, c);
    //=========================================== pnHaut
    //
    parm1 = BuTextField.createDoubleField();
    parm1.setHorizontalAlignment(SwingConstants.RIGHT);
    parm2 = BuTextField.createIntegerField();
    parm3 = new BuTextField();
    parm4 = new BuTextField();
    //
    final String chainw = " sdfghytc";
    label1 = new JLabel(chainw, SwingConstants.RIGHT);
    label2 = new JLabel("tataaaaa  ", SwingConstants.RIGHT);
    label3 = new JLabel("titi  ", SwingConstants.RIGHT);
    label4 = new JLabel("tutu  ", SwingConstants.RIGHT);
    unit1 = new JLabel("  mx ", SwingConstants.LEFT);
    unit2 = new JLabel("  tffaqqqq", SwingConstants.LEFT);
    unit3 = new JLabel("  thhi", SwingConstants.LEFT);
    unit4 = new JLabel("  t4ddt4", SwingConstants.LEFT);
    parm1.setColumns(10);
    //parm2.setVisible(false);
    // label2.setVisible(false);
    int n = 0;
    n = 0;
    placeCompn(pnHaut, label1, 0, n);
    System.out.println(" n  :" + n);
    n++;
    placeCompn(pnHaut, parm1, 0, n);
    System.out.println(" n  :" + n);
    n++;
    placeCompn(pnHaut, unit1, 1, n);
    System.out.println(" n  :" + n);
    n++;
    placeUnit(unit1, 1);
    placeCompn(pnHaut, label2, 1, n);
    System.out.println(" n  :" + n);
    n++;
    placeCompn(pnHaut, parm2, 2, n);
    System.out.println(" n  :" + n);
    n++;
    placeCompn(pnHaut, unit2, 2, n);
    System.out.println(" n  :" + n);
    n++;
    placeUnit(unit2, 2);
    /*
     pnHaut.add( label1,n++);
     pnHaut.add(parm1  ,n++);
     pnHaut.add(unit1  ,n++);
     pnHaut.add(label2 ,n++);
     pnHaut.add(parm2  ,n++);
     pnHaut.add(unit2  ,n++);
     */
    pnHaut.add(label3, n++);
    pnHaut.add(parm3, n++);
    pnHaut.add(unit3, n++);
    pnHaut.add(label4, n++);
    pnHaut.add(parm4, n++);
    pnHaut.add(unit4, n++);
    //================================================== pnBas
    label11 = new JLabel("toto ", SwingConstants.RIGHT);
    label12 = new JLabel("tataaaaa  ", SwingConstants.RIGHT);
    parm11 = BuTextField.createDoubleField();
    parm12 = BuTextField.createIntegerField();
    unit11 = new JLabel("  mx ", SwingConstants.LEFT);
    unit12 = new JLabel("  tffaqqqq", SwingConstants.LEFT);
    final JPanel labelPaneb = new JPanel();
    labelPaneb.setLayout(new GridLayout(0, 1));
    labelPaneb.add(label11);
    labelPaneb.add(label12);
    final JPanel fieldPaneb = new JPanel();
    fieldPaneb.setLayout(new GridLayout(0, 1));
    fieldPaneb.add(parm11);
    fieldPaneb.add(parm12);
    final JPanel unitPaneb = new JPanel();
    unitPaneb.setLayout(new GridLayout(0, 1));
    unitPaneb.add(unit11);
    unitPaneb.add(unit12);
    pnBas.add(labelPaneb, BorderLayout.WEST);
    pnBas.add(fieldPaneb, BorderLayout.CENTER);
    pnBas.add(unitPaneb, BorderLayout.EAST);
    //===================
    //    Table
    //===================
    // originaltable.setPreferredScrollableViewportSize(new Dimension(500, 160));
    //    table.setPreferredScrollableViewportSize(new Dimension(300, 110));
    table.getColumn(table.getColumnName(1)).setCellEditor(anEditor1);
    table.getColumn(table.getColumnName(2)).setCellEditor(anEditor2);
    table.getColumn(table.getColumnName(3)).setCellEditor(anEditor3);
    table.getColumn(table.getColumnName(4)).setCellEditor(anEditor4);
    table.getColumn(table.getColumnName(5)).setCellEditor(anEditor5);
    table.getColumn(table.getColumnName(6)).setCellEditor(anEditor6);
    table.setRowSelectionInterval(0, 0);
    table.setColumnSelectionInterval(1, 1);
    final JScrollPane scrollPane = new JScrollPane(table);
    (table.getTableHeader()).setReorderingAllowed(false);
    table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    //On regle la taille des colonnes
    (table.getColumn(table.getColumnName(0))).setPreferredWidth(120);
    pnMilD.add(scrollPane);
    // pnMil.add( table) ; // si pas scrooalble  (sans acenseurs)
    //====================================================================
    //initaialisation des parametres locaux
    setParametres(parametresLocal);
  }

  public void actionPerformed(final ActionEvent e){
    System.out.println("************ action performed***********");
    parametresLocal = getParametres();
    setVisible(true);
  }

  public void setParametres(final SCotesPrinc parametresProjet_){
    parametresLocal = parametresProjet_;
    if (parametresLocal != null) {
      //titre1.setText (parametresLocal.titreEtude);
      //titre2.setText (parametresLocal.comentEtude);
    }
    // relook();
  }

  public SCotesPrinc getParametres(){
    if (parametresLocal == null /*&& blocus1 && blocus2 && blocus3*/
    ) {
      parametresLocal = new SCotesPrinc();
    }
    //parametresLocal.titreEtude = titre1.getText();
    //parametresLocal.comentEtude = titre2.getText();
    return parametresLocal;
  }

  //===============================================
  class MyTableModel extends AbstractTableModel {

    final String[] columnNames = { "   ", "aa ", "bb", "cc", "dd", "ee", "ff",};

    public int getColumnCount(){
      return columnNames.length;
    }

    public int getRowCount(){
      return data.length;
    }

    public String getColumnName(final int col){
      return columnNames[col];
    }

    public Object getValueAt(final int row,final int col){
      return data[row][col];
    }

    /*public Class getColumnClass(int c) {
     return getValueAt(0, c).getClass();
     }*/
    public boolean isCellEditable(final int row,final int col){
      return col >= 1;
    }

    public boolean isNombre(final String chaine){
      final int taille = chaine.length();
      if (taille != 0) {
        int i = 0;
        boolean bool = true;
        while (i < taille
          && (chaine.charAt(i) >= '0' && chaine.charAt(i) <= '9' || chaine.charAt(i) == '-' || chaine
              .charAt(i) == '.')) {
          if (i + 1 < taille && chaine.charAt(i + 1) == '-') {
            return false;
          }
          if (chaine.charAt(i) == '.') {
            if (!bool) {
              return false;
            }
            bool = false;
          }
          i++;
        }
        return (i == taille);
      }
      return false;
    }

    public void setValueAt(final Object value,final int row,final int col){
      if (isNombre(value.toString())) {
        if (value.toString().charAt(0) == '.') {
          data[row][col] = "0" + value.toString();
        } else {
          data[row][col] = value;
        //System.out.println("Vous avez rentr� "+value+" � la case ("+row+","+col+").");
        }
      } else {
        data[row][col] = "";
      }
    }
  }
}
