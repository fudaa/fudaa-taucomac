/*
 * @file         TaucomacDessin.java
 * @creation     2001-12-20
 * @modification $Date: 2006-09-19 15:08:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.taucomac;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialog;
/**
 * Fenetre de dessin des donnees de taucomac
 *
 * @version      $Id: TaucomacDessinNew.java,v 1.6 2006-09-19 15:08:56 deniger Exp $
 * @author       Jean-Yves RIOU
 */
public class TaucomacDessinNew extends BuDialog implements ActionListener {
  private TaucomacDessinContent content_;
  public TaucomacDessinNew(
    final BuCommonInterface _appli,
    final TaucomacDessinContent _content) {
    super(_appli, _appli.getInformationsSoftware(), "Dessin", _content);
    setSize(600, 700);
    content_= _content;
    content_.fullRepaint();
    content_.addActionListener(this);
  }
  public void actionPerformed(final ActionEvent _ae) {
    final String commande= _ae.getActionCommand();
    System.out.println(commande);
    if ("FERMER".equals(commande)) {
      dispose();
    }
  }
  public JComponent getComponent() {
    return content_;
  }
}
