/*
 * @file         TaucomacSolRupt.java
 * @creation     2000-10-17
 * @modification $Date: 2006-09-19 15:08:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.taucomac;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.taucomac.SRupture;
import org.fudaa.dodico.corba.taucomac.SSolRupt;
/**
 * Onglet du  Sol � la Rupture.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:08:57 $ by $Author: deniger $
 * @author       Jean-Yves Riou
 */
public class TaucomacSolRupt
  extends TaucomacMerePanParm
  implements ActionListener, FocusListener {
  SRupture parametresLocal= null;
  /*
   struct SSolRupt     							//  couches de sol a la rupture
  {
   reel zToit;             // attention vide pour couche 1 et pour Dr(ainees)
   reel poidsPropre;
   reel angleFrot;
   reel cohesion;
  };
  typedef sequence<SSolRupt> VSSolRupts;
  struct SRupture
  {
  entier  		    nbCouchesRupt; 		// sol � la rupture     (ncouch <= 2)
   reel 			coefPartButFav;
   reel 			coefPartButDefav;
   reel 			coefPartCohetFav;
   reel 			coefPartCoheDefav;
   VSSolRupts      couchesRupt;        // non drainees court terme
   VSSolRupts      couchesRuptDr;      // drainees amarrage
  };

  SRupture            rupture;

  */
  public int nbMaxCouchRupt_= 3;
  //
  JPanel pnHaut;
  JPanel pnMil;
  JPanel pnBas;
  //
  JLabel la_nbcouch;
  BuTextField tf_nbcouch;
  JLabel bid0;
  //
  JLabel la_sol1; // sol1 ===========
  JLabel la_bids12;
  JLabel la_bids13;
  JLabel la_bids14;
  JLabel la_bids15;
  JLabel la_bids16;
  JLabel la_ztoit1;
  JLabel la_pp1;
  JLabel la_af1;
  JLabel la_cohe1;
  BuTextField tf_ztoit1; // sol1 premiere colonne
  BuTextField tf_pp1;
  BuTextField tf_af1;
  BuTextField tf_cohe1;
  JLabel unit_ztoit1;
  JLabel unit_pp1;
  JLabel unit_af1;
  JLabel unit_cohe1;
  JLabel bid11;
  JLabel bid12;
  JLabel bid13;
  JLabel bid14;
  BuTextField tf_ztoitdr1; // sol1 deuxieme colonne (dr)
  BuTextField tf_ppdr1;
  BuTextField tf_afdr1;
  BuTextField tf_cohedr1;
  JLabel unit_ztoitdr1;
  JLabel unit_ppdr1;
  JLabel unit_afdr1;
  JLabel unit_cohedr1;
  JLabel la_sol2; // sol2 ===========
  JLabel la_bids22;
  JLabel la_bids23;
  JLabel la_bids24;
  JLabel la_bids25;
  JLabel la_bids26;
  JLabel la_ztoit2;
  JLabel la_pp2;
  JLabel la_af2;
  JLabel la_cohe2;
  BuTextField tf_ztoit2; // sol2 premiere colonne
  BuTextField tf_pp2;
  BuTextField tf_af2;
  BuTextField tf_cohe2;
  JLabel unit_ztoit2;
  JLabel unit_pp2;
  JLabel unit_af2;
  JLabel unit_cohe2;
  JLabel bid21;
  JLabel bid22;
  JLabel bid23;
  JLabel bid24;
  BuTextField tf_ztoitdr2; // sol2 deuxieme colonne (dr)
  BuTextField tf_ppdr2;
  BuTextField tf_afdr2;
  BuTextField tf_cohedr2;
  JLabel unit_ztoitdr2;
  JLabel unit_ppdr2;
  JLabel unit_afdr2;
  JLabel unit_cohedr2;
  //
  JLabel la_coebut;
  JLabel bid31;
  JLabel la_butfav;
  BuTextField tf_butfav;
  JLabel la_butdef;
  BuTextField tf_butdef;
  //
  JLabel la_coecohe;
  JLabel bid32;
  JLabel la_cohefav;
  BuTextField tf_cohefav;
  JLabel la_cohedef;
  BuTextField tf_cohedef;
  //====================================
  //    Constructeur
  //====================================
  public TaucomacSolRupt(final BuCommonInterface _appli) {
    super();
    //======================================
    pnHaut= new JPanel();
    final BuGridLayout loCal4= new BuGridLayout();
    loCal4.setColumns(3);
    loCal4.setHgap(5);
    loCal4.setVgap(5);
    loCal4.setHfilled(true);
    loCal4.setCfilled(true);
    pnHaut.setLayout(loCal4);
    pnHaut.setBorder(new EmptyBorder(5, 5, 5, 400)); //top,left,bottom,right
    la_nbcouch= new JLabel("Nombre de couches du sol ", SwingConstants.LEFT);
    tf_nbcouch= BuTextField.createIntegerField();
    tf_nbcouch.setHorizontalAlignment(SwingConstants.RIGHT);
    bid0= new JLabel("  ", SwingConstants.CENTER);
    tf_nbcouch.addFocusListener(this);
    tf_nbcouch.setColumns(4);
    int n;
    n= 0;
    placeCompn(pnHaut, la_nbcouch, 0, n);
    n++;
    placeCompn(pnHaut, tf_nbcouch, 0, n);
    n++;
    placeCompn(pnHaut, bid0, 0, n);
    n++;
    //======================================
    pnBas= new JPanel();
    final BuGridLayout loCal5= new BuGridLayout();
    loCal5.setColumns(3);
    loCal5.setHgap(5);
    loCal5.setVgap(5);
    loCal5.setHfilled(true);
    loCal5.setCfilled(true);
    pnBas.setLayout(loCal5);
    pnBas.setBorder(new EmptyBorder(5, 5, 5, 120)); //top,left,bottom,right
    la_coebut=
      new JLabel(
        "Coefficient partiel sur le coefficicient de butee ",
        SwingConstants.LEFT);
    la_coebut.setForeground(Color.blue);
    la_butfav=
      new JLabel("  dans le cas d 'une butee favorable ", SwingConstants.RIGHT);
    tf_butfav= BuTextField.createDoubleField();
    tf_butfav.setHorizontalAlignment(SwingConstants.RIGHT);
    la_butdef=
      new JLabel("dans le cas d 'une butee defavorable ", SwingConstants.RIGHT);
    tf_butdef= BuTextField.createDoubleField();
    ;
    tf_butdef.setHorizontalAlignment(SwingConstants.RIGHT);
    la_coecohe= new JLabel("Coefficient partiel sur la cohesion ", SwingConstants.LEFT);
    la_coecohe.setForeground(Color.blue);
    la_cohefav=
      new JLabel("  dans le cas d 'une cohesion favorable ", SwingConstants.RIGHT);
    tf_cohefav= BuTextField.createDoubleField();
    tf_cohefav.setHorizontalAlignment(SwingConstants.RIGHT);
    la_cohedef=
      new JLabel("dans le cas d 'une cohesion defavorable ", SwingConstants.RIGHT);
    tf_cohedef= BuTextField.createDoubleField();
    ;
    tf_cohedef.setHorizontalAlignment(SwingConstants.RIGHT);
    bid31= new JLabel("   ", SwingConstants.RIGHT);
    bid32= new JLabel("   ", SwingConstants.RIGHT);
    tf_butfav.setColumns(6);
    n= 0;
    placeCompn(pnBas, la_coebut, 2, n);
    n++;
    placeCompn(pnBas, la_butfav, 2, n);
    n++;
    placeCompn(pnBas, tf_butfav, 2, n);
    n++;
    placeCompn(pnBas, bid31, 2, n);
    n++;
    placeCompn(pnBas, la_butdef, 2, n);
    n++;
    placeCompn(pnBas, tf_butdef, 2, n);
    n++;
    placeCompn(pnBas, la_coecohe, 2, n);
    n++;
    placeCompn(pnBas, la_cohefav, 2, n);
    n++;
    placeCompn(pnBas, tf_cohefav, 2, n);
    n++;
    placeCompn(pnBas, bid32, 2, n);
    n++;
    placeCompn(pnBas, la_cohedef, 2, n);
    n++;
    placeCompn(pnBas, tf_cohedef, 2, n);
    //===================================
    pnMil= new JPanel();
    final BuGridLayout loCal6= new BuGridLayout();
    loCal6.setColumns(6);
    loCal6.setHgap(5);
    loCal6.setVgap(10);
    loCal6.setHfilled(true);
    loCal6.setCfilled(true);
    pnMil.setLayout(loCal6);
    pnMil.setBorder(new EmptyBorder(5, 10, 5, 10)); //top,left,bottom,right
    //pnBas.setPreferredSize(new Dimension(650,120));
    //  sol1
    la_sol1= new JLabel("Sol1 ", SwingConstants.LEFT);
    la_sol1.setForeground(Color.blue);
    la_bids12= new JLabel("   ", SwingConstants.CENTER);
    la_bids13= new JLabel("   ", SwingConstants.CENTER);
    la_bids14= new JLabel("   ", SwingConstants.CENTER);
    la_bids15= new JLabel("   ", SwingConstants.CENTER);
    la_bids16= new JLabel("   ", SwingConstants.CENTER);
    la_ztoit1= new JLabel("                          ", SwingConstants.RIGHT);
    la_pp1= new JLabel("       poids propre       ", SwingConstants.RIGHT);
    la_af1= new JLabel("angle de frottement       ", SwingConstants.RIGHT);
    la_cohe1= new JLabel("           cohesion       ", SwingConstants.RIGHT);
    //
    tf_ztoit1= BuTextField.createDoubleField();
    tf_ztoit1.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_pp1= BuTextField.createDoubleField();
    tf_pp1.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_af1= BuTextField.createDoubleField();
    tf_af1.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_cohe1= BuTextField.createDoubleField();
    tf_cohe1.setHorizontalAlignment(SwingConstants.RIGHT);
    unit_ztoit1= new JLabel("   ", SwingConstants.LEFT);
    unit_pp1= new JLabel("t/m3   ", SwingConstants.LEFT);
    unit_af1= new JLabel("� (degre)   ", SwingConstants.LEFT);
    unit_cohe1= new JLabel("t/m3   ", SwingConstants.LEFT);
    //
    bid11= new JLabel("   ", SwingConstants.CENTER);
    bid12= new JLabel("   ", SwingConstants.CENTER);
    bid13= new JLabel("   ", SwingConstants.CENTER);
    bid14= new JLabel("   ", SwingConstants.CENTER);
    //
    tf_ztoitdr1= BuTextField.createDoubleField();
    tf_ztoitdr1.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_ppdr1= BuTextField.createDoubleField();
    tf_ppdr1.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_afdr1= BuTextField.createDoubleField();
    tf_afdr1.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_cohedr1= BuTextField.createDoubleField();
    tf_cohedr1.setHorizontalAlignment(SwingConstants.RIGHT);
    unit_ztoitdr1= new JLabel("   ", SwingConstants.LEFT);
    unit_ppdr1= new JLabel("t/m3   ", SwingConstants.LEFT);
    unit_afdr1= new JLabel("� (degre)   ", SwingConstants.LEFT);
    unit_cohedr1= new JLabel("t/m3   ", SwingConstants.LEFT);
    //
    //  sol2
    la_sol2= new JLabel("Sol2 ", SwingConstants.LEFT);
    la_sol2.setForeground(Color.blue);
    la_bids22= new JLabel("   ", SwingConstants.CENTER);
    la_bids23= new JLabel("   ", SwingConstants.CENTER);
    la_bids24= new JLabel("   ", SwingConstants.CENTER);
    la_bids25= new JLabel("   ", SwingConstants.CENTER);
    la_bids26= new JLabel("   ", SwingConstants.CENTER);
    la_ztoit2= new JLabel("Cote du toit de la couche ", SwingConstants.RIGHT);
    la_pp2= new JLabel("             poids propre ", SwingConstants.RIGHT);
    la_af2= new JLabel("      angle de frottement ", SwingConstants.RIGHT);
    la_cohe2= new JLabel("                 cohesion ", SwingConstants.RIGHT);
    //
    tf_ztoit2= BuTextField.createDoubleField();
    tf_ztoit2.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_pp2= BuTextField.createDoubleField();
    tf_pp2.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_af2= BuTextField.createDoubleField();
    tf_af2.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_cohe2= BuTextField.createDoubleField();
    tf_cohe2.setHorizontalAlignment(SwingConstants.RIGHT);
    unit_ztoit2= new JLabel("m   ", SwingConstants.LEFT);
    unit_pp2= new JLabel("t/m3   ", SwingConstants.LEFT);
    unit_af2= new JLabel("� (degre)   ", SwingConstants.LEFT);
    unit_cohe2= new JLabel("t/m3   ", SwingConstants.LEFT);
    //
    bid21= new JLabel("   ", SwingConstants.CENTER);
    bid22= new JLabel("   ", SwingConstants.CENTER);
    bid23= new JLabel("   ", SwingConstants.CENTER);
    bid24= new JLabel("   ", SwingConstants.CENTER);
    //
    tf_ztoitdr2= BuTextField.createDoubleField();
    tf_ztoitdr2.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_ppdr2= BuTextField.createDoubleField();
    tf_ppdr2.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_afdr2= BuTextField.createDoubleField();
    tf_afdr2.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_cohedr2= BuTextField.createDoubleField();
    tf_cohedr2.setHorizontalAlignment(SwingConstants.RIGHT);
    unit_ztoitdr2= new JLabel("   ", SwingConstants.LEFT);
    unit_ppdr2= new JLabel("t/m3   ", SwingConstants.LEFT);
    unit_afdr2= new JLabel("� (degre)   ", SwingConstants.LEFT);
    unit_cohedr2= new JLabel("t/m3   ", SwingConstants.LEFT);
    //
    placeUnit(unit_pp1, 9);
    placeUnit(unit_af1, 5);
    placeUnit(unit_cohe1, 9);
    placeUnit(unit_ppdr1, 9);
    placeUnit(unit_afdr1, 5);
    placeUnit(unit_cohedr1, 9);
    placeUnit(unit_ztoit2, 1);
    placeUnit(unit_pp2, 9);
    placeUnit(unit_af2, 5);
    placeUnit(unit_cohe2, 9);
    placeUnit(unit_ppdr2, 9);
    placeUnit(unit_afdr2, 5);
    placeUnit(unit_cohedr2, 9);
    //
    tf_ztoit1.setColumns(6);
    tf_ztoitdr1.setColumns(6);
    //
    n= 0;
    placeCompn(pnMil, la_sol1, 0, n);
    n++;
    placeCompn(pnMil, tf_ztoit1, 0, n);
    n++;
    placeCompn(pnMil, unit_ztoit1, 0, n);
    n++;
    placeCompn(pnMil, bid11, 0, n);
    n++;
    placeCompn(pnMil, tf_ztoitdr1, 0, n);
    n++;
    placeCompn(pnMil, unit_ztoitdr1, 0, n);
    n++;
    placeCompn(pnMil, la_pp1, 0, n);
    n++;
    placeCompn(pnMil, tf_pp1, 0, n);
    n++;
    placeCompn(pnMil, unit_pp1, 0, n);
    n++;
    placeCompn(pnMil, bid12, 0, n);
    n++;
    placeCompn(pnMil, tf_ppdr1, 0, n);
    n++;
    placeCompn(pnMil, unit_ppdr1, 0, n);
    n++;
    placeCompn(pnMil, la_af1, 0, n);
    n++;
    placeCompn(pnMil, tf_af1, 0, n);
    n++;
    placeCompn(pnMil, unit_af1, 0, n);
    n++;
    placeCompn(pnMil, bid13, 0, n);
    n++;
    placeCompn(pnMil, tf_afdr1, 0, n);
    n++;
    placeCompn(pnMil, unit_afdr1, 0, n);
    n++;
    placeCompn(pnMil, la_cohe1, 0, n);
    n++;
    placeCompn(pnMil, tf_cohe1, 0, n);
    n++;
    placeCompn(pnMil, unit_cohe1, 0, n);
    n++;
    placeCompn(pnMil, bid14, 0, n);
    n++;
    placeCompn(pnMil, tf_cohedr1, 0, n);
    n++;
    placeCompn(pnMil, unit_cohedr1, 0, n);
    n++;
    //
    placeCompn(pnMil, la_sol2, 0, n);
    n++;
    placeCompn(pnMil, la_bids22, 0, n);
    n++;
    placeCompn(pnMil, la_bids23, 0, n);
    n++;
    placeCompn(pnMil, la_bids24, 0, n);
    n++;
    placeCompn(pnMil, la_bids25, 0, n);
    n++;
    placeCompn(pnMil, la_bids26, 0, n);
    n++;
    placeCompn(pnMil, la_ztoit2, 0, n);
    n++;
    placeCompn(pnMil, tf_ztoit2, 0, n);
    n++;
    placeCompn(pnMil, unit_ztoit2, 0, n);
    n++;
    placeCompn(pnMil, bid21, 0, n);
    n++;
    placeCompn(pnMil, tf_ztoitdr2, 0, n);
    n++;
    placeCompn(pnMil, unit_ztoitdr2, 0, n);
    n++;
    placeCompn(pnMil, la_pp2, 0, n);
    n++;
    placeCompn(pnMil, tf_pp2, 0, n);
    n++;
    placeCompn(pnMil, unit_pp2, 0, n);
    n++;
    placeCompn(pnMil, bid22, 0, n);
    n++;
    placeCompn(pnMil, tf_ppdr2, 0, n);
    n++;
    placeCompn(pnMil, unit_ppdr2, 0, n);
    n++;
    placeCompn(pnMil, la_af2, 0, n);
    n++;
    placeCompn(pnMil, tf_af2, 0, n);
    n++;
    placeCompn(pnMil, unit_af2, 0, n);
    n++;
    placeCompn(pnMil, bid23, 0, n);
    n++;
    placeCompn(pnMil, tf_afdr2, 0, n);
    n++;
    placeCompn(pnMil, unit_afdr2, 0, n);
    n++;
    placeCompn(pnMil, la_cohe2, 0, n);
    n++;
    placeCompn(pnMil, tf_cohe2, 0, n);
    n++;
    placeCompn(pnMil, unit_cohe2, 0, n);
    n++;
    placeCompn(pnMil, bid24, 0, n);
    n++;
    placeCompn(pnMil, tf_cohedr2, 0, n);
    n++;
    placeCompn(pnMil, unit_cohedr2, 0, n);
    n++;
    tf_ztoit1.setVisible(false);
    unit_ztoit1.setVisible(false);
    tf_ztoitdr1.setVisible(false);
    unit_ztoitdr1.setVisible(false);
    tf_ztoitdr2.setVisible(false);
    unit_ztoitdr2.setVisible(false);
    //====================================
    //====================================
    this.setLayout(new BorderLayout());
    this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    this.add(pnHaut, BorderLayout.NORTH);
    this.add(pnMil, BorderLayout.CENTER);
    this.add(pnBas, BorderLayout.SOUTH);
    //initialisation des parametres locaux
    setParametres(parametresLocal);
  }
  public void focusLost(final FocusEvent e) {
    final Object src= e.getSource();
    //System.out.println("filleparam focusLost  "+e + " "+src );
    if (src instanceof JTextField) {
      if (src == tf_nbcouch) {
        //System.out.println("focusLost source " + src);
        majsol();
      }
    }
  }
  public void focusGained(final FocusEvent e) {
    //   	  Object src=e.getSource();
    //     System.out.println("filleparam focusGained  "+e + " "+src);
    majsol();
  }
  public void actionPerformed(final ActionEvent e) {
    //     System.out.println("action performed  ");
    //  	 Object src=e.getSource();
    //     System.out.println("action performed  "+e + " "+src );
    parametresLocal= getParametres();
    setVisible(true);
  }
  public void setParametres(final SRupture parametresProjet_) {
    //System.out.println("setParametres" );
    parametresLocal= parametresProjet_;
    if (parametresLocal != null) {
      if (parametresLocal.nbCouchesRupt > 0) {
        tf_nbcouch.setValue(new Integer(parametresLocal.nbCouchesRupt));
      } else {
        tf_nbcouch.setValue("");
      }
      majsol();
      if (parametresLocal.couchesRupt[0].zToit != -999.) {
        tf_ztoit1.setValue(new Double(parametresLocal.couchesRupt[0].zToit));
      } else {
        tf_ztoit1.setValue("");
      }
      if (parametresLocal.couchesRuptDr[0].zToit != -999.) {
        tf_ztoitdr1.setValue(
          new Double(parametresLocal.couchesRuptDr[0].zToit));
      } else {
        tf_ztoitdr1.setValue("");
      }
      if (parametresLocal.couchesRupt[0].poidsPropre >= 0) {
        tf_pp1.setValue(new Double(parametresLocal.couchesRupt[0].poidsPropre));
      } else {
        tf_pp1.setValue("");
      }
      if (parametresLocal.couchesRuptDr[0].poidsPropre >= 0) {
        tf_ppdr1.setValue(
          new Double(parametresLocal.couchesRuptDr[0].poidsPropre));
      } else {
        tf_ppdr1.setValue("");
      }
      if (parametresLocal.couchesRupt[0].angleFrot >= 0) {
        tf_af1.setValue(new Double(parametresLocal.couchesRupt[0].angleFrot));
      } else {
        tf_af1.setValue("");
      }
      if (parametresLocal.couchesRuptDr[0].angleFrot >= 0) {
        tf_afdr1.setValue(
          new Double(parametresLocal.couchesRuptDr[0].angleFrot));
      } else {
        tf_afdr1.setValue("");
      }
      if (parametresLocal.couchesRupt[0].cohesion >= 0) {
        tf_cohe1.setValue(new Double(parametresLocal.couchesRupt[0].cohesion));
      } else {
        tf_cohe1.setValue("");
      }
      if (parametresLocal.couchesRuptDr[0].cohesion >= 0) {
        tf_cohedr1.setValue(
          new Double(parametresLocal.couchesRuptDr[0].cohesion));
      } else {
        tf_cohedr1.setValue("");
      }
      if (parametresLocal.couchesRupt[1].zToit != -999.) {
        tf_ztoit2.setValue(new Double(parametresLocal.couchesRupt[1].zToit));
      } else {
        tf_ztoit2.setValue("");
      }
      if (parametresLocal.couchesRuptDr[1].zToit != -999.) {
        tf_ztoitdr2.setValue(
          new Double(parametresLocal.couchesRuptDr[1].zToit));
      } else {
        tf_ztoitdr2.setValue("");
      }
      if (parametresLocal.couchesRupt[1].poidsPropre >= 0) {
        tf_pp2.setValue(new Double(parametresLocal.couchesRupt[1].poidsPropre));
      } else {
        tf_pp2.setValue("");
      }
      if (parametresLocal.couchesRuptDr[1].poidsPropre >= 0) {
        tf_ppdr2.setValue(
          new Double(parametresLocal.couchesRuptDr[1].poidsPropre));
      } else {
        tf_ppdr2.setValue("");
      }
      if (parametresLocal.couchesRupt[1].angleFrot >= 0) {
        tf_af2.setValue(new Double(parametresLocal.couchesRupt[1].angleFrot));
      } else {
        tf_af2.setValue("");
      }
      if (parametresLocal.couchesRuptDr[1].angleFrot >= 0) {
        tf_afdr2.setValue(
          new Double(parametresLocal.couchesRuptDr[1].angleFrot));
      } else {
        tf_afdr2.setValue("");
      }
      if (parametresLocal.couchesRupt[1].cohesion >= 0) {
        tf_cohe2.setValue(new Double(parametresLocal.couchesRupt[1].cohesion));
      } else {
        tf_cohe2.setValue("");
      }
      if (parametresLocal.couchesRuptDr[1].cohesion >= 0) {
        tf_cohedr2.setValue(
          new Double(parametresLocal.couchesRuptDr[1].cohesion));
      } else {
        tf_cohedr2.setValue("");
      }
      if (parametresLocal.coefPartButFav >= 0) {
        tf_butfav.setValue(new Double(parametresLocal.coefPartButFav));
      } else {
        tf_butfav.setValue("");
      }
      if (parametresLocal.coefPartButDefav >= 0) {
        tf_butdef.setValue(new Double(parametresLocal.coefPartButDefav));
      } else {
        tf_butdef.setValue("");
      }
      if (parametresLocal.coefPartCohetFav >= 0) {
        tf_cohefav.setValue(new Double(parametresLocal.coefPartCohetFav));
      } else {
        tf_cohefav.setValue("");
      }
      if (parametresLocal.coefPartCoheDefav >= 0) {
        tf_cohedef.setValue(new Double(parametresLocal.coefPartCoheDefav));
      } else {
        tf_cohedef.setValue("");
      }
    }
  }
  public SRupture getParametres() {
    //System.out.println("getParametres" );
    if (parametresLocal == null) {
      parametresLocal= new SRupture();
      parametresLocal.couchesRupt= new SSolRupt[nbMaxCouchRupt_];
      parametresLocal.couchesRuptDr= new SSolRupt[nbMaxCouchRupt_];
      //System.out.println(" parametresLocal = "+ parametresLocal);
      for (int i= 0; i < nbMaxCouchRupt_; i++) {
        parametresLocal.couchesRupt[i]= new SSolRupt();
        parametresLocal.couchesRuptDr[i]= new SSolRupt();
      }
    }
    String changed= "";
    double tmpD= 0.;
    int tmpI= 0;
    String tmpS= "";
    final String blanc= "";
    tmpI= parametresLocal.nbCouchesRupt;
    tmpS= tf_nbcouch.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.nbCouchesRupt= 0;
    } else {
      parametresLocal.nbCouchesRupt= Integer.parseInt(tmpS);
    }
    if (parametresLocal.nbCouchesRupt != tmpI) {
      changed += "nbCouchesRupt|";
    }
    tmpD= parametresLocal.couchesRupt[0].zToit;
    tmpS= tf_ztoit1.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.couchesRupt[0].zToit= -999.;
    } else {
      parametresLocal.couchesRupt[0].zToit= Double.parseDouble(tmpS);
    }
    if (parametresLocal.couchesRupt[0].zToit != tmpD) {
      changed += "couchesRupt[0].zToit|";
    }
    tmpD= parametresLocal.couchesRuptDr[0].zToit;
    tmpS= tf_ztoitdr1.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.couchesRuptDr[0].zToit= -999.;
    } else {
      parametresLocal.couchesRuptDr[0].zToit= Double.parseDouble(tmpS);
    }
    if (parametresLocal.couchesRuptDr[0].zToit != tmpD) {
      changed += "couchesRuptDr[0].zToit|";
    }
    tmpD= parametresLocal.couchesRupt[0].poidsPropre;
    tmpS= tf_pp1.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.couchesRupt[0].poidsPropre= -999.;
    } else {
      parametresLocal.couchesRupt[0].poidsPropre= Double.parseDouble(tmpS);
    }
    if (parametresLocal.couchesRupt[0].poidsPropre != tmpD) {
      changed += "couchesRupt[0].poidsPropre|";
    }
    tmpD= parametresLocal.couchesRuptDr[0].poidsPropre;
    tmpS= tf_ppdr1.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.couchesRuptDr[0].poidsPropre= -999.;
    } else {
      parametresLocal.couchesRuptDr[0].poidsPropre= Double.parseDouble(tmpS);
    }
    if (parametresLocal.couchesRuptDr[0].poidsPropre != tmpD) {
      changed += "couchesRuptDr[0].poidsPropre|";
    }
    tmpD= parametresLocal.couchesRupt[0].angleFrot;
    tmpS= tf_af1.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.couchesRupt[0].angleFrot= -999.;
    } else {
      parametresLocal.couchesRupt[0].angleFrot= Double.parseDouble(tmpS);
    }
    if (parametresLocal.couchesRupt[0].angleFrot != tmpD) {
      changed += "couchesRupt[0].angleFrot|";
    }
    tmpD= parametresLocal.couchesRuptDr[0].angleFrot;
    tmpS= tf_afdr1.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.couchesRuptDr[0].angleFrot= -999.;
    } else {
      parametresLocal.couchesRuptDr[0].angleFrot= Double.parseDouble(tmpS);
    }
    if (parametresLocal.couchesRuptDr[0].angleFrot != tmpD) {
      changed += "couchesRuptDr[0].angleFrot|";
    }
    tmpD= parametresLocal.couchesRupt[0].cohesion;
    tmpS= tf_cohe1.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.couchesRupt[0].cohesion= -999.;
    } else {
      parametresLocal.couchesRupt[0].cohesion= Double.parseDouble(tmpS);
    }
    if (parametresLocal.couchesRupt[0].cohesion != tmpD) {
      changed += "couchesRupt[0].cohesion|";
    }
    tmpD= parametresLocal.couchesRuptDr[0].cohesion;
    tmpS= tf_cohedr1.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.couchesRuptDr[0].cohesion= -999.;
    } else {
      parametresLocal.couchesRuptDr[0].cohesion= Double.parseDouble(tmpS);
    }
    if (parametresLocal.couchesRuptDr[0].cohesion != tmpD) {
      changed += "couchesRuptDr[0].cohesion|";
    }
    //
    tmpD= parametresLocal.couchesRupt[1].zToit;
    tmpS= tf_ztoit2.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.couchesRupt[1].zToit= -999.;
    } else {
      parametresLocal.couchesRupt[1].zToit= Double.parseDouble(tmpS);
    }
    if (parametresLocal.couchesRupt[1].zToit != tmpD) {
      changed += "couchesRupt[1].zToit|";
    }
    tmpD= parametresLocal.couchesRuptDr[1].zToit;
    tmpS= tf_ztoitdr2.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.couchesRuptDr[1].zToit= -999.;
    } else {
      parametresLocal.couchesRuptDr[1].zToit= Double.parseDouble(tmpS);
    }
    if (parametresLocal.couchesRuptDr[1].zToit != tmpD) {
      changed += "couchesRuptDr[1].zToit|";
    }
    tmpD= parametresLocal.couchesRupt[1].poidsPropre;
    tmpS= tf_pp2.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.couchesRupt[1].poidsPropre= -999.;
    } else {
      parametresLocal.couchesRupt[1].poidsPropre= Double.parseDouble(tmpS);
    }
    if (parametresLocal.couchesRupt[1].poidsPropre != tmpD) {
      changed += "couchesRupt[1].poidsPropre|";
    }
    tmpD= parametresLocal.couchesRuptDr[1].poidsPropre;
    tmpS= tf_ppdr2.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.couchesRuptDr[1].poidsPropre= -999.;
    } else {
      parametresLocal.couchesRuptDr[1].poidsPropre= Double.parseDouble(tmpS);
    }
    if (parametresLocal.couchesRuptDr[1].poidsPropre != tmpD) {
      changed += "couchesRuptDr[1].poidsPropre|";
    }
    tmpD= parametresLocal.couchesRupt[1].angleFrot;
    tmpS= tf_af2.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.couchesRupt[1].angleFrot= -999.;
    } else {
      parametresLocal.couchesRupt[1].angleFrot= Double.parseDouble(tmpS);
    }
    if (parametresLocal.couchesRupt[1].angleFrot != tmpD) {
      changed += "couchesRupt[1].angleFrot|";
    }
    tmpD= parametresLocal.couchesRuptDr[1].angleFrot;
    tmpS= tf_afdr2.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.couchesRuptDr[1].angleFrot= -999.;
    } else {
      parametresLocal.couchesRuptDr[1].angleFrot= Double.parseDouble(tmpS);
    }
    if (parametresLocal.couchesRuptDr[1].angleFrot != tmpD) {
      changed += "couchesRuptDr[1].angleFrot|";
    }
    tmpD= parametresLocal.couchesRupt[1].cohesion;
    tmpS= tf_cohe2.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.couchesRupt[1].cohesion= -999.;
    } else {
      parametresLocal.couchesRupt[1].cohesion= Double.parseDouble(tmpS);
    }
    if (parametresLocal.couchesRupt[1].cohesion != tmpD) {
      changed += "couchesRupt[1].cohesion|";
    }
    tmpD= parametresLocal.couchesRuptDr[1].cohesion;
    tmpS= tf_cohedr2.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.couchesRuptDr[1].cohesion= -999.;
    } else {
      parametresLocal.couchesRuptDr[1].cohesion= Double.parseDouble(tmpS);
    }
    if (parametresLocal.couchesRuptDr[1].cohesion != tmpD) {
      changed += "couchesRuptDr[1].cohesion|";
    }
    tmpD= parametresLocal.coefPartButFav;
    tmpS= tf_butfav.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.coefPartButFav= -999.;
    } else {
      parametresLocal.coefPartButFav= Double.parseDouble(tmpS);
    }
    if (parametresLocal.coefPartButFav != tmpD) {
      changed += "butfav|";
    }
    tmpD= parametresLocal.coefPartButDefav;
    tmpS= tf_butdef.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.coefPartButDefav= -999.;
    } else {
      parametresLocal.coefPartButDefav= Double.parseDouble(tmpS);
    }
    if (parametresLocal.coefPartButDefav != tmpD) {
      changed += "butdef|";
    }
    tmpD= parametresLocal.coefPartCohetFav;
    tmpS= tf_cohefav.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.coefPartCohetFav= -999.;
    } else {
      parametresLocal.coefPartCohetFav= Double.parseDouble(tmpS);
    }
    if (parametresLocal.coefPartCohetFav != tmpD) {
      changed += "butfav|";
    }
    tmpD= parametresLocal.coefPartCoheDefav;
    tmpS= tf_cohedef.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.coefPartCoheDefav= -999.;
    } else {
      parametresLocal.coefPartCoheDefav= Double.parseDouble(tmpS);
    }
    if (parametresLocal.coefPartCoheDefav != tmpD) {
      changed += "butfav|";
    }
    //     StringTokenizer tok = new StringTokenizer(changed, "|");
    //  while( tok.hasMoreTokens() ) {
    //  FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(new FudaaParamEvent(
    //  this, 0, TaucomacResource.TAUCOMAC01,parametresLocal  , TaucomacResource.TAUCOMAC01+":"+tok.nextToken()));
    return parametresLocal;
  }
  public void majsol() {
    String tmpS= "";
    final String blanc= "";
    int n= 0;
    tmpS= tf_nbcouch.getText();
    if (tmpS.equals(blanc)) {
      n= 0;
    } else {
      n= Integer.parseInt(tmpS);
    }
    System.out.println(" ========= n " + n);
    la_sol1.setVisible(false);
    la_ztoit1.setVisible(false);
    la_pp1.setVisible(false);
    la_af1.setVisible(false);
    la_cohe1.setVisible(false);
    tf_ztoit1.setVisible(false);
    tf_pp1.setVisible(false);
    tf_af1.setVisible(false);
    tf_cohe1.setVisible(false);
    unit_ztoit1.setVisible(false);
    unit_pp1.setVisible(false);
    unit_af1.setVisible(false);
    unit_cohe1.setVisible(false);
    tf_ztoitdr1.setVisible(false);
    tf_ppdr1.setVisible(false);
    tf_afdr1.setVisible(false);
    tf_cohedr1.setVisible(false);
    unit_ztoitdr1.setVisible(false);
    unit_ppdr1.setVisible(false);
    unit_afdr1.setVisible(false);
    unit_cohedr1.setVisible(false);
    la_sol2.setVisible(false);
    la_ztoit2.setVisible(false);
    la_pp2.setVisible(false);
    la_af2.setVisible(false);
    la_cohe2.setVisible(false);
    tf_ztoit2.setVisible(false);
    tf_pp2.setVisible(false);
    tf_af2.setVisible(false);
    tf_cohe2.setVisible(false);
    unit_ztoit2.setVisible(false);
    unit_pp2.setVisible(false);
    unit_af2.setVisible(false);
    unit_cohe2.setVisible(false);
    tf_ztoitdr2.setVisible(false);
    tf_ppdr2.setVisible(false);
    tf_afdr2.setVisible(false);
    tf_cohedr2.setVisible(false);
    unit_ztoitdr2.setVisible(false);
    unit_ppdr2.setVisible(false);
    unit_afdr2.setVisible(false);
    unit_cohedr2.setVisible(false);
    if (n >= 1) {
      la_sol1.setVisible(true);
      la_ztoit1.setVisible(true);
      la_pp1.setVisible(true);
      la_af1.setVisible(true);
      la_cohe1.setVisible(true);
      tf_ztoit1.setVisible(true);
      tf_pp1.setVisible(true);
      tf_af1.setVisible(true);
      tf_cohe1.setVisible(true);
      unit_ztoit1.setVisible(true);
      unit_pp1.setVisible(true);
      unit_af1.setVisible(true);
      unit_cohe1.setVisible(true);
      tf_ztoitdr1.setVisible(true);
      tf_ppdr1.setVisible(true);
      tf_afdr1.setVisible(true);
      tf_cohedr1.setVisible(true);
      unit_ztoitdr1.setVisible(true);
      unit_ppdr1.setVisible(true);
      unit_afdr1.setVisible(true);
      unit_cohedr1.setVisible(true);
    }
    if (n == 2) {
      la_sol2.setVisible(true);
      la_ztoit2.setVisible(true);
      la_pp2.setVisible(true);
      la_af2.setVisible(true);
      la_cohe2.setVisible(true);
      tf_ztoit2.setVisible(true);
      tf_pp2.setVisible(true);
      tf_af2.setVisible(true);
      tf_cohe2.setVisible(true);
      unit_ztoit2.setVisible(true);
      unit_pp2.setVisible(true);
      unit_af2.setVisible(true);
      unit_cohe2.setVisible(true);
      tf_ztoitdr2.setVisible(true);
      tf_ppdr2.setVisible(true);
      tf_afdr2.setVisible(true);
      tf_cohedr2.setVisible(true);
      unit_ztoitdr2.setVisible(true);
      unit_ppdr2.setVisible(true);
      unit_afdr2.setVisible(true);
      unit_cohedr2.setVisible(true);
    }
    tf_ztoit1.setVisible(false);
    unit_ztoit1.setVisible(false);
    tf_ztoitdr1.setVisible(false);
    unit_ztoitdr1.setVisible(false);
    tf_ztoitdr2.setVisible(false);
    unit_ztoitdr2.setVisible(false);
    pnMil.repaint();
  }
  public void majlook(final int _style, final int _phase, final int _unit) {
    //    int style_  =  _style;
    //   int phase_  =  _phase;
    //  int unit_   =  _unit;
    la_sol1.setForeground(Color.blue);
    la_sol2.setForeground(Color.blue);
    la_coebut.setForeground(Color.blue);
    la_coecohe.setForeground(Color.blue);
    tf_ztoit1.setVisible(false);
    unit_ztoit1.setVisible(false);
    tf_ztoitdr1.setVisible(false);
    unit_ztoitdr1.setVisible(false);
    tf_ztoitdr2.setVisible(false);
    unit_ztoitdr2.setVisible(false);
    majsol();
  }
}
