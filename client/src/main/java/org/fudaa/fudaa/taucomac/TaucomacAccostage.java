/*
 * @file         TaucomacAccostage.java
 * @creation     2000-10-17
 * @modification $Date: 2006-09-19 15:08:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.taucomac;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.taucomac.SAccost;
import org.fudaa.dodico.corba.taucomac.SAccostage;
import org.fudaa.dodico.corba.taucomac.SPtInter;

/**
 * Onglet Defenses
 * 
 * @version $Revision: 1.7 $ $Date: 2006-09-19 15:08:57 $ by $Author: deniger $
 * @author Jean-Yves Riou
 */
public class TaucomacAccostage extends TaucomacMerePanParm implements ActionListener, FocusListener {

  SAccost parametresLocal = null;
  public int kgnaf = 0;
  public int selectCol = -9;
  public int selectLin = -9;
  public int linMem = -9;
  public int colMem = -9;
  public int anclinMem = -9;
  public int ancolMem = -9;
  /*
   * struct SAccostage { reel z; entier numDef; entier nbDef; reel vFreqEnr; reel vRareEnr; reel vCalcEnr; reel
   * vAcciEnr; reel vFreqFor; reel vRareFor; reel vCalcFor; reel vAcciFor; reel vFreqDep; reel vRareDep; reel vCalcDep;
   * reel vAcciDep; }; typedef sequence<SAccostage> VSAccostages; struct SAccost { entier nbAccostages; // accostages
   * reel chargeFreq; reel chargeRare; reel chargeCalc; reel chargeAcci; reel psi0Ac; reel psi2Ac; VSAccostages
   * accostages; }; SAccost accost;
   */
  private final boolean DEBUG = true;
  private final boolean ALLOW_COLUMN_SELECTION = true;
  private final boolean ALLOW_ROW_SELECTION = true;
  static protected TaucomacPressioDialog dial;
  protected boolean blocus1 = true;
  SPtInter points[] = null;
  data[][] donnees_;
  private int donneesNbColonne_;
  private int donneesNbLigne_;
  public int colDialog_ = -1;
  // indice colonne qui ouvre la TaucomacPressioDialog
  public int localPhase_;
  public int nbMaxAccostages_ = 22;
  public TaucomacCellEditor[] anEditor;
  // =========
  MyTableModel myModel = new MyTableModel();
  JTable table;
  // =========
  final Object[][] data1 = { { "                       Cote" }, { "                       N� de la d�fense" },
      { "                       Nombre de d�fenses" }, { "Energie d'accostage" },
      { "                       Valeur fr�quente" }, { "                       Valeur rare" },
      { "                       Valeur de calcul" }, { "                       Valeur accidentelle" },
      { "D�placement admissible" }, { "                       Valeur fr�quente" },
      { "                       Valeur rare" }, { "                       Valeur de calcul" },
      { "                       Valeur accidentelle" }, { "Force maximale admissible" },
      { "                       Valeur fr�quente" }, { "                       Valeur rare" },
      { "                       Valeur de calcul" }, { "                       Valeur accidentelle" } };
  MyTableModel1 myModel1 = new MyTableModel1();
  JTable table1 = new JTable(myModel1);
  public TaucomacCellEditor anEditor21 = new TaucomacCellEditor();
  // =========
  final Object[][] data2 = { { "  m" }, { "" }, { "" }, { "" }, { "    t.m" }, { "    t.m" }, { "    t.m" },
      { "    t.m" }, { "" }, { "  cm" }, { "  cm" }, { "  cm" }, { "  cm" }, { "" }, { "    t" }, { "    t" },
      { "    t" }, { "    t" } };
  MyTableModel2 myModel2 = new MyTableModel2();
  JTable table2 = new JTable(myModel2);
  public TaucomacCellEditor anEditor22 = new TaucomacCellEditor();
  // ========
  public int[] lignePhase = new int[19];
  // =========
  JPanel pnHaut;
  JPanel pnMil;
  JPanel pnMilG;
  JPanel pnMilC;
  JPanel pnMilD;
  JPanel pnBasD;
  JPanel pnBasG;
  JPanel pnBas;
  JLabel la_nbAccostages;
  BuTextField tf_nbAccostages;
  JLabel la_bid1;
  JLabel la_bid2;
  JLabel la_bid3;
  JLabel la_bid4;
  JLabel la_bid5;
  JLabel la_bid6;
  JLabel la_bid7;
  JLabel la_bid8;
  JLabel titreBasG;
  JLabel titreBasD1;
  JLabel titreBasD2;
  JLabel la_psi0;
  JLabel la_psi2;
  JLabel la_valfreq;
  JLabel la_valrare;
  JLabel la_valcalc;
  JLabel la_valacci;
  BuTextField tf_valfreq;
  BuTextField tf_valrare;
  BuTextField tf_valcalc;
  BuTextField tf_valacci;
  BuTextField tf_valpsi0;
  BuTextField tf_valpsi2;
  JLabel unit_freq;
  JLabel unit_rare;
  JLabel unit_calc;
  JLabel unit_acci;

  // ==============
  class data {

    Color couleur_ = null;
    String valeur_;

    public data(final String valeur) {
      valeur_ = valeur;
    }

    public String toString() {
      return valeur_;
    }

    public void setCouleur(final Color _c) {
      couleur_ = _c;
    }

    public Color getCouleur() {
      return couleur_;
    }

    public void setValeur(final String _valeur) {
      valeur_ = _valeur;
    }
  };

  // ===============
  class ColorRenderer extends JLabel implements TableCellRenderer {

    Border unselectedBorder = null;
    Border selectedBorder = null;
    boolean isBordered = true;
    Color defautCouleur_ = Color.blue;

    public ColorRenderer(final boolean _isBordered, final boolean isOpaque, final Color _couleurParDefaut) {
      super();
      if (_couleurParDefaut != null) {
        defautCouleur_ = _couleurParDefaut;
      }
      this.isBordered = _isBordered;
      this.setOpaque(isOpaque);
      this.setHorizontalAlignment(RIGHT);
    }

    public Component getTableCellRendererComponent(final JTable _table, final Object value, final boolean isSelected,
        final boolean hasFocus, final int row, final int column) {
      setText("" + value);
      final Color CouleurSpecifique = ((data) value).getCouleur();
      if (CouleurSpecifique != null) {
        this.setBackground(CouleurSpecifique);
      } else {
        this.setBackground(defautCouleur_);
      }
      if (isBordered) {
        if (isSelected) {
          if (selectedBorder == null) {
            selectedBorder = BorderFactory.createMatteBorder(2, 5, 2, 5, _table.getSelectionBackground());
          }
          this.setBorder(selectedBorder);
        } else {
          if (unselectedBorder == null) {
            unselectedBorder = BorderFactory.createMatteBorder(2, 5, 2, 5, _table.getBackground());
          }
          this.setBorder(unselectedBorder);
        }
      }
      return this;
    }
  };

  // ====================================
  // Constructeur
  // ====================================
  public TaucomacAccostage(final BuCommonInterface _appli) {
    super();
    // appli_= _appli;
    // imp_= ((TaucomacImplementation)appli_.getImplementation());
    // ====
    for (int i = 0; i < 19; i++) {
      lignePhase[i] = 0;
    }
    lignePhase[2] = 1;
    lignePhase[3] = 1;
    lignePhase[5] = 2;
    lignePhase[7] = 2;
    lignePhase[8] = 2;
    lignePhase[10] = 2;
    lignePhase[12] = 2;
    lignePhase[13] = 2;
    lignePhase[15] = 2;
    lignePhase[17] = 2;
    lignePhase[18] = 2;
    // ====
    donneesNbColonne_ = 22;
    donneesNbLigne_ = 19;
    initialiseData(donneesNbLigne_, donneesNbColonne_);
    // ====
    table = new JTable(myModel) {

      ColorRenderer cr = new ColorRenderer(false, true, Color.cyan);

      public TableCellRenderer getCellRenderer(final int row, final int column) {
        return cr;
      }
    };
    // =====
    pnHaut = new JPanel();
    final BuGridLayout loCal2 = new BuGridLayout();
    loCal2.setColumns(2);
    loCal2.setHgap(5);
    loCal2.setVgap(0);
    loCal2.setHfilled(false);
    loCal2.setCfilled(true);
    pnHaut.setLayout(loCal2);
    pnHaut.setBorder(new EmptyBorder(2, 60, 1, 480)); // top,left,bottom,right
    // pnHaut.setPreferredSize(new Dimension(650,120));
    la_nbAccostages = new JLabel(" Nombre d'accostages ", SwingConstants.RIGHT);
    tf_nbAccostages = BuTextField.createIntegerField();
    tf_nbAccostages.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_nbAccostages.addFocusListener(this);
    tf_nbAccostages.setColumns(4);
    int n = 0;
    n = 0;
    placeCompn(pnHaut, la_nbAccostages, 0, n);
    n++;
    placeCompn(pnHaut, tf_nbAccostages, 0, n);
    // =======
    pnBasG = new JPanel();
    final BuGridLayout loCal7 = new BuGridLayout();
    loCal7.setColumns(6);
    loCal7.setHgap(4);
    loCal7.setVgap(1);
    loCal7.setHfilled(false);
    loCal7.setCfilled(true);
    pnBasG.setLayout(loCal7);
    pnBasG.setBorder(new EmptyBorder(0, 20, 0, 5)); // top,left,bottom,right
    pnBasG.setPreferredSize(new Dimension(460, 80));
    la_bid1 = new JLabel("   ", SwingConstants.RIGHT);
    la_bid2 = new JLabel("   ", SwingConstants.RIGHT);
    la_bid3 = new JLabel("   ", SwingConstants.RIGHT);
    la_bid4 = new JLabel("   ", SwingConstants.RIGHT);
    la_bid5 = new JLabel("   ", SwingConstants.RIGHT);
    la_bid6 = new JLabel("   ", SwingConstants.RIGHT);
    la_bid7 = new JLabel("   ", SwingConstants.RIGHT);
    la_bid8 = new JLabel("   ", SwingConstants.RIGHT);
    titreBasG = new JLabel("Charge en tete (>0)", SwingConstants.LEFT);
    titreBasD1 = new JLabel("Coefficients de combinaison (> 0)", SwingConstants.LEFT);
    titreBasD1.setForeground(Color.blue);
    // JLabel titreBasD2 = new JLabel ("Coefficient de combinaison psi2 (> 0)",JLabel.CENTER);
    la_valfreq = new JLabel("valeur fr�quente", SwingConstants.LEFT);
    la_valrare = new JLabel("valeur rare     ", SwingConstants.LEFT);
    la_valcalc = new JLabel("     valeur de calcul   ", SwingConstants.LEFT);
    la_valacci = new JLabel("     valeur accidentelle", SwingConstants.LEFT);
    unit_freq = new JLabel("  t ", SwingConstants.LEFT);
    unit_rare = new JLabel("  t ", SwingConstants.LEFT);
    unit_calc = new JLabel("  t ", SwingConstants.LEFT);
    unit_acci = new JLabel("  t ", SwingConstants.LEFT);
    la_psi0 = new JLabel("         psi0", SwingConstants.RIGHT);
    la_psi2 = new JLabel("         psi2", SwingConstants.RIGHT);
    tf_valfreq = BuTextField.createDoubleField();
    tf_valfreq.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_valrare = BuTextField.createDoubleField();
    tf_valrare.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_valcalc = BuTextField.createDoubleField();
    tf_valcalc.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_valacci = BuTextField.createDoubleField();
    tf_valacci.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_valpsi0 = BuTextField.createDoubleField();
    tf_valpsi0.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_valpsi2 = BuTextField.createDoubleField();
    tf_valpsi2.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_valfreq.setColumns(5);
    tf_valrare.setColumns(5);
    tf_valcalc.setColumns(5);
    tf_valacci.setColumns(5);
    tf_valpsi0.setColumns(4);
    tf_valpsi2.setColumns(4);
    n = 0;
    placeCompn(pnBasG, titreBasG, 0, n);
    n++;
    placeCompn(pnBasG, la_bid1, 0, n);
    n++;
    placeCompn(pnBasG, la_bid2, 0, n);
    n++;
    placeCompn(pnBasG, la_bid3, 0, n);
    n++;
    placeCompn(pnBasG, la_bid4, 0, n);
    n++;
    placeCompn(pnBasG, la_bid5, 0, n);
    n++;
    placeCompn(pnBasG, la_valfreq, 2, n);
    n++;
    placeCompn(pnBasG, tf_valfreq, 2, n);
    n++;
    placeCompn(pnBasG, unit_freq, 2, n);
    n++;
    placeCompn(pnBasG, la_valcalc, 2, n);
    n++;
    placeCompn(pnBasG, tf_valcalc, 2, n);
    n++;
    placeCompn(pnBasG, unit_calc, 2, n);
    n++;
    placeCompn(pnBasG, la_valrare, 0, n);
    n++;
    placeCompn(pnBasG, tf_valrare, 0, n);
    n++;
    placeCompn(pnBasG, unit_rare, 0, n);
    n++;
    placeCompn(pnBasG, la_valacci, 2, n);
    n++;
    placeCompn(pnBasG, tf_valacci, 2, n);
    n++;
    placeCompn(pnBasG, unit_acci, 2, n);
    n++;
    placeUnit(unit_freq, 6);
    placeUnit(unit_rare, 6);
    placeUnit(unit_calc, 6);
    placeUnit(unit_acci, 6);
    //
    pnBasD = new JPanel();
    final BuGridLayout loCal8 = new BuGridLayout();
    loCal8.setColumns(2);
    loCal8.setHgap(5);
    loCal8.setVgap(1);
    loCal8.setHfilled(false);
    loCal8.setCfilled(true);
    pnBasD.setLayout(loCal8);
    pnBasD.setBorder(new EmptyBorder(0, 5, 0, 40)); // top,left,bottom,right
    pnBasD.setPreferredSize(new Dimension(290, 80));
    n = 0;
    placeCompn(pnBasD, titreBasD1, 2, n);
    n++;
    placeCompn(pnBasD, la_bid8, 2, n);
    n++;
    placeCompn(pnBasD, la_psi0, 2, n);
    n++;
    placeCompn(pnBasD, tf_valpsi0, 2, n);
    n++;
    placeCompn(pnBasD, la_psi2, 2, n);
    n++;
    placeCompn(pnBasD, tf_valpsi2, 2, n);
    n++;
    // =================
    pnBas = new JPanel();
    pnBas.setBorder(new TitledBorder(""));
    pnBas.setLayout(new BorderLayout());
    pnBas.setBorder(new EmptyBorder(0, 5, 0, 5)); // top,left,bottom,right
    pnBas.add(pnBasG, BorderLayout.WEST);
    pnBas.add(pnBasD, BorderLayout.EAST);
    // ======================
    pnMil = new JPanel();
    pnMil.setBorder(new TitledBorder(""));
    pnMil.setLayout(new BorderLayout());
    pnMil.setBorder(new EmptyBorder(0, 5, 0, 5)); // top,left,bottom,right
    // pnMil.setPreferredSize(new Dimension(700,250));
    pnMilC = new JPanel();
    // pnMilC.setPreferredSize(new Dimension(500,80 ));
    pnMilG = new JPanel();
    pnMilD = new JPanel();
    // ===================
    // Tables
    // ===================
    table1.setBackground(Color.orange);
    table1.setPreferredScrollableViewportSize(new Dimension(230, 290));
    table1.getColumn(table1.getColumnName(0)).setCellEditor(anEditor21);
    final JScrollPane scrollPane1 = new JScrollPane(table1);
    (table1.getTableHeader()).setReorderingAllowed(false);
    table1.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    (table1.getColumn(table1.getColumnName(0))).setPreferredWidth(230);
    table2.setBackground(Color.orange);
    table2.setPreferredScrollableViewportSize(new Dimension(65, 290));
    table2.getColumn(table2.getColumnName(0)).setCellEditor(anEditor22);
    final JScrollPane scrollPane2 = new JScrollPane(table2);
    (table2.getColumn(table2.getColumnName(0))).setPreferredWidth(65);
    (table2.getTableHeader()).setReorderingAllowed(false);
    table2.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    // =====
    table.setColumnSelectionAllowed(false);
    table.setRowSelectionAllowed(false);
    table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    table.setCellSelectionEnabled(true);
    table.setBackground(Color.cyan);
    table.setForeground(Color.black);
    table.setSelectionBackground(Color.white);
    table.setSelectionForeground(Color.blue);
    table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    table.setRowSelectionInterval(0, 0);
    table.setColumnSelectionInterval(1, 1);
    // On regle la taille des colonnes
    TableColumn kolw = null;
    // TableCellRenderer rw;
    for (int i = 0; i < nbMaxAccostages_; i++) {
      kolw = table.getColumnModel().getColumn(i);
      // rw= kolw.getCellRenderer();
      if (table.getColumnName(i).equals("")) {
        kolw.setPreferredWidth(40);
      } else {
        kolw.setPreferredWidth(70);
      }
    }
    //
    table.setPreferredScrollableViewportSize(new Dimension(440, 310));
    /* initialisation du tableau */
    anEditor = new TaucomacCellEditor[nbMaxAccostages_];
    for (int i = 0; i < nbMaxAccostages_; i++) {
      kolw = table.getColumnModel().getColumn(i);
      anEditor[i] = new TaucomacCellEditor();
      // table.getColumn(table.getColumnName(i)).setCellEditor(anEditor[i]);
      kolw.setCellEditor(anEditor[i]);
    }
    final JScrollPane scrollPane = new JScrollPane(table);
    (table.getTableHeader()).setReorderingAllowed(false);
    table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    lookInit();
    // =======================================
    if (ALLOW_ROW_SELECTION) { // true by default
      final ListSelectionModel rowSM = table.getSelectionModel();
      rowSM.addListSelectionListener(new ListSelectionListener() {

        public void valueChanged(final ListSelectionEvent e) {
          // Ignore extra messages.
          if (e.getValueIsAdjusting()) {
            return;
          }
          final ListSelectionModel lsm = (ListSelectionModel) e.getSource();
          if (lsm.isSelectionEmpty()) {
            // System.out.println("No rows are selected.");
            selectLin = -9;
          } else {
            final int selectedRow = lsm.getMinSelectionIndex();
            // System.out.println("Row " + selectedRow
            // + " is now selected.");
            selectLin = selectedRow;
          }
        }
      });
    } else {
      table.setRowSelectionAllowed(false);
    }
    //
    if (ALLOW_COLUMN_SELECTION) { // false by default
      if (ALLOW_ROW_SELECTION) {
        // We allow both row and column selection, which
        // implies that we *really* want to allow individual
        // cell selection.
        table.setCellSelectionEnabled(true);
      }
      table.setColumnSelectionAllowed(true);
      final ListSelectionModel colSM = table.getColumnModel().getSelectionModel();
      colSM.addListSelectionListener(new ListSelectionListener() {

        public void valueChanged(final ListSelectionEvent e) {
          // Ignore extra messages.
          if (e.getValueIsAdjusting()) {
            return;
          }
          final ListSelectionModel lsm = (ListSelectionModel) e.getSource();
          if (lsm.isSelectionEmpty()) {
            // System.out.println("No columns are selected.");
            selectCol = -9;
          } else {
            final int selectedCol = lsm.getMinSelectionIndex();
            // System.out.println("Column " + selectedCol
            // + " is now selected.");
            selectCol = selectedCol;
          }
        }
      });
    }
    if (DEBUG) {
      table.addMouseListener(new MouseAdapter() {

        public void mouseClicked(final MouseEvent e) {
          selectAvise(selectLin, selectCol);
        }
      });
    }
    // =======================================
    pnMilC.add(scrollPane);
    pnMilG.add(scrollPane1);
    pnMilD.add(scrollPane2);
    pnMil.add(pnMilC, BorderLayout.CENTER);
    pnMil.add(pnMilG, BorderLayout.WEST);
    pnMil.add(pnMilD, BorderLayout.EAST);
    // ====================================
    this.setLayout(new BorderLayout());
    this.setBorder(BorderFactory.createEmptyBorder(2, 5, 2, 5));
    this.add(pnHaut, BorderLayout.NORTH);
    this.add(pnMil, BorderLayout.CENTER);
    this.add(pnBas, BorderLayout.SOUTH);
    //
    // initialisation des parametres locaux
    setParametres(parametresLocal);
  }

  // ==== fin constructeur
  private void initialiseData(final int _nbLigne, final int _nbCol) {
    donnees_ = new data[_nbLigne][_nbCol];
    for (int i = 0; i < _nbLigne; i++) {
      for (int j = 0; j < _nbCol; j++) {
        donnees_[i][j] = new data("");
      }
    }
  }

  //
  public void focusLost(final FocusEvent e) {
    final Object src = e.getSource();
    // System.out.println("FocusLost " + e );
    // System.out.println("pressio focusLost "+e + " "+src );
    if (src instanceof JTextField) {
      if (src == tf_nbAccostages) {
        // System.out.println("focusLost source " + src);
        String tmpS = "";
        final String blanc = "";
        int n = 0;
        tmpS = tf_nbAccostages.getText();
        if (tmpS.equals(blanc)) {
          n = 0;
        } else {
          n = Integer.parseInt(tmpS);
        }
        // System.out.println("n " + n);
        final int nw = 2 * n;
        myModel.setNbMaxColEditable(nw);
        TableColumn kolw = null;
        for (int i = 0; i < nw; i++) {
          kolw = table.getColumnModel().getColumn(i);
          if (table.getColumnName(i).equals("")) {
            kolw.setPreferredWidth(40);
          } else {
            kolw.setPreferredWidth(70);
          }
          table.repaint();
        }
      }
    }
  }

  public void focusGained(final FocusEvent e) {
  // System.out.println("FocusGained " + e );
  }

  public void actionPerformed(final ActionEvent e) {
    // System.out.println("actionPerformed " + e );
    parametresLocal = getParametres();
    setVisible(true);
    // Object src=e.getSource();
    // System.out.println("evenement " + e +" source "+src);
  }

  public void setParametres(final SAccost parametresProjet_) {
    // System.out.println(" ===== Accostagesset Parametres =====");
    parametresLocal = parametresProjet_;
    if (parametresLocal != null) {
      int n;
      n = parametresLocal.nbAccostages * 2;
      myModel.setNbMaxColEditable(n);
      if (parametresLocal.nbAccostages > 0) {
        tf_nbAccostages.setValue(new Integer(parametresLocal.nbAccostages));
      } else {
        tf_nbAccostages.setValue("");
      }
      if (parametresLocal.chargeFreq >= 0) {
        tf_valfreq.setValue(new Double(parametresLocal.chargeFreq));
      } else {
        tf_valfreq.setValue("");
      }
      if (parametresLocal.chargeRare >= 0) {
        tf_valrare.setValue(new Double(parametresLocal.chargeRare));
      } else {
        tf_valrare.setValue("");
      }
      if (parametresLocal.chargeCalc >= 0) {
        tf_valcalc.setValue(new Double(parametresLocal.chargeCalc));
      } else {
        tf_valcalc.setValue("");
      }
      if (parametresLocal.chargeAcci >= 0) {
        tf_valacci.setValue(new Double(parametresLocal.chargeAcci));
      } else {
        tf_valacci.setValue("");
      }
      if (parametresLocal.psi0Ac >= 0) {
        tf_valpsi0.setValue(new Double(parametresLocal.psi0Ac));
      } else {
        tf_valpsi0.setValue("");
      }
      if (parametresLocal.psi2Ac >= 0) {
        tf_valpsi2.setValue(new Double(parametresLocal.psi2Ac));
      } else {
        tf_valpsi2.setValue("");
      }
      if (parametresLocal.nbAccostages > 0) {
        int j = 0;
        for (int k = 0; k < parametresLocal.nbAccostages; k++) {
          // =====================
          System.out.println("=== accost [j]. j =  " + j + "  (j pair)");
          if (parametresLocal.accostages[j] == null) {
            parametresLocal.accostages[j] = new SAccostage();
          }
          if (parametresLocal.accostages[j].vFreqDep == 1.) {
            table.setValueAt("oui", 10, j);
            ((data) myModel.getValueAt(10, j)).setCouleur(Color.green);
            ((data) myModel.getValueAt(10, j + 1)).setCouleur(Color.cyan);
          } else {
            table.setValueAt("non", 10, j);
            ((data) myModel.getValueAt(10, j)).setCouleur(Color.red);
            ((data) myModel.getValueAt(10, j + 1)).setCouleur(Color.gray);
          }
          if (parametresLocal.accostages[j].vRareDep == 1.) {
            table.setValueAt("oui", 11, j);
            ((data) myModel.getValueAt(11, j)).setCouleur(Color.green);
            ((data) myModel.getValueAt(11, j + 1)).setCouleur(Color.cyan);
          } else {
            table.setValueAt("non", 11, j);
            ((data) myModel.getValueAt(11, j)).setCouleur(Color.red);
            ((data) myModel.getValueAt(11, j + 1)).setCouleur(Color.gray);
          }
          if (parametresLocal.accostages[j].vCalcDep == 1.) {
            table.setValueAt("oui", 12, j);
            ((data) myModel.getValueAt(12, j)).setCouleur(Color.green);
            ((data) myModel.getValueAt(12, j + 1)).setCouleur(Color.cyan);
          } else {
            table.setValueAt("non", 12, j);
            ((data) myModel.getValueAt(12, j)).setCouleur(Color.red);
            ((data) myModel.getValueAt(12, j + 1)).setCouleur(Color.gray);
          }
          if (parametresLocal.accostages[j].vAcciDep == 1.) {
            table.setValueAt("oui", 13, j);
            ((data) myModel.getValueAt(13, j)).setCouleur(Color.green);
            ((data) myModel.getValueAt(13, j + 1)).setCouleur(Color.cyan);
          } else {
            table.setValueAt("non", 13, j);
            ((data) myModel.getValueAt(13, j)).setCouleur(Color.red);
            ((data) myModel.getValueAt(13, j + 1)).setCouleur(Color.gray);
          }
          if (parametresLocal.accostages[j].vFreqFor == 1.) {
            table.setValueAt("oui", 15, j);
            ((data) myModel.getValueAt(15, j)).setCouleur(Color.green);
            ((data) myModel.getValueAt(15, j + 1)).setCouleur(Color.cyan);
          } else {
            table.setValueAt("non", 15, j);
            ((data) myModel.getValueAt(15, j)).setCouleur(Color.red);
            ((data) myModel.getValueAt(15, j + 1)).setCouleur(Color.gray);
          }
          if (parametresLocal.accostages[j].vRareFor == 1.) {
            table.setValueAt("oui", 16, j);
            ((data) myModel.getValueAt(16, j)).setCouleur(Color.green);
            ((data) myModel.getValueAt(16, j + 1)).setCouleur(Color.cyan);
          } else {
            table.setValueAt("non", 16, j);
            ((data) myModel.getValueAt(16, j)).setCouleur(Color.red);
            ((data) myModel.getValueAt(16, j + 1)).setCouleur(Color.gray);
          }
          if (parametresLocal.accostages[j].vCalcFor == 1.) {
            table.setValueAt("oui", 17, j);
            ((data) myModel.getValueAt(17, j)).setCouleur(Color.green);
            ((data) myModel.getValueAt(17, j + 1)).setCouleur(Color.cyan);
          } else {
            table.setValueAt("non", 17, j);
            ((data) myModel.getValueAt(17, j)).setCouleur(Color.red);
            ((data) myModel.getValueAt(17, j + 1)).setCouleur(Color.gray);
          }
          if (parametresLocal.accostages[j].vAcciFor == 1.) {
            table.setValueAt("oui", 18, j);
            ((data) myModel.getValueAt(18, j)).setCouleur(Color.green);
            ((data) myModel.getValueAt(18, j + 1)).setCouleur(Color.cyan);
          } else {
            table.setValueAt("non", 18, j);
            ((data) myModel.getValueAt(18, j)).setCouleur(Color.red);
            ((data) myModel.getValueAt(18, j + 1)).setCouleur(Color.gray);
          }
          // ======================
          j = j + 1;
          System.out.println("=== accost [j]. j =  " + j + "  (j impair)");
          if (parametresLocal.accostages[j] == null) {
            parametresLocal.accostages[j] = new SAccostage();
          }
          if (parametresLocal.accostages[j].z != -999.) {
            table.setValueAt(new Double(parametresLocal.accostages[j].z), 1, j);
          } else {
            table.setValueAt("", 1, j);
          }
          if (parametresLocal.accostages[j].nbDef != 0) {
            table.setValueAt(new Integer(parametresLocal.accostages[j].nbDef), 2, j);
          } else {
            table.setValueAt("", 2, j);
          }
          if (parametresLocal.accostages[j].numDef != -999) {
            table.setValueAt(new Integer(parametresLocal.accostages[j].numDef), 3, j);
          } else {
            table.setValueAt("", 3, j);
          }
          if (parametresLocal.accostages[j].vFreqEnr != -999.) {
            table.setValueAt(new Double(parametresLocal.accostages[j].vFreqEnr), 5, j);
          } else {
            table.setValueAt("", 5, j);
          }
          if (parametresLocal.accostages[j].vRareEnr != -999.) {
            table.setValueAt(new Double(parametresLocal.accostages[j].vRareEnr), 6, j);
          } else {
            table.setValueAt("", 6, j);
          }
          if (parametresLocal.accostages[j].vCalcEnr != -999.) {
            table.setValueAt(new Double(parametresLocal.accostages[j].vCalcEnr), 7, j);
          } else {
            table.setValueAt("", 7, j);
          }
          if (parametresLocal.accostages[j].vAcciEnr != -999.) {
            table.setValueAt(new Double(parametresLocal.accostages[j].vAcciEnr), 8, j);
          } else {
            table.setValueAt("", 8, j);
          }
          if (parametresLocal.accostages[j].vFreqDep != -999.) {
            table.setValueAt(new Double(parametresLocal.accostages[j].vFreqDep), 10, j);
          } else {
            table.setValueAt("", 10, j);
          }
          if (parametresLocal.accostages[j].vRareDep != -999.) {
            table.setValueAt(new Double(parametresLocal.accostages[j].vRareDep), 11, j);
          } else {
            table.setValueAt("", 11, j);
          }
          if (parametresLocal.accostages[j].vCalcDep != -999.) {
            table.setValueAt(new Double(parametresLocal.accostages[j].vCalcDep), 12, j);
          } else {
            table.setValueAt("", 12, j);
          }
          if (parametresLocal.accostages[j].vAcciDep != -999.) {
            table.setValueAt(new Double(parametresLocal.accostages[j].vAcciDep), 13, j);
          } else {
            table.setValueAt("", 13, j);
          }
          if (parametresLocal.accostages[j].vFreqFor != -999.) {
            table.setValueAt(new Double(parametresLocal.accostages[j].vFreqFor), 15, j);
          } else {
            table.setValueAt("", 15, j);
          }
          if (parametresLocal.accostages[j].vRareFor != -999.) {
            table.setValueAt(new Double(parametresLocal.accostages[j].vRareFor), 16, j);
          } else {
            table.setValueAt("", 16, j);
          }
          if (parametresLocal.accostages[j].vCalcFor != -999.) {
            table.setValueAt(new Double(parametresLocal.accostages[j].vCalcFor), 17, j);
          } else {
            table.setValueAt("", 17, j);
          }
          if (parametresLocal.accostages[j].vAcciFor != -999.) {
            table.setValueAt(new Double(parametresLocal.accostages[j].vAcciFor), 18, j);
          } else {
            table.setValueAt("", 18, j);
          }
          j = j + 1;
        } // fork
      }
    }
  }

  public SAccost getParametres() {
    // System.out.println(" AccostagegetParametres");
    if (parametresLocal == null) {
      parametresLocal = new SAccost();
      parametresLocal.accostages = new SAccostage[nbMaxAccostages_];
      // System.out.println(" parametresLocal = "+parametresLocal);
      for (int i = 0; i < nbMaxAccostages_; i++) {
        parametresLocal.accostages[i] = new SAccostage();
      }
    }
    String changed = "";
    double tmpD = 0.;
    int tmpI = 0;
    String tmpS = "";
    final String blanc = "";
    // int n = 0;
    tmpI = parametresLocal.nbAccostages;
    tmpS = tf_nbAccostages.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.nbAccostages = 0;
    } else {
      parametresLocal.nbAccostages = Integer.parseInt(tmpS);
    }
    if (parametresLocal.nbAccostages != tmpI) {
      changed += "nbAccostages|";
    }
    tmpD = parametresLocal.chargeFreq;
    tmpS = tf_valfreq.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.chargeFreq = -999.;
    } else {
      parametresLocal.chargeFreq = Double.parseDouble(tmpS);
    }
    if (parametresLocal.chargeFreq != tmpD) {
      changed += " chargeFreq|";
    }
    tmpD = parametresLocal.chargeRare;
    tmpS = tf_valrare.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.chargeRare = -999.;
    } else {
      parametresLocal.chargeRare = Double.parseDouble(tmpS);
    }
    if (parametresLocal.chargeRare != tmpD) {
      changed += " chargeRare|";
    }
    tmpD = parametresLocal.chargeCalc;
    tmpS = tf_valcalc.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.chargeCalc = -999.;
    } else {
      parametresLocal.chargeCalc = Double.parseDouble(tmpS);
    }
    if (parametresLocal.chargeCalc != tmpD) {
      changed += " chargeCalc|";
    }
    tmpD = parametresLocal.chargeAcci;
    tmpS = tf_valacci.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.chargeAcci = -999.;
    } else {
      parametresLocal.chargeAcci = Double.parseDouble(tmpS);
    }
    if (parametresLocal.chargeAcci != tmpD) {
      changed += " chargeAcci|";
    }
    tmpD = parametresLocal.psi0Ac;
    tmpS = tf_valpsi0.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.psi0Ac = -999.;
    } else {
      parametresLocal.psi0Ac = Double.parseDouble(tmpS);
    }
    if (parametresLocal.psi0Ac != tmpD) {
      changed += " psi0Ac|";
    }
    tmpD = parametresLocal.psi2Ac;
    tmpS = tf_valpsi2.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.psi2Ac = -999.;
    } else {
      parametresLocal.psi2Ac = Double.parseDouble(tmpS);
    }
    if (parametresLocal.psi2Ac != tmpD) {
      changed += " psi2Ac|";
    }
    int j = 0;
    for (int k = 0; k < parametresLocal.nbAccostages; k++) {
      // =====================
      System.out.println("=== accost [j]. j =  " + j + "  (j pair)");
      if (parametresLocal.accostages[j] == null) {
        parametresLocal.accostages[j] = new SAccostage();
      }
      parametresLocal.accostages[j].z = -999.;
      parametresLocal.accostages[j].nbDef = 0;;
      parametresLocal.accostages[j].numDef = -999;
      parametresLocal.accostages[j].vFreqEnr = -999.;
      parametresLocal.accostages[j].vRareEnr = -999.;
      parametresLocal.accostages[j].vCalcEnr = -999.;
      parametresLocal.accostages[j].vAcciEnr = -999.;
      tmpD = parametresLocal.accostages[j].vFreqDep;
      tmpS = String.valueOf(donnees_[10][j]);
      if (tmpS.equals("oui")) {
        parametresLocal.accostages[j].vFreqDep = 1.;
      } else {
        parametresLocal.accostages[j].vFreqDep = 0.;
      }
      if (parametresLocal.accostages[j].vFreqDep != tmpD) {
        changed += "accostages[j].cFreqDep|";
      }
      tmpD = parametresLocal.accostages[j].vRareDep;
      tmpS = String.valueOf(donnees_[11][j]);
      if (tmpS.equals("oui")) {
        parametresLocal.accostages[j].vRareDep = 1.;
      } else {
        parametresLocal.accostages[j].vRareDep = 0.;
      }
      if (parametresLocal.accostages[j].vRareDep != tmpD) {
        changed += "accostages[j].cRareDep|";
      }
      tmpD = parametresLocal.accostages[j].vCalcDep;
      tmpS = String.valueOf(donnees_[12][j]);
      if (tmpS.equals("oui")) {
        parametresLocal.accostages[j].vCalcDep = 1.;
      } else {
        parametresLocal.accostages[j].vCalcDep = 0.;
      }
      if (parametresLocal.accostages[j].vCalcDep != tmpD) {
        changed += "accostages[j].cCalcDep|";
      }
      tmpD = parametresLocal.accostages[j].vAcciDep;
      tmpS = String.valueOf(donnees_[13][j]);
      if (tmpS.equals("oui")) {
        parametresLocal.accostages[j].vAcciDep = 1.;
      } else {
        parametresLocal.accostages[j].vAcciDep = 0.;
      }
      if (parametresLocal.accostages[j].vAcciDep != tmpD) {
        changed += "accostages[j].cAcciDep|";
      }
      tmpD = parametresLocal.accostages[j].vFreqFor;
      tmpS = String.valueOf(donnees_[15][j]);
      if (tmpS.equals("oui")) {
        parametresLocal.accostages[j].vFreqFor = 1.;
      } else {
        parametresLocal.accostages[j].vFreqFor = 0.;
      }
      if (parametresLocal.accostages[j].vFreqFor != tmpD) {
        changed += "accostages[j].cFreqFor|";
      }
      tmpD = parametresLocal.accostages[j].vRareFor;
      tmpS = String.valueOf(donnees_[16][j]);
      if (tmpS.equals("oui")) {
        parametresLocal.accostages[j].vRareFor = 1.;
      } else {
        parametresLocal.accostages[j].vRareFor = 0.;
      }
      if (parametresLocal.accostages[j].vRareFor != tmpD) {
        changed += "accostages[j].cRareFor|";
      }
      tmpD = parametresLocal.accostages[j].vCalcFor;
      tmpS = String.valueOf(donnees_[17][j]);
      if (tmpS.equals("oui")) {
        parametresLocal.accostages[j].vCalcFor = 1.;
      } else {
        parametresLocal.accostages[j].vCalcFor = 0.;
      }
      if (parametresLocal.accostages[j].vCalcFor != tmpD) {
        changed += "accostages[j].cCalcFor|";
      }
      tmpD = parametresLocal.accostages[j].vAcciFor;
      tmpS = String.valueOf(donnees_[18][j]);
      if (tmpS.equals("oui")) {
        parametresLocal.accostages[j].vAcciFor = 1.;
      } else {
        parametresLocal.accostages[j].vAcciFor = 0.;
      }
      if (parametresLocal.accostages[j].vAcciFor != tmpD) {
        changed += "accostages[j].cAcciFor|";
      }
      // ======================
      j = j + 1;
      System.out.println("=== accost [j]. j =  " + j + "  (j impair)");
      if (parametresLocal.accostages[j] == null) {
        parametresLocal.accostages[j] = new SAccostage();
      }
      tmpD = parametresLocal.accostages[j].z;
      tmpS = String.valueOf(donnees_[1][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.accostages[j].z = -999.;
      } else {
        parametresLocal.accostages[j].z = Double.parseDouble(tmpS);
      }
      if (parametresLocal.accostages[j].z != tmpD) {
        changed += "accostages[j].z|";
      }
      tmpI = parametresLocal.accostages[j].nbDef;
      tmpS = String.valueOf(donnees_[2][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.accostages[j].nbDef = 0;
      } else {
        parametresLocal.accostages[j].nbDef = Integer.parseInt(tmpS);
      }
      if (parametresLocal.accostages[j].nbDef != tmpI) {
        changed += "accostages[j].nbDef|";
      }
      tmpI = parametresLocal.accostages[j].numDef;
      tmpS = String.valueOf(donnees_[3][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.accostages[j].numDef = -999;
      } else {
        parametresLocal.accostages[j].numDef = Integer.parseInt(tmpS);
      }
      if (parametresLocal.accostages[j].numDef != tmpI) {
        changed += "accostages[j].numDef|";
      }
      tmpD = parametresLocal.accostages[j].vFreqEnr;
      tmpS = String.valueOf(donnees_[5][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.accostages[j].vFreqEnr = -999.;
      } else {
        parametresLocal.accostages[j].vFreqEnr = Double.parseDouble(tmpS);
      }
      if (parametresLocal.accostages[j].vFreqEnr != tmpD) {
        changed += "accostages[j].vFreqEnr|";
      }
      tmpD = parametresLocal.accostages[j].vRareEnr;
      tmpS = String.valueOf(donnees_[6][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.accostages[j].vRareEnr = -999.;
      } else {
        parametresLocal.accostages[j].vRareEnr = Double.parseDouble(tmpS);
      }
      if (parametresLocal.accostages[j].vRareEnr != tmpD) {
        changed += "accostages[j].vRareEnr|";
      }
      tmpD = parametresLocal.accostages[j].vCalcEnr;
      tmpS = String.valueOf(donnees_[7][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.accostages[j].vCalcEnr = -999.;
      } else {
        parametresLocal.accostages[j].vCalcEnr = Double.parseDouble(tmpS);
      }
      if (parametresLocal.accostages[j].vCalcEnr != tmpD) {
        changed += "accostages[j].vCalcEnr|";
      }
      tmpD = parametresLocal.accostages[j].vAcciEnr;
      tmpS = String.valueOf(donnees_[8][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.accostages[j].vAcciEnr = -999.;
      } else {
        parametresLocal.accostages[j].vAcciEnr = Double.parseDouble(tmpS);
      }
      if (parametresLocal.accostages[j].vAcciEnr != tmpD) {
        changed += "accostages[j].vAcciEnr|";
      }
      tmpD = parametresLocal.accostages[j].vFreqDep;
      tmpS = String.valueOf(donnees_[10][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.accostages[j].vFreqDep = -999.;
      } else {
        parametresLocal.accostages[j].vFreqDep = Double.parseDouble(tmpS);
      }
      if (parametresLocal.accostages[j].vFreqDep != tmpD) {
        changed += "accostages[j].vFreqDep|";
      }
      tmpD = parametresLocal.accostages[j].vRareDep;
      tmpS = String.valueOf(donnees_[11][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.accostages[j].vRareDep = -999.;
      } else {
        parametresLocal.accostages[j].vRareDep = Double.parseDouble(tmpS);
      }
      if (parametresLocal.accostages[j].vRareDep != tmpD) {
        changed += "accostages[j].vRareDep|";
      }
      tmpD = parametresLocal.accostages[j].vCalcDep;
      tmpS = String.valueOf(donnees_[12][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.accostages[j].vCalcDep = -999.;
      } else {
        parametresLocal.accostages[j].vCalcDep = Double.parseDouble(tmpS);
      }
      if (parametresLocal.accostages[j].vCalcDep != tmpD) {
        changed += "accostages[j].vCalcDep|";
      }
      tmpD = parametresLocal.accostages[j].vAcciDep;
      tmpS = String.valueOf(donnees_[13][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.accostages[j].vAcciDep = -999.;
      } else {
        parametresLocal.accostages[j].vAcciDep = Double.parseDouble(tmpS);
      }
      if (parametresLocal.accostages[j].vAcciDep != tmpD) {
        changed += "accostages[j].vAcciDep|";
      }
      tmpD = parametresLocal.accostages[j].vFreqFor;
      tmpS = String.valueOf(donnees_[15][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.accostages[j].vFreqFor = -999.;
      } else {
        parametresLocal.accostages[j].vFreqFor = Double.parseDouble(tmpS);
      }
      if (parametresLocal.accostages[j].vFreqFor != tmpD) {
        changed += "accostages[j].vFreqFor|";
      }
      tmpD = parametresLocal.accostages[j].vRareFor;
      tmpS = String.valueOf(donnees_[16][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.accostages[j].vRareFor = -999.;
      } else {
        parametresLocal.accostages[j].vRareFor = Double.parseDouble(tmpS);
      }
      if (parametresLocal.accostages[j].vRareFor != tmpD) {
        changed += "accostages[j].vRareFor|";
      }
      tmpD = parametresLocal.accostages[j].vCalcFor;
      tmpS = String.valueOf(donnees_[17][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.accostages[j].vCalcFor = -999.;
      } else {
        parametresLocal.accostages[j].vCalcFor = Double.parseDouble(tmpS);
      }
      if (parametresLocal.accostages[j].vCalcFor != tmpD) {
        changed += "accostages[j].vCalcFor|";
      }
      tmpD = parametresLocal.accostages[j].vAcciFor;
      tmpS = String.valueOf(donnees_[18][j]);
      if (tmpS.equals(blanc)) {
        parametresLocal.accostages[j].vAcciFor = -999.;
      } else {
        parametresLocal.accostages[j].vAcciFor = Double.parseDouble(tmpS);
      }
      if (parametresLocal.accostages[j].vAcciFor != tmpD) {
        changed += "accostages[j].vAcciFor|";
      }
      j = j + 1;
    } // fork
    // StringTokenizer tok = new StringTokenizer(changed, "|");
    // while( tok.hasMoreTokens() ) {
    // FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(new FudaaParamEvent(
    // this, 0, TaucomacResource.TAUCO, params , TaucomacResource.TAUCO+":"+tok.nextToken()));
    return parametresLocal;
  }

  // ===============================================
  class MyTableModel extends AbstractTableModel {

    final String[] columnNames = { "", " 1 ", "", " 2 ", "", " 3 ", "", "4 ", "", "5 ", "", "6 ", "", "7", "", "8", "",
        "9", "", "10", "", " 11" };
    private int nbMaxColEditable_ = columnNames.length;

    public void setNbMaxColEditable(final int _n) {
      if (_n < 0) {
        nbMaxColEditable_ = 0;
      } else if (_n > columnNames.length) {
        nbMaxColEditable_ = columnNames.length;
      }
      if (_n != nbMaxColEditable_) {
        nbMaxColEditable_ = _n;
        fireTableStructureChanged();
        // System.out.println("changement nbMaxColEditable_"+nbMaxColEditable_);
      }
    }

    public boolean isCellEditable(final int row, final int col) {
      if (row == 0) {
        return false;
      }
      if ((col < 0) || (col >= nbMaxColEditable_)) {
        return false;
      }
      return true;
    }

    public int getColumn(final int col) {
      // System.out.println("col = "+col);
      return col;
    }

    public int getColumnCount() {
      // System.out.println("getColumnCount");
      return nbMaxColEditable_;
    }

    public int getRowCount() {
      return donnees_.length;
    }

    public String getColumnName(final int col) {
      if (col < columnNames.length) {
        return columnNames[col];
      }
      return "";
    }

    public Object getValueAt(final int row, final int col) {
      if ((row < donnees_.length) && (col < columnNames.length)) {
        return donnees_[row][col];
      }
      return new data("");
    }

    /*
     * public Class getColumnClass(int c) { return getValueAt(0, c).getClass(); }
     */
    public boolean isNombre(final String chaine) {
      final int taille = chaine.length();
      if (taille != 0) {
        int i = 0;
        boolean bool = true;
        while (i < taille
            && (chaine.charAt(i) >= '0' && chaine.charAt(i) <= '9' || chaine.charAt(i) == '-' || chaine.charAt(i) == '.')) {
          if (i + 1 < taille && chaine.charAt(i + 1) == '-') {
            return false;
          }
          if (chaine.charAt(i) == '.') {
            if (!bool) {
              return false;
            }
            bool = false;
          }
          i++;
        }
        return (i == taille);
      }
      return false;
    }

    public void setValueAt(final Object value, final int row, final int col) {
      // System.out.println("le contenu avant : "+ donnees_[row][col]);
      if (isNombre(value.toString())) {
        if (value.toString().charAt(0) == '.') {
          donnees_[row][col].setValeur("0" + value.toString());
        } else {
          donnees_[row][col].setValeur(value.toString());
          // System.out.println("Vous avez rentr� "+value+" � la case ("+row+","+col+").");
        }
      } else {
        // System.out.println("Vous avez cliqu� "+value+" � la case (" +row+ "," +col+ ").");
        // System.out.println("dont le contenu est : "+ donnees_[row][col]);
        if (row == 0) {
          donnees_[row][col].setValeur(value.toString());
          if ((donnees_[row][col].toString()).equals("oui")) {
            donnees_[row + 1][col].setValeur("*");
          }
          if ((donnees_[row][col].toString()).equals("non")) {
            donnees_[row + 1][col].setValeur("");
          }
        } else {
          donnees_[row][col].setValeur(value.toString());
        }
      }
      if (((data) myModel.getValueAt(row, col)).getCouleur() == Color.white) {
        ((data) myModel.getValueAt(row, col)).setCouleur(Color.cyan);
      }
    }
  }

  // ====================
  class MyTableModel1 extends AbstractTableModel {

    final String[] columnNames = { "Accostage" };

    public int getColumnCount() {
      return columnNames.length;
    }

    public int getRowCount() {
      return data1.length;
    }

    public Object getValueAt(final int row, final int col) {
      return data1[row][col];
    }

    public String getColumnName(final int col) {
      return columnNames[col];
    }

    public boolean isCellEditable(final int row, final int col) {
      return (col != 0);
    }
  };

  // ========================
  class MyTableModel2 extends AbstractTableModel {

    final String[] columnNames = { "Unit�" };

    public int getColumnCount() {
      return columnNames.length;
    }

    public int getRowCount() {
      return data2.length;
    }

    public Object getValueAt(final int row, final int col) {
      return data2[row][col];
    }

    public String getColumnName(final int col) {
      return columnNames[col];
    }

    public boolean isCellEditable(final int row, final int col) {
      return col != 0;
    }
  };

  // =========================
  public void majlook(final int phase, final int unit) {
    // System.out.println(" majlook");
    // int style_ = style;
    final int phase_ = phase;
    final int unit_ = unit;
    localPhase_ = phase_;
    // lookInit();
    titreBasG.setForeground(Color.blue);
    titreBasD1.setForeground(Color.blue);
    if (unit_ == 0) {
      data2[4][0] = "   t.m";
      data2[5][0] = "   t.m";
      data2[6][0] = "   t.m";
      data2[7][0] = "   t.m";
      data2[14][0] = "   t";
      data2[15][0] = "   t";
      data2[16][0] = "   t";
      data2[17][0] = "   t";
    } else {
      data2[4][0] = "   kJ";
      data2[5][0] = "   kJ";
      data2[6][0] = "   kJ";
      data2[7][0] = "   kJ";
      data2[14][0] = "   kN";
      data2[15][0] = "   kN";
      data2[16][0] = "   kN";
      data2[17][0] = "   kN";
    }
    TableColumn kolw = null;
    final int n = myModel.getColumnCount();
    for (int i = 0; i < n; i++) {
      kolw = table.getColumnModel().getColumn(i);
      if (table.getColumnName(i).equals("")) {
        kolw.setPreferredWidth(40);
      } else {
        kolw.setPreferredWidth(70);
      }
    }
    String chaine = "";
    for (int il = 0; il < donneesNbLigne_; il++) {
      for (int ic = 0; ic < donneesNbColonne_; ic++) {
        chaine = String.valueOf(myModel.getValueAt(il, ic));
        // System.out.println("line "+il+" "+" col "+ic+" chaine " + chaine);
        if (chaine.equals("non")) {
          ((data) myModel.getValueAt(il, ic)).setCouleur(Color.red);
          ((data) myModel.getValueAt(il, ic + 1)).setCouleur(Color.gray);
        }
        if (chaine.equals("oui")) {
          ((data) myModel.getValueAt(il, ic)).setCouleur(Color.green);
          ((data) myModel.getValueAt(il, ic + 1)).setCouleur(Color.cyan);
        }
        if (lignePhase[il] > localPhase_) {
          ((data) myModel.getValueAt(il, ic)).setCouleur(Color.gray);
        }
      }
    }
    table.repaint();
  }

  public Class getColumnClass(final int c) {
    return getValueAt(0, c).getClass();
  }

  public Object getValueAt(final int row, final int col) // autre getValueAt
  {
    return donnees_[row][col];
  }

  // =============================
  void selectAvise(final int line, final int col) {
    int kfois = 0;
    if (line >= 0) {
      linMem = line;
    }
    if (col >= 0) {
      colMem = col;
    }
    // javax.swing.table.TableModel model = table.getModel();
    if (ancolMem >= 0 && anclinMem >= 0) {
      if (((data) myModel.getValueAt(anclinMem, ancolMem)).getCouleur() == Color.white) {
        ((data) myModel.getValueAt(anclinMem, ancolMem)).setCouleur(Color.cyan);
      }
    }
    if (colMem >= 0 && linMem >= 0) {
      if (((data) myModel.getValueAt(linMem, colMem)).getCouleur() == Color.cyan) {
        ((data) myModel.getValueAt(linMem, colMem)).setCouleur(Color.white);
      }
      String chaine = "";
      chaine = String.valueOf(myModel.getValueAt(linMem, colMem));
      // System.out.println("line "+linMem+" "+" col "+colMem+" chaine " + chaine);
      if (chaine.equals("oui")) {
        myModel.setValueAt("non", linMem, colMem);
        ((data) myModel.getValueAt(linMem, colMem)).setCouleur(Color.red);
        ((data) myModel.getValueAt(linMem, colMem + 1)).setCouleur(Color.gray);
        kfois = 1;
      }
      if (kfois == 0) {
        if (chaine.equals("non")) {
          myModel.setValueAt("oui", linMem, colMem);
          ((data) myModel.getValueAt(linMem, colMem)).setCouleur(Color.green);
          ((data) myModel.getValueAt(linMem, colMem + 1)).setCouleur(Color.cyan);
        }
      }
      kfois = 0;
      // myModel.fireTableStructureChanged(); surtout pas
      table.repaint();
      anclinMem = linMem;
      ancolMem = colMem;
    }
  }

  public void lookInit() {
    // System.out.println(" lookinit");
    for (int il = 0; il < donneesNbLigne_; il++) {
      for (int ic = 0; ic < donneesNbColonne_; ic++) {
        ((data) myModel.getValueAt(il, ic)).setCouleur(Color.cyan);
        if ((il == 4) || (il == 9) || (il == 14)) {
          ((data) myModel.getValueAt(il, ic)).setCouleur(Color.orange);
        }
        int k;
        k = ic - (ic / 2) * 2;
        if (k == 0) {
          if ((il <= 3) || (il >= 5 && il <= 8)) {
            ((data) myModel.getValueAt(il, ic)).setCouleur(Color.gray);
          }
          if ((il >= 10 && il <= 13) || (il > 14)) {
            myModel.setValueAt("oui", il, ic);
            ((data) myModel.getValueAt(il, ic)).setCouleur(Color.green);
          }
        } else {
          if (il == 0) {
            k = (ic / 2) + 1;
            myModel.setValueAt(String.valueOf(k), il, ic);
          }
        }
        if (lignePhase[il] > localPhase_) {
          ((data) myModel.getValueAt(il, ic)).setCouleur(Color.gray);
        }
      }
    }
    myModel.fireTableStructureChanged();
    table.repaint();
  }
  // ===========================
} // class
