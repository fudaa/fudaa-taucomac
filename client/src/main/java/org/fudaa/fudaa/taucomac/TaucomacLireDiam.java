/*
 * @file         TaucomacLireDiam.java
 * @creation     2000-10-17
 * @modification $Date: 2006-09-19 15:08:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.taucomac;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
/**
 *
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:08:57 $ by $Author: deniger $
 * @author       Jean-Yves Riou
 */
public class TaucomacLireDiam {
  static String[] valDiam1= new String[100];
  static String[] valDiam2= new String[100];
  static String[] valDiam3= new String[100];
  public int nDiam1= 0;
  public int nDiam2= 0;
  public int nDiam3= 0;
  public TaucomacLireDiam() {
    for (int i= 0; i < 100; i++) {
      valDiam1[i]= "";
      valDiam2[i]= "";
      valDiam3[i]= "";
    }
    //System.out.println(" LireDiam   <=  diamcom1.data");
    lirefich1();
    lirefich2();
    lirefich3();
    //for (int i = 0; i <nDiam1; i++)
    //{
    // System.out.println(" i "+i+ " "+ valDiam1[i]);
    //}
  }
  //===============
  public void lirefich1() {
    try {
      System.out.println(" diamcom1.data");
      final FileReader fichier= new FileReader("diamcom1.data");
      final BufferedReader buff= new BufferedReader(fichier);
      boolean eof= false;
      while (!eof) {
        final String line= buff.readLine();
        if (line == null) {
          eof= true;
        } else {
          valDiam1[nDiam1]= line;
          // System.out.println(  line + " ") ;
          nDiam1++;
        }
      }
      buff.close();
      System.out.println("nDiam1 = " + nDiam1);
    } catch (final IOException e) {
      System.out.println("Erreur --  " + e.toString());
    }
  }
  //==============
  public void lirefich2() {
    try {
      System.out.println(" diamcom2.data");
      final FileReader fichier= new FileReader("diamcom2.data");
      final BufferedReader buff= new BufferedReader(fichier);
      boolean eof= false;
      while (!eof) {
        final String line= buff.readLine();
        if (line == null) {
          eof= true;
        } else {
          valDiam2[nDiam2]= line;
          // System.out.println(  line + " ") ;
          nDiam2++;
        }
      }
      buff.close();
      System.out.println("nDiam2 = " + nDiam2);
    } catch (final IOException e) {
      System.out.println("Erreur --  " + e.toString());
    }
  }
  //==============
  public void lirefich3() {
    try {
      System.out.println(" diamperso.data");
      final FileReader fichier= new FileReader("diamperso.data");
      final BufferedReader buff= new BufferedReader(fichier);
      boolean eof= false;
      while (!eof) {
        final String line= buff.readLine();
        if (line == null) {
          eof= true;
        } else {
          valDiam3[nDiam3]= line;
          // System.out.println(  line + " ") ;
          nDiam3++;
        }
      }
      buff.close();
      System.out.println("nDiam3 = " + nDiam3);
    } catch (final IOException e) {
      System.out.println("Erreur --  " + e.toString());
    }
  }
  //==============
  public int getNombreDiam1() {
    return (nDiam1);
  }
  public String getDiam1(final int k) {
    return (valDiam1[k]);
  }
  //===========
  public int getNombreDiam2() {
    return (nDiam2);
  }
  public String getDiam2(final int k) {
    return (valDiam2[k]);
  }
  //===========
  public int getNombreDiam3() {
    return (nDiam3);
  }
  public String getDiam3(final int k) {
    return (valDiam3[k]);
  }
  //===========
}
