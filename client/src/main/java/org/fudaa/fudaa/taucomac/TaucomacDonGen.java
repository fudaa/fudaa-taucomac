/*
 * @file         TaucomacDonGen.java
 * @creation     2000-10-17
 * @modification $Date: 2006-09-19 15:08:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.taucomac;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.taucomac.SDonGeneral;
/**
 * Onglet des titre,Commentaire.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:08:56 $ by $Author: deniger $
 * @author       Jean-Yves Riou
 */
public class TaucomacDonGen
  extends TaucomacMerePanParm
  implements ActionListener {
  SDonGeneral parametresLocal= null;
  //donGen.titreEtude
  //donGen.comentEtude
  //donGen.typeSysUnit
  //donGen.typeChoixCal
  //donGen.typeStyle
  JLabel label1;
  JLabel label2;
  BuTextField parm1;
  BuTextField parm2;
  JPanel pnHaut;
  JPanel pnMil;
  JPanel pnBas;
  JPanel pnMilG;
  JPanel pnMilD;
  TitledBorder titreMilG;
  TitledBorder titreMilD;
  TitledBorder titreBas;
  JRadioButton[] rbs_;
  JRadioButton[] rbd_;
  JRadioButton[] rbi_;
  public TaucomacDonGen(final BuCommonInterface _appli) {
    super();
    pnHaut= new JPanel();
    pnHaut.setBorder(BorderFactory.createEmptyBorder(20, 120, 20, 120));
    pnHaut.setLayout(new BorderLayout());
    pnHaut.setPreferredSize(new Dimension(650, 80));
    //
    pnMil= new JPanel();
    pnMil.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    pnMil.setLayout(new BorderLayout(30, 20));
    pnMil.setPreferredSize(new Dimension(650, 90));
    //
    pnBas= new JPanel();
    titreBas= new TitledBorder("Phase de calcul");
    placeTitre(pnBas, titreBas, 0);
    pnBas.setLayout(new BorderLayout(30, 20));
    pnBas.setPreferredSize(new Dimension(300, 120));
    //
    pnMilG= new JPanel();
    pnMilG.setLayout(new BorderLayout());
    titreMilG= new TitledBorder("Syst�me d'unit�s");
    placeTitre(pnMilG, titreMilG, 0);
    pnMilG.setPreferredSize(new Dimension(300, 80));
    //
    pnMilD= new JPanel();
    pnMilD.setLayout(new BorderLayout());
    titreMilD= new TitledBorder("Style");
    placeTitre(pnMilD, titreMilD, 0);
    pnMilD.setPreferredSize(new Dimension(300, 80));
    //
    //pnMil.add(pnMilG, BorderLayout.WEST);
    //pnMil.add(pnMilD, BorderLayout.EAST);
    //===========================================
    final GridBagLayout lm= new GridBagLayout();
    final GridBagConstraints c= new GridBagConstraints();
    c.ipadx= 30;
    c.ipady= 20;
    c.gridx= 0;
    c.gridy= 0;
    placePanelG(this, pnHaut, lm, c);
    c.gridy= 1;
    placePanelG(this, pnMilG, lm, c);
    c.gridy= 2;
    placePanelG(this, pnMilD, lm, c);
    c.gridy= 3;
    placePanelG(this, pnBas, lm, c);
    //===========================================
    label1= new JLabel("      Titre  ", SwingConstants.RIGHT);
    label2= new JLabel("Commentaire  ", SwingConstants.RIGHT);
    parm1= new BuTextField();
    parm2= new BuTextField();
    final JPanel labelPane= new JPanel();
    labelPane.setLayout(new GridLayout(0, 1));
    placeComp(labelPane, label1, 0);
    placeComp(labelPane, label2, 0);
    final JPanel fieldPane= new JPanel();
    fieldPane.setLayout(new GridLayout(0, 1));
    placeComp(fieldPane, parm1, 0);
    placeComp(fieldPane, parm2, 0);
    pnHaut.add(labelPane, BorderLayout.WEST);
    pnHaut.add(fieldPane, BorderLayout.CENTER);
    //================================================
    pnBas.setLayout(new BuGridLayout(2, 5, 5, true, false));
    final ButtonGroup bi= new ButtonGroup();
    rbi_= new JRadioButton[3];
    for (int i= 0; i < 3; i++) {
      rbi_[i]= new JRadioButton();
      rbi_[i].addActionListener(this);
      bi.add(rbi_[i]);
    }
    int n;
    n= 0;
    pnBas.add(rbi_[0], n++);
    pnBas.add(new BuLabelMultiLine("DUCA,ALBE,COMME"), n++);
    pnBas.add(rbi_[1], n++);
    pnBas.add(new BuLabelMultiLine("Am�lioration nouveau logiciel"), n++);
    pnBas.add(rbi_[2], n++);
    pnBas.add(new BuLabelMultiLine("TAUCOMAC ducs d'Albe"), n++);
    //==============================================
    //(int _columns,  int _hgap, int _vgap, boolean _hfilled, boolean _vfilled)
    pnMilG.setLayout(new BuGridLayout(2, 5, 5, true, false));
    final ButtonGroup bg= new ButtonGroup();
    rbs_= new JRadioButton[2];
    for (int i= 0; i < 2; i++) {
      rbs_[i]= new JRadioButton();
      rbs_[i].addActionListener(this);
      bg.add(rbs_[i]);
    }
    n= 0;
    pnMilG.add(rbs_[0], n++);
    pnMilG.add(new BuLabelMultiLine("Syst�me d'unit� tonnes"), n++);
    pnMilG.add(rbs_[1], n++);
    pnMilG.add(new BuLabelMultiLine("Syst�me d'unit� Newton/Joules"), n++);
    //================================================
    pnMilD.setLayout(new BuGridLayout(2, 5, 5, true, false));
    final ButtonGroup bd= new ButtonGroup();
    rbd_= new JRadioButton[2];
    for (int i= 0; i < 2; i++) {
      rbd_[i]= new JRadioButton();
      rbd_[i].addActionListener(this);
      bd.add(rbd_[i]);
    }
    n= 0;
    pnMilD.add(rbd_[0], n++);
    pnMilD.add(new BuLabelMultiLine("Style standard"), n++);
    pnMilD.add(rbd_[1], n++);
    pnMilD.add(new BuLabelMultiLine("Autre style"), n++);
    //===================================================
    setParametres(parametresLocal);
  }
  public void actionPerformed(final ActionEvent e) {
    //      String cmd=e.getActionCommand();
    //    Object src=e.getSource();
    parametresLocal= getParametres();
    setVisible(true);
  }
  public void setParametres(final SDonGeneral parametresProjet_) {
    parametresLocal= parametresProjet_;
    if (parametresLocal != null) {
      parm1.setText(parametresLocal.titreEtude);
      parm2.setText(parametresLocal.comentEtude);
      setSelectedPhase(parametresLocal.typeChoixCal);
      setSelectedUnit(parametresLocal.typeSysUnit);
      setSelectedStyle(parametresLocal.typeStyle);
    } else {
      setSelectedPhase(2);
      setSelectedUnit(0);
      setSelectedStyle(1);
    }
  }
  public SDonGeneral getParametres() {
    if (parametresLocal == null) {
      parametresLocal= new SDonGeneral();
    }
    parametresLocal.titreEtude= parm1.getText();
    parametresLocal.comentEtude= parm2.getText();
    parametresLocal.typeSysUnit= getSelectedUnit();
    parametresLocal.typeChoixCal= getSelectedPhase();
    parametresLocal.typeStyle= getSelectedStyle();
    return parametresLocal;
  }
  //==========================================================
  public int getSelectedStyle() {
    for (int k= 0; k < 2; k++) {
      //reLook();
      if (rbd_[k].isSelected()) {
        System.out.println("*style =    " + k);
      }
      if (rbd_[k].isSelected()) {
        return k;
      }
    }
    return 0;
  }
  public int getSelectedPhase() {
    for (int i= 0; i < 3; i++) {
      if (rbi_[i].isSelected()) {
        System.out.println("*phase =    " + i);
      }
      if (rbi_[i].isSelected()) {
        return i;
      }
    }
    return 0;
  }
  public int getSelectedUnit() {
    for (int j= 0; j < 2; j++) {
      if (rbs_[j].isSelected()) {
        System.out.println("*unit =    " + j);
      }
      if (rbs_[j].isSelected()) {
        return j;
      }
    }
    return 0;
  }
  protected void setSelectedStyle(final int nStyle) {
    rbd_[nStyle].setSelected(true);
  }
  protected void setSelectedUnit(final int nChoix) {
    rbs_[nChoix].setSelected(true);
  }
  protected void setSelectedPhase(final int nPhase) {
    rbi_[nPhase].setSelected(true);
  }
  //==========================================================
}
