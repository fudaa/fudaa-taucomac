/*
 * @file         TaucomacAmarrage.java
 * @creation     2000-10-17
 * @modification $Date: 2006-09-19 15:08:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.taucomac;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.taucomac.SAmarr;
/**
 * Onglet Ammarage.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:08:56 $ by $Author: deniger $
 * @author       Jean-Yves Riou
 */
public class TaucomacAmarrage
  extends TaucomacMerePanParm
  implements ActionListener, FocusListener {
  SAmarr parametresLocal= null;
  /*
  struct SAmarr
  {
                                     // amarrage

   reel 			horiPerRare;
   reel            horiPerCalc;
   reel			horiPerAcci;

   reel 			horiVarFreq;
   reel 			horiVarRare;
   reel            horiVarCalc;
   reel			horiVarAcci;

   reel 			vertPerRare;
   reel            vertPerCalc;
   reel			vertPerAcci;

   reel 			vertVarFreq;
   reel 			vertVarRare;
   reel            vertVarCalc;
   reel			vertVarAcci;

   reel 			deplAdmFreq;
   reel 			deplAdmRare;
   reel            deplAdmCalc;
   reel			deplAdmAcci;


   reel 			psi0Am;
   reel			psi2Am;

  };

  SAmarr  			amarr;

  */
  /*
   struct SSolRupt     							//  couches de sol a la rupture
  {
   reel zToit;             // attention vide pour couche 1 et pour Dr(ainees)
   reel poidsPropre;
   reel angleFrot;
   reel cohesion;
  };
  typedef sequence<SSolRupt> VSSolRupts;
  struct SRupture
  {
  entier  		    nbCouchesRupt; 		// sol � la rupture     (ncouch <= 2)
   reel 			coefPartButFav;
   reel 			coefPartButDefav;
   reel 			coefPartCohetFav;
   reel 			coefPartCoheDefav;
   VSSolRupts      couchesRupt;        // non drainees court terme
   VSSolRupts      couchesRuptDr;      // drainees amarrage
  };

  SRupture            rupture;

  */
  //
  JPanel pnHaut;
  JPanel pnHautG;
  JPanel pnHautGtrav;
  JPanel pnHautD;
  JPanel pnHautDtrav;
  JPanel pnMil;
  JPanel pnMilG;
  JPanel pnMilD;
  JPanel pnMilGtrav;
  JPanel pnMilDtrav;
  JPanel pnBas;
  JPanel pnBasG;
  JPanel pnBasGtrav;
  JPanel pnBasD;
  JPanel pnBasDtrav;
  //
  JLabel la_titre1;
  JLabel la_rar1;
  JLabel la_cal1;
  JLabel la_acc1;
  BuTextField tf_rar1;
  BuTextField tf_cal1;
  BuTextField tf_acc1;
  JLabel unit_rar1;
  JLabel unit_cal1;
  JLabel unit_acc1;
  JLabel la_titre2;
  JLabel la_fre2;
  JLabel la_rar2;
  JLabel la_cal2;
  JLabel la_acc2;
  BuTextField tf_fre2;
  BuTextField tf_rar2;
  BuTextField tf_cal2;
  BuTextField tf_acc2;
  JLabel unit_fre2;
  JLabel unit_rar2;
  JLabel unit_cal2;
  JLabel unit_acc2;
  JLabel la_titre3;
  JLabel la_rar3;
  JLabel la_cal3;
  JLabel la_acc3;
  BuTextField tf_rar3;
  BuTextField tf_cal3;
  BuTextField tf_acc3;
  JLabel unit_rar3;
  JLabel unit_cal3;
  JLabel unit_acc3;
  JLabel la_titre4;
  JLabel la_fre4;
  JLabel la_rar4;
  JLabel la_cal4;
  JLabel la_acc4;
  BuTextField tf_fre4;
  BuTextField tf_rar4;
  BuTextField tf_cal4;
  BuTextField tf_acc4;
  JLabel unit_fre4;
  JLabel unit_rar4;
  JLabel unit_cal4;
  JLabel unit_acc4;
  JLabel la_titre5;
  JLabel la_fre5;
  JLabel la_rar5;
  JLabel la_cal5;
  JLabel la_acc5;
  BuTextField tf_fre5;
  BuTextField tf_rar5;
  BuTextField tf_cal5;
  BuTextField tf_acc5;
  JLabel unit_fre5;
  JLabel unit_rar5;
  JLabel unit_cal5;
  JLabel unit_acc5;
  JLabel la_titre6;
  JLabel la_titrepsi0;
  BuTextField tf_psi0;
  JLabel la_titrepsi2;
  BuTextField tf_psi2;
  //====================================
  //    Constructeur
  //====================================
  public TaucomacAmarrage(final BuCommonInterface _appli) {
    super();
    //======================================
    la_titre1=
      new JLabel("Force horizontale, composante permanente (>0) ", SwingConstants.LEFT);
    la_titre1.setForeground(Color.blue);
    la_rar1= new JLabel("valeur rare", SwingConstants.RIGHT);
    la_cal1= new JLabel("valeur de calcul", SwingConstants.RIGHT);
    la_acc1= new JLabel("valeur accidentelle", SwingConstants.RIGHT);
    tf_rar1= BuTextField.createDoubleField();
    tf_rar1.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_cal1= BuTextField.createDoubleField();
    tf_cal1.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_acc1= BuTextField.createDoubleField();
    tf_acc1.setHorizontalAlignment(SwingConstants.RIGHT);
    unit_rar1= new JLabel("   t ", SwingConstants.LEFT);
    unit_cal1= new JLabel("   t ", SwingConstants.LEFT);
    unit_acc1= new JLabel("   t ", SwingConstants.LEFT);
    placeUnit(unit_rar1, 6);
    placeUnit(unit_cal1, 6);
    placeUnit(unit_acc1, 6);
    pnHautGtrav= new JPanel();
    final BuGridLayout loCal4= new BuGridLayout();
    loCal4.setColumns(3);
    loCal4.setHgap(5);
    loCal4.setVgap(5);
    loCal4.setHfilled(true);
    loCal4.setCfilled(true);
    pnHautGtrav.setLayout(loCal4);
    pnHautGtrav.setBorder(new EmptyBorder(5, 5, 5, 5)); //top,left,bottom,right
    tf_rar1.setColumns(6);
    int n;
    n= 0;
    placeCompn(pnHautGtrav, la_rar1, 0, n);
    n++;
    placeCompn(pnHautGtrav, tf_rar1, 0, n);
    n++;
    placeCompn(pnHautGtrav, unit_rar1, 0, n);
    n++;
    placeCompn(pnHautGtrav, la_cal1, 2, n);
    n++;
    placeCompn(pnHautGtrav, tf_cal1, 2, n);
    n++;
    placeCompn(pnHautGtrav, unit_cal1, 2, n);
    n++;
    placeCompn(pnHautGtrav, la_acc1, 2, n);
    n++;
    placeCompn(pnHautGtrav, tf_acc1, 2, n);
    n++;
    placeCompn(pnHautGtrav, unit_acc1, 2, n);
    n++;
    la_titre2=
      new JLabel("Force horizontale, composante variable (>0)", SwingConstants.LEFT);
    la_titre2.setForeground(Color.blue);
    la_fre2= new JLabel("valeur fr�quente", SwingConstants.RIGHT);
    la_rar2= new JLabel("valeur rare", SwingConstants.RIGHT);
    la_cal2= new JLabel("valeur de calcul", SwingConstants.RIGHT);
    la_acc2= new JLabel("valeur accidentelle", SwingConstants.RIGHT);
    tf_fre2= BuTextField.createDoubleField();
    tf_fre2.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_rar2= BuTextField.createDoubleField();
    tf_rar2.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_cal2= BuTextField.createDoubleField();
    tf_cal2.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_acc2= BuTextField.createDoubleField();
    tf_acc2.setHorizontalAlignment(SwingConstants.RIGHT);
    unit_fre2= new JLabel("   t ", SwingConstants.LEFT);
    unit_rar2= new JLabel("   t ", SwingConstants.LEFT);
    unit_cal2= new JLabel("   t ", SwingConstants.LEFT);
    unit_acc2= new JLabel("   t ", SwingConstants.LEFT);
    placeUnit(unit_fre2, 6);
    placeUnit(unit_rar2, 6);
    placeUnit(unit_cal2, 6);
    placeUnit(unit_acc2, 6);
    pnHautDtrav= new JPanel();
    final BuGridLayout loCal1= new BuGridLayout();
    loCal1.setColumns(3);
    loCal1.setHgap(5);
    loCal1.setVgap(5);
    loCal1.setHfilled(true);
    loCal1.setCfilled(true);
    pnHautDtrav.setLayout(loCal1);
    pnHautDtrav.setBorder(new EmptyBorder(5, 5, 5, 5)); //top,left,bottom,right
    tf_fre2.setColumns(6);
    n= 0;
    placeCompn(pnHautDtrav, la_fre2, 2, n);
    n++;
    placeCompn(pnHautDtrav, tf_fre2, 2, n);
    n++;
    placeCompn(pnHautDtrav, unit_fre2, 2, n);
    n++;
    placeCompn(pnHautDtrav, la_rar2, 2, n);
    n++;
    placeCompn(pnHautDtrav, tf_rar2, 2, n);
    n++;
    placeCompn(pnHautDtrav, unit_rar2, 2, n);
    n++;
    placeCompn(pnHautDtrav, la_cal2, 2, n);
    n++;
    placeCompn(pnHautDtrav, tf_cal2, 2, n);
    n++;
    placeCompn(pnHautDtrav, unit_cal2, 2, n);
    n++;
    placeCompn(pnHautDtrav, la_acc2, 2, n);
    n++;
    placeCompn(pnHautDtrav, tf_acc2, 2, n);
    n++;
    placeCompn(pnHautDtrav, unit_acc2, 2, n);
    n++;
    pnHautG= new JPanel();
    pnHautG.setLayout(new BorderLayout());
    pnHautG.setBorder(BorderFactory.createEmptyBorder(2, 5, 2, 5));
    pnHautG.setPreferredSize(new Dimension(350, 150));
    pnHautG.add(la_titre1, BorderLayout.NORTH);
    pnHautG.add(pnHautGtrav, BorderLayout.SOUTH);
    pnHautD= new JPanel();
    pnHautD.setLayout(new BorderLayout());
    pnHautD.setBorder(BorderFactory.createEmptyBorder(2, 5, 2, 5));
    pnHautD.setPreferredSize(new Dimension(400, 150));
    pnHautD.add(la_titre2, BorderLayout.NORTH);
    pnHautD.add(pnHautDtrav, BorderLayout.SOUTH);
    pnHaut= new JPanel();
    pnHaut.setLayout(new BorderLayout());
    pnHaut.setBorder(BorderFactory.createEmptyBorder(2, 5, 2, 5));
    pnHaut.add(pnHautG, BorderLayout.WEST);
    pnHaut.add(pnHautD, BorderLayout.EAST);
    //===================================
    la_titre3=
      new JLabel("Force vertical, composante permanente (>0) ", SwingConstants.LEFT);
    la_titre3.setForeground(Color.blue);
    la_rar3= new JLabel("valeur rare", SwingConstants.RIGHT);
    la_cal3= new JLabel("valeur de calcul", SwingConstants.RIGHT);
    la_acc3= new JLabel("valeur accidentelle", SwingConstants.RIGHT);
    tf_rar3= BuTextField.createDoubleField();
    tf_rar3.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_cal3= BuTextField.createDoubleField();
    tf_cal3.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_acc3= BuTextField.createDoubleField();
    tf_acc3.setHorizontalAlignment(SwingConstants.RIGHT);
    unit_rar3= new JLabel("   t ", SwingConstants.LEFT);
    unit_cal3= new JLabel("   t ", SwingConstants.LEFT);
    unit_acc3= new JLabel("   t ", SwingConstants.LEFT);
    placeUnit(unit_rar3, 6);
    placeUnit(unit_cal3, 6);
    placeUnit(unit_acc3, 6);
    pnMilGtrav= new JPanel();
    final BuGridLayout loCal2= new BuGridLayout();
    loCal2.setColumns(3);
    loCal2.setHgap(5);
    loCal2.setVgap(5);
    loCal2.setHfilled(true);
    loCal2.setCfilled(true);
    pnMilGtrav.setLayout(loCal2);
    pnMilGtrav.setBorder(new EmptyBorder(5, 5, 5, 5)); //top,left,bottom,right
    tf_rar3.setColumns(6);
    n= 0;
    placeCompn(pnMilGtrav, la_rar3, 0, n);
    n++;
    placeCompn(pnMilGtrav, tf_rar3, 0, n);
    n++;
    placeCompn(pnMilGtrav, unit_rar3, 0, n);
    n++;
    placeCompn(pnMilGtrav, la_cal3, 2, n);
    n++;
    placeCompn(pnMilGtrav, tf_cal3, 2, n);
    n++;
    placeCompn(pnMilGtrav, unit_cal3, 2, n);
    n++;
    placeCompn(pnMilGtrav, la_acc3, 2, n);
    n++;
    placeCompn(pnMilGtrav, tf_acc3, 2, n);
    n++;
    placeCompn(pnMilGtrav, unit_acc3, 2, n);
    n++;
    la_titre4=
      new JLabel("Force verticale, composante variable (>0)", SwingConstants.LEFT);
    la_titre4.setForeground(Color.blue);
    la_fre4= new JLabel("valeur fr�quente", SwingConstants.RIGHT);
    la_rar4= new JLabel("valeur rare", SwingConstants.RIGHT);
    la_cal4= new JLabel("valeur de calcul", SwingConstants.RIGHT);
    la_acc4= new JLabel("valeur accidentelle", SwingConstants.RIGHT);
    tf_fre4= BuTextField.createDoubleField();
    tf_fre4.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_rar4= BuTextField.createDoubleField();
    tf_rar4.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_cal4= BuTextField.createDoubleField();
    tf_cal4.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_acc4= BuTextField.createDoubleField();
    tf_acc4.setHorizontalAlignment(SwingConstants.RIGHT);
    unit_fre4= new JLabel("   t ", SwingConstants.LEFT);
    unit_rar4= new JLabel("   t ", SwingConstants.LEFT);
    unit_cal4= new JLabel("   t ", SwingConstants.LEFT);
    unit_acc4= new JLabel("   t ", SwingConstants.LEFT);
    placeUnit(unit_fre4, 6);
    placeUnit(unit_rar4, 6);
    placeUnit(unit_cal4, 6);
    placeUnit(unit_acc4, 6);
    pnMilDtrav= new JPanel();
    final BuGridLayout loCal3= new BuGridLayout();
    loCal3.setColumns(3);
    loCal3.setHgap(5);
    loCal3.setVgap(5);
    loCal3.setHfilled(true);
    loCal3.setCfilled(true);
    pnMilDtrav.setLayout(loCal3);
    pnMilDtrav.setBorder(new EmptyBorder(5, 5, 5, 5)); //top,left,bottom,right
    tf_fre4.setColumns(6);
    n= 0;
    placeCompn(pnMilDtrav, la_fre4, 2, n);
    n++;
    placeCompn(pnMilDtrav, tf_fre4, 2, n);
    n++;
    placeCompn(pnMilDtrav, unit_fre4, 2, n);
    n++;
    placeCompn(pnMilDtrav, la_rar4, 2, n);
    n++;
    placeCompn(pnMilDtrav, tf_rar4, 2, n);
    n++;
    placeCompn(pnMilDtrav, unit_rar4, 2, n);
    n++;
    placeCompn(pnMilDtrav, la_cal4, 2, n);
    n++;
    placeCompn(pnMilDtrav, tf_cal4, 2, n);
    n++;
    placeCompn(pnMilDtrav, unit_cal4, 2, n);
    n++;
    placeCompn(pnMilDtrav, la_acc4, 2, n);
    n++;
    placeCompn(pnMilDtrav, tf_acc4, 2, n);
    n++;
    placeCompn(pnMilDtrav, unit_acc4, 2, n);
    n++;
    pnMilG= new JPanel();
    pnMilG.setLayout(new BorderLayout());
    pnMilG.setBorder(BorderFactory.createEmptyBorder(2, 5, 2, 5));
    pnMilG.setPreferredSize(new Dimension(350, 150));
    pnMilG.add(la_titre3, BorderLayout.NORTH);
    pnMilG.add(pnMilGtrav, BorderLayout.SOUTH);
    pnMilD= new JPanel();
    pnMilD.setLayout(new BorderLayout());
    pnMilD.setBorder(BorderFactory.createEmptyBorder(2, 5, 2, 5));
    pnMilD.setPreferredSize(new Dimension(400, 150));
    pnMilD.add(la_titre4, BorderLayout.NORTH);
    pnMilD.add(pnMilDtrav, BorderLayout.SOUTH);
    pnMil= new JPanel();
    pnMil.setLayout(new BorderLayout());
    pnMil.setBorder(BorderFactory.createEmptyBorder(2, 5, 2, 5));
    pnMil.add(pnMilG, BorderLayout.WEST);
    pnMil.add(pnMilD, BorderLayout.EAST);
    //===================================
    la_titre5= new JLabel("D�placement admissible (>0)", SwingConstants.LEFT);
    la_titre5.setForeground(Color.blue);
    la_fre5= new JLabel("valeur fr�quente", SwingConstants.RIGHT);
    la_rar5= new JLabel("valeur rare", SwingConstants.RIGHT);
    la_cal5= new JLabel("valeur de calcul", SwingConstants.RIGHT);
    la_acc5= new JLabel("valeur accidentelle", SwingConstants.RIGHT);
    tf_fre5= BuTextField.createDoubleField();
    tf_fre5.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_rar5= BuTextField.createDoubleField();
    tf_rar5.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_cal5= BuTextField.createDoubleField();
    tf_cal5.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_acc5= BuTextField.createDoubleField();
    tf_acc5.setHorizontalAlignment(SwingConstants.RIGHT);
    unit_fre5= new JLabel("   cm ", SwingConstants.LEFT);
    unit_rar5= new JLabel("   cm ", SwingConstants.LEFT);
    unit_cal5= new JLabel("   cm ", SwingConstants.LEFT);
    unit_acc5= new JLabel("   cm ", SwingConstants.LEFT);
    placeUnit(unit_fre5, 12);
    placeUnit(unit_rar5, 12);
    placeUnit(unit_cal5, 12);
    placeUnit(unit_acc5, 12);
    pnBasGtrav= new JPanel();
    final BuGridLayout loCal0= new BuGridLayout();
    loCal0.setColumns(3);
    loCal0.setHgap(5);
    loCal0.setVgap(5);
    loCal0.setHfilled(true);
    loCal0.setCfilled(true);
    pnBasGtrav.setLayout(loCal0);
    pnBasGtrav.setBorder(new EmptyBorder(5, 5, 5, 5)); //top,left,bottom,right
    tf_fre5.setColumns(6);
    n= 0;
    placeCompn(pnBasGtrav, la_fre5, 2, n);
    n++;
    placeCompn(pnBasGtrav, tf_fre5, 2, n);
    n++;
    placeCompn(pnBasGtrav, unit_fre5, 2, n);
    n++;
    placeCompn(pnBasGtrav, la_rar5, 0, n);
    n++;
    placeCompn(pnBasGtrav, tf_rar5, 0, n);
    n++;
    placeCompn(pnBasGtrav, unit_rar5, 0, n);
    n++;
    placeCompn(pnBasGtrav, la_cal5, 2, n);
    n++;
    placeCompn(pnBasGtrav, tf_cal5, 2, n);
    n++;
    placeCompn(pnBasGtrav, unit_cal5, 2, n);
    n++;
    placeCompn(pnBasGtrav, la_acc5, 2, n);
    n++;
    placeCompn(pnBasGtrav, tf_acc5, 2, n);
    n++;
    placeCompn(pnBasGtrav, unit_acc5, 2, n);
    n++;
    //
    la_titre6= new JLabel("Coefficients de combinaison (> 0)", SwingConstants.LEFT);
    la_titre6.setForeground(Color.blue);
    la_titrepsi0= new JLabel("         psi0", SwingConstants.RIGHT);
    la_titrepsi2= new JLabel("         psi2", SwingConstants.RIGHT);
    tf_psi0= BuTextField.createDoubleField();
    tf_psi0.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_psi2= BuTextField.createDoubleField();
    tf_psi2.setHorizontalAlignment(SwingConstants.RIGHT);
    tf_psi0.setColumns(4);
    tf_psi2.setColumns(4);
    pnBasDtrav= new JPanel();
    final BuGridLayout loCal8= new BuGridLayout();
    loCal8.setColumns(2);
    loCal8.setHgap(5);
    loCal8.setVgap(5);
    loCal8.setHfilled(false);
    loCal8.setCfilled(true);
    pnBasDtrav.setLayout(loCal8);
    pnBasDtrav.setBorder(new EmptyBorder(0, 5, 80, 250));
    //top,left,bottom,right
    pnBasDtrav.setPreferredSize(new Dimension(290, 80));
    n= 0;
    placeCompn(pnBasDtrav, la_titrepsi0, 2, n);
    n++;
    placeCompn(pnBasDtrav, tf_psi0, 2, n);
    n++;
    placeCompn(pnBasDtrav, la_titrepsi2, 2, n);
    n++;
    placeCompn(pnBasDtrav, tf_psi2, 2, n);
    n++;
    //====================================
    pnBasG= new JPanel();
    pnBasG.setLayout(new BorderLayout());
    pnBasG.setBorder(BorderFactory.createEmptyBorder(2, 5, 2, 5));
    pnBasG.setPreferredSize(new Dimension(350, 150));
    pnBasG.add(la_titre5, BorderLayout.NORTH);
    pnBasG.add(pnBasGtrav, BorderLayout.SOUTH);
    pnBasD= new JPanel();
    pnBasD.setLayout(new BorderLayout());
    pnBasD.setBorder(BorderFactory.createEmptyBorder(2, 5, 2, 5));
    pnBasD.setPreferredSize(new Dimension(400, 150));
    pnBasD.add(la_titre6, BorderLayout.NORTH);
    pnBasD.add(pnBasDtrav, BorderLayout.SOUTH);
    pnBas= new JPanel();
    pnBas.setLayout(new BorderLayout());
    pnBas.setBorder(BorderFactory.createEmptyBorder(2, 5, 2, 5));
    pnBas.add(pnBasG, BorderLayout.WEST);
    pnBas.add(pnBasD, BorderLayout.EAST);
    //====================================
    this.setLayout(new BorderLayout());
    this.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
    this.add(pnHaut, BorderLayout.NORTH);
    this.add(pnMil, BorderLayout.CENTER);
    this.add(pnBas, BorderLayout.SOUTH);
    //initialisation des parametres locaux
    setParametres(parametresLocal);
  }
  public void focusLost(final FocusEvent e) {
    /*
      Object src=e.getSource();
      //System.out.println("filleparam focusLost  "+e + " "+src );
      if( src instanceof  JTextField )
      {
         if(  src  == tf_nbcouch)
         {
          //System.out.println("focusLost source " + src);
           majsol();
          }
      }
      */
  }
  public void focusGained(final FocusEvent e) {
    //   	  Object src=e.getSource();
    //     System.out.println("filleparam focusGained  "+e + " "+src);
    /*
          majsol();
          */
  }
  public void actionPerformed(final ActionEvent e) {
    //     System.out.println("action performed  ");
    //  	 Object src=e.getSource();
    //     System.out.println("action performed  "+e + " "+src );
    parametresLocal= getParametres();
    setVisible(true);
  }
  public void setParametres(final SAmarr parametresProjet_) {
    //System.out.println("setParametres" );
    parametresLocal= parametresProjet_;
    if (parametresLocal != null) {
      if (parametresLocal.horiPerRare >= 0) {
        tf_rar1.setValue(new Double(parametresLocal.horiPerRare));
      } else {
        tf_rar1.setValue(new String(""));
      }
      if (parametresLocal.horiPerCalc >= 0) {
        tf_cal1.setValue(new Double(parametresLocal.horiPerCalc));
      } else {
        tf_cal1.setValue("");
      }
      if (parametresLocal.horiPerAcci >= 0) {
        tf_acc1.setValue(new Double(parametresLocal.horiPerAcci));
      } else {
        tf_acc1.setValue("");
      }
      if (parametresLocal.horiVarFreq >= 0) {
        tf_fre2.setValue(new Double(parametresLocal.horiVarFreq));
      } else {
        tf_fre2.setValue("");
      }
      if (parametresLocal.horiVarRare >= 0) {
        tf_rar2.setValue(new Double(parametresLocal.horiVarRare));
      } else {
        tf_rar2.setValue("");
      }
      if (parametresLocal.horiVarCalc >= 0) {
        tf_cal2.setValue(new Double(parametresLocal.horiVarCalc));
      } else {
        tf_cal2.setValue("");
      }
      if (parametresLocal.horiVarAcci >= 0) {
        tf_acc2.setValue(new Double(parametresLocal.horiVarAcci));
      } else {
        tf_acc2.setValue("");
      }
      if (parametresLocal.vertPerRare >= 0) {
        tf_rar3.setValue(new Double(parametresLocal.vertPerRare));
      } else {
        tf_rar3.setValue("");
      }
      if (parametresLocal.vertPerCalc >= 0) {
        tf_cal3.setValue(new Double(parametresLocal.vertPerCalc));
      } else {
        tf_cal3.setValue("");
      }
      if (parametresLocal.vertPerAcci >= 0) {
        tf_acc3.setValue(new Double(parametresLocal.vertPerAcci));
      } else {
        tf_acc3.setValue("");
      }
      if (parametresLocal.vertVarFreq >= 0) {
        tf_fre4.setValue(new Double(parametresLocal.vertVarFreq));
      } else {
        tf_fre4.setValue("");
      }
      if (parametresLocal.vertVarRare >= 0) {
        tf_rar4.setValue(new Double(parametresLocal.vertVarRare));
      } else {
        tf_rar4.setValue("");
      }
      if (parametresLocal.vertVarCalc >= 0) {
        tf_cal4.setValue(new Double(parametresLocal.vertVarCalc));
      } else {
        tf_cal4.setValue("");
      }
      if (parametresLocal.vertVarAcci >= 0) {
        tf_acc4.setValue(new Double(parametresLocal.vertVarAcci));
      } else {
        tf_acc4.setValue("");
      }
      if (parametresLocal.deplAdmFreq >= 0) {
        tf_fre5.setValue(new Double(parametresLocal.deplAdmFreq));
      } else {
        tf_fre5.setValue("");
      }
      if (parametresLocal.deplAdmRare >= 0) {
        tf_rar5.setValue(new Double(parametresLocal.deplAdmRare));
      } else {
        tf_rar5.setValue("");
      }
      if (parametresLocal.deplAdmCalc >= 0) {
        tf_cal5.setValue(new Double(parametresLocal.deplAdmCalc));
      } else {
        tf_cal5.setValue("");
      }
      if (parametresLocal.deplAdmAcci >= 0) {
        tf_acc5.setValue(new Double(parametresLocal.deplAdmAcci));
      } else {
        tf_acc5.setValue("");
      }
      if (parametresLocal.psi0Am >= 0) {
        tf_psi0.setValue(new Double(parametresLocal.psi0Am));
      } else {
        tf_psi0.setValue("");
      }
      if (parametresLocal.psi2Am >= 0) {
        tf_psi2.setValue(new Double(parametresLocal.psi2Am));
      } else {
        tf_psi2.setValue("");
      }
    }
  }
  public SAmarr getParametres() {
    //System.out.println("getParametres" );
    if (parametresLocal == null) {
      parametresLocal= new SAmarr();
    }
    String changed= "";
    double tmpD= 0.;
    //    int tmpI=0;
    String tmpS= "";
    final String blanc= "";
    tmpD= parametresLocal.horiPerRare;
    tmpS= tf_rar1.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.horiPerRare= -999.;
    } else {
      parametresLocal.horiPerRare= Double.parseDouble(tmpS);
    }
    if (parametresLocal.horiPerRare != tmpD) {
      changed += "horiPerRare |";
    }
    tmpD= parametresLocal.horiPerCalc;
    tmpS= tf_cal1.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.horiPerCalc= -999.;
    } else {
      parametresLocal.horiPerCalc= Double.parseDouble(tmpS);
    }
    if (parametresLocal.horiPerCalc != tmpD) {
      changed += "horiPerCalc |";
    }
    tmpD= parametresLocal.horiPerAcci;
    tmpS= tf_acc1.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.horiPerAcci= -999.;
    } else {
      parametresLocal.horiPerAcci= Double.parseDouble(tmpS);
    }
    if (parametresLocal.horiPerAcci != tmpD) {
      changed += "horiPerAcci |";
    }
    tmpD= parametresLocal.horiVarFreq;
    tmpS= tf_fre2.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.horiVarFreq= -999.;
    } else {
      parametresLocal.horiVarFreq= Double.parseDouble(tmpS);
    }
    if (parametresLocal.horiVarFreq != tmpD) {
      changed += "horiVarFreq|";
    }
    tmpD= parametresLocal.horiVarRare;
    tmpS= tf_rar2.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.horiVarRare= -999.;
    } else {
      parametresLocal.horiVarRare= Double.parseDouble(tmpS);
    }
    if (parametresLocal.horiVarRare != tmpD) {
      changed += "horiVarRare |";
    }
    tmpD= parametresLocal.horiVarCalc;
    tmpS= tf_cal2.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.horiVarCalc= -999.;
    } else {
      parametresLocal.horiVarCalc= Double.parseDouble(tmpS);
    }
    if (parametresLocal.horiVarCalc != tmpD) {
      changed += "horiVarCalc |";
    }
    tmpD= parametresLocal.horiVarAcci;
    tmpS= tf_acc2.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.horiVarAcci= -999.;
    } else {
      parametresLocal.horiVarAcci= Double.parseDouble(tmpS);
    }
    if (parametresLocal.horiVarAcci != tmpD) {
      changed += "horiVarAcci |";
    }
    tmpD= parametresLocal.vertPerRare;
    tmpS= tf_rar3.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.vertPerRare= -999.;
    } else {
      parametresLocal.vertPerRare= Double.parseDouble(tmpS);
    }
    if (parametresLocal.vertPerRare != tmpD) {
      changed += "vertPerRare |";
    }
    tmpD= parametresLocal.vertPerCalc;
    tmpS= tf_cal3.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.vertPerCalc= -999.;
    } else {
      parametresLocal.vertPerCalc= Double.parseDouble(tmpS);
    }
    if (parametresLocal.vertPerCalc != tmpD) {
      changed += "vertPerCalc |";
    }
    tmpD= parametresLocal.vertPerAcci;
    tmpS= tf_acc3.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.vertPerAcci= -999.;
    } else {
      parametresLocal.vertPerAcci= Double.parseDouble(tmpS);
    }
    if (parametresLocal.vertPerAcci != tmpD) {
      changed += "vertPerAcci |";
    }
    tmpD= parametresLocal.vertVarFreq;
    tmpS= tf_fre4.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.vertVarFreq= -999.;
    } else {
      parametresLocal.vertVarFreq= Double.parseDouble(tmpS);
    }
    if (parametresLocal.vertVarFreq != tmpD) {
      changed += "vertVarFreq|";
    }
    tmpD= parametresLocal.vertVarRare;
    tmpS= tf_rar4.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.vertVarRare= -999.;
    } else {
      parametresLocal.vertVarRare= Double.parseDouble(tmpS);
    }
    if (parametresLocal.vertVarRare != tmpD) {
      changed += "vertVarRare |";
    }
    tmpD= parametresLocal.vertVarCalc;
    tmpS= tf_cal4.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.vertVarCalc= -999.;
    } else {
      parametresLocal.vertVarCalc= Double.parseDouble(tmpS);
    }
    if (parametresLocal.vertVarCalc != tmpD) {
      changed += "vertVarCalc |";
    }
    tmpD= parametresLocal.vertVarAcci;
    tmpS= tf_acc4.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.vertVarAcci= -999.;
    } else {
      parametresLocal.vertVarAcci= Double.parseDouble(tmpS);
    }
    if (parametresLocal.vertVarAcci != tmpD) {
      changed += "vertVarAcci |";
    }
    tmpD= parametresLocal.deplAdmFreq;
    tmpS= tf_fre5.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.deplAdmFreq= -999.;
    } else {
      parametresLocal.deplAdmFreq= Double.parseDouble(tmpS);
    }
    if (parametresLocal.deplAdmFreq != tmpD) {
      changed += "deplAdmFreq|";
    }
    tmpD= parametresLocal.deplAdmRare;
    tmpS= tf_rar5.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.deplAdmRare= -999.;
    } else {
      parametresLocal.deplAdmRare= Double.parseDouble(tmpS);
    }
    if (parametresLocal.deplAdmRare != tmpD) {
      changed += "deplAdmRare |";
    }
    tmpD= parametresLocal.deplAdmCalc;
    tmpS= tf_cal5.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.deplAdmCalc= -999.;
    } else {
      parametresLocal.deplAdmCalc= Double.parseDouble(tmpS);
    }
    if (parametresLocal.deplAdmCalc != tmpD) {
      changed += "deplAdmCalc |";
    }
    tmpD= parametresLocal.deplAdmAcci;
    tmpS= tf_acc5.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.deplAdmAcci= -999.;
    } else {
      parametresLocal.deplAdmAcci= Double.parseDouble(tmpS);
    }
    if (parametresLocal.deplAdmAcci != tmpD) {
      changed += "deplAdmAcci |";
    }
    tmpD= parametresLocal.psi0Am;
    tmpS= tf_psi0.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.psi0Am= -999.;
    } else {
      parametresLocal.psi0Am= Double.parseDouble(tmpS);
    }
    if (parametresLocal.psi0Am != tmpD) {
      changed += "psi0Am |";
    }
    tmpD= parametresLocal.psi2Am;
    tmpS= tf_psi2.getText();
    if (tmpS.equals(blanc)) {
      parametresLocal.psi2Am= -999.;
    } else {
      parametresLocal.psi2Am= Double.parseDouble(tmpS);
    }
    if (parametresLocal.psi2Am != tmpD) {
      changed += "psi2Am |";
    }
    //     StringTokenizer tok = new StringTokenizer(changed, "|");
    //  while( tok.hasMoreTokens() ) {
    //  FudaaParamEventProxy.FUDAA_PARAM.fireParamStructModified(new FudaaParamEvent(
    //  this, 0, TaucomacResource.TAUCOMAC01,parametresLocal  , TaucomacResource.TAUCOMAC01+":"+tok.nextToken()));
    return parametresLocal;
  }
  /*
      public void majsol()
      {

  String tmpS = "";
  String blanc="";
  int n = 0;
  tmpS = ((String)tf_nbcouch.getText());
  if(tmpS.equals(blanc)){n =0;}
  else{n =Integer.parseInt(tmpS); }
  System.out.println(" ========= n " + n);
  la_sol1.setVisible(false);
  la_ztoit1.setVisible(false);
  la_pp1.setVisible(false);
  la_af1.setVisible(false);
  la_cohe1.setVisible(false);
  tf_ztoit1.setVisible(false);
  tf_pp1.setVisible(false);
  tf_af1.setVisible(false);
  tf_cohe1.setVisible(false);
  unit_ztoit1.setVisible(false);
  unit_pp1.setVisible(false);
  unit_af1.setVisible(false);
  unit_cohe1.setVisible(false);
  tf_ztoitdr1.setVisible(false);
  tf_ppdr1.setVisible(false);
  tf_afdr1.setVisible(false);
  tf_cohedr1.setVisible(false);
  unit_ztoitdr1.setVisible(false);
  unit_ppdr1.setVisible(false);
  unit_afdr1.setVisible(false);
  unit_cohedr1.setVisible(false);
  la_sol2.setVisible(false);
  la_ztoit2.setVisible(false);
  la_pp2.setVisible(false);
  la_af2.setVisible(false);
  la_cohe2.setVisible(false);
  tf_ztoit2.setVisible(false);
  tf_pp2.setVisible(false);
  tf_af2.setVisible(false);
  tf_cohe2.setVisible(false);
  unit_ztoit2.setVisible(false);
  unit_pp2.setVisible(false);
  unit_af2.setVisible(false);
  unit_cohe2.setVisible(false);
  tf_ztoitdr2.setVisible(false);
  tf_ppdr2.setVisible(false);
  tf_afdr2.setVisible(false);
  tf_cohedr2.setVisible(false);
  unit_ztoitdr2.setVisible(false);
  unit_ppdr2.setVisible(false);
  unit_afdr2.setVisible(false);
  unit_cohedr2.setVisible(false);
       if(n >= 1)
       {
  la_sol1.setVisible(true);
  la_ztoit1.setVisible(true);
  la_pp1.setVisible(true);
  la_af1.setVisible(true);
  la_cohe1.setVisible(true);
  tf_ztoit1.setVisible(true);
  tf_pp1.setVisible(true);
  tf_af1.setVisible(true);
  tf_cohe1.setVisible(true);
  unit_ztoit1.setVisible(true);
  unit_pp1.setVisible(true);
  unit_af1.setVisible(true);
  unit_cohe1.setVisible(true);
  tf_ztoitdr1.setVisible(true);
  tf_ppdr1.setVisible(true);
  tf_afdr1.setVisible(true);
  tf_cohedr1.setVisible(true);
  unit_ztoitdr1.setVisible(true);
  unit_ppdr1.setVisible(true);
  unit_afdr1.setVisible(true);
  unit_cohedr1.setVisible(true);
       }
       if(n == 2)
       {
  la_sol2.setVisible(true);
  la_ztoit2.setVisible(true);
  la_pp2.setVisible(true);
  la_af2.setVisible(true);
  la_cohe2.setVisible(true);
  tf_ztoit2.setVisible(true);
  tf_pp2.setVisible(true);
  tf_af2.setVisible(true);
  tf_cohe2.setVisible(true);
  unit_ztoit2.setVisible(true);
  unit_pp2.setVisible(true);
  unit_af2.setVisible(true);
  unit_cohe2.setVisible(true);
  tf_ztoitdr2.setVisible(true);
  tf_ppdr2.setVisible(true);
  tf_afdr2.setVisible(true);
  tf_cohedr2.setVisible(true);
  unit_ztoitdr2.setVisible(true);
  unit_ppdr2.setVisible(true);
  unit_afdr2.setVisible(true);
  unit_cohedr2.setVisible(true);
       }
  	tf_ztoit1.setVisible(false);
    	unit_ztoit1.setVisible(false);
    	tf_ztoitdr1.setVisible(false);
    	unit_ztoitdr1.setVisible(false);
    	tf_ztoitdr2.setVisible(false);
    	unit_ztoitdr2.setVisible(false);
    	pnMil.repaint();
  }
  */
  public void majlook(final int _style, final int _phase, final int _unit) {
    /*
    la_sol1.setForeground(Color.blue);
    la_sol2.setForeground(Color.blue);
    la_coebut.setForeground(Color.blue);
    la_coecohe.setForeground(Color.blue);
    tf_ztoit1.setVisible(false);
    unit_ztoit1.setVisible(false);
    tf_ztoitdr1.setVisible(false);
    unit_ztoitdr1.setVisible(false);
    tf_ztoitdr2.setVisible(false);
    unit_ztoitdr2.setVisible(false);
    majsol();
    */
  }
}
