/*
 * @file         TaucomacCellEditor.java
 * @creation     2000-11-06
 * @modification $Date: 2006-09-19 15:08:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.taucomac;
import java.awt.Component;
import java.util.EventObject;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;
/**
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:08:56 $ by $Author: deniger $
 * @author       Jean-Yves Riou
 */
public class TaucomacCellEditor extends JTextField implements TableCellEditor {
  private final Vector list_= new Vector();
  public Component getTableCellEditorComponent(
    final JTable table,
    final Object value,
    final boolean isSelected,
    final int row,
    final int column) {
    setText(value.toString());
    return this;
  }
  public void addCellEditorListener(final CellEditorListener l) {
    // System.out.println("celleditor  "+l );
    if (!list_.contains(l)) {
      list_.add(l);
    }
  }
  public void removeCellEditorListener(final CellEditorListener l) {
    if (list_.contains(l)) {
      list_.remove(l);
    }
  }
  public void cancelCellEditing() {
    for (int i= 0; i < list_.size(); i++) {
      ((CellEditorListener)list_.get(i)).editingCanceled(new ChangeEvent(this));
    }
  }
  public Object getCellEditorValue() {
    //System.out.println("celleditor gettext  "+getText()  );
    return getText();
  }
  public boolean isCellEditable(final EventObject e) {
    return true;
  }
  public boolean shouldSelectCell(final EventObject e) {
    //System.out.println("shouldselect  "+e );
    //Object src=e.getSource();
    //System.out.println("shouldselect source  "+src );
    return true;
  }
  public boolean stopCellEditing() {
    for (int i= 0; i < list_.size(); i++) {
      ((CellEditorListener)list_.get(i)).editingStopped(new ChangeEvent(this));
    }
    setText("");
    return true;
  }
}
