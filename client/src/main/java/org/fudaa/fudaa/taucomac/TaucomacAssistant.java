/*
 * @file         TaucomacAssistant.java
 * @creation     2000-10-24
 * @modification $Date: 2003-11-25 10:14:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.taucomac;
//import org.fudaa.ebli.bu.BuAssistant;
import com.memoire.bu.BuAssistant;
/**
 * L'assistant du client Taucomac.
 *
 * @version      $Revision: 1.4 $ $Date: 2003-11-25 10:14:26 $ by $Author: deniger $
 * @author       Jean-Yves Riou 
 */
public class TaucomacAssistant extends BuAssistant {}
