/*
* @file         TaucomacDessin.java
* @creation     2000-10-17
* @modification $Date: 2006-09-19 15:08:56 $
* @license      GNU General Public License 2
* @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail         devel@fudaa.org
*/
package org.fudaa.fudaa.taucomac;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;

import org.fudaa.dodico.corba.taucomac.SParametresTAU;

import org.fudaa.ebli.graphe.BGraphe;
/**
 * Fenetre de dessin des donnees de taucomac
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:08:56 $ by $Author: deniger $
 * @author       Jean-Yves RIOU
 */
public class TaucomacDessin extends JDialog implements ActionListener {
  // Déclaration des variables
  //    private   TaucomacImplementation taucomac;
  public BuCommonInterface appli_;
  public TaucomacFilleParametres mere_;
  JComponent content_;
  BuButton btfermer= new BuButton(" Fermer ");
  // dessin
  JPanel pnDes;
  BGraphe pngraphe;
  //================
  // Constructeur
  //================
  public TaucomacDessin(
    final BuCommonInterface _appli,
    final TaucomacFilleParametres _mere,
    final SParametresTAU paramTaucoFille) {
    super(
      _appli instanceof Frame ? (Frame)_appli : (Frame) null,
      "Dessin ",
      true);
    appli_= _appli;
    mere_= _mere;
    setSize(600, 700);
    //setDefaultCloseOperation ( DO_NOTHING_ON_CLOSE );
    if (appli_ instanceof Frame) {
      final Point pos= ((Frame)appli_).getLocationOnScreen();
      pos.x += (((Frame)appli_).getWidth() - getWidth()) / 2;
      pos.y += (((Frame)appli_).getHeight() - getHeight()) / 2;
      //setLocation(pos);
      setLocation(pos.x, 0);
    }
    final JPanel pnValider= new JPanel();
    pnValider.add(btfermer);
    btfermer.addActionListener(new TaucomacfermerListener());
    pnDes= new JPanel();
    pnDes.setPreferredSize(new Dimension(590, 650));
    pnDes.setBackground(Color.white);
    dessine(paramTaucoFille);
    content_= (JComponent)getContentPane();
    content_.setLayout(new BorderLayout());
    content_.setBorder(new EmptyBorder(5, 5, 5, 5));
    content_.add(BorderLayout.CENTER, pnDes);
    content_.add(BorderLayout.SOUTH, pnValider);
    setTitle("Dessin");
    show();
    //dessine(paramTaucoFille);
  }
  //====
  public void actionPerformed(final ActionEvent e) {
    System.out.println("actionPerformed  ");
    setVisible(true);
  }
  //====
  class TaucomacfermerListener implements ActionListener {
    public void actionPerformed(final ActionEvent e) {
      System.out.println("dispose  ");
      dispose();
    }
  }
  //=====
  public void dessine(final SParametresTAU paramTaucoFille) {
    System.out.println("Dessin dessine ");
    System.out.println("*==titreEtude   :" + paramTaucoFille.donGen.titreEtude);
    pnDes.removeAll();
    pngraphe= new TaucomacGraphe(paramTaucoFille);
    pnDes.add("Center", pngraphe);
    //show();
  }
  //====
} //class
