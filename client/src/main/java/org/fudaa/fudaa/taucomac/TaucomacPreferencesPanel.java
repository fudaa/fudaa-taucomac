/*
 * @file         TaucomacPreferencesPanel.java
 * @creation     2000-10-24
 * @modification $Date: 2006-09-19 15:08:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.taucomac;
import java.util.Hashtable;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.AbstractBorder;

import com.memoire.bu.BuAbstractPreferencesPanel;
import com.memoire.bu.BuGridLayout;
/**
 * Panneau de preferences pour Seuil.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:08:56 $ by $Author: deniger $
 * @author       Jean-Yves Riou
 */
public class TaucomacPreferencesPanel extends BuAbstractPreferencesPanel {
  Hashtable optionsStr_;
  BuGridLayout loTaucomac_;
  JPanel pnTaucomacCalq_;
  AbstractBorder boTaucomacCalq_;
  BuGridLayout loTaucomacCalq_;
  JLabel lbTaucomacCalqInter_;
  JComboBox liTaucomacCalqInter_;
  TaucomacPreferences options_;
  TaucomacImplementation taucomac_;
  public String getTitle() {
    return "Taucomac";
  }
  // Constructeur
  public TaucomacPreferencesPanel(final TaucomacImplementation _taucomac) {
    super();
    options_= TaucomacPreferences.TAUCOMAC;
    taucomac_= _taucomac;
    optionsStr_= new Hashtable();
    // Panneau Taucomac
    loTaucomac_= new BuGridLayout();
    loTaucomac_.setColumns(1);
    this.setLayout(loTaucomac_);
    updateComponents();
  }
  public boolean isPreferencesValidable() {
    return true;
  }
  public void validatePreferences() {
    fillTable();
    options_.writeIniFile();
  }
  public boolean isPreferencesApplyable() {
    return true;
  }
  public void applyPreferences() {
    fillTable();
    options_.applyOn(taucomac_);
  }
  public boolean isPreferencesCancelable() {
    return true;
  }
  public void cancelPreferences() {
    options_.readIniFile();
    updateComponents();
  }
  // Methodes privees
  private void fillTable() {}
  private void updateComponents() {}
}
