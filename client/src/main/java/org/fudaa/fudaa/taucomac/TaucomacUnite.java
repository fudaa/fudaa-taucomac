/*
 * @file         TaucomacUnite.java
 * @creation     2000-10-17
 * @modification $Date: 2006-09-19 15:08:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.taucomac;
/**
 *
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:08:57 $ by $Author: deniger $
 * @author       Jean-Yves Riou
 */
public class TaucomacUnite {
  String[][] unites= new String[2][50];
  public TaucomacUnite() {
    // String[][] unites = new String{"  "};
    for (int i= 0; i < 2; i++) {
      for (int j= 0; j < 2; j++) {
        unites[i][j]= "";
      }
    }
    // le deuxieme indice ne doit pas etre nul
    unites[0][1]= "m";
    unites[1][1]= "m";
    unites[0][2]= "m2";
    unites[1][2]= "m2";
    unites[0][3]= "m3";
    unites[1][3]= "m3";
    unites[0][4]= "mm";
    unites[1][4]= "mm";
    unites[0][5]= "(degr�)";
    unites[1][5]= "(degr�)";
    unites[0][6]= "t";
    unites[1][6]= "kN";
    unites[0][7]= "t/m2";
    unites[1][7]= "kPa";
    unites[0][8]= "t/m2";
    unites[1][8]= "MPa";
    unites[0][9]= "t/m3";
    unites[1][9]= "kN/m3";
    unites[0][10]= "t.m";
    unites[1][10]= "MN.m";
    unites[0][11]= "t.m";
    unites[1][11]= "kJ";
    unites[0][12]= "cm";
    unites[1][12]= "cm";
    unites[0][13]= " ";
    unites[1][13]= " ";
    unites[0][14]= " ";
    unites[1][14]= " ";
    unites[0][15]= " ";
    unites[1][15]= " ";
    unites[0][16]= " ";
    unites[1][16]= " ";
    System.out.println("unit 00 " + unites[0][1]);
    System.out.println("unit 10 " + unites[1][1]);
    System.out.println("unit 01 " + unites[0][2]);
    System.out.println("unit 11 " + unites[1][2]);
  }
  public String getUnit(final int ksyst, final int kpos) {
    return (unites[ksyst][kpos]);
  }
}
