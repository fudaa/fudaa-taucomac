/**
 * @file         DParametresSeuil.java
 * @creation     2000-05-26
 * @modification $Date: 2006-09-19 14:45:58 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.taucomac;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import org.fudaa.dodico.calcul.DParametres;
import org.fudaa.dodico.corba.taucomac.IParametresTaucomac;
import org.fudaa.dodico.corba.taucomac.IParametresTaucomacOperations;
import org.fudaa.dodico.corba.taucomac.SParametresTAU;
import org.fudaa.dodico.fortran.FortranWriter;
/**
 * Les parametres Taucomac.
 *
 * @version      $Revision: 1.12 $ $Date: 2006-09-19 14:45:58 $ by $Author: deniger $
 * @author       Jean-Yves Riou
 */
public class DParametresTaucomac
  extends DParametres
  implements IParametresTaucomac,IParametresTaucomacOperations {
  static String chainw;
  private SParametresTAU paramsTAU_;
  public DParametresTaucomac() {
    super();
  }
  public final  Object clone() throws CloneNotSupportedException{
    return new DParametresTaucomac();
  }
  public SParametresTAU parametresTAU() {
    return paramsTAU_;
  }
  public void parametresTAU(final SParametresTAU _p) {
    paramsTAU_= _p;
  }
  public static void ecritParametresTAU(
    final OutputStream _f01,
    final SParametresTAU _params)
    throws IOException {
    System.out.println("Ecriture des parametres de Taucomac");
    final FortranWriter f01= new FortranWriter(new OutputStreamWriter(_f01));
    if (System.getProperty("os.name").startsWith("Windows")) {
      f01.setLineSeparator("\r\n"); // fichiers DOS (\r)
    } else {
      f01.setLineSeparator("\n");
    }
    int[] fmtD= new int[] { 40, 40 };
    int[] fmtP= new int[] { 12, 28, 20, 20 };
    int[] fmtP5= new int[] { 16, 16, 16, 16, 12 };
    //=====================
    //  donnees generales
    //=====================
    f01.stringField(0, _params.donGen.titreEtude);
    f01.stringField(1, "Titre de  l'etude");
    f01.writeFields(fmtD);
    f01.stringField(0, _params.donGen.comentEtude);
    f01.stringField(1, "Commentaire");
    f01.writeFields(fmtD);
    f01.intField(0, _params.donGen.typeSysUnit);
    f01.stringField(1, "type systeme d'unite");
    f01.writeFields(fmtD);
    f01.intField(0, _params.donGen.typeChoixCal);
    f01.stringField(1, "type de calcul a executer");
    f01.writeFields(fmtD);
    f01.intField(0, _params.donGen.typeStyle);
    f01.stringField(1, "type de style a executer");
    f01.writeFields(fmtD);
    //=====================
    //  cas de charges
    //=====================
    f01.intField(0, _params.casCharg.duraAcost);
    f01.stringField(1, "duraAcost (cas de charge)");
    f01.writeFields(fmtD);
    f01.intField(0, _params.casCharg.duraAcostTet);
    f01.stringField(1, "duraAcostTet");
    f01.writeFields(fmtD);
    f01.intField(0, _params.casCharg.duraAmarHoriz);
    f01.stringField(1, "duraAmarHoriz");
    f01.writeFields(fmtD);
    f01.intField(0, _params.casCharg.duraAmarHorizVert);
    f01.stringField(1, "duraAmarHorizVert");
    f01.writeFields(fmtD);
    f01.intField(0, _params.casCharg.acciAcost);
    f01.stringField(1, "acciAcost");
    f01.writeFields(fmtD);
    f01.intField(0, _params.casCharg.acciAcostTet);
    f01.stringField(1, "acciAcostTet");
    f01.writeFields(fmtD);
    f01.intField(0, _params.casCharg.acciAmarHoriz);
    f01.stringField(1, "acciAmarHoriz");
    f01.writeFields(fmtD);
    f01.intField(0, _params.casCharg.acciAmarHorizVert);
    f01.stringField(1, "acciAmarHorizVert");
    f01.writeFields(fmtD);
    //=====================
    //  cotes principales
    //=====================
    f01.intField(0, _params.cotes.nbTubes);
    f01.stringField(1, "nbTubes");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.cotes.zTete);
    f01.stringField(0, chainw);
    f01.stringField(1, "zTete");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.cotes.zPied);
    f01.stringField(0, chainw);
    f01.stringField(1, "zPied");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.cotes.zToitSup);
    f01.stringField(0, chainw);
    f01.stringField(1, "zToitSup");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.cotes.zToitInf);
    f01.stringField(0, chainw);
    f01.stringField(1, "zToitInf");
    f01.writeFields(fmtD);
    // troncons
    f01.intField(0, _params.cotes.nbTronconsTub);
    f01.stringField(1, "nbTronconsTub");
    f01.writeFields(fmtD);
    for (int i= 0; i < _params.cotes.nbTronconsTub; i++) {
      chainw= Double.toString(_params.cotes.carTronc[i].longueur);
      f01.stringField(0, chainw);
      chainw= Double.toString(_params.cotes.carTronc[i].epaisseur);
      f01.stringField(1, chainw);
      chainw= Double.toString(_params.cotes.carTronc[i].limElast);
      f01.stringField(2, chainw);
      f01.stringField(3, "longu.  epais.  lim.");
      f01.writeFields(fmtP);
    }
    //=====================
    //  tubes
    //=====================
    chainw= Double.toString(_params.tubes.diametre);
    f01.stringField(0, chainw);
    f01.stringField(1, "diametre");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.tubes.young);
    f01.stringField(0, chainw);
    f01.stringField(1, "young");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.tubes.distAction);
    f01.stringField(0, chainw);
    f01.stringField(1, "distAction");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.tubes.distPerpend);
    f01.stringField(0, chainw);
    f01.stringField(1, "distPerpend");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.tubes.epCorrTet);
    f01.stringField(0, chainw);
    f01.stringField(1, "epCorrTet");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.tubes.epCorrPied);
    f01.stringField(0, chainw);
    f01.stringField(1, "epCorrPied");
    f01.writeFields(fmtD);
    // corrosion
    f01.intField(0, _params.tubes.nbCorrod);
    f01.stringField(1, "nbCorrod");
    f01.writeFields(fmtD);
    for (int i= 0; i < _params.tubes.nbCorrod; i++) {
      chainw= Double.toString(_params.tubes.corrTubes[i].z);
      f01.stringField(0, chainw);
      chainw= Double.toString(_params.tubes.corrTubes[i].ep);
      f01.stringField(1, chainw);
      f01.stringField(2, "corrod");
      f01.stringField(3, "z  ep");
      f01.writeFields(fmtP);
    }
    //==================
    // sol  a la rupture
    //==================
    chainw= Double.toString(_params.rupture.coefPartButFav);
    f01.stringField(0, chainw);
    f01.stringField(1, "coefPartButFav");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.rupture.coefPartButDefav);
    f01.stringField(0, chainw);
    f01.stringField(1, "coefPartButDefav");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.rupture.coefPartCohetFav);
    f01.stringField(0, chainw);
    f01.stringField(1, "coefPartCohetFav");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.rupture.coefPartCoheDefav);
    f01.stringField(0, chainw);
    f01.stringField(1, "coefPartCoheDefav");
    f01.writeFields(fmtD);
    f01.intField(0, _params.rupture.nbCouchesRupt);
    f01.stringField(1, "nbCouchesRupt");
    f01.writeFields(fmtD);
    for (int i= 0; i < _params.rupture.nbCouchesRupt; i++) {
      chainw= Double.toString(_params.rupture.couchesRupt[i].zToit);
      f01.stringField(0, chainw);
      chainw= Double.toString(_params.rupture.couchesRupt[i].poidsPropre);
      f01.stringField(1, chainw);
      chainw= Double.toString(_params.rupture.couchesRupt[i].angleFrot);
      f01.stringField(2, chainw);
      chainw= Double.toString(_params.rupture.couchesRupt[i].cohesion);
      f01.stringField(3, chainw);
      f01.stringField(4, "z poids angle cohe");
      f01.writeFields(fmtP5);
      chainw= Double.toString(_params.rupture.couchesRuptDr[i].zToit);
      f01.stringField(0, chainw);
      chainw= Double.toString(_params.rupture.couchesRuptDr[i].poidsPropre);
      f01.stringField(1, chainw);
      chainw= Double.toString(_params.rupture.couchesRuptDr[i].angleFrot);
      f01.stringField(2, chainw);
      chainw= Double.toString(_params.rupture.couchesRuptDr[i].cohesion);
      f01.stringField(3, chainw);
      f01.stringField(4, "z poids angle cohe");
      f01.writeFields(fmtP5);
    }
    //=====================
    // sol  elastoplastique
    //=====================
    f01.intField(0, _params.pressio.comportAmar);
    f01.stringField(1, "comportAmar");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.pressio.coefPartInter);
    f01.stringField(0, chainw);
    f01.stringField(1, "coefPartInter");
    f01.writeFields(fmtD);
    f01.intField(0, _params.pressio.nbCouchesPlast);
    f01.stringField(1, "nbCouchesPlast");
    f01.writeFields(fmtD);
    for (int i= 0; i < _params.pressio.nbCouchesPlast; i++) {
      chainw= Double.toString(_params.pressio.couchesPlast[i].zToit);
      f01.stringField(0, chainw);
      chainw= Double.toString(_params.pressio.couchesPlast[i].modulePressio);
      f01.stringField(1, chainw);
      chainw= Double.toString(_params.pressio.couchesPlast[i].pressionFluage);
      f01.stringField(2, chainw);
      chainw= Double.toString(_params.pressio.couchesPlast[i].pressionLimite);
      f01.stringField(3, chainw);
      f01.stringField(4, "z mod flu lim");
      f01.writeFields(fmtP5);
      chainw= Double.toString(_params.pressio.couchesPlast[i].coefRheo);
      f01.stringField(0, chainw);
      chainw= Double.toString(_params.pressio.couchesPlast[i].coefReduc);
      f01.stringField(1, chainw);
      f01.intField(2, _params.pressio.couchesPlast[i].modeSaisCourb);
      f01.intField(3, _params.pressio.couchesPlast[i].nbPtCourb);
      f01.stringField(4, "rheo reduc mode npt");
      f01.writeFields(fmtP5);
      for (int j= 0; j < _params.pressio.couchesPlast[i].nbPtCourb; j++) {
        chainw= Double.toString(_params.pressio.couchesPlast[i].ptCourb[j].def);
        f01.stringField(0, chainw);
        chainw= Double.toString(_params.pressio.couchesPlast[i].ptCourb[j].eff);
        f01.stringField(1, chainw);
        f01.stringField(2, "courbe");
        f01.stringField(3, "def eff");
        f01.writeFields(fmtP);
      }
    }
    //==================
    // defenses
    //==================
    chainw= Double.toString(_params.defens.valCarBas);
    f01.stringField(0, chainw);
    f01.stringField(1, "valCarBas");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.defens.valCarHaut);
    f01.stringField(0, chainw);
    f01.stringField(1, "valCarHaut");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.defens.valCalBas);
    f01.stringField(0, chainw);
    f01.stringField(1, "valCalBas");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.defens.valCalHaut);
    f01.stringField(0, chainw);
    f01.stringField(1, "valCalHaut");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.defens.valAcBas);
    f01.stringField(0, chainw);
    f01.stringField(1, "valAcBas");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.defens.valAcHaut);
    f01.stringField(0, chainw);
    f01.stringField(1, "valAcHaut");
    f01.writeFields(fmtD);
    f01.intField(0, _params.defens.nbDefenses);
    f01.stringField(1, "nbDefenses");
    f01.writeFields(fmtD);
    for (int i= 0; i < _params.defens.nbDefenses; i++) {
      chainw= Double.toString(_params.defens.defenses[i].hauteur);
      f01.stringField(0, chainw);
      chainw= Double.toString(_params.defens.defenses[i].zsup);
      f01.stringField(1, chainw);
      chainw= Double.toString(_params.defens.defenses[i].zinf);
      f01.stringField(2, chainw);
      f01.intField(3, _params.defens.defenses[i].nbPtCourb);
      f01.stringField(4, "h zs zi npt");
      f01.writeFields(fmtP5);
      for (int j= 0; j < _params.defens.defenses[i].nbPtCourb; j++) {
        chainw= Double.toString(_params.defens.defenses[i].ptCourb[j].def);
        f01.stringField(0, chainw);
        chainw= Double.toString(_params.defens.defenses[i].ptCourb[j].eff);
        f01.stringField(1, chainw);
        f01.stringField(2, "courbe");
        f01.stringField(3, "def eff");
        f01.writeFields(fmtP);
      }
    }
    //==================
    // accostages
    //==================
    chainw= Double.toString(_params.accost.chargeFreq);
    f01.stringField(0, chainw);
    f01.stringField(1, "chargeFreq");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.accost.chargeRare);
    f01.stringField(0, chainw);
    f01.stringField(1, "chargeRare");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.accost.chargeCalc);
    f01.stringField(0, chainw);
    f01.stringField(1, "chargeCalc");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.accost.chargeAcci);
    f01.stringField(0, chainw);
    f01.stringField(1, "chargeAcci");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.accost.psi0Ac);
    f01.stringField(0, chainw);
    f01.stringField(1, "psi0Ac");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.accost.psi2Ac);
    f01.stringField(0, chainw);
    f01.stringField(1, "psi2Ac");
    f01.writeFields(fmtD);
    f01.intField(0, _params.accost.nbAccostages);
    f01.stringField(1, "nbAccostages");
    f01.writeFields(fmtD);
    for (int i= 0; i < _params.accost.nbAccostages; i++) {
      chainw= Double.toString(_params.accost.accostages[i].z);
      f01.stringField(0, chainw);
      f01.intField(1, _params.accost.accostages[i].numDef);
      f01.intField(2, _params.accost.accostages[i].nbDef);
      f01.stringField(3, "z num nb");
      f01.writeFields(fmtP);
      chainw= Double.toString(_params.accost.accostages[i].vFreqEnr);
      f01.stringField(0, chainw);
      chainw= Double.toString(_params.accost.accostages[i].vRareEnr);
      f01.stringField(1, chainw);
      chainw= Double.toString(_params.accost.accostages[i].vCalcEnr);
      f01.stringField(2, chainw);
      chainw= Double.toString(_params.accost.accostages[i].vAcciEnr);
      f01.stringField(3, chainw);
      f01.stringField(4, "Enr F R C A ");
      f01.writeFields(fmtP5);
      chainw= Double.toString(_params.accost.accostages[i].vFreqFor);
      f01.stringField(0, chainw);
      chainw= Double.toString(_params.accost.accostages[i].vRareFor);
      f01.stringField(1, chainw);
      chainw= Double.toString(_params.accost.accostages[i].vCalcFor);
      f01.stringField(2, chainw);
      chainw= Double.toString(_params.accost.accostages[i].vAcciFor);
      f01.stringField(3, chainw);
      f01.stringField(4, "For F R C A ");
      f01.writeFields(fmtP5);
      chainw= Double.toString(_params.accost.accostages[i].vFreqDep);
      f01.stringField(0, chainw);
      chainw= Double.toString(_params.accost.accostages[i].vRareDep);
      f01.stringField(1, chainw);
      chainw= Double.toString(_params.accost.accostages[i].vCalcDep);
      f01.stringField(2, chainw);
      chainw= Double.toString(_params.accost.accostages[i].vAcciDep);
      f01.stringField(3, chainw);
      f01.stringField(4, "Dep  F R C A");
      f01.writeFields(fmtP5);
    }
    //==============
    //  amarrage
    //=============
    chainw= Double.toString(_params.amarr.horiPerRare);
    f01.stringField(0, chainw);
    f01.stringField(1, "horiPerRare");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.amarr.horiPerCalc);
    f01.stringField(0, chainw);
    f01.stringField(1, "horiPerCalc");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.amarr.horiPerAcci);
    f01.stringField(0, chainw);
    f01.stringField(1, "horiPerAcci");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.amarr.horiVarFreq);
    f01.stringField(0, chainw);
    f01.stringField(1, "horiVarFreq");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.amarr.horiVarRare);
    f01.stringField(0, chainw);
    f01.stringField(1, "horiVarRare");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.amarr.horiVarCalc);
    f01.stringField(0, chainw);
    f01.stringField(1, "horiVarCalc");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.amarr.horiVarAcci);
    f01.stringField(0, chainw);
    f01.stringField(1, "horiVarAcci");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.amarr.vertPerRare);
    f01.stringField(0, chainw);
    f01.stringField(1, "vertPerRare");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.amarr.vertPerCalc);
    f01.stringField(0, chainw);
    f01.stringField(1, "vertPerCalc");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.amarr.vertPerAcci);
    f01.stringField(0, chainw);
    f01.stringField(1, "vertPerAcci");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.amarr.vertVarFreq);
    f01.stringField(0, chainw);
    f01.stringField(1, "vertVarFreq");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.amarr.vertVarRare);
    f01.stringField(0, chainw);
    f01.stringField(1, "vertVarRare");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.amarr.vertVarCalc);
    f01.stringField(0, chainw);
    f01.stringField(1, "vertVarCalc");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.amarr.vertVarAcci);
    f01.stringField(0, chainw);
    f01.stringField(1, "vertVarAcci");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.amarr.deplAdmFreq);
    f01.stringField(0, chainw);
    f01.stringField(1, "deplAdmFreq");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.amarr.deplAdmRare);
    f01.stringField(0, chainw);
    f01.stringField(1, "deplAdmRare");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.amarr.deplAdmCalc);
    f01.stringField(0, chainw);
    f01.stringField(1, "deplAdmCalc");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.amarr.deplAdmAcci);
    f01.stringField(0, chainw);
    f01.stringField(1, "deplAdmAcci");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.amarr.psi0Am);
    f01.stringField(0, chainw);
    f01.stringField(1, "psi0Am");
    f01.writeFields(fmtD);
    chainw= Double.toString(_params.amarr.psi2Am);
    f01.stringField(0, chainw);
    f01.stringField(1, "psi2Am");
    f01.writeFields(fmtD);
    f01.flush();
  }
  public static void ecritParametresTAU(final File _f01, final SParametresTAU _params)
    throws IOException {
    final FileOutputStream fichier= new FileOutputStream(_f01);
    ecritParametresTAU(fichier, _params);
    fichier.close();
  }
  public static void ecritParametresTAU(final SParametresTAU _params)
    throws IOException {
    ecritParametresTAU(new File("taucomac" + ".01"), _params);
  }
  public static SParametresTAU litParametresTAU(final InputStream _f01)
    throws IOException {
    System.out.println("Lecture des parametres");
    final SParametresTAU params= new SParametresTAU();
    //    FortranReader f01=new FortranReader(new InputStreamReader(_f01));
    // a faire
    // int i;
    //int  fmtD[];
    //fmtD=new int[] { 40, 40};
    //
    // il faut tout allouer avant de rentrer
    // dans les boucles while d'affectation
    //
    System.out.println("*** fin lecture parametres  ***");
    return params;
  }
  public static SParametresTAU litParametresTAU(final File _f01) throws IOException {
    SParametresTAU res= null;
    final FileInputStream fichier= new FileInputStream(_f01);
    res= litParametresTAU(fichier);
    fichier.close();
    return res;
  }
  public static SParametresTAU litParametresTAU() throws IOException {
    return litParametresTAU(new File("taucomac01" + ".seu"));
  }
}
