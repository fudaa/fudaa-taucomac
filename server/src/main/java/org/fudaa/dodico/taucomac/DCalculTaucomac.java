/**
 * @file         DCalculTaucomac.java
 * @creation     2000-07-20
 * @modification $Date: 2006-09-19 14:45:58 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.taucomac;
import java.io.File;

import org.fudaa.dodico.calcul.DCalcul;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.taucomac.ICalculTaucomac;
import org.fudaa.dodico.corba.taucomac.ICalculTaucomacOperations;
import org.fudaa.dodico.corba.taucomac.IParametresTaucomac;
import org.fudaa.dodico.corba.taucomac.IParametresTaucomacHelper;
import org.fudaa.dodico.corba.taucomac.IResultatsTaucomac;
import org.fudaa.dodico.corba.taucomac.IResultatsTaucomacHelper;
import org.fudaa.dodico.corba.taucomac.SResultatsTAU;
import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.CExec;
/**
 * Classe Calcul de Taucomac.
 *
 * @version      $Revision: 1.13 $ $Date: 2006-09-19 14:45:58 $ by $Author: deniger $
 * @author       Jean-Yves Riou 
 */
public class DCalculTaucomac
  extends DCalcul
  implements ICalculTaucomac,ICalculTaucomacOperations {
  public DCalculTaucomac() {
    super();
    setFichiersExtensions(new String[] { ".tau", "" });
  }
  public final  Object clone() throws CloneNotSupportedException {
    return new DCalculTaucomac();
  }
  public String toString() {
    return "DCalculTaucomac()";
  }
  public String description() {
    return "Taucomac, serveur de calcul: " + super.description();
  }
  public void calcul(final IConnexion c) {
    if (!verifieConnexion(c)) {
      return;
    }
    final IParametresTaucomac params= IParametresTaucomacHelper.narrow(parametres(c));
    if (params == null) {
      CDodico.exceptionAxel(this, new Exception("params non definis (null)"));
      return;
    }
    final IResultatsTaucomac results= IResultatsTaucomacHelper.narrow(resultats(c));
    if (results == null) {
      CDodico.exceptionAxel(this, new Exception("results non definis (null)"));
    }
    log(c, "lancement du calcul");
    final int noEtude= c.numero();
    final String os= System.getProperty("os.name");
    final String path= cheminServeur();
    final File fic01= getFichier(c, ".tau");
    final File ficRes= getFichier(c, "");
    try {
      DParametresTaucomac.ecritParametresTAU(fic01, params.parametresTAU());
      System.out.println("Appel de l'executable seuil pour l'etude " + noEtude);
      String[] cmd;
      if (os.startsWith("Windows")) {
        cmd= new String[4];
        cmd[0]= path + "taucomac-win.bat";
        if (path.indexOf(':') != -1) {
          // lettre de l'unite (ex: "C:")
          cmd[1]= path.substring(0, path.indexOf(':') + 1);
          // chemin du serveur
          cmd[2]= path.substring(path.indexOf(':') + 1);
        } else {
          // si pas de lettre dans le chemin
          cmd[1]= "fake_cmd";
          cmd[2]= path;
        }
        cmd[3]= "" + noEtude;
        System.out.println(cmd[0] + " " + cmd[1] + " " + cmd[2] + " " + cmd[3]);
      } else {
        cmd= new String[3];
        cmd[0]= path + "taucomac.sh";
        cmd[1]= path;
        cmd[2]= "" + noEtude;
        System.out.println(cmd[0] + " " + cmd[1] + " " + cmd[2]);
      }
      final CExec ex= new CExec();
      ex.setCommand(cmd);
      ex.setOutStream(System.out);
      ex.setErrStream(System.err);
      ex.exec();
      System.out.println("Fin du calcul");
      System.out.println("Les resultats sont dans taucomac" + noEtude);
      System.out.println("Lecture des resultats");
      final SResultatsTAU sres= DResultatsTaucomac.litResultatsTAU(ficRes);
      results.resultatsTAU(sres);
      log(c, "calcul termin�");
    } catch (final Exception ex) {
      log(c, "erreur du calcul");
      CDodico.exceptionAxel(this, ex);
    }
  }
}
