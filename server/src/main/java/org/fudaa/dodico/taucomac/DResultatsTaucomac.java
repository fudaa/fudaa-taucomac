/**
 * @file         DResultatsTaucomac.java
 * @creation     2000-07-19
 * @modification $Date: 2006-09-19 14:45:58 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.taucomac;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.fudaa.dodico.calcul.DResultats;
import org.fudaa.dodico.corba.taucomac.IResultatsTaucomac;
import org.fudaa.dodico.corba.taucomac.IResultatsTaucomacOperations;
import org.fudaa.dodico.corba.taucomac.SResultatsTAU;
/**TaucomacSeuil.
 *
 * @version      $Revision: 1.11 $ $Date: 2006-09-19 14:45:58 $ by $Author: deniger $
 * @author       Jean-Yves Riou 
 */
public class DResultatsTaucomac
  extends DResultats
  implements IResultatsTaucomac,IResultatsTaucomacOperations {
  private SResultatsTAU resultsTAU_;
  public DResultatsTaucomac() {
    super();
    System.out.println("CResultatsTaucomac()");
  }
  public final  Object clone() throws CloneNotSupportedException{
    return new DResultatsTaucomac();
  }
  public String toString() {
    return "DResultatsTaucomac()";
  }
  public SResultatsTAU resultatsTAU() {
    //System.out.println("SResultatsTAU path"   +path);
    //System.out.println("SResultatsTAU noEtude"+noEtude);
    //System.out.println("SResultatsTAU resultsTAU = "+resultsTAU);
    return resultsTAU_;
  }
  public void resultatsTAU(final SResultatsTAU _p) {
    resultsTAU_= _p;
  }
  //=============================================================
  public static SResultatsTAU litResultatsTAU(final InputStream _f02)
    throws IOException {
    System.out.println("Lecture des Resultats litResultatsTAU ");
    final SResultatsTAU results= new SResultatsTAU();
    //    FortranReader f02=new FortranReader(new InputStreamReader(_f02));
    //
    //      int  fmtT[];
    //      int  fmtD1[];
    //      int  fmtS1[];
    //      int  fmtS2[];
    //      int  fmtD2[];
    //      int  fmtD3[];
    //      fmtT =new int[] { 20, 55};    //titre
    //      fmtD1=new int[] { 47, 9};
    //      fmtS1=new int[] { 31, 16};
    //      fmtS2=new int[] { 44, 13};
    //      fmtD2=new int[] { 9,13,1,13,1,13,1,13};
    //      fmtD3=new int[] { 44, 11};
    //====================
    // Rappel de donnees
    //====================
    // on saute 13 lignes
    //for(int i = 0; i < 9;i++)
    //{
    //  f02.readFields(fmtT);
    //}
    // titre de l'etude
    //f02.readFields(fmtT);
    //results.titreEtude =f02.stringField(1);
    //System.out.println(" litResultatsTAU() titreEtude ="+results.titreEtude);
    return results;
  }
  public static SResultatsTAU litResultatsTAU(final File _f02) throws IOException {
    System.out.println(" litResultatsTAU()=> _f02" + _f02);
    SResultatsTAU res= null;
    final FileInputStream fichier= new FileInputStream(_f02);
    res= litResultatsTAU(fichier);
    fichier.close();
    return res;
  }
  public static SResultatsTAU litResultatsTAU() throws IOException {
    System.out.println(" litResultatsTAU()=> Restaucomac");
    return litResultatsTAU(new File("Restaucomac"));
  }
}
